/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "lib_output.h"

#define LED 5
void app_main(void)
{
    GPIO_Init(LED, GPIO_PULLUP_ONLY);
    while (1) {
        
        GPIO_TogglePin(LED);
        printf("trang thai cua led : %d\n", GPIO_ReadPin(LED));
        vTaskDelay(200 / portTICK_PERIOD_MS);
    }
}
