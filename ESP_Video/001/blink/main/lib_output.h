#ifndef _LIB_OUTPUT_H
#define _LIB_OUTPUT_H
#include "driver/gpio.h"

/*
typedef enum {
    GPIO_PULLUP_ONLY,               !< Pad pull up            
    GPIO_PULLDOWN_ONLY,             !< Pad pull down          
    GPIO_PULLUP_PULLDOWN,           !< Pad pull up + pull down
    GPIO_FLOATING,                  !< Pad floating           
} gpio_pull_mode_t;
*/
// Hàm khởi tạo GPIO Output
void GPIO_Output_Init(gpio_num_t gpio_num, gpio_pull_mode_t pull );
// Hàm ghi level ra PIN
void GPIO_Output_WritePin(gpio_num_t gpio_num, uint32_t level);
// Hàm đọc level của PIN
int GPIO_Output_ReadPin(gpio_num_t gpio_num);
// Hàm đảo trạng thái của PIN
void GPIO_Output_TogglePin(gpio_num_t gpio_num);
#endif