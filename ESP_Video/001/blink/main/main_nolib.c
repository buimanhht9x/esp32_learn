/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"


#define LED 2


void GPIO_Config1()
{
    gpio_set_direction(LED, GPIO_MODE_OUTPUT);
    gpio_set_pull_mode(LED, GPIO_PULLUP_ONLY);
}

void GPIO_Config2()
{
    gpio_config_t led_config = {
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = true,
        .pin_bit_mask = (1 << LED)
    };
    gpio_config(&led_config);
}


int state_led = 0;
void blink_led()
{
    gpio_set_level(LED, state_led);
    state_led = 1 - state_led;
    state_led == 1 ? printf("LED OFF\n") : printf("LED ON\n");
}
void app_main(void)
{
    GPIO_Config2();
    while (1) {

        blink_led();
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}
