#include "lib_input.h"

input_callback_t input_callback = NULL;

// Hàm ngắt ngoài input
static void IRAM_ATTR gpio_input_handler(void* arg)
{
    int gpio_num = (uint32_t)arg;
    input_callback(gpio_num);
}

// Hàm set callback ở main
void input_set_callback(void* cb)
{
    input_callback = cb;
}

// Hàm khởi tạo input
void input_io_init(gpio_num_t gpio_num, gpio_pull_mode_t pull, gpio_int_type_t intr)
{
    // cấu hình input hay output
    gpio_set_direction(gpio_num, GPIO_MODE_INPUT);
    // cấu hình pullup, pulldown
    gpio_set_pull_mode(gpio_num, pull);
    // Cấu hình ngắt cạnh nào
    gpio_set_intr_type(gpio_num, intr);
    if(intr != GPIO_INTR_DISABLE)
    {
      // cấu hình ngắt, có ngắt hay không
      // install ngắt
      gpio_install_isr_service(0);
      // đăng kí hàm  handle cho ngắt
      gpio_isr_handler_add(gpio_num, gpio_input_handler, (void*) gpio_num);
    }
}

// Hàm đọc input
int input_io_read(gpio_num_t gpio_num)
{
   return gpio_get_level( gpio_num);
}


