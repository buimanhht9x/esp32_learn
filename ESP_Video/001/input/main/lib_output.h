#ifndef _LIB_OUTPUT_H
#define _LIB_OUTPUT_H
#include "driver/gpio.h"


// Hàm khởi tạo GPIO Output
void GPIO_Init(gpio_num_t gpio_num);
// Hàm ghi level ra PIN
void GPIO_WritePin(gpio_num_t gpio_num, uint32_t level);
// Hàm đọc level của PIN
int GPIO_ReadPin(gpio_num_t gpio_num);
// Hàm đảo trạng thái của PIN
void GPIO_TogglePin(gpio_num_t gpio_num);
#endif