#include "lib_output.h"


// Hàm khởi tạo GPIO Output  

void GPIO_Init(gpio_num_t gpio_num )
{
    gpio_set_direction(gpio_num, GPIO_MODE_OUTPUT);
}

// Hàm ghi level ra PIN
void GPIO_WritePin(gpio_num_t gpio_num, uint32_t level )
{
    gpio_set_level(gpio_num,level);
}

// Hàm đọc level của PIN
int GPIO_ReadPin(gpio_num_t gpio_num)
{
    gpio_set_direction(gpio_num, GPIO_MODE_INPUT_OUTPUT);
    int level = gpio_get_level(gpio_num);
  //  gpio_set_direction(gpio_num, GPIO_MODE_OUTPUT);
    return level;
}

// Hàm đảo trạng thái của PIN
void GPIO_TogglePin(gpio_num_t gpio_num)
{
    int currentLevel = GPIO_ReadPin(gpio_num);
    gpio_set_level(gpio_num, 1 - currentLevel);
}

