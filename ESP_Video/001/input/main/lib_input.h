#ifndef _LIB_INPUT_H_
#define _LIB_INPUT_H_
#include "driver/gpio.h"

typedef void (*input_callback_t)(int);


void input_set_callback(void* cb);
void input_io_init(gpio_num_t gpio_num, gpio_pull_mode_t pull, gpio_int_type_t intr);
int input_io_read(gpio_num_t gpio_num);
/*
typedef enum {
    GPIO_PULLUP_ONLY,               !< Pad pull up            
    GPIO_PULLDOWN_ONLY,             !< Pad pull down          
    GPIO_PULLUP_PULLDOWN,           !< Pad pull up + pull down
    GPIO_FLOATING,                  !< Pad floating           
} gpio_pull_mode_t;

typedef enum {
    GPIO_INTR_DISABLE = 0,     !< Disable GPIO interrupt                             
    GPIO_INTR_POSEDGE = 1,     !< GPIO interrupt type : rising edge                  
    GPIO_INTR_NEGEDGE = 2,     !< GPIO interrupt type : falling edge                 
    GPIO_INTR_ANYEDGE = 3,     !< GPIO interrupt type : both rising and falling edge 
    GPIO_INTR_LOW_LEVEL = 4,   !< GPIO interrupt type : input low level trigger      
    GPIO_INTR_HIGH_LEVEL = 5,  !< GPIO interrupt type : input high level trigger     
    GPIO_INTR_MAX,
} gpio_int_type_t;

*/

#endif