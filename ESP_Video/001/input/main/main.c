#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "lib_input.h"

#define BUTTON 0
int count;
void input_callback_handle(int gpio_num)
{
    count++;
}
void app_main(void)
{
    // Hàm sử dụng nếu không dùng ngắt
    input_io_init(BUTTON, GPIO_PULLUP_ONLY, GPIO_INTR_POSEDGE);
    input_set_callback(input_callback_handle);
    while (1) {
       printf("hello %d\n", count);
       vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
