//https://docs.espressif.com/projects/esp-idf/en/v4.2.5/esp32/api-reference/storage/nvs_flash.html
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "nvs_flash.h"




void app_main(void)
{
    // Khoi tao NVS Partition
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    vTaskDelay(1000/portTICK_PERIOD_MS);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
         //read data
        // key : value
        // "data" : 1111
        size_t required_size;
        nvs_get_str(nvs_handle,"string",NULL,&required_size);
        char *server_name = malloc(required_size);
        nvs_get_str(nvs_handle,"string",server_name,&required_size);
        printf("read string : %s \n",server_name);

        //write data
        // key : value
        char *write_string = "hello from nvs5566..\0";
        nvs_set_str(nvs_handle,"string",write_string);
        printf("write string : %s \n",write_string);

        nvs_commit(nvs_handle);

        nvs_close(nvs_handle);
    }
   

}