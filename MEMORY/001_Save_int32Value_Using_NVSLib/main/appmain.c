
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "nvs_flash.h"




void app_main(void)
{
    // Khoi tao NVS Partition
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    vTaskDelay(1000/portTICK_PERIOD_MS);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
         //read data
        // key : value
        // "data" : 1111 
        int32_t write_data = 5555;
        nvs_set_i32(nvs_handle,"data",write_data);
        printf("write data : %d \n",write_data);
        int32_t read_data;
        nvs_get_i32(nvs_handle,"data",&read_data); 
        printf("read data : %d \n",read_data);

        //write data
        // key : value
       

        nvs_commit(nvs_handle);

        nvs_close(nvs_handle);
    }
   

}