import express from "express"

const app = express();
const PORT = 3000;

const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];


app.get("/",(req, res) => {
    const d = new Date();
    let day = weekday[d.getDay()];
    let dostCheck = "" ;
    console.log(day);
    if(day === "Sunday" || day === "Saturday")  {
        dostCheck = "hang out..";
    } else {
        dostCheck = "work hard..";
    }

        
    res.render("index.ejs", {
        weekday : day,
        weekdaydost : dostCheck,
    });
})

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});
