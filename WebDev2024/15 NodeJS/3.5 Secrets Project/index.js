//To see how the final website should work, run "node solution.js".
//Make sure you have installed all the dependencies with "npm i".
//The password is ILoveProgramming

import express from "express"
import bodyParser from "body-parser";

const app = express();
const PORT = 3000;

import { dirname } from "path";
import { fileURLToPath } from "url";
const __dirname = dirname(fileURLToPath(import.meta.url));

app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/index.html");
});



app.post("/check", (req, res) => {
    var checkPass = req.body["password"];
    if(checkPass === "ILoveU")
        res.sendFile(__dirname + "/public/secret.html");
    else
    res.sendFile(__dirname + "/public/index.html");
});


app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});