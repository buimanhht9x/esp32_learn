import express from "express";
import bodyParser from "body-parser";

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
   res.render("index.ejs");
});

app.post("/submit", (req, res) => {
  var fName = req.body["fName"];
  var lName = req.body["lName"];
  
  var numberLetter = fName.length + lName.length;
  console.log(numberLetter);
  res.render("index.ejs", {
    nlName : numberLetter
  });
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
