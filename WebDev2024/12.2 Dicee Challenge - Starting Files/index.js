function getRandom() {
    var number = Math.random();
    number = Math.floor(number*6) + 1;
    return number;
}
function displayDicee() {
    var number1 = getRandom();
    var number2 = getRandom();
    var img1 = document.querySelector(".img1");
    var img2 = document.querySelector(".img2");
    var img1src = "./images/dice"+number1+".png";
    var img2src = "./images/dice"+number2+".png";

    img1.setAttribute("src",img1src);

    img2.setAttribute("src",img2src);

    if(number1 > number2) {
        document.querySelector("h1").innerHTML= "&#128681 Player1 Win"
    } else if(number1 < number2) {
        document.querySelector("h1").innerHTML= "Player2 Win &#128681"
    } else {
        document.querySelector("h1").innerHTML= "&#128681Split Prize&#128681"
    }
}
displayDicee();

window.setInterval(displayDicee, 2000);
