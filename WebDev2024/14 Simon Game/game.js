var buttonColours = ["red", "blue", "green", "yellow" ] ;
var gamePattern = [];
var userClickedPattern = [];
var level = 0;
var numberUserClick = 0;
var countAnswerRight = 0;
function nextSequence() {
    var randomNumber = Math.random();
    randomNumber = randomNumber * 4;
    randomNumber = Math.floor(randomNumber);
    
    var randomChosenColour = buttonColours[randomNumber];
    gamePattern.push(randomChosenColour);
    console.log(gamePattern);
    for(let i = 0 ; i <= level; i++) { 
        setTimeout(function() {
            $("." + gamePattern[i]).fadeOut(100).fadeIn(100);
            var audio = new Audio("./sounds/" + gamePattern[i] + ".mp3");
            audio.play();
        }, i * 300);
    } 
}

// Hàm check nút nhấn màu xanh
$(".green").click(function(){
    greenPress();
});

$(".red").click(function(){
    redPress();
})
function redPress() {
    $(".red").fadeOut(100).fadeIn(100);
    var red = new Audio('./sounds/red.mp3');
    red.play();
    userClickedPattern.push("red");
    console.log(userClickedPattern);
    numberUserClick++;
    checkAnswer();
}
function greenPress() {
    $(".green").fadeOut(100).fadeIn(100);
    var green = new Audio('./sounds/green.mp3');
    green.play();
    userClickedPattern.push("green");
    console.log(userClickedPattern);
    numberUserClick++;
    checkAnswer();
}

function yellowPress() {
    $(".yellow").fadeOut(100).fadeIn(100);
    var green = new Audio('./sounds/yellow.mp3');
    green.play();
    userClickedPattern.push("yellow");
    console.log(userClickedPattern);
    numberUserClick++;
    checkAnswer(); 
}

$(".yellow").click(function(){
    yellowPress() ;
})

$(".blue").click(function(){
    bluePress() ;
})
function bluePress() {
    $(".blue").fadeOut(100).fadeIn(100);
    var blue = new Audio('./sounds/blue.mp3');
    blue.play();
    userClickedPattern.push("blue");
    console.log(userClickedPattern);
    numberUserClick++;
    checkAnswer();
}

$(document).keypress(function(e) {
    var k = e.key;
    if(k === " " || k === "Space") {
        $("h1").text("Level 0");
        nextSequence();
    } 
    else if(k === "5")
        greenPress();
    else if(k === "6")
        redPress();
    else if(k === "3")
        bluePress();
    else if(k === "2")
        yellowPress();
    else
        console.log(k);
});

function checkAnswer() {
    
    if(numberUserClick === level + 1 ) {
        for(var i = 0; i < level + 1; i++) {
            if(gamePattern[i] === userClickedPattern[i]) {
                countAnswerRight++;
            }
        }
        console.log("countAnswerRight" + countAnswerRight);
        if(countAnswerRight === level + 1 ) {
            $("h1").text("WELL DONE");
            level++;
            setTimeout(function() {
                $("h1").text("Level " +  level);
                nextSequence();
            }, 1500);
            
            
           
        }
        else { 
            $("h1").text("YOU LOSE");
            setTimeout(function() {
                $("h1").text("Press Space Key to Start" );
            }, 1000);    
            level = 0;
        }
        numberUserClick = 0;countAnswerRight = 0;
        userClickedPattern = [];
    }
    
}
