import express from "express";
import bodyParser from "body-parser";
import pg from "pg";

const db = new pg.Client({
  user: "postgres",
  host: "localhost",
  database: "world",
  password: "matkhau",
  port: 5432
});
db.connect();
const app = express();
const port = 3000;

let totalCorrect = 0;
var country_code = [];

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));



app.get("/", async(req, res) => {
  //Write your code here.
  const result = await db.query("SELECT country_code FROM visited_countries");
  // result.rows  kết quả nhận được, cần phải tách ra
  // [
  //   { country_code: 'FR' },
  //   { country_code: 'GB' },
  //   { country_code: 'US' }
  // ]
  result.rows.forEach((country)  => {
    country_code.push(country.country_code);
  });
  // [ 'FR', 'GB', 'US' ]  sau khi tách
  console.log(country_code);
  res.render("index.ejs", { 
    countries: country_code, 
    total  : country_code.length
  });
});

app.post("/add", async(req, res) => {
  console.log(req.body);
  const result =  await db.query("SELECT country_code FROM countries WHERE country_name = $1", [req.body.country]);
  if (result.rows.length !== 0) {
    const code = result.rows[0].country_code
    console.log(code);
    await db.query("INSERT INTO visited_countries (country_code) VALUES ($1)", [code]);
    res.redirect("/");
  }
  
});



app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
