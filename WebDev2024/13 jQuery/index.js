$(document).keypress(function(e) {
    $("h1").text(e.key);
});

var temp = 0;
$("button").click( function(){
    temp = 1 - temp;
    if(temp === 0)
         $("h1").addClass("fontItalic");
    else
        $("h1").removeClass("fontItalic");
});

$("h1").on("mouseover mouseout", function(){
    $("h1").toggleClass("fontItalic");
})