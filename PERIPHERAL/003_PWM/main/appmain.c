/*
        Thiết bị ngoại vi điều khiển đèn LED (LEDC) được thiết kế chủ yếu 
    để kiểm soát cường độ của đèn LED, mặc dù nó cũng có thể được sử 
    dụng để tạo tín hiệu PWM cho các mục đích khác. Nó có 16 kênh có
    thể tạo ra các dạng sóng độc lập có thể được sử dụng, ví dụ, để 
    điều khiển các thiết bị LED RGB.

        Các kênh LEDC được chia thành hai nhóm, mỗi nhóm 8 kênh. Một nhóm 
    kênh LEDC hoạt động ở chế độ tốc độ cao. Chế độ này được triển khai
    trong phần cứng và cung cấp khả năng thay đổi chu kỳ nhiệm vụ PWM 
    tự động và không trục trặc. Nhóm kênh còn lại hoạt động ở chế độ 
    tốc độ thấp, chu kỳ nhiệm vụ PWM phải được thay đổi bởi trình 
    điều khiển trong phần mềm. Mỗi nhóm kênh cũng có thể sử dụng các 
    nguồn đồng hồ khác nhau.

        Bộ điều khiển PWM có thể tự động tăng hoặc giảm chu kỳ làm việc dần dần,
    cho phép giảm dần mà không có bất kỳ sự can thiệp nào của bộ xử lý.


    Các bước:
    B1 : Khai báo timer cho ledC
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_10_BIT,
        .freq_hz    = 1000,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .timer_num  = LEDC_TIMER_0,
        .clk_cfg    = LEDC_AUTO_CLK,
    };
    ledc_timer_config(&ledc_timer);

    B2: Khai báo channel gpio cho ledc

    ledc_channel.channel = LEDC_CHANNEL_0;
    ledc_channel.duty = 0;
    ledc_channel.gpio_num = LEDC_GPIO;
    ledc_channel.speed_mode = LEDC_HIGH_SPEED_MODE;
    ledc_channel.hpoint = 0;
    ledc_channel.timer_sel = LEDC_TIMER_0;
    ledc_channel.intr_type = LEDC_INTR_FADE_END; // đây là dùng ngắt fade

    ledc_channel_config(&ledc_channel);

    ledc_fade_func_install(0); // Nếu dùng ngắt fade thì gọi hàm này


    B3 : Hàm chính để gọi
    ledc_set_fade_with_time(ledc_mode_t speed_mode, ledc_channel_t channel, uint32_t target_duty, int max_fade_time_ms)
                            Trong đó: target_duty là duty mục tiêu
                                      max_fade_time_ms là thời gian từ now_duty đến target duty
    ledc_fade_start(ledc_channel.speed_mode, ledc_channel.channel, LEDC_FADE_NO_WAIT);


    ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, duty);
            Với duty là = đến 2^độ phân giải
    ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);

*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/ledc.h"

#define LEDC_GPIO 2
static ledc_channel_config_t ledc_channel;

#define LEDC_TEST_DUTY         (1023)  // 10 bit là 2^10 = 1024 - 1 = 1023
#define LEDC_TEST_FADE_TIME    (3000)  // thời gian fade

static void init_hw(void)
{
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_10_BIT,           // Khởi tạo timer độ phân giải 10bit
        .freq_hz    = 1000,                             // Khởi tạo tần số timer
        .speed_mode = LEDC_HIGH_SPEED_MODE,             // Khởi tạo speed cho timer
        .timer_num  = LEDC_TIMER_0,                     // Khởi tạo timer cho timer
        .clk_cfg    = LEDC_AUTO_CLK,                    // Khởi tạo nguồn clock source cho timer
    };

    ledc_timer_config(&ledc_timer);                     // Cấu hình timer

    ledc_channel.channel = LEDC_CHANNEL_0;              // Khởi tạo channel 
    ledc_channel.duty = 0;                              // khởi tạo duty cho channel
    ledc_channel.gpio_num = LEDC_GPIO;                  // Khởi tạo GPIO cho channel
    ledc_channel.speed_mode = LEDC_HIGH_SPEED_MODE;     // Khởi tạo tốc độ chân channel
    ledc_channel.hpoint = 0;                            // Khởi tạo hpoint
    ledc_channel.timer_sel = LEDC_TIMER_0;              // Khởi tạo timer cho channel
    ledc_channel.intr_type = LEDC_INTR_FADE_END;        // Khởi tạo ngắt để dùng hàm fade

    ledc_channel_config(&ledc_channel);                 // Config channel
    ledc_fade_func_install(0);                          // Install fade function

}

void app_main()
{
    init_hw();
    while (1)
    {
        printf("1. LEDC fade up to duty = %d\n", LEDC_TEST_DUTY);
        ledc_set_fade_with_time(ledc_channel.speed_mode, ledc_channel.channel, LEDC_TEST_DUTY, LEDC_TEST_FADE_TIME);
        ledc_fade_start(ledc_channel.speed_mode, ledc_channel.channel, LEDC_FADE_NO_WAIT);
        vTaskDelay(LEDC_TEST_FADE_TIME / portTICK_PERIOD_MS);

        printf("2. LEDC fade down to duty = 0\n");
        ledc_set_fade_with_time(ledc_channel.speed_mode, ledc_channel.channel, 0, LEDC_TEST_FADE_TIME);
        ledc_fade_start(ledc_channel.speed_mode, ledc_channel.channel, LEDC_FADE_NO_WAIT);
        vTaskDelay(LEDC_TEST_FADE_TIME / portTICK_PERIOD_MS);

        printf("3. LEDC set duty = %d without fade\n", LEDC_TEST_DUTY);
        ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, LEDC_TEST_DUTY);
        ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
        vTaskDelay(1000 / portTICK_PERIOD_MS);

        printf("4. LEDC set duty = 0 without fade\n");
        ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, 0);
        ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
        vTaskDelay(1000 / portTICK_PERIOD_MS);

        // for (int i = 0; i < 1024; ++i)
        // {
        //     ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, i);
        //     ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
        //     vTaskDelay(20 / portTICK_RATE_MS);
        //     printf("%d\n",ledc_get_duty(ledc_channel.speed_mode, ledc_channel.channel));
        // }  
    }
}