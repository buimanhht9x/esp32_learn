#ifndef INPUT_IO_H
#define INPUT_IO_H

#include "driver/gpio.h"
#include"hal/gpio_types.h"
#include "stdint.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

void gpio_input_create(gpio_num_t gpio_num, gpio_int_type_t gpio_intr_type);

uint8_t gpio_input_read(gpio_num_t gpio_num);

void input_test_callback(gpio_num_t gpio_num);

void gpio_input_set_callback(void *cb);



#endif
