
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "input_iot.h"


#define LED_GPIO 2
#define BTN_GPIO 0
int k = 0;
void input_event_callback(int pin)
{
    k = 1- k;
    gpio_set_level(LED_GPIO, k);
}

void button_task(void *pvParameter)
{
    // OUTPUT
    gpio_pad_select_gpio(LED_GPIO);
    gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);

    // INPUT
    input_io_create(BTN_GPIO,GPIO_INTR_DISABLE );

    while(1)
    {
        runINPUT(BTN_GPIO);
    }
}


void app_main(void)
{
    input_set_callback(input_event_callback);
    xTaskCreate(&button_task, "button_task", 512, NULL, 5, NULL);
}