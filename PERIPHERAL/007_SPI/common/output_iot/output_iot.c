#include "output_iot.h"



void gpio_output_create(gpio_num_t gpio_num)
{
    // chon io làm GPIO
    gpio_pad_select_gpio(gpio_num);
    // Set làm input Mode
    gpio_set_direction(gpio_num, GPIO_MODE_OUTPUT);
}

void gpio_output_write(gpio_num_t gpio_num, int level)
{
    gpio_set_level(gpio_num, level);
}

void gpio_output_toggle(gpio_num_t gpio_num)
{
    uint8_t old_state = gpio_get_level(gpio_num);
    gpio_set_level(gpio_num, 1 - old_state);
}
