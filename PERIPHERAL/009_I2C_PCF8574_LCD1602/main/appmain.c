/*
    Ver 1, chatGPT
        - tạo một bộ điều khiển lệnh I2C, đại diện cho một chuỗi các hoạt động I2C sẽ được thực thi bởi trình điều khiển I2C.
        - i2c_cmd_link_create() tạo một bộ điều khiển lệnh I2C trống và trả về một con trỏ tới nó. Bộ xử lý lệnh này được sử
        - dụng để xây dựng một chuỗi hoạt động I2C, chẳng hạn như bắt đầu, dừng, đọc và ghi, sẽ được thực thi theo một thứ tự cụ thể.
        - sau khi tạo bộ điều khiển lệnh, bạn có thể sử dụng các chức năng khác nhau từ API trình điều khiển I2C, chẳng hạn như
        i2c_master_start(), i2c_master_write_byte(), i2c_master_read_byte()và i2c_master_stop(), để thêm các hoạt động I2C riêng lẻ vào bộ điều khiển lệnh.
        - khi bạn đã thêm tất cả các thao tác I2C cần thiết vào bộ điều khiển lệnh, bạn có thể thực hiện chuỗi thao tác bằng cách chuyển
        bộ điều khiển lệnh tới i2c_master_cmd_begin(). Trình điều khiển I2C sau đó sẽ xử lý các hoạt động trong bộ điều khiển lệnh và
        thực hiện giao tiếp cần thiết với thiết bị I2C.
        - gọi i2c_cmd_link_delete() sau khi thực hiện xử lý lệnh để giải phóng tài nguyên được phân bổ và tránh rò rỉ bộ nhớ.

    kết nối pcf8574  với   lcd1602
         P0                RS
         P1                RW
         P2                E
         P3                Đèn nền
         P4                D4
         P5                D5
         P6                D6
         P7                D7



    Với LCD1602 chúng có 2 line
        Line 1: từ 0x80 tới 0x8F
        Line 2: từ 0xC0 tới 0xCF

    Pin:
        VSS: tương đương với GND – cực âm
        VDD: tương đương với VCC – cực dương (5V)
        Constrast Voltage (Vo): điều khiển độ sáng màn hình
        Register Select (RS): điều khiển địa chỉ nào sẽ được ghi dữ liệu
        Read/Write (RW): Bạn sẽ đọc (read mode) hay ghi (write mode) dữ liệu? Nó sẽ phụ thuộc vào bạn gửi giá trị gì vào.
        Enable pin: Cho phép ghi vào LCD
        D0 – D7: 8 chân dư liệu, mỗi chân sẽ có giá trị HIGH hoặc LOW nếu bạn đang ở chế độ đọc (read mode) và nó sẽ nhận giá trị HIGH hoặc LOW nếu đang ở chế độ ghi (write mode)
        Backlight (Backlight Anode (+) và Backlight Cathode (-)): Tắt bật đèn màn hình LCD.

    Quy trình ghi vào LCD1602 như sau:
        Chân RS kéo xuống 0 nếu gửi Lệnh (Command) điều khiển LCD, lên 1 gửi data (ghi vào DD RAM)
        Chân R/W: Kéo xuống 0 ghi dữ liệu
        Các chân D0 – D7: Khi ghi dữ liệu, các chân D0-D7 của LCD sẽ ở chế độ Input, chân MCU kết nối vào sẽ ở chế độ OutPut
        Chân EN sẽ được kéo lên 1 để chốt dữ liệu vào LCD, sau đó lại nhả về 0

    Quy trình đọc LCD1602:
        Chân RS kéo xuống 0 nếu gửi Lệnh (Command) điều khiển LCD, lên 1 gửi data (ghi vào DD RAM)
        Chân R/W: Kéo lên 1 để đọc dữ liệu
        Các chân D0 – D7: Khi đọc dữ liệu, các chân D0-D7 của LCD sẽ ở chế độ Output, chân MCU kết nối vào sẽ ở chế độ Input
        Chân EN sẽ được kéo lên 1 để chốt dữ liệu vào LCD, sau đó lại nhả về 0


    //  P7 P6 P5 P4 P3 P2 P1 P0
    //  D7 D6 D5 D4 BL E  RW RS
*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "unistd.h"

#define PCF8574_ADDRESS 0x27
#define i2c_sda_pin 21
#define i2c_scl_pin 22

esp_err_t i2c_init()
{
    i2c_config_t i2c_config = {
                .mode = I2C_MODE_MASTER,
                .sda_io_num = i2c_sda_pin,
                .scl_io_num = i2c_scl_pin,
                .sda_pullup_en = true,
                .scl_pullup_en = true,
                .master.clk_speed = 100000
                };
    i2c_param_config(I2C_NUM_0, &i2c_config);
    i2c_driver_install(I2C_NUM_0,i2c_config.mode, 0, 0, 0);
    return ESP_OK;
}

void lcd_send_cmd(char cmd)
{
    //  Tach cmd thanh 2 phan, 4 bit cao, 4 bit thap
    //  P7 P6 P5 P4 P3 P2 P1 P0
    //  D7 D6 D5 D4 BL E  RW RS
    //  RS = 0   R/W:0,   EN 1-> 0
    char cmd_4bit_h = (cmd & 0xF0);
    char cmd_4bit_l = ((cmd  << 4 ) & 0xF0);

    uint8_t cmd_send[4];
    cmd_send[0] = cmd_4bit_h|0x0C;           // en1  rs0  rw0
    cmd_send[1] = cmd_4bit_h|0x08;           // en0  rs0  rw0
    cmd_send[2] = cmd_4bit_l|0x0C;           // en1  rs0  rw0
    cmd_send[3] = cmd_4bit_l|0x08;           // en0  rs0  rw0

    i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
    i2c_master_start(cmd_handle);
    i2c_master_write_byte(cmd_handle, (PCF8574_ADDRESS << 1)|I2C_MASTER_WRITE, true);
    i2c_master_write(cmd_handle, cmd_send, 4, true);
    i2c_master_stop(cmd_handle);

    i2c_master_cmd_begin(I2C_NUM_0,cmd_handle,pdTICKS_TO_MS(100));
    i2c_cmd_link_delete(cmd_handle);

}

void lcd_send_data(char data)
{
    //  Tach cmd thanh 2 phan, 4 bit cao, 4 bit thap
    //  P7 P6 P5 P4 P3 P2 P1 P0
    //  D7 D6 D5 D4 BL E  RW RS
    //  RS = 1   R/W:0,   EN 1-> 0
    char cmd_4bit_h = data & 0xF0;
    char cmd_4bit_l = (data  << 4 ) & 0xF0;

    uint8_t cmd_send[4];
    cmd_send[0] = cmd_4bit_h|0x0D;           // en1  rs1  rw0
    cmd_send[1] = cmd_4bit_h|0x09;           // en0  rs1  rw0
    cmd_send[2] = cmd_4bit_l|0x0D;           // en1  rs1  rw0
    cmd_send[3] = cmd_4bit_l|0x09;           // en0  rs1  rw0

    i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
    i2c_master_start(cmd_handle);
    i2c_master_write_byte(cmd_handle, (PCF8574_ADDRESS << 1)|I2C_MASTER_WRITE, true);
    i2c_master_write(cmd_handle, cmd_send, 4, true);
    i2c_master_stop(cmd_handle);

    i2c_master_cmd_begin(I2C_NUM_0,cmd_handle,pdTICKS_TO_MS(100));
    i2c_cmd_link_delete(cmd_handle);

}

void lcd_init()
{
     // delay 1ms
    vTaskDelay(1/portTICK_PERIOD_MS); 

    // send cmd   00110000
    lcd_send_cmd(0x30);
    // delay 5ms
    vTaskDelay(5/portTICK_PERIOD_MS);

    // send cmd   00110000
    lcd_send_cmd(0x30);
     // delay 1ms
    vTaskDelay(1/portTICK_PERIOD_MS); 

    // send cmd   00110000
    lcd_send_cmd(0x30);
    vTaskDelay(1/portTICK_PERIOD_MS); 

    // send cmd   00100000
    lcd_send_cmd(0x20);   // 4bit
    vTaskDelay(1/portTICK_PERIOD_MS); 

    // send cmd  0 0 1 0 N F 0 0
    //           0 0 1 0 1 0 0 0
    lcd_send_cmd(0x28);   // 4bit  2 dong
    vTaskDelay(1/portTICK_PERIOD_MS); 

         // send cmd  0x0C      DISPLAY ON
    //           0 0 0 0 1 1 0 0
    lcd_send_cmd(0x0C);
    vTaskDelay(1/portTICK_PERIOD_MS);

    // send cmd  0x01
    //           0 0 0 0 0 0 0 1
    lcd_send_cmd(0x01);
    vTaskDelay(1/portTICK_PERIOD_MS); 

    // send cmd  0 0 0 0 0 1 I/D S
    //           0 0 0 0 0 1 1   0
    lcd_send_cmd(0x06);
    vTaskDelay(1/portTICK_PERIOD_MS); 




}

void lcd_init2 (void)
{
	// 4 bit initialisation
	usleep(50000);  // wait for >40ms
	lcd_send_cmd (0x30);
	usleep(4500);  // wait for >4.1ms
	lcd_send_cmd (0x30);
	usleep(200);  // wait for >100us
	lcd_send_cmd (0x30);
	usleep(200);
	lcd_send_cmd (0x20);  // 4bit mode
	usleep(200);

  // dislay initialisation
	lcd_send_cmd (0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
	usleep(1000);
	lcd_send_cmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
	usleep(1000);
	lcd_send_cmd (0x01);  // clear display
	usleep(1000);
	usleep(1000);
	lcd_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
	usleep(1000);
	lcd_send_cmd (0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
	usleep(2000);
}
void lcd_init3 (void)
{
	vTaskDelay(1/portTICK_PERIOD_MS); 
	lcd_send_cmd (0x30);
	vTaskDelay(5/portTICK_PERIOD_MS); 
	lcd_send_cmd (0x30);
	vTaskDelay(1/portTICK_PERIOD_MS); 
	lcd_send_cmd (0x30);
	vTaskDelay(1/portTICK_PERIOD_MS); 
	lcd_send_cmd (0x20);  // 4bit mode
	vTaskDelay(1/portTICK_PERIOD_MS); 


	lcd_send_cmd (0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
	vTaskDelay(1/portTICK_PERIOD_MS); 

   lcd_send_cmd (0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
	vTaskDelay(1/portTICK_PERIOD_MS); 

	lcd_send_cmd (0x01);  // clear display
	vTaskDelay(1/portTICK_PERIOD_MS); 

	lcd_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
	vTaskDelay(1/portTICK_PERIOD_MS); 

	
}
void lcd_set_cur(uint8_t row, uint8_t col)
{
    if(row == 0)
    {
        col = 0x80|col;
    }
    else if(row == 1)
    {
        col = 0xC0|col;
    }
    lcd_send_cmd(col);
}

void lcd_send_string(char *str)
{
    while(*str != '\0')
    {
        lcd_send_data(*str);
        str++;
    }
}

void lcd_clear()
{
    lcd_send_cmd(0x01);
    vTaskDelay(10 /portTICK_PERIOD_MS); 
}
void app_main(void)
{
    i2c_init();
    lcd_init2();
    lcd_set_cur(0,2);
    lcd_send_string("manhdzvcll");
    lcd_set_cur(1,0);
    lcd_send_string("banlinhkien.com");
    while(1)
    {
        lcd_clear();
        lcd_set_cur(0,2);
        lcd_send_string("manhdzvcll");
        lcd_set_cur(1,0);
        lcd_send_string("banlinhkien.com");
        vTaskDelay(1000/portTICK_PERIOD_MS);
        lcd_clear();
        lcd_set_cur(0,2);
        lcd_send_string("dang cap");
        lcd_set_cur(1,2);
        lcd_send_string("la mai mai ");
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
}
