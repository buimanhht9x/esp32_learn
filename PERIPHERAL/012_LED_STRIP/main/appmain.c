// not done
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/rmt.h>
#include "string.h"
#define LED_GPIO_NUM    18
#define LED_COUNT       12

void set_led_color(rmt_item32_t *led_data, uint8_t led, uint8_t red, uint8_t green, uint8_t blue) {
    uint32_t bits = (green << 24) | (red << 16) | (blue << 8);
    for (int8_t i = 23; i >= 0; i--) {
        if (bits & (1 << i)) {
            led_data[led * 24 + i + 8].duration0 = 10;
            led_data[led * 24 + i + 8].level0 = 1;
            led_data[led * 24 + i + 8].duration1 = 6;
            led_data[led * 24 + i + 8].level1 = 0;
        } else {
            led_data[led * 24 + i + 8].duration0 = 6;
            led_data[led * 24 + i + 8].level0 = 1;
            led_data[led * 24 + i + 8].duration1 = 10;
            led_data[led * 24 + i + 8].level1 = 0;
        }
    }
}

void ws2812_task(void *pvParameters) {
    rmt_config_t config = RMT_DEFAULT_CONFIG_TX(LED_GPIO_NUM, RMT_CHANNEL_0);
    config.clk_div = 2;

    rmt_config(&config);
    rmt_driver_install(config.channel, 0, 0);

    rmt_item32_t *led_data = (rmt_item32_t *)malloc(LED_COUNT * 24 * sizeof(rmt_item32_t));
    memset(led_data, 0, LED_COUNT * 24 * sizeof(rmt_item32_t));

    while (1) {
        // Set the color of each LED
        for (int i = 0; i < LED_COUNT; i++) {
            set_led_color(led_data, i, 255, 0, 0); // Red color
        }

        // Send the LED data
        rmt_write_items(config.channel, led_data, LED_COUNT * 24, true);

        // Delay before updating LED colors
        vTaskDelay(1000 / portTICK_PERIOD_MS);

        // Clear the LED data
        memset(led_data, 0, LED_COUNT * 24 * sizeof(rmt_item32_t));
        rmt_write_items(config.channel, led_data, LED_COUNT * 24, true);

        // Delay before the next cycle
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void app_main() {
    xTaskCreate(ws2812_task, "ws2812_task", 2048, NULL, 5, NULL);
}