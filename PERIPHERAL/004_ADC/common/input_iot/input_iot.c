#include "input_iot.h"

typedef void (*input_callback_t)(int);
input_callback_t input_callback = NULL;

static void IRAM_ATTR gpio_input_isr_handler(void *arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    input_callback(gpio_num);

} 

void gpio_input_create(gpio_num_t gpio_num, gpio_int_type_t gpio_intr_type)
{
    // chon io làm GPIO
    gpio_pad_select_gpio(gpio_num);
    // Set làm input Mode
    gpio_set_direction(gpio_num, GPIO_MODE_INPUT);
    // cấu hình Pullup 
    gpio_set_pull_mode(gpio_num, GPIO_PULLUP_ENABLE);
    // cấu hình ngắt
    gpio_set_intr_type(gpio_num, gpio_intr_type);

    gpio_install_isr_service(0);
    gpio_isr_handler_add(gpio_num,gpio_input_isr_handler, (void*) gpio_num);
}

uint8_t gpio_input_read(gpio_num_t gpio_num)
{
    return gpio_get_level(gpio_num);
}


void gpio_input_set_callback(void *cb)
{
    input_callback = cb;
}