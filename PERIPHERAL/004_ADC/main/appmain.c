/*
    ADC
    ESP32 ADC có thể đọc các giá trị tương tự trong khoảng 0-3,3V. 
    ADC Chuyển đổi tín hiệu tương tự thành giá trị kỹ thuật số. 
    Tín hiệu điện áp đo được có giá trị trong khoảng từ 0 đến 4095. 
    Theo đó, giá trị 0 tương đương với 0V, trong khi giá trị tối 
    đa 4095 tương đương với 3,3V

    Độ phân giải có thể thay đổi

    Chân ADC1
        ADC1_CH0 :GPIO 36
        ADC1_CH1 :GPIO 37 (KHÔNG CÓ SẴN)
        ADC1_CH2 :GPIO 38 (KHÔNG CÓ SẴN)
        ADC1_CH3 :GPIO 39
        ADC1_CH4 :GPIO 32
        ADC1_CH5 :GPIO 33
        ADC1_CH6 :GPIO 34
        ADC1_CH7 :GPIO 35
    
    Chân ADC2
        ADC2_CH0 :GPIO 4
        ADC2_CH1 :GPIO 0
        ADC2_CH2 :GPIO 2
        ADC2_CH3 :GPIO 15
        ADC2_CH4 : GPIO 13
        ADC2_CH5 :GPIO 12
        ADC2_CH6 : GPIO 14
        ADC2_CH7 :GPIO 27
        ADC2_CH8 :GPIO 25
        ADC2_CH9 :GPIO 26

    Thư viện: driver/adc.h và esp_adc_cal.h

    các hàm chính:
    // cấu hình dải điện áp đầu vào cho adc
    adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_11);  
        0dB	    ADC_ATTEN_DB_0	    ~100-950 mV
        2,5dB	ADC_ATTEN_DB_2_5	~100-1250mV
        6dB	    ADC_ATTEN_DB_6	    ~150-1750mV
        11dB	ADC_ATTEN_DB_11	    ~150-2450mV

     

    // Hiệu chỉnh ADC
    esp_adc_cal_characteristics_t adc1_chars;
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_DEFAULT, 0, &adc1_chars);

    // cấu hình Độ rộng Bit.
    adc1_config_width(ADC_WIDTH_BIT_DEFAULT);
    ADC_WIDTH_BIT_DEFAULT (12-bit)
    ADC_WIDTH_BIT_9
    ADC_WIDTH_BIT_10
    ADC_WIDTH_BIT_11
    ADC_WIDTH_BIT_12

    // đọc adc
    int adc_value = adc1_get_raw(ADC1_CHANNEL_0);

    // đọc điện áp adc
    uint32_t mV = esp_adc_cal_raw_to_voltage(adc1_get_raw(ADC1_CHANNEL_0), &adc1_chars);


*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include"freertos/timers.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"

esp_adc_cal_characteristics_t   adc_char;

TimerHandle_t timer1 ;
// cấu hình adc
void adc_config()
{ 
    // Hiệu chỉnh ADC
    esp_adc_cal_characterize(ADC_UNIT_1,ADC_ATTEN_DB_6,ADC_WIDTH_BIT_10, 0, &adc_char);
    // cấu hình độ rộng ADC
    adc1_config_width(ADC_WIDTH_BIT_10);
    // cấu hình dải điện áp đầu vào cho adc 
    adc1_config_channel_atten(ADC1_CHANNEL_4,ADC_ATTEN_DB_6);
}

void timer1_callback()
{
        // đọc adc
        int adc_value = adc1_get_raw(ADC1_CHANNEL_4);
        // đọc vol
        uint32_t mV = esp_adc_cal_raw_to_voltage(adc1_get_raw(ADC1_CHANNEL_4), &adc_char);
        printf("adc = %d , vol = %dmV\n",adc_value,mV);
}
void app_main(void)
{
    // tạo timer 10ms
    timer1 = xTimerCreate("timer1", 20/portTICK_PERIOD_MS, 1, 0, timer1_callback );
    xTimerStart(timer1,0);

    adc_config();
    while (1)
    {
        vTaskDelay(200/portTICK_PERIOD_MS);
    }
    
}