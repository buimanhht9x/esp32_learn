/**
 * @brief I2S ADC/DAC example
 *        1. Erase flash
 *        2. Record audio from ADC and save in flash
 *        3. Read flash and replay the sound via DAC
 *        4. Play an example audio file(file format: 8bit/8khz/single channel)
 *        5. Loop back to step 3
 */

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/i2s.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "driver/ledc.h"
#include "freertos/queue.h"
#include "wifi_sta_connect.h"
#include "nvs_flash.h"
#include "esp_http_server.h"


void example_disp_buf(int* buf, int length)
{
    for (int i = 1; i < length; i = i + 2) {
        // buf[i] = (float)(buf[i]-2048)*(1.0f/2048.0f);
        buf[i] = buf[i] - 512;
        printf("%d\n", buf[i]);
    }
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
    return (x- in_min)*(out_max - out_min)/(in_max- in_min) + out_min;
}

void readerTask()
{
    ESP_LOGI("I2S ADC","Init adc task");
    int i2s_read_len = 16 * 1024;
    size_t bytes_read;
    int* i2s_read_buff = (int*) calloc(i2s_read_len, sizeof(int));
   
    while (1) {
        i2s_read(I2S_NUM_0, (void*)i2s_read_buff, 1024*sizeof(uint16_t), &bytes_read, portMAX_DELAY);
        if( I2S_EVENT_RX_DONE && bytes_read>0)
        {
           for (int i = 1; i < 512; i = i + 2) 
            {
                i2s_read_buff[i] = (float)(i2s_read_buff[i]-2048)*(1.0f/2048.0f);
                //i2s_read_buff[i] = i2s_read_buff[i] - 512;
                i2s_read_buff[i] = map(i2s_read_buff[i],600000,800000,0,1000);
                printf("%d", i2s_read_buff[i]);
                printf("\t");
                printf("%d", 0);
                printf("\t");
                printf("%d", 1000);
                printf("\n");
            }
        }
        // vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI("SERVER", "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Registering the ws handler
        ESP_LOGI("SERVER", "Registering URI handlers");
        return server;
    }
    ESP_LOGI("SERVER", "Error starting server!");
    return NULL;
}

void app_main(void)
{
    i2s_config_t i2s_config = 
    {
        .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN),  // I2S receive mode with ADC
        .sample_rate = 44100,                                               // set I2S ADC sample rate
        .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,                                 // 16 bit I2S (even though ADC is 12 bit)
        .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,                                 // handle adc data as single channel (right)
        .communication_format = (i2s_comm_format_t)I2S_COMM_FORMAT_I2S,               // I2S format
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,                                                        // 
        .dma_buf_count = 4,                                                           // number of DMA buffers >=2 for fastness
        .dma_buf_len = 1024,                                                // number of samples per buffer
        .use_apll = 0,     
        .tx_desc_auto_clear = false,
        .fixed_mclk = 0,                                                           // no Audio PLL - buggy and not well documented
  };


  i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);

  i2s_set_adc_mode(ADC_UNIT_1, ADC1_CHANNEL_5);
  i2s_adc_enable(I2S_NUM_0);
  vTaskDelay(100/portTICK_PERIOD_MS);
  wifi_sta_connect();
  httpd_handle_t server = NULL;
  server = start_webserver(); 
  xTaskCreatePinnedToCore(readerTask, "Reader", 8192,NULL, 1,NULL, 0);
}



////  read adc  
// #include <stdio.h>
// #include "freertos/FreeRTOS.h"
// #include "freertos/task.h"
// #include"freertos/timers.h"
// #include "driver/gpio.h"
// #include "driver/adc.h"
// #include "esp_adc_cal.h"

// esp_adc_cal_characteristics_t   adc_char;

// TimerHandle_t timer1 ;
// // cấu hình adc
// void adc_config()
// { 
//     // Hiệu chỉnh ADC
//     esp_adc_cal_characterize(ADC_UNIT_1,ADC_ATTEN_DB_6,ADC_WIDTH_BIT_10, 0, &adc_char);
//     // cấu hình độ rộng ADC
//     adc1_config_width(ADC_WIDTH_BIT_10);
//     // cấu hình dải điện áp đầu vào cho adc 
//     adc1_config_channel_atten(ADC1_CHANNEL_5,ADC_ATTEN_DB_6);
// }
       
// int buffer[512];
// void app_main(void)
// {
//     adc_config();
//     while (1)
//     {
//         for(int i = 0; i < 512; ++i)
//         {
//             buffer[i] = adc1_get_raw(ADC1_CHANNEL_5)-512;
//         }
//         for(int i = 0; i < 512; ++i)
//         {
//             printf("%d",buffer[i]);
//             printf("\t");
//             printf("%d", 1000);
//             printf("\t");
//             printf("%d", -1000);
//             printf("\n");
//         }
//         vTaskDelay(10/portTICK_PERIOD_MS);
//     }
    
// }
