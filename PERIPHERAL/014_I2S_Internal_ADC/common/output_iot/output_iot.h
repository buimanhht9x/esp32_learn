#ifndef OUTPUT_IO_H
#define OUTPUT_IO_H

#include "driver/gpio.h"
#include "hal/gpio_types.h"
#include "stdint.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"


void gpio_output_create(gpio_num_t gpio_num);

void gpio_output_write(gpio_num_t gpio_num, int level);

void gpio_output_toggle(gpio_num_t gpio_num);

#endif
