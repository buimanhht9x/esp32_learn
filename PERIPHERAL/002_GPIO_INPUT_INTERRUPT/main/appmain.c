
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "input_iot.h"
#include "output_iot.h"

#define LED_GPIO 2
#define BTN_GPIO 0

int k = 0;
void gpio_input_callback_main(gpio_num_t gpio_num)
{
    if(gpio_num == BTN_GPIO)
    {
        k = 1- k ;
        gpio_set_level(LED_GPIO, k);
    }
    
}
void button_task(void *pvParameter)
{
    while(1)
    {
    }
}


void app_main(void)
{
    gpio_input_create(BTN_GPIO,GPIO_INTR_NEGEDGE);

    gpio_output_create(LED_GPIO);

    gpio_input_set_callback(gpio_input_callback_main);
    xTaskCreate(&button_task, "button_task", 512, NULL, 5, NULL);
}