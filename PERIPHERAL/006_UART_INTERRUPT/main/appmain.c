/*
    UART0: pin tx1 pin rx3
    UART1: pin tx4 pin rx5
*/

 /* This example shows how to use the UART driver to handle UART interrupt.
 *
 * - Port: UART0
 * - Receive (Rx) buffer: on
 * - Transmit (Tx) buffer: off
 * - Flow control: off
 * - Event queue: on
 * - Pin assignment: TxD (default), RxD (default)
 */

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_intr_alloc.h"


QueueHandle_t uart_queue;



void uart_event_task()
{
    uart_config_t uart0_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .parity    = UART_PARITY_DISABLE,
        .source_clk= UART_SCLK_APB,
        .stop_bits = UART_STOP_BITS_1,
    };
    uart_param_config(UART_NUM_0, &uart0_config);
    uart_set_pin(UART_NUM_0, 1, 3, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM_0, 2048, 2048,10, &uart_queue, 0);

    uart_event_t uart_event;
    uint8_t data[50];
    while (1)
    {
        if(xQueueReceive(uart_queue,&uart_event,portMAX_DELAY))
        {
            int len = uart_read_bytes(UART_NUM_0,data, uart_event.size, 10/portTICK_PERIOD_MS);
            uart_write_bytes(UART_NUM_0,(char*) data, uart_event.size);
            data[len] = '\0';
            if(strcmp((char *)data,"on") == 0)
            {
                gpio_set_level(2, 1);
            }
            else if(strcmp((char *)data,"off") == 0)
            {
                gpio_set_level(2, 0);
            }
        }
    }
    
    
    free(data);
    vTaskDelete(NULL);
}

void app_main(void)
{
    gpio_pad_select_gpio(2);
    gpio_set_direction(2,GPIO_MODE_OUTPUT);

    xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, 12, NULL);
 
}

