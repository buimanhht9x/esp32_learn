// Ver 1, chatGPT

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"

#define ds3231_address   0x68
#define time_out_ms      1000
#define delay_ms         1000

#define i2c_sda_pin     21
#define i2c_scl_pin     22

static const char *TAG  =  "i2c";

static esp_err_t i2c_master_init(void)
{
    i2c_config_t i2c_config = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = i2c_sda_pin,
        .scl_io_num =i2c_scl_pin,
        .sda_pullup_en = true,
        .scl_pullup_en =true,
        .master.clk_speed = 100000
    };

    i2c_param_config(I2C_NUM_0,&i2c_config);

    // the remaining three parameters (0, 0, 0) are related to the I2C interrupt configuration
    // dùng cho slave nhận data từ master
    // khởi tạo và định cấu hình trình điều khiển I2C cho cổng chính I2C được chỉ định, cho phép
    // giao tiếp với các thiết bị I2C được kết nối với cổng đó. Sau khi trình điều khiển được 
    // cài đặt, bạn có thể sử dụng các chức năng khác do API
    i2c_driver_install(I2C_NUM_0, i2c_config.mode, 0,0,0);

    return ESP_OK;
}
// nhi phan -> thap phan
uint8_t BCDtoDEC(uint8_t val)
{
    return  (val >> 4)*10 + (val & 0x0F);
}
// phap phan -> nhi phan
 uint8_t DECtoBCD(uint8_t val)
{
    return  ((val/10) << 4 )  + (val % 10);
}

void ds3231_settime(uint8_t hour, uint8_t minute, uint8_t second)
{
    // - tạo một bộ điều khiển lệnh I2C, đại diện cho một chuỗi các hoạt động I2C sẽ được thực thi bởi trình điều khiển I2C.
    // - i2c_cmd_link_create() tạo một bộ điều khiển lệnh I2C trống và trả về một con trỏ tới nó. Bộ xử lý lệnh này được sử 
    // - dụng để xây dựng một chuỗi hoạt động I2C, chẳng hạn như bắt đầu, dừng, đọc và ghi, sẽ được thực thi theo một thứ tự cụ thể.
    // - sau khi tạo bộ điều khiển lệnh, bạn có thể sử dụng các chức năng khác nhau từ API trình điều khiển I2C, chẳng hạn như
    // i2c_master_start(), i2c_master_write_byte(), i2c_master_read_byte()và i2c_master_stop(), để thêm các hoạt động I2C riêng lẻ vào bộ điều khiển lệnh.
    // - khi bạn đã thêm tất cả các thao tác I2C cần thiết vào bộ điều khiển lệnh, bạn có thể thực hiện chuỗi thao tác bằng cách chuyển 
    // bộ điều khiển lệnh tới i2c_master_cmd_begin(). Trình điều khiển I2C sau đó sẽ xử lý các hoạt động trong bộ điều khiển lệnh và
    // thực hiện giao tiếp cần thiết với thiết bị I2C.
    // - gọi i2c_cmd_link_delete() sau khi thực hiện xử lý lệnh để giải phóng tài nguyên được phân bổ và tránh rò rỉ bộ nhớ.
    i2c_cmd_handle_t  cmd  =  i2c_cmd_link_create();

    i2c_master_start(cmd);
     
    // write 1 byte
    i2c_master_write_byte(cmd,(ds3231_address << 1)| I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd,0x00, true);
    i2c_master_write_byte(cmd, DECtoBCD(second), true);
    i2c_master_write_byte(cmd, DECtoBCD(minute), true);
    i2c_master_write_byte(cmd, DECtoBCD(hour), true);
    
    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0,cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);
}

void ds3231_settime_2(uint8_t hour, uint8_t minute, uint8_t second)
{
     i2c_cmd_handle_t  cmd  =  i2c_cmd_link_create();

    i2c_master_start(cmd);
    uint8_t buff[5];
    buff[0] = (ds3231_address << 1)| I2C_MASTER_WRITE;
    buff[1] = 0x00;
    buff[2] = DECtoBCD(second);
    buff[3] = DECtoBCD(minute);
    buff[4] = DECtoBCD(hour);

    // write nhieu byte
    i2c_master_write(cmd,buff,5,true);

    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0,cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);
}


void ds3231_settime_3(uint8_t dayweek,uint8_t day, uint8_t month, uint8_t year, uint8_t hour, uint8_t minute, uint8_t second)
{
     i2c_cmd_handle_t  cmd  =  i2c_cmd_link_create();

    i2c_master_start(cmd);
    uint8_t buff[9];
    buff[0] = (ds3231_address << 1)| I2C_MASTER_WRITE;
    buff[1] = 0x00;
    buff[2] = DECtoBCD(second);
    buff[3] = DECtoBCD(minute);
    buff[4] = DECtoBCD(hour);
    buff[5] = DECtoBCD(dayweek);
    buff[6] = DECtoBCD(day);
    buff[7] = DECtoBCD(month);
    buff[8] = DECtoBCD(year);

    // write nhieu byte
    i2c_master_write(cmd,buff,9,true);

    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0,cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);
}


void ds13231_gettime(uint8_t *hour, uint8_t *minute, uint8_t *second)
{
    i2c_cmd_handle_t cmd =  i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd,(ds3231_address << 1)|I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, 0x00,true);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd,(ds3231_address << 1)|I2C_MASTER_READ, true);
    i2c_master_read_byte(cmd, second, I2C_MASTER_ACK);
    i2c_master_read_byte(cmd, minute, I2C_MASTER_ACK);
    i2c_master_read_byte(cmd, hour, I2C_MASTER_NACK);

    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0,cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);

    *second = BCDtoDEC(*second);
    *minute = BCDtoDEC(*minute);
    *hour   = BCDtoDEC(*hour);

}


void ds13231_gettime_2(uint8_t *day, uint8_t *month, uint8_t *year, uint8_t *hour, uint8_t *minute, uint8_t *second)
{
    i2c_cmd_handle_t cmd =  i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd,(ds3231_address << 1)|I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, 0x00,true);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd,(ds3231_address << 1)|I2C_MASTER_READ, true);
    
    uint8_t buff[7];
    i2c_master_read(cmd, buff, 7, I2C_MASTER_ACK);

    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0,cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);

    *second = BCDtoDEC(buff[0]);
    *minute = BCDtoDEC(buff[1]);
    *hour   = BCDtoDEC(buff[2]);
    *day    = BCDtoDEC(buff[4]);
    *month  = BCDtoDEC(buff[5]);
    *year   = BCDtoDEC(buff[6]);

  //  ESP_LOG_BUFFER_HEX("DS3231", buff,7);
}


void app_main(void)
{
    uint8_t sec, min, hour, day, month, year;
    i2c_master_init();
    // dayweek  day  month  year  hour  min  sec  
    ds3231_settime_3(4,12,7,23,14,39,00);
    while(1)
    {
        ds13231_gettime_2(&day, &month, &year, &hour, &min, &sec);
        printf("current time : h:%d  mi:%d  s:%d \n", hour, min, sec);
        printf("current time : d:%d  mo:%d  y:%d \n", day, month, year);
        vTaskDelay(1000 / portTICK_RATE_MS);
    }
}

