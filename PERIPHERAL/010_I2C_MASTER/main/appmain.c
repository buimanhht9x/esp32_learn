/*
Nhiệm vụ
master : esp32
slave  : esp32
viết task, 1s gửi chuỗi "ledon" "ledoff" từ master cho slave

slave nhận được data thì turn on/off led2 theo lệnh nhận được
*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "input_iot.h"

#define slave_address   0x33

void i2c_master_init()
{
    i2c_config_t i2c_config = {
            .mode = I2C_MODE_MASTER,
            .sda_io_num = 21,
            .scl_io_num = 22,
            .sda_pullup_en = true,
            .scl_pullup_en = true,
            .master.clk_speed = 100000
    };
    i2c_param_config(I2C_NUM_0, &i2c_config);
    i2c_driver_install(I2C_NUM_0,i2c_config.mode,0,0,0);
}

void i2c_master_send_string(char *data)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd,(slave_address << 1) |I2C_MASTER_WRITE,true);
    i2c_master_write(cmd,(uint8_t *)&data,255,true);
    i2c_master_stop(cmd);

    i2c_master_cmd_begin(I2C_NUM_0,cmd, pdTICKS_TO_MS(100));
    i2c_cmd_link_delete(cmd);
}

void app_main(void)
{
    i2c_master_init();
    while (1)
    {
        i2c_master_send_string("ledon");
        vTaskDelay(1000/portTICK_PERIOD_MS);
        i2c_master_send_string("ledoff");
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
    
}