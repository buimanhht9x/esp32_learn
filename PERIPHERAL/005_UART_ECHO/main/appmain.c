/*
    UART0: pin tx1 pin rx3
    UART1: pin tx4 pin rx5
*/
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "driver/uart.h"

#define uart_num UART_NUM_0
#define BUF_SIZE (1024)

void uart_echo_task(void *arg)
{
    // uint8_t rxbuf[100];
    uart_config_t uart0 = {
            .baud_rate = 115200,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .source_clk = UART_SCLK_APB,
            .stop_bits = UART_STOP_BITS_1,     
    };

    uart_driver_install(uart_num,BUF_SIZE*2, BUF_SIZE*2, 0, NULL, 0);
    uart_param_config(uart_num, &uart0);
    uart_set_pin(uart_num,1,3,UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);

    uint8_t *data = (uint8_t *) malloc(BUF_SIZE);

    while (1)
    {
        // đọc tất cả byte nhận được
        int len =  uart_read_bytes(uart_num, data, BUF_SIZE, 20/portTICK_PERIOD_MS);
        
        // len ở đây là chiều dài mảng nhận được
        if (len > 0)
        {
            data[len] = '\0'; // thêm kí tự null kết thúc
            /*
            Việc thêm ký tự kết thúc null là cần thiết bởi vì hầu hết các hàm thao tác chuỗi trong C,
            chẳng hạn như strcmp, mong muốn các chuỗi sẽ bị kết thúc null. Bằng cách đặt null 
            ở cuối, nó cho phép chúng ta sử dụng uart_datanhư một chuỗi và thực hiện so sánh chuỗi
            bằng cách sử dụng strcmp để kiểm tra xem lệnh nhận được là "bật" hay "tắt".
            */
            if (strcmp((char *)data, "on") == 0)  // turn  on led
            {
                gpio_set_level(2, 1);
            }
            else if (strcmp((char *)data, "off") == 0) // turn  off led
            {
                gpio_set_level(2, 0);
            }
        } 

        // gửi tất cả byte trong buff 
        uart_write_bytes(uart_num, (char *)data, len);
    }
}

void app_main(void)
{
    gpio_pad_select_gpio(2);
    gpio_set_direction(2, GPIO_MODE_OUTPUT);
    gpio_set_level(2, 0);
    xTaskCreate(uart_echo_task,"uart_echo_task",2048,NULL,10,NULL);
    // while(1)
    // {
    //     gpio_set_level(2, 0);
    //     vTaskDelay(500/portTICK_PERIOD_MS);
    //     gpio_set_level(2, 1);
    //     vTaskDelay(500/portTICK_PERIOD_MS);
    // }
}

