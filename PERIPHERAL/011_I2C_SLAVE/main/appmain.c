/*
Nhiệm vụ
master : esp32
slave  : esp32
nhận chuỗi "ledon" "ledoff" từ master cho slave

slave nhận được data thì turn on/off led2 theo lệnh nhận được
*/


#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "input_iot.h"
#include "string.h"

#define slave_address   0x33

void i2c_slave_init()
{
    i2c_config_t i2c_config = {
            .mode = I2C_MODE_SLAVE,
            .sda_io_num = 21,
            .scl_io_num = 22,
            .sda_pullup_en = true,
            .scl_pullup_en = true,
            .slave.addr_10bit_en =  0 ,
            .slave.slave_addr = slave_address
    };
    i2c_param_config(I2C_NUM_0, &i2c_config);
    i2c_driver_install(I2C_NUM_0,i2c_config.mode,0,0,0);
}
void gpio_init()
{
    gpio_pad_select_gpio(2);
    gpio_set_direction(2,GPIO_MODE_OUTPUT);
    
    gpio_set_level(2,1);
}
void app_main(void)
{
    i2c_slave_init();
    gpio_init();
    uint8_t rx_buff[255];
    while (1)
    {
        int len = i2c_slave_read_buffer(I2C_NUM_0,rx_buff,255, pdTICKS_TO_MS(100));
        if(len > 0)
        {
                i2c_reset_rx_fifo(I2C_NUM_0);
                if(strcmp((char *)rx_buff, (char *)"ledon") == 0)
                {
                    gpio_set_level(2,1);
                    i2c_slave_write_buffer(I2C_NUM_0,(uint8_t *)"hello from slave led ON",255, pdTICKS_TO_MS(100));
                }
                else if (strcmp((char *)rx_buff, (char *)"ledoff") == 0)
                {
                    gpio_set_level(2,0);
                    i2c_slave_write_buffer(I2C_NUM_0,(uint8_t *)"hello from slave led OFF",255, pdTICKS_TO_MS(100));
                }
                else
                {
                    printf("%s\n",rx_buff);
                    i2c_slave_write_buffer(I2C_NUM_0,(uint8_t *)"hello from slave",255, pdTICKS_TO_MS(100));
                }
        }
    }
    
}