/* OTA example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "string.h"

#include "nvs.h"
#include "nvs_flash.h"
#include <sys/socket.h>
#include "wifi_station_connect.h"
#include <string.h>

#define HASH_LEN 32


#define FILE_URL "https://raw.githubusercontent.com/buimanhht9x/ESP32testOTA/master/version.txt"

extern const uint8_t cert_crt_start[] asm("_binary_cert_crt_start");
extern const uint8_t cert_crt_end[]   asm("_binary_cert_crt_end");

static const char *TAG = "simple_ota_example";

#define OTA_URL_SIZE 256
char version[20];
int len_version;

esp_err_t http_event_handler(esp_http_client_event_t *evt) {
    switch (evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGE("HTTP", "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI("HTTP", "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI("HTTP", "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI("HTTP", "HTTP_EVENT_ON_HEADER");
            printf("%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGI("HTTP", "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
           // printf("%.*s", evt->data_len, (char*)evt->data);
            memcpy(version, evt->data, evt->data_len); // Copy the data
            version[evt->data_len] = '\0'; // Add null t
            
           // len_version = evt->data_len;
            printf("%.*s\n", evt->data_len, (char*)version);
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI("HTTP", "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI("HTTP", "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}

void fetchAndPrintFile() {
    esp_http_client_config_t config = {
        .url = FILE_URL,
        .cert_pem =(const char *) cert_crt_start,
        .event_handler = http_event_handler,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if (err != ESP_OK) {
        ESP_LOGE("HTTP", "HTTP request failed: %s", esp_err_to_name(err));
    }
    esp_http_client_cleanup(client);
}

void simple_ota_example_task(void *pvParameter)
{
    ESP_LOGI(TAG, "Starting OTA example");
    esp_http_client_config_t config = {
        .url = FILE_URL,
        .method = HTTP_GET,
        .cert_pem =(const char *) cert_crt_start,
        .event_handler = http_event_handler,

        .keep_alive_enable = true,
    };


    esp_err_t ret = esp_https_ota(&config);
    if (ret == ESP_OK) {
        esp_restart();
    } else {
        ESP_LOGE(TAG, "Firmware upgrade failed");
    }
}


// void app_main(void)
// {
//     wifi_station_init();




//     xTaskCreate(&simple_ota_example_task, "ota_example_task", 8192, NULL, 5, NULL);
// }

void app_main() {

    wifi_station_init();
    vTaskDelay(1000/portTICK_PERIOD_MS);

    fetchAndPrintFile();
}
