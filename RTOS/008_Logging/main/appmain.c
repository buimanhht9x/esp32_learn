/*

     xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , &task_handle_task_print_terminal);
     
     BaseType_t xTaskCreate (TaskFunction_t pvTaskCode,
        const char * const pcName,
        const uint32_t usStackDepth,
        void * const pvParameters,
        UBaseType_t uxPriority,
        TaskHandle_t * const pvCreatedTask)

        • pvTaskCode: tên Task (tên hàm) muốn chạy
        • *pcName: tên do người dùng đặt cho cái task đó.
        • usStackDepth: size hay gọi là Depth của Task, đơn vị của nó là 4 bytes.
        • *pvParameters: 1 con trỏ dùng để chứa các tham số của Task khi task còn hoạt động, thường để NULL.
        • uxPriority: độ ưu tiên của task này, số càng lớn thì độ ưu tiên càng lớn.
        • *pxCreatedTask: 1 con trỏ đại diện Task, dùng để điểu khiển task từ 1 task khác, ví dụ xóa task này từ 1 task đang
        chạy khác.
        Sau khi tạo task, ta gọi hàm vTaskStartScheduler() dùng để yêu cầu nhân (kernel) của RTOS bắt
        đầu chạy. Ta sẽ xóa task thông qua hàm xTaskDelete()


    void vTaskSuspend(TaskHandle_txTaskToSuspend)
        Suspend a task.

    void vTaskResume(TaskHandle_txTaskToResume)
        Resumes a suspended task.

    void vTaskDelete(TaskHandle_txTaskToDelete)
        Remove a task from the RTOS real time kernel’s management.

    eTaskState eTaskGetState(TaskHandle_txTask)
        Obtain the state of any task.
        eTaskState: 
            eRunning = 0
            eReady		
            eBlocked	
            eSuspended
            eDeleted
    
    TickType_t xTaskGetTickCount(void)
        Get tick count
        int c = xTaskGetTickCount();
        c = 100 tương đương 1s

    BaseType_t xTaskResumeFromISR(TaskHandle_t xTaskToResume)
        An implementation of vTaskResume() that can be called from within an ISR.


*/


/*
Mục đích code;
    Cơ bản về thư viện esp_log.h
    ESP_LOGI("tag", "  ");     log information in ra chữ màu xanh
    ESP_LOGW("tag", "  ");     log warning in ra chữ màu vàng
    ESP_LOGE("tag", "  ");     log EROR in ra chữ màu đỏ
    xTaskGetTickCount();       lấy tick count, 100tickcount tương đương 1000ms
    esp_log_timestamp();       giống millis   

*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"

#define LED  2
#define BTN  0


TaskHandle_t button_task_handle = NULL;

static void btn_isr_handler(void *arg)
{
    xTaskResumeFromISR(button_task_handle);

}
int led_state = 0;
void task_button(void *arg)
{
    while(1)
    {
        led_state = 1 - led_state;
        gpio_set_level(LED, led_state);
        ESP_LOGE("button_interrupt", "hello from button");
        fflush(stdout);
        vTaskSuspend(NULL);

    }
}

void task1()
{
    while(1)
    {
        ESP_LOGW("task1", "hello from task1");
        vTaskDelay(1000/portTICK_PERIOD_MS);
        printf("tick count : %d, time stamp: %d \n", xTaskGetTickCount(), esp_log_timestamp());
    }
}

void task2()
{
    while(1)
    {
        ESP_LOGI("task1", "hello from task2");
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
}
void app_main(void)
{

    gpio_pad_select_gpio(BTN);
    gpio_set_direction(BTN, GPIO_MODE_INPUT);
    gpio_set_pull_mode(BTN, GPIO_PULLUP_ENABLE);
    gpio_set_intr_type(BTN,GPIO_INTR_NEGEDGE);

    gpio_install_isr_service(0);
    gpio_isr_handler_add(BTN, btn_isr_handler, NULL);

    // OUTPUR
    gpio_pad_select_gpio(LED);
    gpio_set_direction(LED, GPIO_MODE_OUTPUT);

    // Lưu ý &button_task_handle
    xTaskCreate(task_button, "task_buton", 4096, NULL,10, &button_task_handle);
    xTaskCreate(task1, "task1", 4096, NULL, 10, NULL);
    xTaskCreatePinnedToCore(task2, "task2", 4096, NULL, 9, NULL,1);


}