/*
 Tạo 2 Task
    - Task 1 : task_blink_led : blink led chu kì 1s
    - Task 2 : task_print_terminal : in ra terminal mỗi 1s

     xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , NULL);
     
     BaseType_t xTaskCreate (TaskFunction_t pvTaskCode,
        const char * const pcName,
        const uint32_t usStackDepth,
        void * const pvParameters,
        UBaseType_t uxPriority,
        TaskHandle_t * const pvCreatedTask)

        • pvTaskCode: tên Task (tên hàm) muốn chạy
        • *pcName: tên do người dùng đặt cho cái task đó.
        • usStackDepth: size hay gọi là Depth của Task, đơn vị của nó là 4 bytes.
        • *pvParameters: 1 con trỏ dùng để chứa các tham số của Task khi task còn hoạt động, thường để NULL.
        • uxPriority: độ ưu tiên của task này, số càng lớn thì độ ưu tiên càng lớn.
        • *pxCreatedTask: 1 con trỏ đại diện Task, dùng để điểu khiển task từ 1 task khác, ví dụ xóa task này từ 1 task đang
        chạy khác.
        Sau khi tạo task, ta gọi hàm vTaskStartScheduler() dùng để yêu cầu nhân (kernel) của RTOS bắt
        đầu chạy. Ta sẽ xóa task thông qua hàm xTaskDelete()


    void vTaskSuspend(TaskHandle_txTaskToSuspend)
        Suspend a task.

    void vTaskResume(TaskHandle_txTaskToResume)
        Resumes a suspended task.

    void vTaskDelete(TaskHandle_txTaskToDelete)
        Remove a task from the RTOS real time kernel’s management.

    eTaskState eTaskGetState(TaskHandle_txTask)
        Obtain the state of any task.
        eTaskState: 
            eRunning = 0
            eReady		
            eBlocked	
            eSuspended
            eDeleted
    
    TickType_t xTaskGetTickCount(void)
        Get tick count
        int c = xTaskGetTickCount();
        c = 100 tương đương 1s

    BaseType_t xTaskResumeFromISR(TaskHandle_t xTaskToResume)
        An implementation of vTaskResume() that can be called from within an ISR.



    Queue: Dùng để gửi dữ liệu từ task này sang task khác


*/
/*
Mục đích code;
    Tạo 2 Task

    Task 1 : khởi tạo 1 queue 
    QueueHandle_t queuehandle_task1 = NULL;

    tạo queue với 5 ô
    queuehandle_task1 = xQueueCreate(5, sizeof(txBuff));
    
    txBuf
    cách thêm vào queue
    xQueueSend(queuehandle_task1, txBuff, 0); trong đó txBuff là địa chỉ, not giá trị

    xQueueSendToFront(queuehandle_task1, txBuff, 0); sẽ thêm data vào đầu queue
    xQueueSendToBack(queuehandle_task1, txBuff, 0); sẽ thêm data vào cuối queue
    queue sẽ thêm theo thứ tự
      |   task1 hello 1    |  |   task1 hello 1    |  |   task1 hello 1    |
      |                    |  |   task1 hello 2    |  |   task1 hello 2    |
      |                    |  |                    |  |   task1 hello 3    |
      |                    |  |                    |
      |                    |  |                    |
    trong while(1), in ra số data đang đợi trong queue, và số ô còn trống trong queue bằng hàm:
    uxQueueMessagesWaiting(queuehandle_task1) : trả về số data đợi trong queue
    uxQueueSpacesAvailable(queuehandle_task1) : trả về số ô trống chưa có data trong queue

    Task2 : Tạo 1 buff để nhận data trong queue  
    rxBuff
    mỗi 1s sẽ lấy data trong queue ra để in lên monitor
    kiểm tra  xQueueReceive(queuehandle_task1,(void *)&rxBuff, (TickType_t) 5) > 0 thì trong queue còn data 
    Nếu xQueueReceive >0 , tức là trong queue còn data

    xQueuePeek(queuehandle_task1,rxBuff, 5), sẽ đọc data đầu tiên trong queue mà không xóa data trong queue, data trong queue nguyên vẹn
    
    queue lấy ra theo thứ tự
     |   task1 hello 1    |             |   task1 hello 2    |           |   task1 hello 3    |       |                    |
     |   task1 hello 2    |             |   task1 hello 3    |           |                    |       |                    |
     |   task1 hello 3    |             |                    |           |                    |       |                    |
    para1: tên Queue
    para2: bufer, ở đây là con trỏ

    Các hàm chính:
        QueueHandle_t xQueueCreate( UBaseType_t uxQueueLength,  // Số lượng ô tối đa mà hàng đợi có thể chứa tại một thời điểm
                                    UBaseType_t uxItemSize );   // Kích thước, tính bằng byte, được yêu cầu để chứa từng ô trong hàng đợi
        VD: QueueHandle_t = xQueue1;
            xQueue1 = xQueueCreate(10, sizeof(unsigned long));
        
        void vQueueDelete( QueueHandle_t xQueue );  // xóa queue

        BaseType_t xQueueSend(                                   // hàm gửi data vào queue, return 1 nếu gửi thành công, return 0 nếu queue đầy
                               QueueHandle_t xQueue,             // queue cần gửi vào
                               const void * pvItemToQueue,       // con trỏ, là địa chỉ của biến cần gửi vào
                               ickType_t xTicksToWait            // thời gian đợi để gửi
                             );

       
        BaseType_t xQueueSendFromISR(                               // hàm gửi data vào queue từ trong hàm ngắt, return 1 nếu gửi thành công, return 0 nếu queue đầy
                                        QueueHandle_t xQueue,
                                        const void *pvItemToQueue,
                                        BaseType_t *pxHigherPriorityTaskWoken  // đặt null
                                    );
    
        BaseType_t xQueueSendToBack(                                               // hàm gửi data vào vị trí đầu của queue, return 1 nếu gửi thành công, return 0 nếu queue đầy
                                   QueueHandle_t xQueue,
                                   const void * pvItemToQueue,
                                   TickType_t xTicksToWait
                               );

        BaseType_t xQueueSendToFrontFromISR(                                        // hàm gửi data vào queue, return 1 nếu gửi thành công, return 0 nếu queue đầy
                                                QueueHandle_t xQueue,
                                                const void *pvItemToQueue,
                                                BaseType_t *pxHigherPriorityTaskWoken
                                            );
                            
        BaseType_t xQueueReceive(                                                   // hàm nhận data từ queue, return 1 nếu nhận thành công, return 0 nếu queue rỗng     
                               QueueHandle_t xQueue,
                               khoảng trống *pvBuffer,
                               TickType_t xTicksToWait
                            );

        BaseType_t xQueueReceiveFromISR                                             // hàm nhận data trong ngắt từ queue, return 1 nếu nhận thành công, return 0 nếu queue rỗng     
                                (
                                    QueueHandle_t xQueue,
                                    khoảng trống *pvBuffer,
                                    BaseType_t *pxHigherPriorityTaskWoken
                                );

        UBaseType_t uxQueueMessagesWaiting( QueueHandle_t xQueue );                 // hàm trả về số lượng data còn trong queue

        UBaseType_t uxQueueMessagesWaitingFromISR( QueueHandle_t xQueue );          // hàm trả về số lượng data còn trong queue, dùng trong hàm ngắt

        UBaseType_t uxQueueSpacesAvailable( QueueHandle_t xQueue );                 // hàm trả về số lượng space còn lại trong queue

        void vQueueDelete( QueueHandle_t xQueue );                                  // xóa queue

        BaseType_t xQueueReset( QueueHandle_t xQueue );                             // reset queue, queue trống

        BaseType_t xQueueIsQueueEmptyFromISR( const QueueHandle_t xQueue );         // kiểm tra queue trống không, return 1 nếu queue trống

        BaseType_t xQueueIsQueueFullFromISR( const QueueHandle_t xQueue );          // kiểm tra queue đầy chưa, return 1 nếu queue đầy

        BaseType_t xQueueOverwrite(                                                  // ghi đè giá trị đầu tiên trong queue
                                 QueueHandle_t xQueue,
                                 const void * pvItemToQueue
                                 );

        BaseType_t xQueueOverwrite                                                    // ghi đè giá trị đầu tiên trong queue, dùng trong hàm ngắt
                                (
                                QueueHandle_t xQueue,
                                const void * pvItemToQueue
                                BaseType_t *pxHigherPriorityTaskWoken
                                );

        BaseType_t xQueuePeek(                                                          // Đọc 1 giá trị trong queue mà không xóa nó, gọi là nhìn trộm
                             QueueHandle_t xQueue,
                             khoảng trống *pvBuffer,
                             TickType_t xTicksToWait
                             );
                        
        BaseType_t xQueuePeekFromISR(                                                    // Đọc 1 giá trị trong queue mà không xóa nó, gọi là nhìn trộm, dùng trong hàm ngắt
                                    QueueHandle_t xQueue,
                                    void *pvBuffer,
                                    );

        

*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

TaskHandle_t taskhandle_task1 = NULL;
TaskHandle_t taskhandle_task2 = NULL;

QueueHandle_t queuehandle_task1 = NULL;

void delay(uint32_t millis)
{
    vTaskDelay(pdMS_TO_TICKS(millis));
}

void task1(void *arg)
{
    char txBuff[50];
    queuehandle_task1 = xQueueCreate(5, sizeof(txBuff));

    if(queuehandle_task1 == 0)
    {
        printf("queue create fail\n");
    }

    sprintf(txBuff, " task1 hello 1");
    xQueueSend(queuehandle_task1, txBuff, 0);

    sprintf(txBuff, " task1 hello 2");
    xQueueSend(queuehandle_task1, txBuff, 0);

    sprintf(txBuff, " task1 hello 3");
    xQueueSend(queuehandle_task1, txBuff, 0);

    sprintf(txBuff, " task1 hello 4");
    xQueueSendToFront(queuehandle_task1, txBuff, 0);

    sprintf(txBuff, " task1 hello 5");
    xQueueSendToBack(queuehandle_task1, txBuff, 0);

    while(1)
    {
        
        printf("queue wait %d, space free in queue %d\n", uxQueueMessagesWaiting(queuehandle_task1), uxQueueSpacesAvailable(queuehandle_task1));
        fflush(stdout);
        delay(1000);
    }
}

void task2(void *arg)
{
    char rxBuff[50];
    while(1)
    {
        if(xQueueReceive(queuehandle_task1,&rxBuff, 5))
        {
            printf("%s\n",rxBuff);
            delay(1000);
        }
        
    }
}

void app_main(void)
{
    xTaskCreate(task1, "task1",4096,NULL, 10, &taskhandle_task1);
    xTaskCreatePinnedToCore(task2, "task2",4096,NULL, 10, &taskhandle_task2,1);

}