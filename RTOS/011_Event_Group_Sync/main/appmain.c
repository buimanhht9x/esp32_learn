/*
    Event Bits (Event Flags): Các bit sự kiện được sử dụng để cho biết liệu một sự kiện có xảy ra hay không
    Group Bits  : Là tập hợp của 24 Event Bits

    Các nhóm sự kiện cũng có thể được sử dụng để đồng bộ hóa các tác vụ, tạo ra thứ thường được gọi là 'điểm hẹn' 
    của tác vụ. Điểm đồng bộ hóa tác vụ là một vị trí trong mã ứng dụng mà tại đó tác vụ sẽ đợi ở trạng thái Bị chặn 
    (không tiêu tốn bất kỳ thời gian nào của CPU) cho đến khi tất cả các tác vụ khác tham gia đồng bộ hóa cũng đạt đến
    điểm đồng bộ hóa của chúng.

    Trong Group bit, cho phép kiểm tra 1 bit, hoặc nếu tất cả các bit trong group đều được set thì mới cho sự kiện tiếp theo xảy ra

    Ưu điểm:
        Việc triển khai nhóm sự kiện sẽ kiểm soát được sự kiện nếu:
            Không rõ ai chịu trách nhiệm xóa các bit (hoặc cờ) riêng lẻ.
            Không rõ khi nào một bit sẽ bị xóa.
            Không rõ liệu một bit đã được đặt hay xóa tại thời điểm một tác vụ thoát khỏi chức năng API đã kiểm tra giá trị của bit đó (có thể một tác vụ hoặc ngắt khác đã thay đổi trạng thái của bit).
  
    Thư viện 
    #include "event_groups.h"

    //  Declare a variable to hold the created event group. 
    EventGroupHandle_t xCreatedEventGroup;

    // Attempt to create the event group. 
    xCreatedEventGroup = xEventGroupCreate();

    // Was the event group created successfully? 
    if( xCreatedEventGroup == NULL )
    {
        // The event group was not created because there was insufficient FreeRTOS heap available. 
    }
    else
    {
       //  The event group was created. 
    }

    // Các hàm chính
     EventGroupHandle_t event_group_1;              
     event_group_1 = xEventGroupCreate( void );     // Tạo 1 event gr

     void vEventGroupDelete( EventGroupHandle_t xEventGroup ); // xoá event gr

    EventBits_t xEventGroupWaitBits(
                       const EventGroupHandle_t xEventGroup,
                       const EventBits_t uxBitsToWaitFor,
                       const BaseType_t xClearOnExit,
                       const BaseType_t xWaitForAllBits,
                       TickType_t xTicksToWait );
    Đọc các bit trong một nhóm sự kiện RTOS , tùy ý vào trạng thái Bị chặn (có thời gian chờ) để đợi một bit hoặc nhóm bit được thiết lập.
     
    Ví dụ:
     #define BIT_0 ( 1 << 0 )
     #define BIT_4 ( 1 << 4 )
     EventBits_t uxBits;
     TickType_t xTicksToWait = 100 / portTICK_PERIOD_MS;
     uxBits = xEventGroupWaitBits(
            xEventGroup,     Nhóm sự kiện đang được thử nghiệm. 
            BIT_0 | BIT_4,   Các bit trong nhóm sự kiện cần chờ. 
            pdTRUE,          BIT_0 & BIT_4 phải được xóa trước khi quay lại. 
            pdFALSE,         false: đợi 1 bit được set, true: đợi tất cả các bit trong nhóm sự kiện được set
            xTicksToWait );  Đợi tối đa 100 mili giây để thiết lập một trong hai bit.
    
            if  ( ( uxBits & ( BIT_0 | BIT_4 ) ) == ( BIT_0 | BIT_4 ) )
            {
                 xEventGroupWaitBits() được trả về vì cả hai bit đã được đặt. 
            }
            else if ( ( uxBits & BIT_0 ) != 0 )
            {
                 xEventGroupWaitBits() được trả về vì chỉ BIT_0 được đặt. 
            }
            else if ( ( uxBits & BIT_4 ) != 0 )
            {
                 xEventGroupWaitBits() được trả về vì chỉ BIT_4 được đặt. 
            }
            else 
            {
                 xEventGroupWaitBits() được trả về vì xTicksToWait đã vượt qua
                mà không đặt BIT_0 hoặc BIT_4. 
            }
    EventBits_t xEventGroupSetBits( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToSet );
            Đặt bit (cờ) trong nhóm sự kiện RTOS . Chức năng này không thể được gọi từ một ngắt. Ngắt dùng xEventGroupSetBitsFromISR() 
    EventBits_t xEventGroupClearBits(EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToClear );
            Xóa các bit (cờ) trong một nhóm sự kiện RTOS . Chức năng này không thể được gọi từ một ngắt. Ngắt dùng  xEventGroupClearBitsFromISR() 
    EventBits_t xEventGroupGetBits( EventGroupHandle_t xEventGroup );
            Trả về giá trị hiện tại của bit sự kiện (cờ sự kiện) trong nhóm sự kiện RTOS . Chức năng này không thể được sử dụng từ một ngắt. Ngắt dùng xEventGroupGetBitsFromISR() 

    EventBits_t xEventGroupSync( EventGroupHandle_t xEventGroup,
                              const EventBits_t uxBitsToSet,                // Bit hoặc nhóm bit sẽ được SET
                              const EventBits_t uxBitsToWaitFor,            // Bit Nhóm bit đợi để so sánh
                              TickType_t xTicksToWait );                    // Thời gian đợi
            Chức năng này thường được sử dụng để đồng bộ hóa nhiều tác vụ (thường được gọi là điểm hẹn nhiệm vụ), 
            trong đó mỗi tác vụ phải đợi các tác vụ khác đạt đến điểm đồng bộ hóa trước khi tiếp tục.
    Code mẫu

        Số bit được sử dụng bởi ba tác vụ. 
        #define TASK_0_BIT        ( 1 << 0 )
        #define TASK_1_BIT        ( 1 << 1 )
        #define TASK_2_BIT        ( 1 << 2 )

        #define ALL_SYNC_BITS ( TASK_0_BIT | TASK_1_BIT | TASK_2_BIT )

        // Sử dụng một nhóm sự kiện để đồng bộ ba tác vụ. Người ta cho rằng sự kiện này
        nhóm đã được tạo ở nơi khác. 
        EventGroupHandle_t xEventBits;

        void vTask0( void *pvParameters )
        {
        EventBits_t uxReturn;
        TickType_t xTicksToWait = 100 / cổngTICK_PERIOD_MS;

            while( ;; )
            {

                Đặt bit 0 trong nhóm sự kiện để lưu ý tác vụ này đã đạt đến
                điểm đồng bộ. Hai nhiệm vụ khác sẽ thiết lập hai bit khác được xác định
                bởi ALL_SYNC_BITS. Cả ba nhiệm vụ đã đạt đến sự đồng bộ hóa
                điểm khi tất cả ALL_SYNC_BITS được đặt. Chờ tối đa 100ms
                để điều này xảy ra. 

                uxReturn = xEventGroupSync( xEventBits,
                                            TASK_0_BIT,
                                            ALL_SYNC_BITS,
                                            xTicksToWait );

                if ( ( uxReturn & ALL_SYNC_BITS ) == ALL_SYNC_BITS )
                {
                    Cả ba tác vụ đều đạt đến điểm đồng bộ hóa trước cuộc gọi
                    đến xEventGroupSync() đã hết thời gian chờ. 
                }
            }
        }

        void vTask1(void *pvParameter)
        {
            vì( ;; )
            {

                Đặt bit 1 trong nhóm sự kiện để lưu ý rằng nhiệm vụ này đã đạt đến
                điểm đồng bộ hóa. Hai nhiệm vụ khác sẽ đặt hai nhiệm vụ khác
                bit được xác định bởi ALL_SYNC_BITS. Cả ba nhiệm vụ đều đạt
                điểm đồng bộ hóa khi tất cả ALL_SYNC_BITS được đặt. Chờ đợi
                vô thời hạn để điều này xảy ra. 

                xEventGroupSync( xEventBits, TASK_1_BIT, ALL_SYNC_BITS, portMAX_DELAY );

                xEventGroupSync() được gọi với thời gian chặn không xác định, vì vậy
                tác vụ này sẽ chỉ đến đây nếu việc đồng bộ hóa được thực hiện bởi tất cả
                ba tác vụ nên không cần kiểm tra giá trị trả về. 
            }
        }

        void vTask2(void *pvParameter)
        {
            while( ;; )
            {

                Đặt bit 2 trong nhóm sự kiện để lưu ý tác vụ này đã đạt đến
                điểm đồng bộ hóa. Hai nhiệm vụ khác sẽ đặt hai nhiệm vụ khác
                bit được xác định bởi ALL_SYNC_BITS. Cả ba nhiệm vụ đều đạt
                điểm đồng bộ hóa khi tất cả ALL_SYNC_BITS được đặt. Chờ đợi
                vô thời hạn để điều này xảy ra. 
                xEventGroupSync( xEventBits, TASK_2_BIT, ALL_SYNC_BITS, portMAX_DELAY );

                xEventGroupSync() được gọi với thời gian chặn không xác định, vì vậy
                tác vụ này sẽ chỉ đến đây nếu việc đồng bộ hóa được thực hiện bởi tất cả
                ba tác vụ nên không cần kiểm tra giá trị trả về. 
            }
        }

*/

/*
    Mục đích code
        Tạo ra event_group_a với 3 eventbit
        Tạo 3 task
        
        Task1 mỗi 5s sẽ set bit3 
        Task3 mỗi 3s sẽ set bit1 
        timer1_callback() sẽ set bit2 mỗi 10s

        task2 sẽ nhận bit, xem bit nào đc set thì in ra monitor, sau đó clear bit đi

        evBit = xEventGroupWaitBits(event_group_a,
                            EV_BIT1|EV_BIT2|EV_BIT3,
                            1,
                            1, // Bit này là đợi cả 3 bit cùng được set
                            portMAX_DELAY); 
 */


/*
    Mục đích code
    Tạo ra 3 task, mối task sẽ set 1 bit trong eventGroup trong các thời gian khác nhau
     
    Khi cả 3 cùng được set, thì sẽ trigged in ra monitor
    Dùng hàm EventBits_t xEventGroupSync( EventGroupHandle_t xEventGroup,
                              const EventBits_t uxBitsToSet,
                              const EventBits_t uxBitsToWaitFor,
                              TickType_t xTicksToWait );
    // Hàm này vừa set uxBitsToSet, kết quả trả về lại là tất cả các bit trong eventgroup, xem trong task 3

    Hàm này khá giống evBit = xEventGroupWaitBits(event_group_a,
                            EV_BIT1|EV_BIT2|EV_BIT3,
                            1,
                            1, // Khi set bit này lên 1, Bit này là đợi cả 3 bit cùng được set
                            portMAX_DELAY); 
*/

#include "stdint.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "stdio.h"

EventGroupHandle_t evgroup1 = 0;

#define ev_bit1  (1 << 0)
#define ev_bit2  (1 << 1)
#define ev_bit3  (1 << 2)

#define ALL_SYNC_BITS (ev_bit1 | ev_bit2 | ev_bit3)

void task1()
{
    while(1)
    {
        vTaskDelay(2000/portTICK_PERIOD_MS);
        xEventGroupSync( evgroup1, ev_bit1, ALL_SYNC_BITS, 1000/portTICK_PERIOD_MS );
        ESP_LOGE("task1", " task 1 trigged");
        
    }
}

void task2()
{
    while(1)
    {
        vTaskDelay(5000/portTICK_PERIOD_MS);
        ESP_LOGE("task2", " task 2 trigged");
        xEventGroupSync( evgroup1, ev_bit2, ALL_SYNC_BITS, 1000/portTICK_PERIOD_MS );
        
        
    }
}

void task3()
{
    EventBits_t uxReturn;
    TickType_t xTicksToWait = 100 / portTICK_PERIOD_MS;
    while(1)
    { 
        uxReturn = xEventGroupSync( evgroup1,
                                    ev_bit3,
                                    ALL_SYNC_BITS,
                                    xTicksToWait); // Nếu để đây portMaxDelay thì sẽ bị reset
        if( ( uxReturn & ALL_SYNC_BITS ) == ALL_SYNC_BITS )
        {
            /* All three tasks reached the synchronisation point before the call
            to xEventGroupSync() timed out. */
            ESP_LOGW("task3", " 3 task trigged");
        }
        
        
    }
}



void app_main(void)
{
    evgroup1 =  xEventGroupCreate();
    xTaskCreate(task1, "task1", 2048, NULL, 10 , NULL);
    xTaskCreate(task2, "task2", 2048, NULL, 10 , NULL);
    xTaskCreate(task3, "task3", 2048, NULL, 10 , NULL);
}