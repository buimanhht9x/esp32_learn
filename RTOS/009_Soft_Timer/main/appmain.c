/*
 Tạo 2 Task
    - Task 1 : task_blink_led : blink led chu kì 1s
    - Task 2 : task_print_terminal : in ra terminal mỗi 1s

     xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , NULL);
     
     BaseType_t xTaskCreate (TaskFunction_t pvTaskCode,
        const char * const pcName,
        const uint32_t usStackDepth,
        void * const pvParameters,
        UBaseType_t uxPriority,
        TaskHandle_t * const pvCreatedTask)

        • pvTaskCode: tên Task (tên hàm) muốn chạy
        • *pcName: tên do người dùng đặt cho cái task đó.
        • usStackDepth: size hay gọi là Depth của Task, đơn vị của nó là 4 bytes.
        • *pvParameters: 1 con trỏ dùng để chứa các tham số của Task khi task còn hoạt động, thường để NULL.
        • uxPriority: độ ưu tiên của task này, số càng lớn thì độ ưu tiên càng lớn.
        • *pxCreatedTask: 1 con trỏ đại diện Task, dùng để điểu khiển task từ 1 task khác, ví dụ xóa task này từ 1 task đang
        chạy khác.
        Nếu là vanila FreeRtos , Sau khi tạo task, ta gọi hàm vTaskStartScheduler() dùng để yêu cầu nhân (kernel) của RTOS bắt
        đầu chạy. 
        Ta sẽ xóa task thông qua hàm xTaskDelete()


*/


/*
    Cơ bản về thư viện esp_log.h
    ESP_LOGI("tag", "  ");     log information in ra chữ màu xanh
    ESP_LOGW("tag", "  ");     log warning in ra chữ màu vàng
    ESP_LOGE("tag", "  ");     log EROR in ra chữ màu đỏ
    xTaskGetTickCount();       lấy tick count, 100tickcount tương đương 1000ms
    esp_log_timestamp();       giống millis   

*/


/*
    TimerHandle_t xTimerCreate (
                   const char * const pcTimerName,              // tên timer, ví dự "timer1"
                   const TickType_t xTimerPeriod,               // thời gian đếm timer , phụ thuộc vào stick rate, vào menuconfig -> RTOS -> rate, mặc đinh là 100 Hz, tương đương 10ms
                   const UBaseType_t uxAutoReload,              // có auto reload giá trị sau khi tràn timer không
                   void * const pvTimerID,                      // ID của timer
                   TimerCallbackFunction_t pxCallbackFunction   // hàm callback của timer
                   );
    Cách sử dụng 
        TimerHandle_t xTimers = NULL;
        xTimerCreate("Timer", pdTRUE, 0,TimerID, vTimerCallback);

    Sau đó gọi hàm:
    xTimerStart( TimerHandle_t xTimer,TickType_t xBlockTime ); // Hàm gọi bắt đầu start timer

    Một số hàm khác:
    xTimerStop( TimerHandle_t xTimer,TickType_t xBlockTime );   // Hàm stop timer
    xTimerDelete( TimerHandle_t xTimer,TickType_t xBlockTime ); // Hàm delete timer
    xTimerReset( TimerHandle_t xTimer,TickType_t xBlockTime );  // Hàm reset timer

    xTimerIsTimerActive( TimerHandle_t xTimer );                 // kiểm tra xem timer đã active chưa, trả về true/false
    xTimerChangePeriod( TimerHandle_t xTimer, TickType_t xNewPeriod, TickType_t xBlockTime ); // Thay đổi thời gian của timer khi timer đã start rồi
    xTimerStartFromISR( TimerHandle_t xTimer, BaseType_t *pxHigherPriorityTaskWoken);         // Start timer trong hàm ngắt
    xTimerStopFromISR(TimerHandle_t xTimer,BaseType_t *pxHigherPriorityTaskWoken);            // Stop timer trong hàm ngắt   
    xTimerChangePeriodFromISR(TimerHandle_t xTimer, TickType_t xNewPeriod, BaseType_t *pxHigherPriorityTaskWoken);  // Thay đổi thời gian của timer trong ngắt khi timer đã start rồi
    xTimerResetFromISR(TimerHandle_t xTimer,BaseType_t *pxHigherPriorityTaskWoken);            // Reset timer trong ngắt
    pvTimerGetTimerID (TimerHandle_t  xTimer )  // Lấy ID của timer
    vTimerSetReloadMode( TimerHandle_t xTimer, const UBaseType_t uxAutoReload );    // Set timer có auto reload không
    TimerSetTimerID( TimerHandle_t xTimer, void *pvNewID );                         // Set ID mới cho timer




*/
/*
    Mục đích code
        Tạo ra 2 soft timer

*/


#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "esp_log.h"

TimerHandle_t timerhandle[2];
#define TIMER1_ID     0
#define TIMER2_ID     1
static void timer_func_callback(void *timer)
{

    if(pvTimerGetTimerID(timer) == TIMER1_ID)
    {
        ESP_LOGI("timer Fn", "timer1 trig");
    }
    else if(pvTimerGetTimerID(timer) == TIMER2_ID)
    {
        ESP_LOGE("timer Fn", "timer2 trig");
    }
}

void app_main(void)
{
    // Khởi tạo timer
    timerhandle[0] = xTimerCreate("timer1",1000/portTICK_PERIOD_MS,TIMER1_ID,0, timer_func_callback );
    timerhandle[1] = xTimerCreate("timer2",1000/portTICK_PERIOD_MS,TIMER2_ID,1, timer_func_callback );

    //Start timer để bắt đầu chạy
    xTimerStart(timerhandle[0], portMAX_DELAY);
    xTimerStart(timerhandle[1], portMAX_DELAY);
}