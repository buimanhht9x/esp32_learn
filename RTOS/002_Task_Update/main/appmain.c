/*
 Tạo 2 Task
    - Task 1 : task_blink_led : blink led chu kì 1s
    - Task 2 : task_print_terminal : in ra terminal mỗi 1s

     xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , NULL);
     
     BaseType_t xTaskCreate (TaskFunction_t pvTaskCode,
        const char * const pcName,
        const uint32_t usStackDepth,
        void * const pvParameters,
        UBaseType_t uxPriority,
        TaskHandle_t * const pvCreatedTask)

        • pvTaskCode: tên Task (tên hàm) muốn chạy
        • *pcName: tên do người dùng đặt cho cái task đó.
        • usStackDepth: size hay gọi là Depth của Task, đơn vị của nó là 4 bytes.
        • *pvParameters: 1 con trỏ dùng để chứa các tham số của Task khi task còn hoạt động, thường để NULL.
        • uxPriority: độ ưu tiên của task này, số càng lớn thì độ ưu tiên càng lớn.
        • *pxCreatedTask: 1 con trỏ đại diện Task, dùng để điểu khiển task từ 1 task khác, ví dụ xóa task này từ 1 task đang
        chạy khác.
        Sau khi tạo task, ta gọi hàm vTaskStartScheduler() dùng để yêu cầu nhân (kernel) của RTOS bắt
        đầu chạy. Ta sẽ xóa task thông qua hàm xTaskDelete()


    void vTaskSuspend(TaskHandle_txTaskToSuspend)
        Suspend a task.

    void vTaskResume(TaskHandle_txTaskToResume)
        Resumes a suspended task.

    void vTaskDelete(TaskHandle_txTaskToDelete)
        Remove a task from the RTOS real time kernel’s management.

    eTaskState eTaskGetState(TaskHandle_txTask)
        Obtain the state of any task.
        eTaskState: 
            eRunning = 0
            eReady		
            eBlocked	
            eSuspended
            eDeleted
    
    TickType_t xTaskGetTickCount(void)
        Get tick count
        int c = xTaskGetTickCount();
        c = 100 tương đương 1s


*/
/*
Mục đích code;
    Tạo 2 Task, điều khiển task 2 từ task 1 (suppend, resume, delete)
    Chạy task 2 in ra monitor "hello from task 2"

    Chạy task 1 in ra monitor "hello from task 1"
    Sau 3s, suppend task2
    Sau 5s, resume task2
    Sau 8s, delete task2

*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

TaskHandle_t task1_handle = NULL;
TaskHandle_t task2_handle = NULL;
// Hàm print terminal
// Lưu ý khi dùng printf, phía sau phaior dùng fflush(stdout)
// nếu không sẽ bị reset liên tọi

int count = 0;
void task1(void *arg)
{
    while(1)
    { 
        vTaskDelay(1000/portTICK_PERIOD_MS);
        printf("hello world from task 1\n");
        fflush(stdout);

        count =  xTaskGetTickCount();

        if(count == 300)
        {
            vTaskSuspend(task2_handle);
            printf("vTaskSuspend task 2\n");
            fflush(stdout);
        }
            
        else if(count == 500)
        {
            vTaskResume(task2_handle);
            printf("vTaskResume task 2\n");
            fflush(stdout);
        }
            
        else if(count == 800)
        {
            vTaskDelete(task2_handle);
            printf("vTaskDelete task 2\n");
            fflush(stdout);
        }
            

    }
    
}

// Hàm print terminal
// Lưu ý khi dùng printf, phía sau phaior dùng fflush(stdout)
// nếu không sẽ bị reset liên tọi
void task2(void *arg)
{
    while(1)
    { 
        vTaskDelay(1000/portTICK_PERIOD_MS);
        printf("hello world from task 2\n");
        fflush(stdout);
       
    }
    
}

void app_main(void)
{

    xTaskCreate(task1, "task1",1024, NULL, 5 , &task1_handle );
    xTaskCreatePinnedToCore(task2, "task2",1024, NULL, 4 , &task2_handle, 1);


}