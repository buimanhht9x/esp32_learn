/*
 Tạo 2 Task
    - Task 1 : task_blink_led : blink led chu kì 1s
    - Task 2 : task_print_terminal : in ra terminal mỗi 1s

     xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , NULL);
     
     BaseType_t xTaskCreate (TaskFunction_t pvTaskCode,
        const char * const pcName,
        const uint32_t usStackDepth,
        void * const pvParameters,
        UBaseType_t uxPriority,
        TaskHandle_t * const pvCreatedTask)

        • pvTaskCode: tên Task (tên hàm) muốn chạy
        • *pcName: tên do người dùng đặt cho cái task đó.
        • usStackDepth: size hay gọi là Depth của Task, đơn vị của nó là 4 bytes.
        • *pvParameters: 1 con trỏ dùng để chứa các tham số của Task khi task còn hoạt động, thường để NULL.
        • uxPriority: độ ưu tiên của task này, số càng lớn thì độ ưu tiên càng lớn.
        • *pxCreatedTask: 1 con trỏ đại diện Task, dùng để điểu khiển task từ 1 task khác, ví dụ xóa task này từ 1 task đang
        chạy khác.
        Nếu là vanila FreeRtos , Sau khi tạo task, ta gọi hàm vTaskStartScheduler() dùng để yêu cầu nhân (kernel) của RTOS bắt
        đầu chạy. 
        Ta sẽ xóa task thông qua hàm xTaskDelete()


*/


#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#define LED 2

void gpio_output_create(gpio_num_t gpio_num)
{
    // chon io làm GPIO
    gpio_pad_select_gpio(gpio_num);
    // Set làm input Mode
    gpio_set_direction(gpio_num, GPIO_MODE_OUTPUT);
}

uint8_t k = 0;

// Hàm blink led
void task_blink_led(void *arg)
{
    while(1)
    { 
        k = 1 - k;
        gpio_set_level(LED, k);
        vTaskDelay(1000/portTICK_PERIOD_MS);
    }
    
}

// Hàm print terminal
// Lưu ý khi dùng printf, phía sau phaior dùng fflush(stdout)
// nếu không sẽ bị reset liên tọi
void task_print_terminal(void *arg)
{
    printf("Hello world!\n"); 
    fflush(stdout);
    while(1)
    { 
        vTaskDelay(1000/portTICK_PERIOD_MS);
        printf("hello world\n");
        fflush(stdout);
       
    }
    
}

void app_main(void)
{
    gpio_output_create(LED);
    xTaskCreate(task_blink_led     , "task_blink_led"     ,1024, NULL, 5 , NULL);
    xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , NULL);


}