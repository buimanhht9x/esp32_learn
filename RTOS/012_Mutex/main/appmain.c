/*
        • Về cơ bản thì MUTEX tương tự như Binary Semaphore nhưng có tích thêm cơ chế “kế thừa mức ưu tiên” 
        và được dùng cho mục đích hạn chế quyền truy cập vào tài nguyên của các task khác chứ không phải 
        là để đồng bộ như Semaphore.
        • Khi 1 task muốn truy cập vào tài nguyên chung để thực thi nhiệm vụ thì sẽ take Mutex. Trong
        lúc đó, bất kì task nào muốn take Mutex đều bị block cho tới khi task đang giữ Mutex “give” về
        chỗ cũ.
        • Điểm khác biệt so với Semaphore là ở chỗ ở Mutex thì task nào giữ Mutex mới được give, còn Semaphore 
        thì mọi task đều có thể give. Chính vì vậy trình phục vụ ngắt có thể give Semaphore nhưng đối với Mutex 
        thì không

        • Binary semaphore có duy nhất 1 token và nó chính là chìa khóa để sử dụng nguồn tài nguyên chung
        • 1 Task sẽ nắm giữ chìa khóa, khi nào nó dùng xong tài nguyên thì sẽ đẩy ra 1 thông báo cho task nào
        muốn sử dụng thông qua xSemaphoreGive()
        • Task nào muốn sử dụng tài nguyên chung thì phải bắt được chìa khóa, tức là nhận được thông báo từ
        task vừa give thông qua hàm xSemaphoreTake()
        • Như vậy, Binary semaphore được sử dụng như là 1 thông báo từ task này sang task kia. Đây chính là
        sự khác biệt cơ bản của Semaphore so với mutex.

        SemaphoreHandle_t xSemaphoreCreateMutex( void );   // Khởi tạo mutex
        VD: SemaphoreHandle_t  mutex1;
            mutex1 = xSemaphoreCreateMutex();



        xSemaphoreTake( SemaphoreHandle_t xSemaphore,      // Hàm lấy key, return 1 nếu lấy thành công
                            TickType_t xTicksToWait );
        
        xSemaphoreTakeFromISR(SemaphoreHandle_t xSemaphore,signed BaseType_t *pxHigherPriorityTaskWoken); 



        SemaphoreHandle_t xSemaphoreCreateRecursiveMutex( void ) ;                       // khởi tạo mutex đệ quy
        xSemaphoreTakeRecursive( SemaphoreHandle_t xMutex, TickType_t xTicksToWait );    // lấy key từ mutex đệ quy, nếu take 5 lần thì phải give lại 5 lần
        xSemaphoreGiveRecursive( SemaphoreHandle_t xMutex ) ;                            // give lại key cho mutex đệ quy


*/

/*
    Mục đích code
        Tạo ra 3 task , task1 3s , task2 1s, task3 200ms
        Tạo ra 1 biến toàn cục, tạo 1 mutex

        task1, task2, task3 sẽ lần lượt lấy mutex, và tăng biến count lên 1 đơn vị sau mỗi lần lấy mutex 

        monitor sẽ in ra với task1 core0, task2 task3 core1
        
        0 : count task 1 :1
        0 : count task 2 :2
        0 : count task 3 :3
        210 : count task 3 :4
        410 : count task 3 :5
        610 : count task 3 :6
        810 : count task 3 :7
        1000 : count task 2 :8
        1010 : count task 3 :9
        1210 : count task 3 :10
        1410 : count task 3 :11
        1610 : count task 3 :12
        1810 : count task 3 :13
        2000 : count task 2 :14
        2010 : count task 3 :15
        2210 : count task 3 :16
        2410 : count task 3 :17
        2610 : count task 3 :18
        2810 : count task 3 :19
        3000 : count task 1 :20
        3000 : count task 2 :21
        3010 : count task 3 :22
        3210 : count task 3 :23
        3410 : count task 3 :24
        3610 : count task 3 :25
        3810 : count task 3 :26
        4000 : count task 2 :27
        4010 : count task 3 :28
        4210 : count task 3 :29

*/

#include <stdio.h>
#include "stdlib.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "esp_log.h"

TaskHandle_t task_handle_task1 = NULL;
TaskHandle_t task_handle_task2 = NULL;
SemaphoreHandle_t mutex1;
int global_variable = 0;

void task1()
{
    // Khởi tạo mutex
    
    int local1_variable = 0;
    while (1)
    {
        if(xSemaphoreTake(mutex1,0) == pdTRUE)
        {
            local1_variable = global_variable ;
            local1_variable ++;
            
            global_variable = local1_variable;
            printf("%d : count task 1 :%d\n",xTaskGetTickCount(),local1_variable);
            fflush(stdout);
            xSemaphoreGive(mutex1);
            vTaskDelay(3000/portTICK_PERIOD_MS); // phải để delay sau give, để task khác có thể lấy được mutex
        }
    }
}



void task2()
{
    int local2_variable = 0;
    while (1)
    {
        if(xSemaphoreTake(mutex1, 0) == 1)
        {
            local2_variable = global_variable ;
            local2_variable ++;
            global_variable = local2_variable;
             printf("%d : count task 2 :%d\n",xTaskGetTickCount(),local2_variable);
            xSemaphoreGive(mutex1);
            vTaskDelay(1000/portTICK_PERIOD_MS);
        }
    }
}

void task3()
{
    int local3_variable = 0;
    while (1)
    {
        if(xSemaphoreTake(mutex1, 0) == 1)
        {
            local3_variable = global_variable ;
            local3_variable ++; 
            global_variable = local3_variable;
            printf("%d : count task 3 :%d\n",xTaskGetTickCount(),local3_variable);
            xSemaphoreGive(mutex1);
            vTaskDelay(200/portTICK_PERIOD_MS);
        }
    }
}

void app_main(void)
{
    mutex1 = xSemaphoreCreateMutex();
    xTaskCreate(task1, "task1", 10000, NULL, 10 , NULL);
    xTaskCreatePinnedToCore(task2, "task2", 10000, NULL, 5 , NULL,0);
    xTaskCreatePinnedToCore(task3, "task3", 10000, NULL, 15 , NULL,0);
}