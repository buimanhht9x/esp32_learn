/*
 Tạo 2 Task
    - Task 1 : task_blink_led : blink led chu kì 1s
    - Task 2 : task_print_terminal : in ra terminal mỗi 1s

     xTaskCreate(task_print_terminal, "task_print_terminal",1024, NULL, 4 , NULL);

     BaseType_t xTaskCreate (TaskFunction_t pvTaskCode,
        const char * const pcName,
        const uint32_t usStackDepth,
        void * const pvParameters,
        UBaseType_t uxPriority,
        TaskHandle_t * const pvCreatedTask)

        • pvTaskCode: tên Task (tên hàm) muốn chạy
        • *pcName: tên do người dùng đặt cho cái task đó.
        • usStackDepth: size hay gọi là Depth của Task, đơn vị của nó là 4 bytes.
        • *pvParameters: 1 con trỏ dùng để chứa các tham số của Task khi task còn hoạt động, thường để NULL.
        • uxPriority: độ ưu tiên của task này, số càng lớn thì độ ưu tiên càng lớn.
        • *pxCreatedTask: 1 con trỏ đại diện Task, dùng để điểu khiển task từ 1 task khác, ví dụ xóa task này từ 1 task đang
        chạy khác.
        Sau khi tạo task, ta gọi hàm vTaskStartScheduler() dùng để yêu cầu nhân (kernel) của RTOS bắt
        đầu chạy. Ta sẽ xóa task thông qua hàm xTaskDelete()


    void vTaskSuspend(TaskHandle_txTaskToSuspend)
        Suspend a task.

    void vTaskResume(TaskHandle_txTaskToResume)
        Resumes a suspended task.

    void vTaskDelete(TaskHandle_txTaskToDelete)
        Remove a task from the RTOS real time kernel’s management.

    eTaskState eTaskGetState(TaskHandle_txTask)
        Obtain the state of any task.
        eTaskState:
            eRunning = 0
            eReady
            eBlocked
            eSuspended
            eDeleted

    TickType_t xTaskGetTickCount(void)
        Get tick count
        int c = xTaskGetTickCount();
        c = 100 tương đương 1s

    BaseType_t xTaskResumeFromISR(TaskHandle_t xTaskToResume)
        An implementation of vTaskResume() that can be called from within an ISR.
*/


/*

Queue: Dùng để gửi dữ liệu từ task này sang task khác

Mục đích code;
    Tạo 2 Task, task 2 mỗi 1s đọc queue 1 lần

    Task 1 : khởi tạo 1 queue
    QueueHandle_t queuehandle_task1 = NULL;

    tạo queue với 5 ô
    queuehandle_task1 = xQueueCreate(5, sizeof(txBuff));

    txBuf
    cách thêm vào queue
    xQueueSend(queuehandle_task1, txBuff, 0); trong đó txBuff là địa chỉ, not giá trị
    xQueueOverwrite(queuehandle_task1,txBuf) ; ghi đè


    xQueueSendToFront(queuehandle_task1, txBuff, 0); sẽ thêm data vào đầu queue
    xQueueSendToBack(queuehandle_task1, txBuff, 0); sẽ thêm data vào cuối queue
    queue sẽ thêm theo thứ tự
      |   task1 hello 1    |  |   task1 hello 1    |  |   task1 hello 1    |
      |                    |  |   task1 hello 2    |  |   task1 hello 2    |
      |                    |  |                    |  |   task1 hello 3    |
      |                    |  |                    |
      |                    |  |                    |
    trong while(1), in ra số data đang đợi trong queue, và số ô còn trống trong queue bằng hàm:
    uxQueueMessagesWaiting(queuehandle_task1) : trả về số data đợi trong queue
    uxQueueSpacesAvailable(queuehandle_task1) : trả về số ô trống chưa có data trong queue

    Task2 : Tạo 1 buff để nhận data trong queue
    rxBuff
    mỗi 1s sẽ lấy data trong queue ra để in lên monitor
    kiểm tra  xQueueReceive(queuehandle_task1,(void *)&rxBuff, (TickType_t) 5) > 0 thì trong queue còn data
    Nếu xQueueReceive >0 , tức là trong queue còn data

    xQueuePeek(queuehandle_task1,&rxBuff, 5), sẽ đọc data đầu tiên trong queue mà không xóa data trong queue, data trong queue nguyên vẹn

    queue lấy ra theo thứ tự
     |   task1 hello 1    |             |   task1 hello 2    |           |   task1 hello 3    |       |                    |
     |   task1 hello 2    |             |   task1 hello 3    |           |                    |       |                    |
     |   task1 hello 3    |             |                    |           |                    |       |                    |
    para1: tên Queue
    para2: bufer, ở đây xem giá trị lưu trong rxBuff nên &rxBuff

*/

/*
    Semaphore dùng để truyền notify, Truyền flag từ task này sang task khác
     Binary semaphore có duy nhất 1 token và nó chính là chìa khóa để sử dụng nguồn tài nguyên chung
    • 1 Task sẽ nắm giữ chìa khóa, khi nào nó dùng xong tài nguyên thì sẽ đẩy ra 1 thông báo cho task nào
    muốn sử dụng thông qua xSemaphoreGive()
    • Task nào muốn sử dụng tài nguyên chung thì phải bắt được chìa khóa, tức là nhận được thông báo từ
    task vừa give thông qua hàm xSemaphoreTake()

    Các hàm cơ bản thường dùng với Semaphore:
    • Khởi tạo 1 binary semaphore
        SemaphoreHandle_t xSemaphore = xSemaphoreCreateBinary();
    • Khởi tạo 1 couting semaphore với 2 tham số truyền vào là số lượng max và số khởi đầu
        xSemaphoreCreateCounting( uxMaxCount, uxInitialCount )
    • Trả “chìa khóa “ với tham số truyền vào là Semaphore đã khởi tạo
        xSemaphoreGive(SemaphoreHandle_t xSemaphore)
    • Lấy “chìa khóa “ với tham số truyền vào là Semaphore đã khởi tạo
        xSemaphoreTake(SemaphoreHandle_t xSemaphore)
    • Trả “chìa khóa “ qua ISR, tham số truyền vào là Semaphore đã khởi tạo và
        xSemaphoreGiveFromISR( xSemaphore, pxHigherPriorityTaskWoken )

*/

/*
Tạo ngắt nút nhấn, khi có ngắt, sẽ gửi semaphore báo cho task2 biết
*/
#include "stdint.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"

#define btn   0
#define led   2

int led_state = 0;

TaskHandle_t taskhandle_task1 = NULL;
TaskHandle_t taskhandle_task2 = NULL;

SemaphoreHandle_t semaphorehandle_task1 = NULL;



void delay(uint32_t millis)
{
    vTaskDelay(millis/portTICK_PERIOD_MS);
}

void IRAM_ATTR gpio_input_isr_handler(void* arg)
{
    xSemaphoreGiveFromISR(semaphorehandle_task1,NULL);
}

void task1()
{
 
    while(1)
    {

    }
}

void task2()
{

    while(1)
    {
       if(xSemaphoreTake(semaphorehandle_task1, portMAX_DELAY))
       {
            printf("recieve flag semaphore from task 1'\n");
            fflush(stdout);
       }
    }
}

void app_main(void)
{

    // Khoi tao ngat button input
    gpio_pad_select_gpio(btn);
    gpio_set_direction(btn, GPIO_MODE_INPUT);
    gpio_set_pull_mode(btn, GPIO_PULLUP_ENABLE);
    // Ngắt nhận sườn cao xuống thấp
    gpio_set_intr_type(btn, GPIO_INTR_NEGEDGE);

    // install ngắt gpio
    gpio_install_isr_service(0);
    // thêm btn vào trình ngắt gpio
    gpio_isr_handler_add(btn, gpio_input_isr_handler, NULL);

    // LED
    gpio_pad_select_gpio(led);
    gpio_set_direction(led, GPIO_MODE_OUTPUT);

    // Khởi tạo semaphore binary
    semaphorehandle_task1 = xSemaphoreCreateBinary();
    // Khởi tạo mặc định task1 chạy core 0
    xTaskCreate(task1,"task1",4096,NULL,10, &taskhandle_task1);

    // Khởi tạo task2 chạy core 1
    xTaskCreatePinnedToCore(task2,"task2",4096,NULL,10, &taskhandle_task2,1);

}