/*
    Event Bits (Event Flags): Các bit sự kiện được sử dụng để cho biết liệu một sự kiện có xảy ra hay không
    Group Bits  : Là tập hợp của 24 Event Bits

    Các nhóm sự kiện cũng có thể được sử dụng để đồng bộ hóa các tác vụ, tạo ra thứ thường được gọi là 'điểm hẹn' 
    của tác vụ. Điểm đồng bộ hóa tác vụ là một vị trí trong mã ứng dụng mà tại đó tác vụ sẽ đợi ở trạng thái Bị chặn 
    (không tiêu tốn bất kỳ thời gian nào của CPU) cho đến khi tất cả các tác vụ khác tham gia đồng bộ hóa cũng đạt đến
    điểm đồng bộ hóa của chúng.

    Trong Group bit, cho phép kiểm tra 1 bit, hoặc nếu tất cả các bit trong group đều được set thì mới cho sự kiện tiếp theo xảy ra

    Ưu điểm:
        Việc triển khai nhóm sự kiện sẽ kiểm soát được sự kiện nếu:
            Không rõ ai chịu trách nhiệm xóa các bit (hoặc cờ) riêng lẻ.
            Không rõ khi nào một bit sẽ bị xóa.
            Không rõ liệu một bit đã được đặt hay xóa tại thời điểm một tác vụ thoát khỏi chức năng API đã kiểm tra giá trị của bit đó (có thể một tác vụ hoặc ngắt khác đã thay đổi trạng thái của bit).
  
    Thư viện 
    #include "event_groups.h"

    //  Declare a variable to hold the created event group. 
    EventGroupHandle_t xCreatedEventGroup;

    // Attempt to create the event group. 
    xCreatedEventGroup = xEventGroupCreate();

    // Was the event group created successfully? 
    if( xCreatedEventGroup == NULL )
    {
        // The event group was not created because there was insufficient FreeRTOS heap available. 
    }
    else
    {
       //  The event group was created. 
    }

    // Các hàm chính
     EventGroupHandle_t event_group_1;              
     event_group_1 = xEventGroupCreate( void );     // Tạo 1 event gr

     void vEventGroupDelete( EventGroupHandle_t xEventGroup ); // xoá event gr

    EventBits_t xEventGroupWaitBits(
                       const EventGroupHandle_t xEventGroup,
                       const EventBits_t uxBitsToWaitFor,
                       const BaseType_t xClearOnExit,
                       const BaseType_t xWaitForAllBits,
                       TickType_t xTicksToWait );
    Đọc các bit trong một nhóm sự kiện RTOS , tùy ý vào trạng thái Bị chặn (có thời gian chờ) để đợi một bit hoặc nhóm bit được thiết lập.
     
    Ví dụ:
     #define BIT_0 ( 1 << 0 )
     #define BIT_4 ( 1 << 4 )
     EventBits_t uxBits;
     TickType_t xTicksToWait = 100 / portTICK_PERIOD_MS;
     uxBits = xEventGroupWaitBits(
            xEventGroup,     Nhóm sự kiện đang được thử nghiệm. 
            BIT_0 | BIT_4,   Các bit trong nhóm sự kiện cần chờ. 
            pdTRUE,          BIT_0 & BIT_4 phải được xóa trước khi quay lại. 
            pdFALSE,         false: đợi 1 bit được set, true: đợi tất cả các bit trong nhóm sự kiện được set
            xTicksToWait );  Đợi tối đa 100 mili giây để thiết lập một trong hai bit.
    
            if  ( ( uxBits & ( BIT_0 | BIT_4 ) ) == ( BIT_0 | BIT_4 ) )
            {
                 xEventGroupWaitBits() được trả về vì cả hai bit đã được đặt. 
            }
            else if ( ( uxBits & BIT_0 ) != 0 )
            {
                 xEventGroupWaitBits() được trả về vì chỉ BIT_0 được đặt. 
            }
            else if ( ( uxBits & BIT_4 ) != 0 )
            {
                 xEventGroupWaitBits() được trả về vì chỉ BIT_4 được đặt. 
            }
            else 
            {
                 xEventGroupWaitBits() được trả về vì xTicksToWait đã vượt qua
                mà không đặt BIT_0 hoặc BIT_4. 
            }
    EventBits_t xEventGroupSetBits( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToSet );
            Đặt bit (cờ) trong nhóm sự kiện RTOS . Chức năng này không thể được gọi từ một ngắt. Ngắt dùng xEventGroupSetBitsFromISR() 
    EventBits_t xEventGroupClearBits(EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToClear );
            Xóa các bit (cờ) trong một nhóm sự kiện RTOS . Chức năng này không thể được gọi từ một ngắt. Ngắt dùng  xEventGroupClearBitsFromISR() 
    EventBits_t xEventGroupGetBits( EventGroupHandle_t xEventGroup );
            Trả về giá trị hiện tại của bit sự kiện (cờ sự kiện) trong nhóm sự kiện RTOS . Chức năng này không thể được sử dụng từ một ngắt. Ngắt dùng xEventGroupGetBitsFromISR() 

    EventBits_t xEventGroupSync( EventGroupHandle_t xEventGroup,
                              const EventBits_t uxBitsToSet,                // Bit hoặc nhóm bit sẽ được SET
                              const EventBits_t uxBitsToWaitFor,            // Bit Nhóm bit đợi để so sánh
                              TickType_t xTicksToWait );                    // Thời gian đợi
            Chức năng này thường được sử dụng để đồng bộ hóa nhiều tác vụ (thường được gọi là điểm hẹn nhiệm vụ), 
            trong đó mỗi tác vụ phải đợi các tác vụ khác đạt đến điểm đồng bộ hóa trước khi tiếp tục.
    Code mẫu

        Số bit được sử dụng bởi ba tác vụ. 
        #define TASK_0_BIT        ( 1 << 0 )
        #define TASK_1_BIT        ( 1 << 1 )
        #define TASK_2_BIT        ( 1 << 2 )

        #define ALL_SYNC_BITS ( TASK_0_BIT | TASK_1_BIT | TASK_2_BIT )

        // Sử dụng một nhóm sự kiện để đồng bộ ba tác vụ. Người ta cho rằng sự kiện này
        nhóm đã được tạo ở nơi khác. 
        EventGroupHandle_t xEventBits;

        void vTask0( void *pvParameters )
        {
        EventBits_t uxReturn;
        TickType_t xTicksToWait = 100 / cổngTICK_PERIOD_MS;

            while( ;; )
            {

                Đặt bit 0 trong nhóm sự kiện để lưu ý tác vụ này đã đạt đến
                điểm đồng bộ. Hai nhiệm vụ khác sẽ thiết lập hai bit khác được xác định
                bởi ALL_SYNC_BITS. Cả ba nhiệm vụ đã đạt đến sự đồng bộ hóa
                điểm khi tất cả ALL_SYNC_BITS được đặt. Chờ tối đa 100ms
                để điều này xảy ra. 

                uxReturn = xEventGroupSync( xEventBits,
                                            TASK_0_BIT,
                                            ALL_SYNC_BITS,
                                            xTicksToWait );

                if ( ( uxReturn & ALL_SYNC_BITS ) == ALL_SYNC_BITS )
                {
                    Cả ba tác vụ đều đạt đến điểm đồng bộ hóa trước cuộc gọi
                    đến xEventGroupSync() đã hết thời gian chờ. 
                }
            }
        }

        void vTask1(void *pvParameter)
        {
            vì( ;; )
            {

                Đặt bit 1 trong nhóm sự kiện để lưu ý rằng nhiệm vụ này đã đạt đến
                điểm đồng bộ hóa. Hai nhiệm vụ khác sẽ đặt hai nhiệm vụ khác
                bit được xác định bởi ALL_SYNC_BITS. Cả ba nhiệm vụ đều đạt
                điểm đồng bộ hóa khi tất cả ALL_SYNC_BITS được đặt. Chờ đợi
                vô thời hạn để điều này xảy ra. 

                xEventGroupSync( xEventBits, TASK_1_BIT, ALL_SYNC_BITS, portMAX_DELAY );

                xEventGroupSync() được gọi với thời gian chặn không xác định, vì vậy
                tác vụ này sẽ chỉ đến đây nếu việc đồng bộ hóa được thực hiện bởi tất cả
                ba tác vụ nên không cần kiểm tra giá trị trả về. 
            }
        }

        void vTask2(void *pvParameter)
        {
            while( ;; )
            {

                Đặt bit 2 trong nhóm sự kiện để lưu ý tác vụ này đã đạt đến
                điểm đồng bộ hóa. Hai nhiệm vụ khác sẽ đặt hai nhiệm vụ khác
                bit được xác định bởi ALL_SYNC_BITS. Cả ba nhiệm vụ đều đạt
                điểm đồng bộ hóa khi tất cả ALL_SYNC_BITS được đặt. Chờ đợi
                vô thời hạn để điều này xảy ra. 
                xEventGroupSync( xEventBits, TASK_2_BIT, ALL_SYNC_BITS, portMAX_DELAY );

                xEventGroupSync() được gọi với thời gian chặn không xác định, vì vậy
                tác vụ này sẽ chỉ đến đây nếu việc đồng bộ hóa được thực hiện bởi tất cả
                ba tác vụ nên không cần kiểm tra giá trị trả về. 
            }
        }

*/

/*
    Mục đích code
        Tạo ra event_group_a với 3 eventbit
        Tạo 3 task
        
        Task1 mỗi 5s sẽ set bit3 
        Task3 mỗi 3s sẽ set bit1 
        timer1_callback() sẽ set bit2 mỗi 10s

        task2 sẽ nhận bit, xem bit nào đc set thì in ra monitor, sau đó clear bit đi

        evBit = xEventGroupWaitBits(event_group_a,
                            EV_BIT1|EV_BIT2|EV_BIT3,
                            1,
                            1, // Bit này là đợi cả 3 bit cùng được set
                            portMAX_DELAY); 
 */
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/timers.h"

TimerHandle_t timer1 = NULL;

EventGroupHandle_t   event_group_a;
TaskHandle_t task_handle_task1 = NULL;
TaskHandle_t task_handle_task2 = NULL;
#define EV_BIT1  (1 << 0)
#define EV_BIT2  (1 << 1)
#define EV_BIT3  (1 << 2)

void task3()
{
    while(1)
    {
        xEventGroupSetBits(event_group_a,EV_BIT1);
        vTaskDelay(3000/portTICK_PERIOD_MS);
    }
}
void task1()
{
    while(1)
    {
        xEventGroupSetBits(event_group_a,EV_BIT3);
        vTaskDelay(5000/portTICK_PERIOD_MS);
    }
}

void task2()
{
    while(1)
    {
        EventBits_t  evBit = 0;
        evBit = xEventGroupWaitBits(event_group_a,
                                    EV_BIT1|EV_BIT2|EV_BIT3,
                                    1,
                                    1,
                                    portMAX_DELAY);
        if((evBit & EV_BIT1) ==  EV_BIT1)
        {
            ESP_LOGI("task2", "bit1 trigged")  ;
        }
        else if((evBit & EV_BIT3) ==  EV_BIT3)
        {
            ESP_LOGE("task2", "bit3 trigged")  ;
        }
        else if((evBit & EV_BIT2) ==  EV_BIT2)
        {
            ESP_LOGW("task2", "bit2 trigged")  ;
        }

        if((evBit & (EV_BIT1 | EV_BIT2 | EV_BIT3)) ==  (EV_BIT1 | EV_BIT2 | EV_BIT3))
        {
            ESP_LOGI("task2", "3 bit trigged")  ;
        }
        
    }
}

void timer1_callback()
{
      xEventGroupSetBits(event_group_a,EV_BIT2);
}

void app_main(void)
{
   
   timer1 = xTimerCreate("timer1", 10000/ portTICK_PERIOD_MS,1,0, timer1_callback);
   xTimerStart(timer1, portMAX_DELAY);
   event_group_a = xEventGroupCreate();

   xTaskCreate(task1, "task1", 2048, NULL, 10, task_handle_task1);
   xTaskCreate(task3, "task3", 2048, NULL, 10, NULL);
   xTaskCreatePinnedToCore(task2, "task2", 2048, NULL, 10, task_handle_task2, 1);

}