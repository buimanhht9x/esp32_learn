#ifndef _WIFI_SOFTAP_CAPTIVE_PORTAL_H
#define _WIFI_SOFTAP_CAPTIVE_PORTAL_H
#include <sys/param.h>

#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"

#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_netif.h"
#include "lwip/inet.h"

#include "esp_http_server.h"
#include "dns_server.h"

/*
//B0 Khai báo 1 file index.html để portal_captive trả về
//B1............Khai báo wifi_config_t ..................
#define WIFI_SSID  "esp32"
#define WIFI_PASS  "12345678"
#define MAX_STA_CONN  5

wifi_config_t wifi_config_init  = {
    .ap = {
        .ssid = WIFI_SSID,
        .ssid_len = strlen(WIFI_SSID),
        .password = WIFI_PASS,
        .max_connection = MAX_STA_CONN,
        .authmode = WIFI_AUTH_WPA_WPA2_PSK
    },
};

//B2............Gọi hàm ..................
wifi_softap_captive_portal_init(wifi_config_init);
wifi_softap_captive_portal_start();

*/
void wifi_softap_captive_portal_init(wifi_config_t wifi_config);
void wifi_softap_captive_portal_start(void);


#endif