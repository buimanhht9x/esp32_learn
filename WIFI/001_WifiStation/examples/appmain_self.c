

/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

/* The examples use WiFi configuration that you can set via project configuration menu

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/

#define SSID      "Tplink 2.4 ghz"
#define PASS      "hoianhson"
#define MAXIMUM_RETRY  10


/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT   BIT0
#define WIFI_FAIL_BIT        BIT1

static const char *TAG = "wifi station";

static int s_retry_num = 0;
EventGroupHandle_t eventGroupWifi;
void event_handler(void* arg,esp_event_base_t event_base,int32_t event_id,void* event_data)
{
    if(event_base == WIFI_EVENT &&  event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if(s_retry_num < MAXIMUM_RETRY)
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry connnect %d", s_retry_num);
        }
        else
        {
            xEventGroupSetBits(eventGroupWifi, WIFI_FAIL_BIT);
            ESP_LOGI(TAG, "cant connect");
        }

        
    }
    else if(event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        xEventGroupSetBits(eventGroupWifi, WIFI_CONNECTED_BIT);
        s_retry_num = 0;
    }

}
void wifi_init_sta(void)
{

//..................................  Phase 1: Wi-Fi/LwIP Init Phase...................................................
//  s1.1: The main task calls esp_netif_init() to create an LwIP core task and initialize LwIP-related work.
    ESP_ERROR_CHECK(esp_netif_init());
//  s1.2: The main task calls esp_event_loop_init() to create a system Event task and initialize an application event’s callback function. In the scenario above, the application event’s callback function does nothing but relaying the event to the application task.
    ESP_ERROR_CHECK(esp_event_loop_create_default());
//  s1.3: The main task calls esp_netif_create_default_wifi_ap() or esp_netif_create_default_wifi_sta() to create default network interface instance binding station or AP with TCP/IP stack.
    esp_netif_create_default_wifi_sta();
//  s1.4: The main task calls esp_wifi_init() to create the Wi-Fi driver task and initialize the Wi-Fi driver.
    wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));
//  s1.5: The main task calls OS API to create the application task

/*.................................. Phase 2: Wi-Fi Configuration Phase..............................................
   Once the Wi-Fi driver is initialized, you can start configuring the Wi-Fi driver. In this scenario, the mode
   is Station, so you may need to call esp_wifi_set_mode(WIFI_MODE_STA) to configure the Wi-Fi mode as Station. 
   You can call other esp_wifi_set_xxx APIs to configure more settings, such as the protocol mode, country code, 
   bandwidth, etc  */

    

    esp_event_handler_register( WIFI_EVENT, // trong file esp_event_base.h
                                ESP_EVENT_ANY_ID,
                                event_handler,
                                NULL);
    esp_event_handler_register( IP_EVENT,
                                ESP_EVENT_ANY_ID,
                                event_handler,
                                NULL);

    eventGroupWifi =  xEventGroupCreate();

    // cấu hình wifi, nếu không thì phải cấu hình trong menuconfig
    wifi_config_t wifi_config = {
            .sta = {
                 .ssid = SSID,
                 .password = PASS,
                 .threshold.authmode = WIFI_AUTH_WPA2_PSK
            },
    };

    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );

// ..................................Phase 3: Wi-Fi Start Phase......................................................
//  s3.1: Call esp_wifi_start to start the Wi-Fi driver.
    ESP_ERROR_CHECK(esp_wifi_start());

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
   
    EventBits_t  ebit = xEventGroupWaitBits(eventGroupWifi,
                        WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                        pdFALSE,
                        pdFALSE,
                        portMAX_DELAY);
    

//  s3.2: The Wi-Fi driver posts <WIFI_EVENT_STA_START> to the event task; then, the event task will do some common 
//  things and will call the application event callback function.

//  s3.3: The application event callback function relays the <WIFI_EVENT_STA_START> to the application task. 
//  We recommend that you call esp_wifi_connect(). However, you can also call esp_wifi_connect() in other phrases 
//  after the <WIFI_EVENT_STA_START> arises.


    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if(ebit & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(TAG,"connect success");
    }
    else if (ebit & WIFI_FAIL_BIT)
    {
        ESP_LOGI(TAG,"connect fail");
    } 
    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT,ESP_EVENT_ANY_ID, event_handler));
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT  ,ESP_EVENT_ANY_ID, event_handler));
    vEventGroupDelete(eventGroupWifi);
}

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    wifi_init_sta();
}
