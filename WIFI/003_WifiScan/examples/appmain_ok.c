 /*
 *                            default handler              user handler
 *  -------------             ---------------             ---------------
 *  |           |   event     |             | callback or |             |
 *  |   tcpip   | --------->  |    event    | ----------> | application |
 *  |   stack   |             |     task    |  event      |    task     |
 *  |-----------|             |-------------|             |-------------|
 *                                  /|\                          |
 *                                   |                           |
 *                            event  |                           |
 *                                   |                           |
 *                                   |                           |
 *                             ---------------                   |
 *                             |             |                   |
 *                             | WiFi Driver |/__________________|
 *                             |             |\     API call
 *                             |             |
 *                             |-------------|
 *
 */


#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event_loop.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"

//  A constant MAXIMUM_AP is defined with a value of 20, representing the maximum 
//  number of access points that will be stored in the wifi_records array.
#define MAXIMUM_AP 20

// Kiểu bảo mật dữ liệu
/*
typedef enum {
    WIFI_AUTH_OPEN = 0,          authenticate mode : open 
    WIFI_AUTH_WEP,               authenticate mode : WEP 
    WIFI_AUTH_WPA_PSK,           authenticate mode : WPA_PSK 
    WIFI_AUTH_WPA2_PSK,          authenticate mode : WPA2_PSK 
    WIFI_AUTH_WPA_WPA2_PSK,      authenticate mode : WPA_WPA2_PSK 
    WIFI_AUTH_WPA2_ENTERPRISE,   authenticate mode : WPA2_ENTERPRISE 
    WIFI_AUTH_WPA3_PSK,          authenticate mode : WPA3_PSK 
    WIFI_AUTH_WPA2_WPA3_PSK,     authenticate mode : WPA2_WPA3_PSK 
    WIFI_AUTH_MAX
} wifi_auth_mode_t;
*/
static char *auth_mode_type(wifi_auth_mode_t auth_mode)
{
    char *types[] = {"OPEN", "WEP", "WPA PSK", "WPA2 PSK", "WPA WPA2 PSK", "WPA3_PSK","WPA2_WPA3_PSK","MAX"};
    return types[auth_mode];
}

// Khởi tạo wifi chế độ station
void wifiInit() {

    // Khởi tạo bộ nhớ cố định để lưu các giá trị cài đặt cố định cho wifi
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // Khởi tạo TCP/IP adapter stack
    tcpip_adapter_init();

    // Khởi tạo vòng lặp sự kiện để xử lý các sự kiện Wi-Fi
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // init wifi
    wifi_init_config_t wifi_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_config));

    // Khởi tạo wifi chế độ station
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    // Start wifi
    ESP_ERROR_CHECK(esp_wifi_start());

}

void wifiScan()
{
    // Khởi tạo wifi scan
    wifi_scan_config_t scan_config = {
    .ssid = 0,
    .bssid = 0,
    .channel = 0,
    .show_hidden = true
    };

    // start wifi scan
    ESP_ERROR_CHECK(esp_wifi_scan_start(&scan_config, true));

    // khởi ta
    wifi_ap_record_t wifi_records[MAXIMUM_AP];

    // Số wifi scan được
    uint16_t max_records = MAXIMUM_AP;

    // Lưu giá trị scan được vào wifi_records
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&max_records, wifi_records));

    // In ra màn hình
    printf("Number of Access Points Found: %d\n", max_records);
    printf("\n");
    printf("               SSID              | Channel | RSSI |   Authentication Mode \n");
    printf("***************************************************************\n");
    for (int i = 0; i < max_records; i++)
      printf("%32s | %7d | %4d | %12s\n", (char *)wifi_records[i].ssid, wifi_records[i].primary, wifi_records[i].rssi, auth_mode_type(wifi_records[i].authmode));
    
    printf("***************************************************************\n");
}

void app_main()
{
    wifiInit();
    wifiScan();
}
