 /*
 *                            default handler              user handler
 *  -------------             ---------------             ---------------
 *  |           |   event     |             | callback or |             |
 *  |   tcpip   | --------->  |    event    | ----------> | application |
 *  |   stack   |             |     task    |  event      |    task     |
 *  |-----------|             |-------------|             |-------------|
 *                                  /|\                          |
 *                                   |                           |
 *                            event  |                           |
 *                                   |                           |
 *                                   |                           |
 *                             ---------------                   |
 *                             |             |                   |
 *                             | WiFi Driver |/__________________|
 *                             |             |\     API call
 *                             |             |
 *                             |-------------|
 *
 */


#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event_loop.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"

//  A constant MAXIMUM_AP is defined with a value of 20, representing the maximum 
//  number of access points that will be stored in the wifi_records array.


// Kiểu bảo mật dữ liệu
/*
typedef enum {
    WIFI_AUTH_OPEN = 0,          authenticate mode : open 
    WIFI_AUTH_WEP,               authenticate mode : WEP 
    WIFI_AUTH_WPA_PSK,           authenticate mode : WPA_PSK 
    WIFI_AUTH_WPA2_PSK,          authenticate mode : WPA2_PSK 
    WIFI_AUTH_WPA_WPA2_PSK,      authenticate mode : WPA_WPA2_PSK 
    WIFI_AUTH_WPA2_ENTERPRISE,   authenticate mode : WPA2_ENTERPRISE 
    WIFI_AUTH_WPA3_PSK,          authenticate mode : WPA3_PSK 
    WIFI_AUTH_WPA2_WPA3_PSK,     authenticate mode : WPA2_WPA3_PSK 
    WIFI_AUTH_MAX
} wifi_auth_mode_t;
*/
char  *check_auth_mode(wifi_auth_mode_t wifi_auth_mode)
{
    char *buff[] = {"OPEN", "WEP","WPA_PSK","WPA2_PSK","WPA_WPA2_PSK","WPA2_ENTERPRISE","WPA3_PSK","WPA2_WPA3_PSK","MAX"};
    return buff[wifi_auth_mode];
}

// Khởi tạo wifi chế độ station
void wifiInit() 
{
    // khởi tạo nvs
    esp_err_t ret = nvs_flash_init();
    if(ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // Khoi tạo tcp adapter
    tcpip_adapter_init();

    // Khởi tạo event loop cho wifi
    ESP_ERROR_CHECK(esp_event_loop_create_default());


    wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_start());
}

void wifiScan()
{   
    // khởi tạo wifi scan
    wifi_scan_config_t wifi_scan = {
        .ssid = 0,
        .bssid = 0,
        .channel = 0,
        .show_hidden = 1
    };
    // start wifi scan 
    esp_wifi_scan_start(&wifi_scan, true);
    // khởi tạo buff lưu wifi ap scan
    wifi_ap_record_t wifi_ap_record[20];
    // phải để wifi_count = 20; để = 0 thì k quét đc
    uint16_t wifi_count = 20;

    // Quets wifi
    esp_wifi_scan_get_ap_records(&wifi_count,wifi_ap_record);

    // in ra màn hình
    printf("found %d wifi around \n", wifi_count );
    printf("               SSID              | Channel | RSSI |   Authentication Mode \n");
    printf("***************************************************************\n");
    for (int i = 0; i < wifi_count; i++)
      printf("%32s | %7d | %4d | %12s\n", (char *)wifi_ap_record[i].ssid, wifi_ap_record[i].primary, wifi_ap_record[i].rssi, check_auth_mode(wifi_ap_record[i].authmode));
    
    printf("***************************************************************\n");
}

void app_main()
{
    wifiInit();
   
    while (1)
    {
        wifiScan();
        vTaskDelay(2000/portTICK_PERIOD_MS);
    } 
}

