 /*
 *                            default handler              user handler
 *  -------------             ---------------             ---------------
 *  |           |   event     |             | callback or |             |
 *  |   tcpip   | --------->  |    event    | ----------> | application |
 *  |   stack   |             |     task    |  event      |    task     |
 *  |-----------|             |-------------|             |-------------|
 *                                  /|\                          |
 *                                   |                           |
 *                            event  |                           |
 *                                   |                           |
 *                                   |                           |
 *                             ---------------                   |
 *                             |             |                   |
 *                             | WiFi Driver |/__________________|
 *                             |             |\     API call
 *                             |             |
 *                             |-------------|
 *
 */

/* Esptouch example
    các bước :
        B1: Khởi tạo wifi ở chế độ station, đăng kí các event cho wifi, IP, SC
        B2: esp_wifi_start wifi
        B3: sau khi start, ta sẽ nhận được bit WIFI_EVENT_STA_START
        khi đó, ta sẽ esp_smartconfig_set_type() và esp_smartconfig_start()
        B4: SC_EVENT_SCAN_DONE
        B5: SC_EVENT_FOUND_CHANNEL
        B6: SC_EVENT_GOT_SSID_PSWD, ta sẽ lấy ssid và pwd ở đây
        B7: copy ssid và pwd vào wifi_config_t rồi esp_wifi_connect();
        B8: IP_EVENT_STA_GOT_IP là wifi connect thành công
        B9: nhận được SC_EVENT_SEND_ACK_DONE thì esp_smartconfig_stop()
*/

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_wpa2.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_smartconfig.h"


static const char *TAG = "smartconfig_example";


static void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    // wifi_event_t
    // smartconfig_event_t
    // ip_event_t
    //..........................WIFI_EVENT..............................
    if(event_base ==  WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_smartconfig_set_type(SC_TYPE_ESPTOUCH);
        smartconfig_start_config_t smcf = SMARTCONFIG_START_CONFIG_DEFAULT();
        esp_smartconfig_start(&smcf);
    }
    else if( event_base ==  WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        ESP_LOGI(TAG, "wifi disconnected");
        esp_wifi_connect();
    }
    //..........................IP_EVENT..............................
    else if(event_base ==  IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ESP_LOGI(TAG, "wifi connected");
    }
    //..........................SC_EVENT..............................
    else if(event_base == SC_EVENT && event_id == SC_EVENT_SCAN_DONE)
    {
        ESP_LOGI(TAG, "smartconfig scan done");
    }
    else if(event_base == SC_EVENT && event_id == SC_EVENT_FOUND_CHANNEL)
    {
        ESP_LOGI(TAG, "smartconfig found channel");
    }
    else if(event_base == SC_EVENT && event_id == SC_EVENT_GOT_SSID_PSWD)
    {
        // tạo smartconfig_event_got_ssid_pswd_t để lấy giá trị đọc từ smartconfig cellphone
        smartconfig_event_got_ssid_pswd_t *evnt = (smartconfig_event_got_ssid_pswd_t *) event_data;
        wifi_config_t  wifi_config;
        uint8_t ssid[30];
        uint8_t pwd[20];
        
        // clear data
        bzero(&wifi_config, sizeof(wifi_config_t));

        // copy data vào wifi config để cấu hình
        memcpy(wifi_config.sta.ssid, evnt->ssid, sizeof( wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, evnt->password, sizeof( wifi_config.sta.password));

        // copy để in ra màn hình
        memcpy(ssid, evnt->ssid, sizeof(ssid));
        memcpy(pwd, evnt->password, sizeof(pwd));

        // smartconfig phải check bssid_set == 1
        // whether set MAC address of target AP or not. Generally, station_config.bssid_set 
        // needs to be 0; and it needs to be 1 only when users need to check the MAC address of the AP
        wifi_config.sta.bssid_set = evnt->bssid_set;
        if(wifi_config.sta.bssid_set == 1)
        {
            memcpy(wifi_config.sta.bssid, evnt->bssid, sizeof( evnt->bssid));
        }
        ESP_LOGI(TAG, "smartconfig got ssid: %s and pwd: %s",ssid,pwd);

        // lấy được id rồi thì connect thôi
        esp_wifi_connect();
    }
    else if(event_base == SC_EVENT && event_id == SC_EVENT_SEND_ACK_DONE)
    {
        ESP_LOGI(TAG, "smartconfig over");
        esp_smartconfig_stop();
    }
}



static void initialise_wifi(void)
{
    esp_netif_init();
    esp_event_loop_create_default();
    esp_netif_create_default_wifi_sta();
    wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wifi_init_config);

    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID,event_handler, NULL);
    esp_event_handler_register(IP_EVENT,   ESP_EVENT_ANY_ID,event_handler, NULL);
    esp_event_handler_register(SC_EVENT,   ESP_EVENT_ANY_ID,event_handler, NULL);

    esp_wifi_set_mode(WIFI_MODE_STA) ;
    esp_wifi_start();
}



void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    initialise_wifi();
}

