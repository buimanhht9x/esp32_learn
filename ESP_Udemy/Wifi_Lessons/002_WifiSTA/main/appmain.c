/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

/* The examples use WiFi configuration that you can set via project configuration menu

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/


#define EXAMPLE_ESP_WIFI_SSID      CONFIG_ESP_WIFI_SSID
#define EXAMPLE_ESP_WIFI_PASS      CONFIG_ESP_WIFI_PASSWORD
#define EXAMPLE_ESP_MAXIMUM_RETRY  CONFIG_ESP_MAXIMUM_RETRY

#if CONFIG_ESP_WIFI_AUTH_OPEN
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_OPEN
#elif CONFIG_ESP_WIFI_AUTH_WEP
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WEP
#elif CONFIG_ESP_WIFI_AUTH_WPA_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA2_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA_WPA2_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_WPA2_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA3_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA3_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA2_WPA3_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_WPA3_PSK
#elif CONFIG_ESP_WIFI_AUTH_WAPI_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WAPI_PSK
#endif

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

static const char *TAG = "wifi station";

static int s_retry_num = 0;


static void event_handler(void* arg, esp_event_base_t event_base,int32_t event_id, void* event_data)
{
    // Khối này kiểm tra xem sự kiện nhận được có phải là sự kiện WiFi không ( WIFI_EVENT) và có ID 
    // sự kiện WIFI_EVENT_STA_START. Sự kiện này thường được kích hoạt khi chế độ trạm WiFi đang bắt đầu.
    // Nếu điều kiện này đúng, nó sẽ gọi esp_wifi_connect()để bắt đầu quá trình kết nối với mạng WiFi 
    // được chỉ định.
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } 

    // Khối này kiểm tra xem sự kiện nhận được có phải là sự kiện WiFi không ( WIFI_EVENT) và có ID sự 
    // kiện WIFI_EVENT_STA_DISCONNECTED. Sự kiện này được kích hoạt khi chế độ trạm bị ngắt kết nối với
    // mạng WiFi.
    // Nếu số lần thử lại kết nối ( s_retry_num) nhỏ hơn mức tối đa được chỉ định ( EXAMPLE_ESP_MAXIMUM_RETRY),
    // nó sẽ cố gắng kết nối lại bằng cách gọi esp_wifi_connect()lại, tăng bộ đếm thử lại ( s_retry_num) 
    // và ghi lại thông báo thử lại.
    // Nếu đạt đến số lần thử lại tối đa, nó sẽ đặt WIFI_FAIL_BIT trong nhóm sự kiện 
    // xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT) cho biết không thể kết nối
    // với mạng WiFi. Nó cũng ghi lại một thông báo lỗi.
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } 
        else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } 

    // Khối này kiểm tra xem sự kiện nhận được có phải là sự kiện IP ( IP_EVENT) hay không và có ID sự kiện 
    // hay không IP_EVENT_STA_GOT_IP. Sự kiện này được kích hoạt khi chế độ trạm lấy địa chỉ IP thành công.
    // Nếu điều kiện này là đúng, nó sẽ trích xuất thông tin địa chỉ IP từ dữ liệu sự kiện, ghi lại địa chỉ 
    // IP thu được, đặt s_retry_numthành 0 (cho biết kết nối thành công) và đặt WIFI_CONNECTED_BIT trong
    // nhóm sự kiện ( xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT)), cho biết kết nối thành
    // công với mạng WiFi .
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    // Initialize the underlying TCP/IP stack
    ESP_ERROR_CHECK(esp_netif_init());

    // Hàm này tạo một vòng lặp sự kiện mặc định để xử lý các sự kiện ESP-IDF. 
    // Các sự kiện trong ESP-IDF được sử dụng để xử lý các sự kiện không đồng bộ 
    // như thay đổi kết nối Wi-Fi, thu thập địa chỉ IP và các sự kiện hệ thống khác.
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // tạo giao diện mạng Wi-Fi mặc định ( esp_netif) cho ESP32 và định cấu hình nó 
    // để sử dụng làm STATION. Nó thiết lập các tham số giao diện mạng cần thiết.
    esp_netif_create_default_wifi_sta();

    // khởi tạo cấu trúc cấu hình ( wifi_init_config_t) cho mô-đun Wi-Fi bằng các 
    // giá trị mặc định. Macro WIFI_INIT_CONFIG_DEFAULT()thiết lập cấu hình mặc định.
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

    // khởi chạy mô-đun Wi-Fi với cấu hình được cung cấp trong tệp cfg.   
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));


    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;

    // đăng ký trình xử lý sự kiện cho một loại sự kiện và ID sự kiện cụ thể.
    // trong trường hợp này, nó đăng ký event_handlerchức năng xử lý mọi sự kiện 
    // WiFi ( WIFI_EVENT) với bất kỳ ID sự kiện nào ( ESP_EVENT_ANY_ID). Tham số này
    // &instance_any_id là một con trỏ tới biến nơi phiên bản xử lý sự kiện sẽ được lưu trữ.
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));

    // đăng ký event_handlerchức năng tương tự để xử lý các sự kiện IP ( IP_EVENT) cụ thể là 
    // sự kiện trạm (thiết bị) lấy địa chỉ IP ( IP_EVENT_STA_GOT_IP). tham số này &instance_got_ip
    // là một con trỏ tới biến nơi phiên bản xử lý sự kiện sẽ được lưu trữ.                                                   
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    // struct để định cấu hình Station Wi-Fi. Nó bao gồm các tham số như SSID (tên mạng), mật khẩu, 
    // mode bảo mật, 
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
            // Đặt mật khẩu nghĩa là trạm sẽ kết nối với tất cả các chế độ bảo mật bao gồm WEP/WPA.
            // Tuy nhiên, các chế độ này không được dùng nữa và không nên sử dụng. Trong trường hợp 
            // Access Point của bạn không hỗ trợ WPA2, bạn có thể kích hoạt chế độ này bằng cách cấu hình bên dưới
            .threshold.authmode = ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD,
            .sae_pwe_h2e = WPA3_SAE_PWE_BOTH,  //  SAE (Simultaneous Authentication of Equals) 
        },
    };

    // // đặt chế độ Wi-Fi thành chế độ STA.
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );

    // đặt cấu hình Wi-Fi đã đặt trước wifi_config đó cho STA.
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );

    // khởi động mô-đun Wi-Fi ở chế độ STA.
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    // Chờ cho đến khi kết nối được thiết lập (WIFI_CONNECTED_BIT) hoặc kết nối không thành công 
    // ở mức tối đa
    // số lần thử lại MAXIMUM_RETRY (WIFI_FAIL_BIT). Các bit được đặt bởi event_handler()
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    // trả về các bit trước khi s_wifi_event_group trả về, do đó chúng ta có thể kiểm tra xem sự kiện 
    // nào đã thực sự xảy ra. 
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    // Các event_handler được sử dụng để quản lý và kiểm soát việc đăng ký và hủy đăng ký 
    // của các trình xử lý sự kiện. Bằng cách hủy đăng ký các trình xử lý sự kiện, mã sẽ giải phóng 
    // các tài nguyên liên quan đến các trình xử lý này 
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
}

void app_main(void)
{
    // khởi tạo NVS trên ESP32 và nếu có lỗi cụ thể liên quan đến việc khởi tạo NVS, 
    // mã này sẽ cố gắng xóa dữ liệu hiện có và khởi tạo lại. Điều này có thể hữu ích 
    // để xử lý các tình huống trong đó việc khởi tạo NVS không thành công do không đủ 
    // trang trống hoặc phát hiện thấy phiên bản NVS mới.
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");


    wifi_init_sta();
}
