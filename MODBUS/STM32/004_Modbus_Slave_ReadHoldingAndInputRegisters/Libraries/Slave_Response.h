#ifndef _SLAVE_RESPONSE_H_
#define _SLAVE_RESPONSE_H_

#include "main.h"
#include "CRC_Genaration.h"
#include "data.h"
#include "string.h"

// SLAVE ADDRESS
#define SLAVE_ADDRESS   							 0x05

// DEFINE ILLEGAL

#define  ILLEGAL_FUNCTION 						 0x01 
#define  ILLEGAL_DATA_ADDRESS 				 0x02
#define  ILLEGAL_DATA_VALUE 					 0x03

typedef enum {
	READ_COILS = 1,
	READ_INPUTS = 2,
	READ_HOLDING_REGISTERS = 3,
	READ_INPUT_REGISTERS = 4,
	WRITE_SINGLE_COIL = 5,
	WRITE_SINGLE_REGISTER = 6,
	WRITE_COILS = 15,
	WRITE_HOLDING_REGISTERS = 16,
}FUNCTION_CODE;
void initModbusSlave(UART_HandleTypeDef *huart);
uint8_t checkIllegalAndSendToMaster(uint8_t *data,int size);

#endif

