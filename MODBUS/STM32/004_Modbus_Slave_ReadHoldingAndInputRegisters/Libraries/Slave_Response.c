
#include "Slave_Response.h"

UART_HandleTypeDef *uart;
uint8_t TxData[255];
void initModbusSlave(UART_HandleTypeDef *huart)
{
	 uart = huart;
}
// Send Data to Master
void sendDataToMaster(uint8_t *data, int size)
{
	  uint16_t crc = CRC16(TxData,size);
		TxData[size] = (crc>>8)&0xFF;
	  TxData[size+1] =  crc&0xFF;
	  HAL_GPIO_WritePin(TxEnable_GPIO_Port,TxEnable_Pin, GPIO_PIN_SET);
	  HAL_UART_Transmit(uart,TxData, size + 2,1000);
	  HAL_GPIO_WritePin(TxEnable_GPIO_Port,TxEnable_Pin, GPIO_PIN_RESET);
}

// Modbus Exeption
void sendExceptionToMaster(uint8_t *data, uint8_t exceptionCode)
{
	  /*
			| SLAVE_ADDRESS |  FUNCTION_CODE | Exception Code |   CRC   |
				   1byte						1byte							1byte				 2byte
	
			FUNCTION_CODE adding 1 to the MSB
		*/
	  TxData[0] = data[0];
		TxData[1] = data[1] | 0x80;
		TxData[2] = exceptionCode;
		sendDataToMaster(TxData,3);
}

uint16_t HoldingRegistersCopy[50];
void copyBufferHoldingRegisters()
{
	  for(int i = 0; i< 50; ++i)
		{	
				HoldingRegistersCopy[i] = HoldingRegisters[i];
		}
}
uint16_t CoilsCopy[25];
void copyBufferCoils()
{
	  for(int i = 0; i< 25; ++i)
		{	
				CoilsCopy[i] = Coils[i];
		}
}
uint8_t checkIllegalAndSendToMaster(uint8_t *data,int size)
{
	
	  
	  // Check CRC
		uint16_t crc = CRC16(data,size - 2);
		uint16_t crc_recieve = (*(data + (size - 2))<<8) |(*(data + (size - 1)));
		if(crc == crc_recieve)
		{
				// Check Slave Address
			  uint8_t slave_address = data[0];
				if( slave_address ==  SLAVE_ADDRESS) 
				{
						// Check Function Code
					  uint8_t function_code = data[1];
						if(function_code == 1 || function_code == 2 || function_code == 3 ||function_code == 4 ||function_code == 5 ||function_code == 6 || function_code == 15 || function_code == 16 )
						{	
							
							 if(function_code <= 4)
							 {
										// register address = (Rx[2] << 8)|Rx[3]
										// number elements wanna read  = (Rx[4] << 8)|Rx[5]
										uint16_t startAddressHigh = data[2];
										uint16_t startAddressLow = data[3];
										uint16_t numberElementsHigh = data[4];
										uint16_t numberElementsLow = data[5];
										uint16_t startAddress = (startAddressHigh << 8)|startAddressLow;
										uint16_t numberElements = (numberElementsHigh << 8)|numberElementsLow;
										uint16_t endAddress = startAddress + numberElements;
							 

							
										// Prepare Data Send to Master
										//| SLAVE_ID | FUNCTION_CODE | BYTE COUNT | DATA      | CRC     |
										//| 1 BYTE   |  1 BYTE       |  1 BYTE    | N*2 BYTES | 2 BYTES |
										
										memset(TxData, '\0', 256);
										TxData[0] = slave_address;
										TxData[1] = function_code;
										
										switch(function_code)
										{
											case READ_COILS: 						
													if( numberElements < 1 || numberElements > 2000)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_VALUE);
														 return 0;
													}
													// Check ILLEGAL_DATA_ADDRESS
													if(endAddress >= MAX_ELEMENT_COILS)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														 return 0;
													}	
													TxData[2] = numberElements/8 + ((numberElements%8) > 0 ? 1 : 0);
													int indexCoils = 3;
													
												/* The approach is simple. We will read 1 bit at a time and store them in the Txdata buffer.
												 * First find the offset in the first byte we read from, for eg- if the start coil is 13,
												 * we will read from database[1] with an offset of 5. This bit will be stored in the TxData[0] at 0th position.
												 * Then we will keep shifting the database[1] to the right and read the bits.
												 * Once the bitposition has crossed the value 7, we will increment the startbyte
												 * When the indxposition exceeds 7, we increment the indx variable, so to copy into the next byte of the TxData
												 * This keeps going until the number of coils required have been copied
												 */
													int startByte = startAddress/8;
													int bitPosition = startAddress%8 ;
													int indexPosition = 0;
													for (int i=0; i<numberElements; i++)
													{
														TxData[indexCoils] |= ((Coils[startByte] >> bitPosition) &0x01) << indexPosition;
														indexPosition++; bitPosition++;
														if (indexPosition>7)  // if the indxposition exceeds 7, we have to copy the data into the next byte position
														{
															indexPosition = 0;
															indexCoils++;
														}
														if (bitPosition>7)  // if the bitposition exceeds 7, we have to increment the startbyte
														{
															bitPosition=0;
															startByte++;
														}
													}

													if(numberElements%8 != 0) indexCoils++;  // increment the indx variable, only if the numcoils is not a multiple of 8
													
													sendDataToMaster(TxData, indexCoils); 
													break;
											case READ_INPUTS: 						  // 2
													if( numberElements < 1 || numberElements > 2000)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_VALUE);
														 return 0;
													}
													// Check ILLEGAL_DATA_ADDRESS
													if(endAddress >= MAX_ELEMENT_COILS)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														 return 0;
													}	
													TxData[2] = numberElements/8 + ((numberElements%8) > 0 ? 1 : 0);
													int indexIP = 3;
													
												/* The approach is simple. We will read 1 bit at a time and store them in the Txdata buffer.
												 * First find the offset in the first byte we read from, for eg- if the start coil is 13,
												 * we will read from database[1] with an offset of 5. This bit will be stored in the TxData[0] at 0th position.
												 * Then we will keep shifting the database[1] to the right and read the bits.
												 * Once the bitposition has crossed the value 7, we will increment the startbyte
												 * When the indxposition exceeds 7, we increment the indx variable, so to copy into the next byte of the TxData
												 * This keeps going until the number of coils required have been copied
												 */
													int startByteIP = startAddress/8;
													int bitPositionIP = startAddress%8 ;
													int indexPositionIP = 0;
													for (int i=0; i<numberElements; i++)
													{
														TxData[indexIP] |= ((DiscreteInputs[startByteIP] >> bitPositionIP) &0x01) << indexPositionIP;
														indexPositionIP++; bitPositionIP++;
														if (indexPositionIP>7)  // if the indxposition exceeds 7, we have to copy the data into the next byte position
														{
															indexPositionIP = 0;
															indexIP++;
														}
														if (bitPositionIP>7)  // if the bitposition exceeds 7, we have to increment the startbyte
														{
															bitPositionIP=0;
															startByteIP++;
														}
													}

													if(numberElements%8 != 0) indexIP++;  // increment the indx variable, only if the numcoils is not a multiple of 8
													
													sendDataToMaster(TxData, indexIP); 
													break;
											case READ_HOLDING_REGISTERS:  	// 3
													
													// Check ILLEGAL_DATA_VALUE
													if( numberElements < 1 || numberElements > 125)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_VALUE);
														 return 0;
													}
													
													 // Check ILLEGAL_DATA_ADDRESS
													if(endAddress >= MAX_ELEMENT_HOLDING_REGISTERS)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														 return 0;
													}	
													// Byte count
													TxData[2] = numberElements*2;
													int indexHR = 3;
													for(int i = startAddress; i< endAddress ; ++i)
													{
														 TxData[indexHR]     = (HoldingRegisters[i]>>8)&0xFF;
														 TxData[indexHR + 1] =  HoldingRegisters[i]&0xFF; 
														 indexHR = indexHR + 2;
													}	
													sendDataToMaster(TxData,indexHR);
													break;
											case READ_INPUT_REGISTERS:  	 	// 4
													// Check ILLEGAL_DATA_VALUE
													if( numberElements < 1 || numberElements > 125)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_VALUE);
														 return 0;
													}
													
													 // Check ILLEGAL_DATA_ADDRESS
													if(endAddress >= MAX_ELEMENT_HOLDING_REGISTERS)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														 return 0;
													}	
													// Byte count
													TxData[2] = numberElements*2;
													int indexIR = 3;
													for(int i = startAddress; i< endAddress ; ++i)
													{
														 TxData[indexIR]     = (InputRegisters[i]>>8)&0xFF;
														 TxData[indexIR + 1] =  InputRegisters[i]&0xFF; 
														 indexIR = indexIR + 2;
													}	
													sendDataToMaster(TxData,indexIR);
													break;
											default: 
													break;
												
										}
										
							}
							if(function_code > 4)
							{
								
								 switch(function_code)
								 {
									    case WRITE_SINGLE_COIL:  			 	// 5
													// Data recieve from master is
													//  SLAVE ID  |  FUNCTION CODE |  ADDRESS   |   DATA   |  CRC
											    //    1 byte         1 byte      2 bytes       2 bytes     2 bytes
													// If write 1, data is FF 00
											    // If write 0, data is 00 00
											    // Prepare Data to Response
											    //  SLAVE ID  |  FUNCTION CODE |  ADDRESS   |   DATA   |  CRC
											    //    1 byte         1 byte      2 bytes       2 bytes     2 bytes
													memset(TxData, '\0', 256);
													int valueWSC =   (data[4]<<8)|data[5]; 
											    int addressWSC = (data[2]<<8)|data[3]; 
											    int startByteWSC = addressWSC/8;
													int bitPositionWSC = addressWSC%8;
													if(addressWSC >= MAX_ELEMENT_COILS)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														 return 0;
													}														
											
													if(valueWSC == 0xFF00)
														  Coils[startByteWSC] |= (1<<bitPositionWSC);  // set bit
													else
														  Coils[startByteWSC] &= ~(1<<bitPositionWSC);  // clear bit
													TxData[0] = slave_address;
												  TxData[1] = function_code;
													TxData[2] = data[2];
													TxData[3] = data[3];
													TxData[4] = data[4];
													TxData[5] = data[5];
											    sendDataToMaster(TxData,6);
													copyBufferCoils();
													break;
											case WRITE_COILS:  		// 15
													// Data recieve from master is
													//  SLAVE ID  |  FUNCTION CODE |  START ADDRESS   |   NumberofPoint |  Byte Count |  DATA      |  CRC
											    //    1 byte         1 byte        2 bytes                 2 bytes     1 byte       n bytes      2 bytes
											    // Prepare Data to Response
											    //  SLAVE ID  |  FUNCTION CODE |  ADDRESS   |   DATA   |  CRC
											    //    1 byte         1 byte      2 bytes       2 bytes     2 bytes
												  memset(TxData, '\0', 256);
													
													int addressWR = (data[2]<<8)|data[3]; 
												  int numberPointWR =  (data[4]<<8)|data[5]; 
													int startByteWR = addressWR/8;
													int bitPositionWR = addressWR%8;
                          int indxPositionWR = 0; 											
													int indexWC = 7;
													if( numberPointWR >1968 && numberPointWR < 0)
													{
														   sendExceptionToMaster(data,ILLEGAL_DATA_VALUE);
														   return 0;
													}
													if(numberPointWR > MAX_ELEMENT_COILS)
													{
														   sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														   return 0;
													}
													for(int i = 0; i< numberPointWR; ++i)
													{
															if( ((data[indexWC] >> indxPositionWR) & 0x01) == 1)
																   Coils[startByteWR] |= (1<<bitPositionWR);
															else 
																	 Coils[startByteWR] &= ~(1<<bitPositionWR);
															indxPositionWR++;bitPositionWR++;
															if(indxPositionWR>7)
															{
																  indxPositionWR = 0;
																  indexWC ++;
															}
															if(bitPositionWR > 7)
															{
																  bitPositionWR = 0;
																	startByteWR++;
															}
													}
													TxData[0] = slave_address;
												  TxData[1] = function_code;
													TxData[2] = data[2];
													TxData[3] = data[3];
													TxData[4] = data[4];
													TxData[5] = data[5];
											    sendDataToMaster(TxData,6);
													copyBufferCoils();
													break;
											case WRITE_SINGLE_REGISTER:   						// 6
													// Data recieve from master is
													//  SLAVE ID  |  FUNCTION CODE |  ADDRESS   |   DATA   |  CRC
											    //    1 byte         1 byte      2 bytes       2 bytes     2 bytes
											    // Prepare Data to Response
											    //  SLAVE ID  |  FUNCTION CODE |  ADDRESS   |   DATA   |  CRC
											    //    1 byte         1 byte      2 bytes       2 bytes     2 bytes
											    memset(TxData, '\0', 256);
												  
													int valueWSR =   (data[4]<<8)|data[5]; 
											    int addressWSR = (data[2]<<8)|data[3]; 
											    if(addressWSR > MAX_ELEMENT_HOLDING_REGISTERS)
													{
														 sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														 return 0;
													}
											    HoldingRegisters[addressWSR] = valueWSR;
													TxData[0] = slave_address;
												  TxData[1] = function_code;
													TxData[2] = data[2];
													TxData[3] = data[3];
													TxData[4] = data[4];
													TxData[5] = data[5];
											    sendDataToMaster(TxData,6);
													copyBufferHoldingRegisters();
													break; 
											case WRITE_HOLDING_REGISTERS:   // 16  
												  // Data recieve from master is
													//  SLAVE ID  |  FUNCTION CODE |  START ADDRESS   |   NumberofPoint |  Byte Count |  DATA      |  CRC
											    //    1 byte         1 byte        2 bytes                 2 bytes     1 byte       n bytes      2 bytes
											    // Prepare Data to Response
											    //  SLAVE ID  |  FUNCTION CODE |  ADDRESS   |   DATA   |  CRC
											    //    1 byte         1 byte      2 bytes       2 bytes     2 bytes
													memset(TxData, '\0', 256);
													int startAddrWHR = (data[2]<<8)|data[3];
											    int numRegWHR = (data[4]<<8)|data[5];
											    int endAddrWHR = startAddrWHR + numRegWHR - 1;
													if( numRegWHR >125 && numRegWHR < 0)
													{
														   sendExceptionToMaster(data,ILLEGAL_DATA_VALUE);
														   return 0;
													}
													if(endAddrWHR > MAX_ELEMENT_HOLDING_REGISTERS)
													{
														   sendExceptionToMaster(data,ILLEGAL_DATA_ADDRESS);
														   return 0;
													}
													int indxWHR = 7;
													for(int i = 0; i < numRegWHR; ++i)
													{
														  HoldingRegisters[startAddrWHR] = data[indxWHR]<<8|data[indxWHR+1];
														  indxWHR +=2;
														  startAddrWHR++;
													}
													TxData[0] = slave_address;
												  TxData[1] = function_code;
													TxData[2] = data[2];
													TxData[3] = data[3];
													TxData[4] = data[4];
													TxData[5] = data[5];
											    sendDataToMaster(TxData,6);
													copyBufferHoldingRegisters();
													break;
											default: 
													break;
								 }
									
							}
							return 1;
						}		
						sendExceptionToMaster(data,ILLEGAL_FUNCTION);
						return 0;
					}
					return 0;
		}
		return 0;
	  
}


