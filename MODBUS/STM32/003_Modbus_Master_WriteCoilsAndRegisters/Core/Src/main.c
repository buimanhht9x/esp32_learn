/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "CRC_Genaration.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

uint8_t TxBuff[20];
uint8_t RxBuff[30];
int Data[20];
int sz = 0;
void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
	 // TODO
	 sz = Size;
}

void sendData(uint8_t *data)
{
		HAL_GPIO_WritePin(TxEnable_GPIO_Port, TxEnable_Pin, 1);
		HAL_UART_Transmit(&huart1,data,15,1000);
	  HAL_GPIO_WritePin(TxEnable_GPIO_Port, TxEnable_Pin, 0);
}

// 
void WriteDataSingleCoilAndRegister()
{
		/*
		Write Single Coil
		Frame:
			Slave Address  | Function Code | Register Addr High | Register Addr Low |   Data      |    CRC
				1 byte            1 byte					1 byte							1 byte	 		  	2 bytes       2bytes
				
				Data  =>    0xFF00 is ON ( High byte is FF, Low byte is 00)
				Data  =>    0x0000 is ON ( High byte is 00, Low byte is 00)
	*/
	
	/*
		Write Single Register
		Frame:
			Slave Address  | Function Code | Register Addr High | Register Addr Low |   Data      |    CRC
				1 byte            1 byte					1 byte							1 byte	 		  	2 bytes       2bytes
				
			
	*/
	
	
	
	// TxBuff[1] = 0x05; Write 1 Coil
	// TxBuff[1] = 0x06; Write 1 Register (Analog Data)
	
	TxBuff[0] = 0x05;
	TxBuff[1] = 0x06;
	TxBuff[2] = 0x00;
	TxBuff[3] = 0x00;
	TxBuff[4] = 0x12;
	TxBuff[5] = 0x34;
	uint16_t crc = CRC16(TxBuff,6);
	TxBuff[6] = (crc >> 8)& 0xFF;
	TxBuff[7] = crc & 0xFF;
	
	sendData(TxBuff);
}


void WriteDataMultiCoils()
{
		/*
		Write Multi Coils
		Frame:
			Slave Address  | Function Code | Register Addr High | Register Addr Low |   Number of Point | Byte count |   Data      |    CRC
				1 byte            1 byte					1 byte							1 byte	 		  	           2 bytes      1bytes      nbytes        2bytes
	
			Function Code : 0x0F
    	Number of Point : So coil muon write
			Byte count = NumberofPoint/8  =>  9 Coils is 2 bytes, 8 Coils is 1 byte
	
		*/
		
		TxBuff[0] = 0x05;  // Slave Address
	
		TxBuff[1] = 0x0F;  // Function Code
	
		TxBuff[2] = 0x00;  // Register Addr High
		TxBuff[3] = 0x00;  // Register Addr Low
	
		TxBuff[4] = 0x00;  // Number of Point 
		TxBuff[5] = 0x10;  // 16 bit
		
		TxBuff[6] = 0x02;  // Byte count   16/8bit = 2
		
		TxBuff[7] = 0x03;  // Data  0000 0011  => address 8  -> 1
		TxBuff[8] = 0xA5;  // Data  1010 0101  => address 16 -> 9
		
		uint16_t crc = CRC16(TxBuff,9);
		TxBuff[9]  = (crc >> 8)& 0xFF;
		TxBuff[10] =  crc & 0xFF;
		sendData(TxBuff);
		
}

void WriteDataMultiRegisters()
{
		/*
		Write Multi Registers
		Frame:
			Slave Address  | Function Code | Register Addr High | Register Addr Low |   Number of Point | Byte count |   Data      |    CRC
				1 byte            1 byte					1 byte							1 byte	 		  	           2 bytes      1bytes      nbytes        2bytes
	
			Function Code : 0x10   // 16
    	Number of Point : So coil muon write
			Byte count = NumberofPoint * 2  =>  3 Registers is 6 bytes
	
		*/

		TxBuff[0] = 0x05;  // Slave Address
	
		TxBuff[1] = 0x10;  // Function Code
	
		TxBuff[2] = 0x00;  // Register Addr High
		TxBuff[3] = 0x00;  // Register Addr Low
	
		TxBuff[4] = 0x00;  // Number of Point 
		TxBuff[5] = 0x03;  // 3 number = 2bytes *3 = 6
		
		TxBuff[6] = 0x06;  // Byte count  3 number = 2bytes *3 = 6
		
		TxBuff[7] = 0x03;  // Value at address 40001
		TxBuff[8] = 0xA5; 
		
		TxBuff[9] = 0x04;  // Value at address 40002
		TxBuff[10] = 0xA6;
		
		TxBuff[11] = 0x01; // // Value at address 40003
		TxBuff[12] = 0xAD;
			
		uint16_t crc = CRC16(TxBuff,13);
		TxBuff[13]  = (crc >> 8)& 0xFF;
		TxBuff[14] =  crc & 0xFF;
		sendData(TxBuff);
		
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
	
	HAL_UARTEx_ReceiveToIdle_IT(&huart1, RxBuff, 30);
	
  WriteDataMultiRegisters();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TxEnable_GPIO_Port, TxEnable_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : TxEnable_Pin */
  GPIO_InitStruct.Pin = TxEnable_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TxEnable_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
