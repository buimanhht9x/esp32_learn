#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"

#include <led_strip.h>
#include "math.h"
#include "esp_log.h"

/*........................................Timer.......................................*/
#include "driver/timer.h"

#define TIMER_DIVIDER         (16)  //  Hardware timer clock divider
#define TIMER_SCALE           1000 // convert counter value to seconds

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
    bool auto_reload;
} example_timer_info_t;

/**
 * @brief A sample structure to pass events from the timer ISR to task
 *
 */
typedef struct {
    example_timer_info_t info;
    uint64_t timer_counter_value;
} example_timer_event_t;

static xQueueHandle s_timer_queue;

/*
 * A simple helper function to print the raw timer counter value
 * and the counter value converted to seconds
 */
static void inline print_timer_counter(uint64_t counter_value)
{
    printf("Counter: 0x%08x%08x\r\n", (uint32_t) (counter_value >> 32),
           (uint32_t) (counter_value));
    printf("Time   : %.8f s\r\n", (double) counter_value / TIMER_SCALE);
}

static bool IRAM_ATTR timer_group_isr_callback(void *args)
{
    BaseType_t high_task_awoken = pdFALSE;
    example_timer_info_t *info = (example_timer_info_t *) args;

    uint64_t timer_counter_value = timer_group_get_counter_value_in_isr(info->timer_group, info->timer_idx);

    /* Prepare basic event data that will be then sent back to task */
    example_timer_event_t evt = {
        .info.timer_group = info->timer_group,
        .info.timer_idx = info->timer_idx,
        .info.auto_reload = info->auto_reload,
        .info.alarm_interval = info->alarm_interval,
        .timer_counter_value = timer_counter_value
    };

    if (!info->auto_reload) {
        timer_counter_value += info->alarm_interval * TIMER_SCALE;
        timer_group_set_alarm_value_in_isr(info->timer_group, info->timer_idx, timer_counter_value);
    }

    /* Now just send the event data back to the main program task */
    xQueueSendFromISR(s_timer_queue, &evt, &high_task_awoken);

    return high_task_awoken == pdTRUE; // return whether we need to yield at the end of ISR
}

/**
 * @brief Initialize selected timer of timer group
 *
 * @param group Timer Group number, index from 0
 * @param timer timer ID, index from 0
 * @param auto_reload whether auto-reload on alarm event
 * @param timer_interval_sec interval of alarm
 */
static void example_tg_timer_init(int group, int timer, bool auto_reload, int timer_interval_sec)
{
    /* Select and initialize basic parameters of the timer */
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = auto_reload,
    }; // default clock source is APB
    timer_init(group, timer, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(group, timer, 0);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(group, timer, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(group, timer);

    example_timer_info_t *timer_info = calloc(1, sizeof(example_timer_info_t));
    timer_info->timer_group = group;
    timer_info->timer_idx = timer;
    timer_info->auto_reload = auto_reload;
    timer_info->alarm_interval = timer_interval_sec;
    timer_isr_callback_add(group, timer, timer_group_isr_callback, timer_info, 0);

    timer_start(group, timer);
}
/*..................................................................................*/


////////////////////////////

#define LED_TYPE LED_STRIP_WS2812
#define LED_LENGTH 512
#define LED_GPIO 5
#define LED_STRIP_BRIGHTNESS 50
#define CUBE_SIZE 8
///////////////////////////////////////////////////////////////
typedef enum{
  INACTIVE = 0,
  STARTING = 1,
  RUNNING = 2,
  ENDING = 3
}state_t;

void delay_ms(int time_delay)
{
    vTaskDelay(time_delay/portTICK_RATE_MS);
}

int random_number(int minN, int maxN){
	return minN + rand() % (maxN + 1 - minN);
}
rgb_t layer_buffer[512];
uint8_t onoff_buffer[512];
uint8_t cube[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];
uint8_t buffer[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];

// cấu hình strip
led_strip_t strip = {
        .type = LED_TYPE,
        .length = LED_LENGTH,
        .gpio = LED_GPIO,
        .buf = NULL,
        #ifdef LED_STRIP_BRIGHTNESS
                .brightness = 50,
        #endif
        };

uint32_t map(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
void set_brightness(int percentage)
{
    if(percentage < 0 ) percentage = 0;
    else if( percentage > 100) percentage = 100;
    int value = map(percentage, 0,100,0,255);
    strip.brightness = value;
}



uint8_t inrange(uint8_t z, uint8_t y, uint8_t x) {
    if (x>=0 && x<CUBE_SIZE && y>=0 && y<CUBE_SIZE && z>=0 && z<CUBE_SIZE) {
        return 1;
    } else {
        return 0;
    }
}
void set_pixel_zyx(uint8_t z,uint8_t y, uint8_t x, rgb_t data)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            led_strip_set_pixel(&strip, z*8 + x*64 + y,  data);
        else if(z %2 == 1)
            led_strip_set_pixel(&strip, z*8 + x*64 + 7 - y,  data);
    }
}
void clear_pixel_zyx(uint8_t z,uint8_t y, uint8_t x)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            led_strip_set_pixel(&strip, z*8 + x*64 + y,   (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 });
        else if(z %2 == 1)
            led_strip_set_pixel(&strip, z*8 + x*64 + 7 - y,   (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 });
    }
}

//......................SET, CLEAR, SHOW buffer layer......................................
void set_color_to_buffer(uint8_t z,uint8_t y, uint8_t x, rgb_t data)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            layer_buffer[z*8 + x*64 + y] =  data;
        else if(z %2 == 1)
            layer_buffer[z*8 + x*64 + 7 - y] =  data;
    }
}

void clear_color_in_buffer(uint8_t z,uint8_t y, uint8_t x)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            layer_buffer[z*8 + x*64 + y] =  (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
        else if(z %2 == 1)
            layer_buffer[z*8 + x*64 + 7 - y] =  (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
    }
}
void clear_layer_buffer()
{
    for(int i = 0; i<512; i++)
    {
        layer_buffer[i] = (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
    }
}

void show_led()
{
    ESP_ERROR_CHECK(led_strip_set_pixels(&strip, 0, 512, layer_buffer));
    ESP_ERROR_CHECK(led_strip_flush(&strip));
}


//......................SET, CLEAR, READ buffer ONOFF......................................
void clear_onoff_buffer()
{
    for(int i = 0; i<512; i++)
    {
        onoff_buffer[i] = 0;
    }
}
void set_onoff_buffer(uint8_t z, uint8_t y, uint8_t x) 
{
     if(inrange(z,y,x))
    {
        if(z %2 == 0)
        {
            onoff_buffer[z*8 + x*64 + y] = 1;
        }   
        else if(z %2 == 1)
        {
            onoff_buffer[z*8 + x*64 + 7 - y] = 1;
        }
    }
}
uint8_t read_onoff_buffer(uint8_t z, uint8_t y, uint8_t x) 
{
    static uint8_t value;
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
        {
            value = onoff_buffer[z*8 + x*64 + y];
        }   
        else if(z %2 == 1)
        {
            value = onoff_buffer[z*8 + x*64 + 7 - y];
        }
        return value;
    }
    return 0;
}


/*..............................................................................*/

rgb_t rgb_setvoxel =  { .r = 255, .g = 0, .b = 0 };


void set_color_setvoxel(char rs, char gs, char bs)
{
    rgb_setvoxel.r = rs;
    rgb_setvoxel.g = gs;
    rgb_setvoxel.b = bs;
}
void setvoxel(uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)) {
        set_color_to_buffer(z,y,x,rgb_setvoxel);
        cube[z][y][x] = 1;
    }
}


void clrvoxel(uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)){
        set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0x00, .b = 0x00 });
        cube[z][y][x] = 0;

    }
}

uint8_t getvoxel(uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)) {
        if (cube[z][y][x]==1) 
        {
            return 1;
        } 
        else
        {
            return 0;
        }
    } 
    else 
    {
        return 0;
    }
}

void setplane_x(uint8_t x) {
    uint8_t z, y;

    if (x>=0 && x<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++) {
                cube[z][y][x]=1;
                set_color_to_buffer(z,y,x,rgb_setvoxel);
            }
        }
    }
}
void clrplane_x(uint8_t x) {
    uint8_t z, y;

    if (x>=0 && x<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++) {
                cube[z][y][x]=0;
                set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
            }
        }
    }
}
void setplane_y(uint8_t y) {
    uint8_t z, x;

    if (y>=0 && y<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (x=0; x<CUBE_SIZE; x++) {
                cube[z][y][x]=1;
                set_color_to_buffer(z,y,x,rgb_setvoxel);
            }
        }
    }
}
void clrplane_y(uint8_t y) {
    uint8_t z, x;

    if (y>=0 && y<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (x=0; x<CUBE_SIZE; x++) {
                cube[z][y][x]=0x00;
                set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
            }
        }
    }
}

void fill_layer(uint8_t layer, uint8_t color) {
    uint8_t y, x;

    for (y=0; y<CUBE_SIZE; y++) {
        for (x=0; x<CUBE_SIZE; x++) {
            cube[layer][y][x]= color    ;
            set_color_to_buffer(layer,y,x,rgb_setvoxel);
        }
    }
}

void fill_cube(uint8_t colour) {
    uint8_t z, y, x;

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if(colour == 0)
                {
                     cube[z][y][x]=0;
                    set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
                }
                else
                {
                     cube[z][y][x]=1;
                    set_color_to_buffer(z,y,x,rgb_setvoxel);
                }
               
            }
        }
    }
}
void setplane(uint8_t axis, uint8_t i) {
    switch (axis) {
        case 1: //AXIS_Z:
            fill_layer(i, 1);
            break;
        case 2: //AXIS_Y:
            setplane_y(i);
            break;
        case 3: //AXIS_X:
            setplane_x(i);
            break;
    }
}

void clrplane(uint8_t axis, uint8_t i) {
    switch (axis) {
        case 1: //AXIS_Z:
            fill_layer(i, 0);
            break;
        case 2: //AXIS_Y:
            clrplane_y(i);
            break;
        case 3: //AXIS_X:
            clrplane_x(i);
            break;
    }
}

void altervoxel(uint8_t z, uint8_t y, uint8_t x, uint8_t state) {
    if (inrange(z, y, x)) {
        if (state==1) {
            setvoxel(z, y, x);
        }
        if (state==0) {
            clrvoxel(z, y, x);
        }
    }
}
void shift(char axis, char direction) {
    char z, y, x, ii, iii, state;

    if (axis==1) // AXIS_Z - 1
    {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) // direction=0 (-1) - ïî AXIS_Z ñïóñêàåòñÿ âíèç
                    { // direction=1 - ïî AXIS_Z ïîäûìàåòñÿ ââåðõ
                        ii=z;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - z;
                        iii=ii - 1;
                    }
                    state=getvoxel(iii, y, x);
                    altervoxel(ii, y, x, state);
                }
            }
        }
    }
    if (axis==2) // AXIS_Y - 2
    {
        for (z=0; z<CUBE_SIZE; z++) {
             for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) // direction=0 (-1) - ïî AXIS_Y äâèæåòñÿ âëåâî
                    { // direction=1 - ïî AXIS_Y äâèæåòñÿ âïðàâî
                        ii=(CUBE_SIZE - 1) - y;
                        iii=ii - 1;
                    } else {
                        ii=y;
                        iii=ii + 1;
                    }
                    state=getvoxel(z, iii, x);
                    altervoxel(z, ii, x, state);
                }
            }
        }
    }
    if (axis==3) // AXIS_X - 3
    {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) 
                    { 
                        ii=x;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - x;
                        iii=ii - 1;
                    }
                    state=getvoxel(z, y, iii);
                    altervoxel(z, y, ii, state);
                }
            }
        }
    }
}

void sendvoxel_axis(uint8_t z, uint8_t y, uint8_t x, char axis, int delay) {
    int i, ii;

    if (axis==1) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (z==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(ii + 1, y, x);
            } else {
                ii=i;
                clrvoxel(ii - 1, y, x);
            }
            setvoxel(ii, y, x);
            delay_ms(delay);
        }
    }

    if (axis==2) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (y==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(z, ii + 1, x);
            } else {
                ii=i;
                clrvoxel(z, ii - 1, x);
            }
            setvoxel(z, ii, x);
            delay_ms(delay);
        }
    }

    if (axis==3) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (x==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(z, y, ii + 1);
            } else {
                ii=i;
                clrvoxel(z, y, ii - 1);
            }
            setvoxel(z, y, ii);
             delay_ms(delay);
        }
    }
}
void argorder(uint8_t ix1, uint8_t ix2, uint8_t *ox1, uint8_t *ox2) {
    if (ix1>ix2) {
        char tmp;
        tmp=ix1;
        ix1=ix2;
        ix2=tmp;
    }
    *ox1=ix1;
    *ox2=ix2;
}
void box_wireframe(uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2) {
    uint8_t iz, iy, ix;

    argorder(x1, x2, &x1, &x2);
    argorder(y1, y2, &y1, &y2);
    argorder(z1, z2, &z1, &z2);

    for (iz=z1; iz<=z2; iz++) {
        for (iy=y1; iy<=y2; iy++) {
            for (ix=x1; ix<=x2; ix++) {
                if ((iz>=z1 && iz<=z2 && iy==y1 && ix==x1) || // ïðÿìàÿ (z1,y1,x1)-(z2,y1,x1)
                        (iz>=z1 && iz<=z2 && iy==y2 && ix==x1) || // ïðÿìàÿ (z1,y2,x1)-(z2,y2,x1)
                        (iz>=z1 && iz<=z2 && iy==y1 && ix==x2) || // ïðÿìàÿ (z1,y1,x2)-(z2,y1,x2)
                        (iz>=z1 && iz<=z2 && iy==y2 && ix==x2) || // ïðÿìàÿ (z1,y2,x2)-(z2,y2,x2)

                        (iz==z1 && iy==y1 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z1,y1,x1)-(z1,y1,x2)
                        (iz==z1 && iy==y2 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z1,y2,x1)-(z1,y2,x2)
                        (iz==z2 && iy==y1 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z2,y1,x1)-(z1,y1,x2)
                        (iz==z2 && iy==y2 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z2,y2,x1)-(z1,y2,x2)

                        (iz==z1 && iy>=y1 && iy<=y2 && ix==x1) || // ïðÿìàÿ (z1,y1,x1)-(z1,y2,x1)
                        (iz==z1 && iy>=y1 && iy<=y2 && ix==x2) || // ïðÿìàÿ (z1,y1,x2)-(z1,y2,x2)
                        (iz==z2 && iy>=y1 && iy<=y2 && ix==x1) || // ïðÿìàÿ (z2,y1,x1)-(z2,y2,x1)
                        (iz==z2 && iy>=y1 && iy<=y2 && ix==x2)) // ïðÿìàÿ (z2,y1,x2)-(z2,y2,x2)
                {
                    setvoxel(iz, iy, ix);
                    
                } else {
                    clrvoxel(iz, iy, ix);
                }
            }
        }
    }
}

void cube2buffer(void) {
    uint8_t z, y, x;

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                buffer[z][y][x]=cube[z][y][x];
            }
        }
    }
}

void mirror_z(void) {
    uint8_t z, y, x;
    cube2buffer(); // êîïèðóåò ñîäåðæèìîå êóáà â áóôåð
    fill_cube(0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (buffer[z][y][x]==1)
                    setvoxel(CUBE_SIZE - 1 - z, y, x);
            }
        }
    }
}


void mirror_y(void) {
    uint8_t z, y, x;
    cube2buffer(); 
    fill_cube(0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (buffer[z][y][x]==1)
                    setvoxel(z, CUBE_SIZE - 1 - y, x);
            }
        }
    }
}

void mirror_x(void) {
    uint8_t z, y, x;
    cube2buffer(); 
    fill_cube(0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (buffer[z][y][x]==1)
                    setvoxel(z, y, CUBE_SIZE - 1 - x);
            }
        }
    }
}

void draw_positions_axis(char axis, unsigned char positions[CUBE_SIZE*CUBE_SIZE], int invert) {
    int x, y, p;
    fill_cube(0x00);

    for (x=0; x<CUBE_SIZE; x++) {
        for (y=0; y<CUBE_SIZE; y++) {
            if (invert) {
                p=(CUBE_SIZE - 1) - positions[x * CUBE_SIZE + y];
            } else {
                p=positions[x * CUBE_SIZE + y];
            }
            if (axis==1) // AXIS_Z
                setvoxel(p, y, x); // (x,y,p)

            if (axis==2) // AXIS_Y
                setvoxel(y, p, x); // (x,p,y)

            if (axis==3) // AXIS_X
                setvoxel(x, y, p); // (p,y,x)
        }
    }
}

void line(char z1, char y1, char x1, char z2, char y2, char x2) {
    char x, y, z, last_y, last_z;
    float xy, xz; // êîëè÷åñòâî óçëîâûõ òî÷åê, ïðè ïðîõîæäåíèè îò "y" ê "x"
    // òèï float ïîâûøàåò òî÷íîñòü ðèñîâàíèÿ (íà ïðÿìûõ, ïðîõîäÿùèõ
    // íå ÷åðåç óçëû), íî èñïîëüçóåò ROM íà 7% áîëüøåì, ÷åì òèï char

    if (inrange(z1, y1, x1) & inrange(z2, y2, x2)) {
        // Ìû âñåãäà ðèñóåì ëèíèè îò x=0 ê x=7
        // Åñëè x1 áîëüøå x2, òî íåîáõîäèìî ïîìåíÿòü êîîðäèíàòû ìåñòàìè
        if (x1>x2) {
            char tmp;
            tmp=x2;
            x2=x1;
            x1=tmp;
            tmp=y2;
            y2=y1;
            y1=tmp;
            tmp=z2;
            z2=z1;
            z1=tmp;
        }

        if (y1>y2) {
            xy=(float) (y1 - y2) / (float) (x2 - x1);
            last_y=y2;
        } else {
            xy=(float) (y2 - y1) / (float) (x2 - x1);
            last_y=y1;
        }

        if (z1>z2) {
            xz=(float) (z1 - z2) / (float) (x2 - x1);
            last_z=z2;
        } else {
            xz=(float) (z2 - z1) / (float) (x2 - x1);
            last_z=z1;
        }
        for (x=x1; x<=x2; x++) {
            y=(xy * (x - x1)) + y1;
            z=(xz * (x - x1)) + z1;
            setvoxel(z, y, x);
        }
    }
}

void box_filled(uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2) {
    uint8_t iz, iy, ix;

    argorder(x1, x2, &x1, &x2);
    argorder(y1, y2, &y1, &y2);
    argorder(z1, z2, &z1, &z2);

    for (iz=z1; iz<=z2; iz++) {
        for (iy=y1; iy<=y2; iy++) {
            for (ix=x1; ix<=x2; ix++) {
                cube[iz][iy][ix]=1;
            }
        }
    }
}


/*..................Hiệu ứng rainbow.............................................*/
rgb_t Scroll(int pos) {
	rgb_t color = { .r = 255, .g = 0, .b = 0 };
	if(pos < 85) {
		color.g = 0;
		color.r = ((float)pos / 85.0f) * 255.0f;
		color.b = 255 - color.r;
	} else if(pos < 170) {
		color.g = ((float)(pos - 85) / 85.0f) * 255.0f;
		color.r = 255 - color.g;
		color.b = 0;
	} else if(pos < 256) {
		color.b = ((float)(pos - 170) / 85.0f) * 255.0f;
		color.g = 255 - color.b;
		color.r = 1;
	}
	return color;
}
void change_rgb_color()
{
    static char scroll;
    rgb_t rgb = Scroll(scroll);
    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
    scroll++;
}

void effect_rainbow(int speed)
{
    for(int j = 0; j < 256; j++) {
		for(int i = 0; i < 512; i++) {
			layer_buffer[i] = Scroll((i * 256 / 512 + j) % 256);  
           // delay_ms(3);    
		} 
        delay_ms(speed);
	} 

}
/*..................Hiệu ứng mưa.............................................*/
void effect_rain(int speed, int direction, int iterations) {
    int i, ii;
    int rnd_x;
    int rnd_y;
    int rnd_num;
    static char scroll = 0;
    
    clear_layer_buffer();
    for (ii=0; ii< iterations; ii++) {
       
        rnd_num = random_number(0,7);
        for (i=0; i<rnd_num; i++) {
            rnd_x=random_number(0,7) ;
            rnd_y=random_number(0,7) ;
            setvoxel(7, rnd_y, rnd_x);
        }
        delay_ms(speed);
        shift(1, direction);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
        
    }
}


/*.................Hiệu ứng đổi màu side.....................................*/
static rgb_t eff_side_change_color_buffer[13] = {
    (rgb_t) { .r = 255, .g = 0, .b = 0 },
    (rgb_t) { .r = 255, .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 255 },
    (rgb_t) { .r = 0, .g = 0, .b = 255 } ,
    (rgb_t) { .r = 255, .g = 0, .b = 255 } ,
    (rgb_t) { .r = 255, .g = 0, .b = 0 },
    (rgb_t) { .r = 255, .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 255 },
    (rgb_t) { .r = 0, .g = 0, .b = 255 }  ,
    (rgb_t) { .r = 0, .g = 255, .b = 100 } ,
    (rgb_t) { .r = 255, .g = 255, .b = 255 }  
};
void eff_side_change_color()
{
    static uint8_t k = 0;
    if(k%3 == 0)
    {
        for(int z = 0; z <= CUBE_SIZE; z ++)
        {
            for(int x = 0; x <= CUBE_SIZE; x ++)
            {
                for(int y = 0; y <= CUBE_SIZE; y ++)
                {
                    set_color_to_buffer(z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                } 
            }
            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
    else if(k%3 == 1)
    {
        for(int z = CUBE_SIZE; z >= 0; z --)
        {
            for(int x = 0; x <= CUBE_SIZE; x ++)
            {
                for(int y = 0; y <= CUBE_SIZE; y ++)
                {
                    set_color_to_buffer(z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                } 
            }
            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
    else if(k%3 == 2)
    {
        for(int x = CUBE_SIZE; x >= 0; x --)
        {
            for(int z = CUBE_SIZE; z >= 0; z --)
            {
                for(int y = 0; y <= CUBE_SIZE; y ++)
                {
                    set_color_to_buffer(z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                } 
            }
            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
    
    k++;
    if(k > 12)  k = 0;
       
}

/*.......................Hiệu ứng planboing*/
#define AXIS_Z 1
#define AXIS_Y 2
#define AXIS_X 3
void effect_planboing(char axis, int speed) {
    int i;
    static char scroll = 0;

    for (i=0; i<CUBE_SIZE; i++) {
         fill_cube(0x00);
        setplane(axis, i);
        delay_ms(speed);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
    }
    for (i=(CUBE_SIZE - 1); i>=0; i--) {
         fill_cube(0x00);
        setplane(axis, i);
        delay_ms(speed);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
    }
}

/*.......................Hiệu ứng Send voxel rand..............................*/
void effect_sendvoxels_rand_axis(int iterations, char axis, char delay, char wait) {
    unsigned char x, y, z, i, last_x=0, last_y=0;
    fill_cube(0);
    static char scroll;
    if (axis==1)
    {
        // for (x=0; x<CUBE_SIZE; x++) {
        //     for (y=0; y<CUBE_SIZE; y++) {
        //         setvoxel(((rand() % 2)*(CUBE_SIZE - 1)), y, x);
        //     }
        // }
        for (i=0; i<iterations; i++) {
            x=rand() % CUBE_SIZE;
            y=rand() % CUBE_SIZE;
            if (y != last_y && x != last_x) {
                if (getvoxel(0, y, x)) {
                    sendvoxel_axis(0, y, x, axis, delay);
                } else {
                    sendvoxel_axis((CUBE_SIZE - 1), y, x, axis, delay);
                }
                delay_ms(wait);
                rgb_t rgb = Scroll(scroll);
                set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                scroll++;
        
                last_y=y;
                last_x=x;
            }
        }
    }

    if (axis==2) {
        // for (x=0; x<CUBE_SIZE; x++) {
        //     for (y=0; y<CUBE_SIZE; y++) {
        //         setvoxel(y, ((rand() % 2)*(CUBE_SIZE - 1)), x);
        //     }
        // }
        for (i=0; i<iterations; i++) {
            x=rand() % CUBE_SIZE;
            y=rand() % CUBE_SIZE;
            if (y != last_y && x != last_x) {

                if (getvoxel(y, 0, x)) {
                    sendvoxel_axis(y, 0, x, axis, delay);
                } else {
                    sendvoxel_axis(y, (CUBE_SIZE - 1), x, axis, delay);
                }
                delay_ms(wait);
                rgb_t rgb = Scroll(scroll);
                set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                scroll++;
                last_y=y;
                last_x=x;
            }
        }
    }

    if (axis==3) {
        // for (x=0; x<CUBE_SIZE; x++) {
        //     for (y=0; y<CUBE_SIZE; y++) {
        //         setvoxel(y, x, ((rand() % 2)*(CUBE_SIZE - 1)));
        //     }
        // }
        for (i=0; i<iterations; i++) {
            x=rand() % CUBE_SIZE;
            y=rand() % CUBE_SIZE;
            if (y != last_y && x != last_x) {
                if (getvoxel(y, x, 0)) {
                    sendvoxel_axis(y, x, 0, axis, delay);
                } else {
                    sendvoxel_axis(y, x, (CUBE_SIZE - 1), axis, delay);
                }
                delay_ms(wait);
                rgb_t rgb = Scroll(scroll);
                set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                scroll++;
                last_y=y;
                last_x=x;
            }
        }
    }
}

/*............................Hiệu ứng Box Shrinkgrow and Woopwoop ..........................*/
void effect_box_shrink_grow(int speed)
{
    int x, i, ii = 0, xyz;
    int iterations=1;
    int flip = (ii & (CUBE_SIZE / 2));
    static char scroll;

    for (ii=0; ii<CUBE_SIZE; ii++) {
        for (x=0; x<iterations; x++) {
            for (i=0; i<(CUBE_SIZE * 2); i++) {
                xyz=(CUBE_SIZE - 1) - i; 
                if (i>(CUBE_SIZE - 1))
                    xyz=i - CUBE_SIZE; 

                
                fill_cube(0x00);
                delay_ms(2);
                box_wireframe(0, 0, 0, xyz, xyz, xyz);

                if (flip>0) 
                    mirror_z();
                if (ii==(CUBE_SIZE - 3) || ii==(CUBE_SIZE - 1))
                    mirror_y();
                if (ii==(CUBE_SIZE - 2) || ii==(CUBE_SIZE - 1))
                    mirror_x();
                    
                delay_ms(speed);
                fill_cube(0x00);
                rgb_t rgb = Scroll(scroll);
                set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                scroll++;
                
                
            }
            
        }
        
    }
}
void effect_box_woopwoop(int delay, int grow) 
{
    int i, ii;
    char ci = CUBE_SIZE / 2;
    fill_cube(0x00);

    for (i=0; i<ci; i++) {
        ii=i;
        if (grow>0)
            ii=(ci - 1) - i;

        box_wireframe(ci + ii, ci + ii, ci + ii, (ci - 1) - ii, (ci - 1) - ii, (ci - 1) - ii);
        delay_ms(delay);
        fill_cube(0x00);
    }
}


/*....................................Hiệu ứng Axis updown rand suppend............................................*/
void effect_axis_updown_randsuspend(char axis, char delay, int sleep, char invert) {
    unsigned char positions[CUBE_SIZE * CUBE_SIZE];
    unsigned char destinations[CUBE_SIZE * CUBE_SIZE];
    int i, px;
    static char scroll;
    fill_cube(0x00);

    // Set 64 random positions
    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        positions[i]=0; // Set all starting positions to 0
        destinations[i]=rand() % CUBE_SIZE;
    }

    // Loop 8 times to allow destination 7 to reach all the way
    for (i=0; i<CUBE_SIZE; i++) {
        // For every iteration, move all position one step closer to their destination
        for (px=0; px<(CUBE_SIZE * CUBE_SIZE); px++) {
            if (positions[px]<destinations[px]) {
                positions[px]++;
            }
        }
        // Draw the positions and take a nap
        draw_positions_axis(axis, positions, invert);
        delay_ms(delay);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
                
    }

    // Set all destinations to 7 (opposite from the side they started out)
    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        destinations[i]=CUBE_SIZE - 1;
    }

    // Suspend the positions in mid-air for a while
    delay_ms(sleep*2);

    // Then do the same thing one more time
    for (i=0; i<CUBE_SIZE; i++) {
        for (px=0; px<(CUBE_SIZE * CUBE_SIZE); px++) {
            if (positions[px]<destinations[px]) {
                positions[px]++;
            }
            if (positions[px]>destinations[px]) {
                positions[px]--;
            }
        }
        draw_positions_axis(axis, positions, invert);
        delay_ms(delay);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
                
       
    }

    // Suspend the positions in top-bottom for a while
     delay_ms(sleep*2);
}

/*..............................Hiệu ứng ripple.............................*/
int totty_sin(unsigned char LUT[65],int sin_of)
{
    unsigned char inv=0;
    if (sin_of<0)
    {
        sin_of=-sin_of;
        inv=1;
    }
    sin_of&=0x7f;  // 127
    if (sin_of>64)
    {
        sin_of-=64;
        inv=1-inv;
    }
    if (inv)
        return -LUT[sin_of];
    else
        return LUT[sin_of];
}

int totty_cos(unsigned char LUT[65],int cos_of)
{
    unsigned char inv=0;
    cos_of+=32;    // Simply rotate by 90 degrees for COS
    cos_of&=0x7f;  // 127
    if (cos_of>64)
    {
        cos_of-=64;
        inv=1;
    }
    if (inv)
        return -LUT[cos_of];
    else
        return LUT[cos_of];
}
void init_LUT(unsigned char LUT[65])
{
    unsigned char i;
    float sin_of,sine;
    for (i=0; i<65; i++)
    {
        sin_of=i*3.14/64; // Just need half a sin wave
        sine=sin(sin_of);
        // Use 181.0 as this squared is <32767, so we can multiply two sin or cos without overflowing an int.
        LUT[i]=sine*181.0;
    }
}



void effect_int_ripples(int iterations, int delay) {
	// 16 values for square root of a^2+b^2.  index a*4+b=10*sqrt
	// This gives the distance to 3.5,3.5 from the point
	unsigned char sqrt_LUT[]={49,43,38,35,43,35,29,26,38,29,21,16,35,25,16,7};
	// LUT_START // Macro from new tottymath.  Commented and replaced with full code
	unsigned char LUT[65];
	init_LUT(LUT);
	int i;
	unsigned char x,y,height,distance;
	for (i=0; i<iterations; i++)
	{
		fill_cube(1);
		for (x=0; x<CUBE_SIZE; x++)
        {
            for(y=0; y<CUBE_SIZE; y++)
			{
				// x+y*4 gives no. from 0-15 for sqrt_LUT
				distance=sqrt_LUT[x+y*CUBE_SIZE];  // distance is 0-50 roughly
				// height is sin of distance + iteration*4
				// height=4+totty_sin(LUT,distance+i)/49;
				height=(196+totty_sin(LUT,distance+i))/49;
				// Use 4-way mirroring to save on calculations
				clrvoxel(x,y,height);
				clrvoxel((CUBE_SIZE-1)-x,y,height);
				clrvoxel(x,(CUBE_SIZE-1)-y,height);
				clrvoxel((CUBE_SIZE-1)-x,(CUBE_SIZE-1)-y,height);
			}
            
        }delay_ms(delay);
            change_rgb_color();
			
	}
}

/*.........................Hiệu ứng sidewaves...................................*/

void effect_int_sidewaves(int iterations, int delay, uint8_t pickcolor) {
	unsigned char LUT[65];
	int i;
	int origin_x, origin_y, distance, height;
	int x_dist,x_dist2,y_dist;
	int x,y,x_vox;
    static char scroll;
	init_LUT(LUT);

	for (i=0; i<iterations; i++)
	{	// To provide some finer control over the integer calcs the x and y
		// parameters are scaled up by a factor of 15.  This is primarily to
		// keep the sum of their squares within the scale of an integer.
		// 120^2 + 120^2=28800
		// If we scaled by 16 we would overflow at the very extremes,
		// e.g 128^2+128^2=32768.  The largest int is 32767.
		//
		// Because origin_x/y is a sin/cos pair centred at 60, the actual
		// highest distance in this effect would be at:
		// x=8,y=8, origin_x=102, origin_y=102
		//=approximate sum of 17400.
		//
		// It is probably safer to work at a scale of 15 to allow simple changes
		// to the maths to be made without risking an overflow.
		origin_x=(totty_sin(LUT,i/2)+180)/3;  // Approximately 0 to 120
		origin_y=(totty_cos(LUT,i/2)+180)/3;
		fill_cube(0x00);
		for (x=8; x<120; x+=15)// 8 steps from 8 to 113
		{
			// Everything in here happens 8 times per cycle
			x_dist=abs(x-origin_x);
			x_dist2=x_dist*x_dist;  // square of difference
			x_vox=x/15;  // Unscale x
			for (y=8; y<120; y+=15)
			{
				// Everything in here happens 64 times per cycle
				y_dist=abs(y-origin_y);
				if (x_dist||y_dist)  // Either x OR y non-zero
				{
					// Calculate sum of squares of linear distances
					// We use a 1st order Newton approximation:
					// sqrt=(N/guess+guess)/2
					distance=(x_dist2+y_dist*y_dist);
					height=(x_dist+y_dist)/2;  // Approximate quotient
					// We divide by 30.  1st approx would be /2
					// but we have a factor of 15 included in our scale calcs
					distance=(distance/height+height)/3; // 1st approx at sqrt
				}
				else
					distance=0;  // x and y=origin_x and origin_y
				height=(totty_sin(LUT,distance+i)+180)/49 ;
				setvoxel(height,y/15,x_vox);
                if(pickcolor == 0)
                {
                    if(height == 0)   set_color_setvoxel( 0,0, 255);
                    if(height == 1)   set_color_setvoxel( 15,0, 210);
                    if(height == 2)   set_color_setvoxel( 44,0, 180);
                    if(height == 3)   set_color_setvoxel( 70,0, 150);
                    if(height == 4)   set_color_setvoxel( 100,0, 130);
                    if(height == 5)   set_color_setvoxel( 130,0, 100);
                    if(height == 6)   set_color_setvoxel( 160,0, 80);
                    if(height == 7)   set_color_setvoxel( 200,0, 50);
                }
                else if(pickcolor == 1)
                {
                    if(height == 0)   set_color_setvoxel(0, 0, 255);
                    if(height == 1)   set_color_setvoxel(0, 15, 210);
                    if(height == 2)   set_color_setvoxel(0, 44, 180);
                    if(height == 3)   set_color_setvoxel(0, 70, 150);
                    if(height == 4)   set_color_setvoxel(0, 100, 130);
                    if(height == 5)   set_color_setvoxel(0, 130, 100);
                    if(height == 6)   set_color_setvoxel(0, 160, 80);
                    if(height == 7)   set_color_setvoxel(0, 200, 50);
                }
               
                
			}
		}
        if(pickcolor == 2)
        {
            rgb_t rgb = Scroll(scroll);
            set_color_setvoxel(rgb.r, rgb.g, rgb.b);
            scroll++;
        }
		delay_ms(delay);
	}
}


/*.............................Hieu ung firework.............................*/

void effect_fireworks(uint8_t iterations) {
    int i, f, z;
    int n=80; 
    int origin_x, origin_y, origin_z;
    float slowrate, gravity; 
    uint8_t particles[80][6]; 

    fill_cube(0x00);

    for (i=0; i<iterations; i++) {
        origin_x=rand() % 4 + 2; // (0..3)+2=2..5
        origin_y=rand() % 4 + 2; // (0..3)+2=2..5
        origin_z=1;

      
        for (z=0; z < CUBE_SIZE; z++) {
            setvoxel(z, origin_y, origin_x);
            if(z%2 == 0 && origin_x < CUBE_SIZE) origin_x = origin_x + random_number(-1,1);
            if(z%2 == 1 && origin_y < CUBE_SIZE) origin_y = origin_y + random_number(-1,1);
            delay_ms(100);
            fill_cube(0x00);
        }

        
        for (f=0; f<n; f++) {
            particles[f][0]=origin_x;
            particles[f][1]=origin_y;
            particles[f][2]=origin_z;

            particles[f][3]=1 - rand() % 3; // dx  //=1-(float)rand_x/100;
            particles[f][4]=1 - rand() % 3; // dy
            particles[f][5]=1 + rand() % 2; // dz
        }


        for (z=0; z<5; z++) 
        {
            gravity =tan((z+0.1)/20)/2;
            for (f=0; f<n; f++) 
            {
                particles[f][0] += particles[f][3] + (int)gravity;
                particles[f][1] += particles[f][4] + (int)gravity;
                particles[f][2] += particles[f][5] - rand() % 2 + (int)gravity;
                if(particles[f][2]==0) 
                {
                    particles[f][2]++;
                }
                setvoxel(particles[f][2], particles[f][1], particles[f][0]);
            }
            delay_ms(100);
            fill_cube(0x00);
        }
        delay_ms(600); 
    }
}

/*.........................Hiệu ứng wormsqueeze.............................*/
void effect_wormsqueeze(int axis, int direction, int iterations, int delay) {
    int x, y, i, j, k, dx, dy, size, cube_size;
    int origin=0;
    static char scroll;

    if (CUBE_SIZE==4)
        size=1;
    if (CUBE_SIZE==8)
        size=2;

    if (direction==-1)
        origin=(CUBE_SIZE - 1);

    cube_size=CUBE_SIZE - (size - 1);

    x=rand() % CUBE_SIZE;
    y=rand() % CUBE_SIZE;

    for (i=0; i<iterations; i++) {
        dx=((rand() % (CUBE_SIZE / 2 - 1)) - 1); // %3
        dy=((rand() % (CUBE_SIZE / 2 - 1)) - 1); // %3

        if ((x + dx)>0 && (x + dx)<CUBE_SIZE)
            x += dx;

        if ((y + dy)>0 && (y + dy)<CUBE_SIZE)
            y += dy;

        shift(axis, direction);

        for (j=0; j<size; j++) {
            for (k=0; k<size; k++) {
                if (axis==1) // AXIS_Z
                {
                    setvoxel(origin, y + k, x + j);
                    origin--;
                } // setvoxel(x+j,y+k,origin);

                if (axis==2) // AXIS_Y
                    setvoxel(y + k, origin, x + j); // setvoxel(x+j,origin,y+k);

                if (axis==3) // AXIS_X
                    setvoxel(x + k, y + j, origin); // setvoxel(origin,y+j,x+k);
                

                rgb_t rgb = Scroll(scroll);
                set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                scroll++;
            }
            
        }
       
        delay_ms(delay); 
    }
}



/*..............................Hiệu ứng boingboing..........................*/
void effect_boingboing(int iterations, char delay, unsigned char mode, unsigned char drawmode) {
    uint8_t x, y, z; // Current coordinates for the point                  // mode=0x02;                 // íå ðàáîòàåò
    int dx, dy, dz; // Direction of movement                     // delay=4; mode=0x01; drawmode=0x01;  // òî÷êà, ïåðåìåùàþùàÿñÿ ïî êóáó
    int lol, i; // lol?                                      // delay=2; mode=0x01; drawmode=0x02;  // òî÷êà, ïåðåìåùàþùàÿñÿ ïî êóáó îñòÿâëÿþùàÿ ñëåä è çàïîëíÿþùàÿ âåñü êóá
    unsigned char crash_x, crash_y, crash_z; // delay=3; mode=0x01; drawmode=0x03;  // çìåéêà, ïåðåìåùàþùàÿñÿ ïî êóáó
    int snake[CUBE_SIZE][3];

    fill_cube(0x00); // Blank the cube

    y=rand() % CUBE_SIZE;
    x=rand() % CUBE_SIZE;
    z=rand() % CUBE_SIZE;

    // Coordinate array for the snake
    for (i=0; i<CUBE_SIZE; i++) {
        snake[i][0]=x;
        snake[i][1]=y;
        snake[i][2]=z;
    }

    dx=1;
    dy=1;
    dz=1;

    while (iterations) {
        crash_x=0;
        crash_y=0;
        crash_z=0;

        // Let's mix things up a little:
        if (rand() % 3==0) {
            // Pick a random axis, and set the speed to a random number.
            lol=rand() % 3;
            if (lol==0)
                dx=rand() % 3 - 1;

            if (lol==1)
                dy=rand() % 3 - 1;

            if (lol==2)
                dz=rand() % 3 - 1;
        }

        // The point has reached 0 on the x-axis and is trying to go to -1
        // aka a crash
        if (dx==-1 && x==0) {
            crash_x=0x01;
            if (rand() % 3==1) {
                dx=1;
            } else {
                dx=0;
            }
        }

        // y axis 0 crash
        if (dy==-1 && y==0) {
            crash_y=0x01;
            if (rand() % 3==1) {
                dy=1;
            } else {
                dy=0;
            }
        }

        // z axis 0 crash
        if (dz==-1 && z==0) {
            crash_z=0x01;
            if (rand() % 3==1) {
                dz=1;
            } else {
                dz=0;
            }
        }

        // x axis 7 crash
        if (dx==1 && x==(CUBE_SIZE - 1)) {
            crash_x=0x01;
            if (rand() % 3==1) {
                dx=-1;
            } else {
                dx=0;
            }
        }

        // y axis 7 crash
        if (dy==1 && y==(CUBE_SIZE - 1)) {
            crash_y=0x01;
            if (rand() % 3==1) {
                dy=-1;
            } else {
                dy=0;
            }
        }

        // z azis 7 crash
        if (dz==1 && z==(CUBE_SIZE - 1)) {
            crash_z=0x01;
            if (rand() % 3==1) {
                dz=-1;
            } else {
                dz=0;
            }
        }

        // mode bit 0 sets crash action enable
        if (mode | 0x01) {
            if (crash_x) {
                if (dy==0) {
                    if (y==(CUBE_SIZE - 1)) {
                        dy=-1;
                    } else if (y==0) {
                        dy=+1;
                    } else {
                        if (rand() % 2==0) {
                            dy=-1;
                        } else {
                            dy=1;
                        }
                    }
                }
                if (dz==0) {
                    if (z==(CUBE_SIZE - 1)) {
                        dz=-1;
                    } else if (z==0) {
                        dz=1;
                    } else {
                        if (rand() % 2==0) {
                            dz=-1;
                        } else {
                            dz=1;
                        }
                    }
                }
            }

            if (crash_y) {
                if (dx==0) {
                    if (x==(CUBE_SIZE - 1)) {
                        dx=-1;
                    } else if (x==0) {
                        dx=1;
                    } else {
                        if (rand() % 2==0) {
                            dx=-1;
                        } else {
                            dx=1;
                        }
                    }
                }
                if (dz==0) {
                    if (z==3) {
                        dz=-1;
                    } else if (z==0) {
                        dz=1;
                    } else {
                        if (rand() % 2==0) {
                            dz=-1;
                        } else {
                            dz=1;
                        }
                    }
                }
            }

            if (crash_z) {
                if (dy==0) {
                    if (y==(CUBE_SIZE - 1)) {
                        dy=-1;
                    } else if (y==0) {
                        dy=1;
                    } else {
                        if (rand() % 2==0) {
                            dy=-1;
                        } else {
                            dy=1;
                        }
                    }
                }
                if (dx==0) {
                    if (x==(CUBE_SIZE - 1)) {
                        dx=-1;
                    } else if (x==0) {
                        dx=1;
                    } else {
                        if (rand() % 2==0) {
                            dx=-1;
                        } else {
                            dx=1;
                        }
                    }
                }
            }
        }

        // mode bit 1 sets corner avoid enable       (CUBE_SIZE-1)
        if (mode | 0x02) {
            if (// We are in one of 8 corner positions
                    (x==0 && y==0 && z==0) ||
                    (x==0 && y==0 && z==(CUBE_SIZE - 1)) ||
                    (x==0 && y==(CUBE_SIZE - 1) && z==0) ||
                    (x==0 && y==(CUBE_SIZE - 1) && z==(CUBE_SIZE - 1)) ||
                    (x==(CUBE_SIZE - 1) && y==0 && z==0) ||
                    (x==(CUBE_SIZE - 1) && y==0 && z==(CUBE_SIZE - 1)) ||
                    (x==(CUBE_SIZE - 1) && y==(CUBE_SIZE - 1) && z==0) ||
                    (x==(CUBE_SIZE - 1) && y==(CUBE_SIZE - 1) && z==(CUBE_SIZE - 1))
                    ) {
                // At this point, the voxel would bounce
                // back and forth between this corner,
                // and the exact opposite corner
                // We don't want that!

                // So we alter the trajectory a bit,
                // to avoid corner stickyness
                lol=rand() % 3;
                if (lol==0)
                    dx=0;

                if (lol==1)
                    dy=0;

                if (lol==2)
                    dz=0;
            }
        }

        // one last sanity check
        if (x==0 && dx==-1)
            dx=1;

        if (y==0 && dy==-1)
            dy=1;

        if (z==0 && dz==-1)
            dz=1;

        if (x==(CUBE_SIZE - 1) && dx==1)
            dx=-1;

        if (y==(CUBE_SIZE - 1) && dy==1)
            dy=-1;

        if (z==(CUBE_SIZE - 1) && dz==1)
            dz=-1;


        // Finally, move the voxel.
        x=x + dx;
        y=y + dy;
        z=z + dz;

        // show one voxel at time (îäèí ñâåòîäèîä)
        if (drawmode==0x01)
        {
            setvoxel(z, y, x);
            delay_ms(delay);
            clrvoxel(z, y, x);
            change_rgb_color();
        }
        // flip the voxel in question (ïåðåäåëàíî â äâèæóùèéñÿ ñâåòîäèîä, îñòàâëÿþùèé ñëåä)
        if (drawmode==0x02)
        {
            // flpvoxel(z,y,x); // èñõîäíûé âàðèàíò
            setvoxel(z, y, x);
            delay_ms(delay);
            change_rgb_color();
        }
        // draw a snake (çìåéêà)
        if (drawmode==0x03)
        {
            for (i=(CUBE_SIZE - 1); i>=0; i--) {
                snake[i][0]=snake[i - 1][0];
                snake[i][1]=snake[i - 1][1];
                snake[i][2]=snake[i - 1][2];
            }
            snake[0][0]=x;
            snake[0][1]=y;
            snake[0][2]=z;

            for (i=0; i<CUBE_SIZE; i++) {
                setvoxel(snake[i][2], snake[i][1], snake[i][0]);
            }
            delay_ms(delay);
            change_rgb_color();
            for (i=0; i<CUBE_SIZE; i++) {
                clrvoxel(snake[i][2], snake[i][1], snake[i][0]);
            }
        }
        iterations--;
    }
}

/*..............................Hiệu ứng filler...............................*/

void effect_random_filler(int delay, char state) {
	char z,y,x;
	int loop;

	for (loop=0; loop<1500; loop++)  // CUBE_BYTES-3
	{
		z=rand()%CUBE_SIZE;
		y=rand()%CUBE_SIZE;
        x=rand()%CUBE_SIZE;
		if ((state==0 && getvoxel(z,y,x)==0x01) || (state==1 && getvoxel(z,y,x)==0x00))
			altervoxel(z,y,x,state);
       delay_ms(delay);
       change_rgb_color();
	}
}



/*...........................................................................*/

#define millis()  esp_log_timestamp()

void trans_effect()
{
    for(int y = 0; y < CUBE_SIZE  ; y++)
    {
        for(int x = 0; x < CUBE_SIZE; x++)
        {
            for(int z = 0; z < CUBE_SIZE; z++)
             {
                clrvoxel(z,y,x);
             }
        }
        delay_ms(200);
    }
}

void cube_run(void *pvParameters)
{
    // Khởi tạo strip
    ESP_ERROR_CHECK(led_strip_init(&strip));

    set_brightness(5);
    ESP_ERROR_CHECK(led_strip_flush(&strip));


  //set_color_to_buffer(2,0,0, (rgb_t){ .r = 255, .g = 0, .b = 0 });
    show_led();
    set_color_to_buffer(7,7,7,(rgb_t) { .r = 255, .g = 0, .b = 0 });
    fill_cube(0x00);

    while (1)
    {
       
        // effect_rain(50,0,1000);   // 1 ok 

        // // effect_rainbow(15);       //2 ok 

        // effect_planboing(AXIS_Z,50); //3
        // effect_planboing(AXIS_X,50);
        // effect_planboing(AXIS_Y,50);
        // effect_planboing(AXIS_Z,50);
        // effect_planboing(AXIS_X,50);
        // effect_planboing(AXIS_Y,50);

        // effect_sendvoxels_rand_axis(200, AXIS_Z, 10, 500); //4

        // effect_sendvoxels_rand_axis(200, AXIS_Y, 10, 500); //5

        // effect_sendvoxels_rand_axis(200, AXIS_X, 10, 500); //6

        // effect_box_shrink_grow(50);  //7
        // effect_box_woopwoop(50, 0);
        // effect_box_woopwoop(50, 1);
        // effect_box_woopwoop(50, 0);
        // effect_box_woopwoop(50, 1);
        // effect_box_woopwoop(50, 0);
        // effect_box_woopwoop(50, 1);
        // effect_box_woopwoop(50, 0);

        // effect_axis_updown_randsuspend(3, 50, 0, 0);  // 8(char axis, int delay, int sleep, int invert) x
        // effect_axis_updown_randsuspend(3, 50, 0, 1);

        // effect_axis_updown_randsuspend(1, 50, 0, 0);  // 9(char axis, int delay, int sleep, int invert) z
        // effect_axis_updown_randsuspend(1, 50, 0, 1);

        // effect_axis_updown_randsuspend(2, 50, 0, 0);  // 10(char axis, int delay, int sleep, int invert) y
        // effect_axis_updown_randsuspend(2, 50, 0, 1);

        // effect_int_sidewaves(2500,10,2);  // 11

        // effect_int_sidewaves(2500,10,2);  // 12

        // effect_int_sidewaves(2500,10,2);  // 13



        // effect_wormsqueeze(2, 0, 200, 50); //14
        // effect_wormsqueeze(3, 1, 200, 50);


        effect_boingboing(500, 40, 0x01, 0x01); //15        
        effect_boingboing(500, 30, 0x01, 0x03);
        effect_boingboing(500, 20, 0x01, 0x02); 


        // effect_random_filler(20,1);  // 16
        // effect_random_filler(20,0);
        // effect_random_filler(20,1);
        // effect_random_filler(20,0);






   //     effect_int_ripples(1000,50);
       

    //  effect_fireworks(5);


      
         
       
    }

}

void timer_interrupt()
{
    s_timer_queue = xQueueCreate(10, sizeof(example_timer_event_t));

    example_tg_timer_init(TIMER_GROUP_0, TIMER_0, true, 3);

    while (1) {
        example_timer_event_t evt;
        xQueueReceive(s_timer_queue, &evt, portMAX_DELAY);
        show_led();
        
        // /* Print the timer values as visible by this task */
        // printf("-------- TASK TIME --------\n");
        // uint64_t task_counter_value;
        // timer_get_counter_value(evt.info.timer_group, evt.info.timer_idx, &task_counter_value);
        // print_timer_counter(task_counter_value);
    }
}

void app_main()
{
    led_strip_install();
    xTaskCreatePinnedToCore(cube_run, "cube_run",8192, NULL, 5, NULL,1);
    xTaskCreatePinnedToCore(timer_interrupt, "timer_interrupt", 8192, NULL, 5, NULL,0);

    
  


}

