#ifndef WIFI_CONNECT_H
#define WIFI_CONNECT_H

void wifi_sta_connect(const char *ssid, const char *password);

#endif