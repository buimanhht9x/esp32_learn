/* Captive Portal Example

    This example code is in the Public Domain (or CC0 licensed, at your option.)

    Unless required by applicable law or agreed to in writing, this
    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
    CONDITIONS OF ANY KIND, either express or implied.
*/

#include <sys/param.h>

#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"

#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_netif.h"
#include "lwip/inet.h"

#include "esp_http_server.h"
#include "dns_server.h"
//#include "wifi_softap_captive_portal.h"
#include "wifi_sta_connect.h"

#define WIFI_AP_SSID  "esp32"
#define WIFI_AP_PASS  "12345678"
#define MAX_STA_CONN  5

// wifi_config_t wifi_config_init  = {
//     .ap = {
//         .ssid = WIFI_AP_SSID,
//         .ssid_len = strlen(WIFI_AP_SSID),
//         .password = WIFI_AP_PASS,
//         .max_connection = MAX_STA_CONN,
//         .authmode = WIFI_AUTH_WPA_WPA2_PSK
//     },
// };

// void wifi_sta_info_event_callback(char* ssidpass)
// {
//     printf("%s\n",ssidpass);
//     const char *ssid = strtok(ssidpass, "/");
//     const char *pass = strtok(NULL, "/");
//     printf("ssid : %s\n",ssid);
//     printf("pass : %s\n",pass);
//     wifi_sta_connect(ssid, pass);
//     stop_dns_server();
// }

void app_main(void)
{
  // wifi_softap_captive_portal_init(wifi_config_init);
  // wifi_softap_captive_portal_start();

  // wifi_sta_info_set_callback(wifi_sta_info_event_callback);
   wifi_sta_connect("Tplink 2.4 ghz", "hoianhson");
}
