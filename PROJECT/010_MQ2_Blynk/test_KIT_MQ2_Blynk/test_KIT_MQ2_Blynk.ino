#define BLYNK_PRINT Serial
// You should get Auth Token in the Blynk App.
#define BLYNK_TEMPLATE_ID           "TMPL6VHV8uDUc"
#define BLYNK_TEMPLATE_NAME         "testBlynkMQ2"
#define BLYNK_AUTH_TOKEN            "CYkUpAZ1Pc-3JGPrtCZKBlZu4JyB3Qsk"

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <LiquidCrystal.h>
#include "config.h"
#include "mybutton.h"
//-------------------- Khai báo Button-----------------------------
// Khai báo chân các nút nhấn
#define buttonPinMENU    21
#define buttonPinDOWN    19
#define buttonPinUP      18
#define buttonPinONOFF   5
#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
#define BUTTON4_ID  4
Button buttonMENU;
Button buttonDOWN;
Button buttonUP;
Button buttonONOFF;
void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);

//----------------------Khai báo LCD1602---------------------------
// Create An LCD Object. Signals: [ RS, EN, D4, D5, D6, D7 ]
#define LCD_RS  15
#define LCD_EN  13
#define LCD_D4  12
#define LCD_D5  14
#define LCD_D6  27
#define LCD_D7  26
LiquidCrystal My_LCD(15, 13, 12, 14, 27, 26);

//------------------- Khai báo cảm biến ----------------------------
#define SENSOR_MQ2  34
#define SENSOR_IR   35





void TaskButton(void *pvParameters);
void TaskBlynk(void *pvParameters);
void TaskMainDisplay(void *pvParameters);
void LCD1602_Init();

void setup() {  

  Serial.begin(115200);
  pinMode(2, OUTPUT);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");  
  delay(500);

  // ---------- Khai báo hàm FreeRTOS ---------
  xTaskCreatePinnedToCore(TaskButton, "TaskButton" ,  1024*4 ,  NULL,   5 ,  NULL ,  0);
  xTaskCreatePinnedToCore(TaskBlynk, "TaskBlynk" ,  1024*4 ,  NULL,  5 ,  NULL ,  0);
  xTaskCreatePinnedToCore(TaskMainDisplay, "TaskMainDisplay" ,  1024*4 ,  NULL,  5 ,  NULL ,  0);

  //----------- Khởi tạo LCD ------------------
  LCD1602_Init();
  delay(500);
}


void loop(){
     vTaskDelete(NULL);
}
//----------------LCD1602 Init------------------------
void LCD1602_Init()
{
   My_LCD.begin(16, 2);
   My_LCD.clear();
   My_LCD.print("Gas alarm system");
   My_LCD.setCursor(2, 1);
   My_LCD.print("BANLINHKIEN");
}

void TaskMainDisplay(void *pvParameters) 
{
    while(1)
    {
         float MQ2_Value = analogRead(SENSOR_MQ2);
         MQ2_Value = (MQ2_Value/4096)*100;
         My_LCD.clear();
         My_LCD.setCursor(2, 1);
         My_LCD.print(String(MQ2_Value) + "%");
         vTaskDelay(2000/portTICK_PERIOD_MS);
    }
}

//----------------Task Blynk---------------------------

BlynkTimer timer; 
void myTimer() 
{
  // This function describes what will happen with each timer tick
  // e.g. writing sensor value to datastream V5
  int sensorVal = analogRead(34);
  Blynk.virtualWrite(V0, sensorVal);  
}

BLYNK_WRITE(V1)
{
  int pinLED = param.asInt();
  digitalWrite(2,pinLED);
}

void TaskBlynk(void *pvParameters) 
{
   Blynk.config(BLYNK_AUTH_TOKEN);
   timer.setInterval(1000L, myTimer); 
   Blynk.connect();
   while(1)
   {
      Blynk.run();
      timer.run();
      vTaskDelay(10/portTICK_PERIOD_MS);
   }
}

//----------------Task Button---------------------------
void TaskButton(void *pvParameters) 
{
   // ---------- Khởi tạo BUTTON --------------
   button_init(&buttonMENU, buttonPinMENU, BUTTON1_ID);
   button_init(&buttonDOWN, buttonPinDOWN, BUTTON2_ID);
   button_init(&buttonUP,   buttonPinUP,   BUTTON3_ID);
   button_init(&buttonONOFF,buttonPinONOFF,BUTTON4_ID);
   button_pressshort_set_callback((void *)button_press_short_callback);
   button_presslong_set_callback((void *)button_press_long_callback);
   // -----------------------------------------
   while(1)
   {
      handle_button(&buttonMENU);
      handle_button(&buttonDOWN);
      handle_button(&buttonUP);
      handle_button(&buttonONOFF);
      vTaskDelay(10/portTICK_PERIOD_MS);
   }
}

//-----------------Hàm xử lí nút nhấn nhả ----------------------
void button_press_short_callback(uint8_t button_id)
{
  switch(button_id)
   {
      case BUTTON1_ID :
        Serial.println("button1 press short");
        break;
      case BUTTON2_ID :
        Serial.println("button2 press short");
        break;
      case BUTTON3_ID :
        Serial.println("button3 press short");
        break;  
      case BUTTON4_ID :
        Serial.println("button4 press short");
        break;     
   } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id)
{
    switch(button_id)
   {
      case BUTTON1_ID :
        Serial.println("button1 press long");
        break;
      case BUTTON2_ID :
        Serial.println("button2 press long");
        break;
      case BUTTON3_ID :
        Serial.println("button3 press long");
        break;  
      case BUTTON4_ID :
        Serial.println("button4 press long");
        break; 
   }   
}
