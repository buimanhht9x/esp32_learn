#include <LiquidCrystal.h>
#include <ESP32Servo.h>


Servo myservo1; 
Servo myservo2; 

#define SERVO1 33
#define SERVO2 25

#define LCD_RS  15
#define LCD_EN  13
#define LCD_D4  12
#define LCD_D5  14
#define LCD_D6  27
#define LCD_D7  26

#define SIM800L_Tx  16
#define SIM800L_Tx  17

#define BUTTON_SET    21
#define BUTTON_UP     19
#define BUTTON_DOWN   18
#define BUTTON_ONOFF  5

#define RELAY1  22
#define RELAY2  32

#define SENSOR_MQ2  34
#define SENSOR_IR   35

//Create software serial object to communicate with SIM800L

// Create An LCD Object. Signals: [ RS, EN, D4, D5, D6, D7 ]
LiquidCrystal My_LCD(15, 13, 12, 14, 27, 26);
