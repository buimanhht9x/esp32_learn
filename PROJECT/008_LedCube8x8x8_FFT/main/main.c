#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "freertos/semphr.h"

#include <led_strip_idf.h>
#include "math.h"
#include "esp_log.h"

#include "wifi_sta_connect.h"
#include "nvs_flash.h"

#include "esp_http_server.h"
#include "driver/i2s.h"


// Khai báo file html
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");

#define millis()  esp_log_timestamp()
#define sec()  esp_log_timestamp()/1000

/*........................................Timer.......................................*/
#include "driver/timer.h"

#define TIMER_DIVIDER         (16)  //  Hardware timer clock divider
#define TIMER_SCALE           5000 // convert counter value to seconds

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
    bool auto_reload;
} example_timer_info_t;

/**
 * @brief A sample structure to pass events from the timer ISR to task
 *
 */
typedef struct {
    example_timer_info_t info;
    uint64_t timer_counter_value;
} example_timer_event_t;

static xQueueHandle s_timer_queue;

/*
 * A simple helper function to print the raw timer counter value
 * and the counter value converted to seconds
 */
static void inline print_timer_counter(uint64_t counter_value)
{
    printf("Counter: 0x%08x%08x\r\n", (uint32_t) (counter_value >> 32),
           (uint32_t) (counter_value));
    printf("Time   : %.8f s\r\n", (double) counter_value / TIMER_SCALE);
}

static bool IRAM_ATTR timer_group_isr_callback(void *args)
{
    BaseType_t high_task_awoken = pdFALSE;
    example_timer_info_t *info = (example_timer_info_t *) args;

    uint64_t timer_counter_value = timer_group_get_counter_value_in_isr(info->timer_group, info->timer_idx);

    /* Prepare basic event data that will be then sent back to task */
    example_timer_event_t evt = {
        .info.timer_group = info->timer_group,
        .info.timer_idx = info->timer_idx,
        .info.auto_reload = info->auto_reload,
        .info.alarm_interval = info->alarm_interval,
        .timer_counter_value = timer_counter_value
    };

    if (!info->auto_reload) {
        timer_counter_value += info->alarm_interval * TIMER_SCALE;
        timer_group_set_alarm_value_in_isr(info->timer_group, info->timer_idx, timer_counter_value);
    }

    /* Now just send the event data back to the main program task */
    xQueueSendFromISR(s_timer_queue, &evt, &high_task_awoken);

    return high_task_awoken == pdTRUE; // return whether we need to yield at the end of ISR
}

/**
 * @brief Initialize selected timer of timer group
 *
 * @param group Timer Group number, index from 0
 * @param timer timer ID, index from 0
 * @param auto_reload whether auto-reload on alarm event
 * @param timer_interval_sec interval of alarm
 */
static void example_tg_timer_init(int group, int timer, bool auto_reload, int timer_interval_sec)
{
    /* Select and initialize basic parameters of the timer */
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = auto_reload,
    }; // default clock source is APB
    timer_init(group, timer, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(group, timer, 0);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(group, timer, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(group, timer);

    example_timer_info_t *timer_info = calloc(1, sizeof(example_timer_info_t));
    timer_info->timer_group = group;
    timer_info->timer_idx = timer;
    timer_info->auto_reload = auto_reload;
    timer_info->alarm_interval = timer_interval_sec;
    timer_isr_callback_add(group, timer, timer_group_isr_callback, timer_info, 0);

    timer_start(group, timer);
}
/*..................................................................................*/


////////////////////////////

#define LED_TYPE LED_STRIP_WS2812
#define LED_LENGTH 512
#define LED_GPIO 5
#define LED_STRIP_BRIGHTNESS 50
#define CUBE_SIZE 8
///////////////////////////////////////////////////////////////
typedef enum{
  INACTIVE = 0,
  STARTING = 1,
  RUNNING = 2,
  ENDING = 3
}state_t;

uint8_t restart_value = 0;

void delay_ms(int time_delay)
{
    vTaskDelay(time_delay/portTICK_RATE_MS);
}

int random_number(int minN, int maxN){
	return minN + rand() % (maxN + 1 - minN);
}
rgb_t layer_buffer[600];
uint8_t onoff_buffer[600];
uint8_t cube[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];
uint8_t buffer[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];

// cấu hình strip
led_strip_t strip = {
        .type = LED_TYPE,
        .length = LED_LENGTH,
        .gpio = LED_GPIO,
        .buf = NULL,
        #ifdef LED_STRIP_BRIGHTNESS
                .brightness = 50,
        #endif
        };

uint32_t map(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
void set_brightness(int percentage)
{
    if(percentage < 0 ) percentage = 0;
    else if( percentage > 100) percentage = 100;
    int value = map(percentage, 0,100,0,255);
    strip.brightness = value;
}



uint8_t inrange(int z, int y, int x) {
    if (x>=0 && x<CUBE_SIZE && y>=0 && y<CUBE_SIZE && z>=0 && z<CUBE_SIZE) {
        return 1;
    } else {
        return 0;
    }
}
void set_pixel_zyx(uint8_t z,uint8_t y, uint8_t x, rgb_t data)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            led_strip_set_pixel(&strip, z*8 + x*64 + y,  data);
        else if(z %2 == 1)
            led_strip_set_pixel(&strip, z*8 + x*64 + 7 - y,  data);
    }  
}
void clear_pixel_zyx(uint8_t z,uint8_t y, uint8_t x)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            led_strip_set_pixel(&strip, z*8 + x*64 + y,   (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 });
        else if(z %2 == 1)
            led_strip_set_pixel(&strip, z*8 + x*64 + 7 - y,   (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 });
    }
}

//......................SET, CLEAR, SHOW buffer layer......................................
void set_color_to_buffer(uint8_t z,uint8_t y, uint8_t x, rgb_t data)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            layer_buffer[z*8 + x*64 + y] =  data;
        else if(z %2 == 1)
            layer_buffer[z*8 + x*64 + 7 - y] =  data;
    }
}

void clear_color_in_buffer(uint8_t z,uint8_t y, uint8_t x)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            layer_buffer[z*8 + x*64 + y] =  (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
        else if(z %2 == 1)
            layer_buffer[z*8 + x*64 + 7 - y] =  (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
    }
}
void clear_layer_buffer()
{
    for(int i = 0; i<512; i++)
    {
        layer_buffer[i] = (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
    }
}

void show_led()
{
    ESP_ERROR_CHECK(led_strip_set_pixels(&strip, 0, 512, layer_buffer));
    ESP_ERROR_CHECK(led_strip_flush(&strip));
}


//......................SET, CLEAR, READ buffer ONOFF......................................
void clear_onoff_buffer()
{
    for(int i = 0; i<512; i++)
    {
        onoff_buffer[i] = 0;
    }
}
void set_onoff_buffer(uint8_t z, uint8_t y, uint8_t x) 
{
     if(inrange(z,y,x))
    {
        if(z %2 == 0)
        {
            onoff_buffer[z*8 + x*64 + y] = 1;
        }   
        else if(z %2 == 1)
        {
            onoff_buffer[z*8 + x*64 + 7 - y] = 1;
        }
    }
}
uint8_t read_onoff_buffer(uint8_t z, uint8_t y, uint8_t x) 
{
    static uint8_t value;
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
        {
            value = onoff_buffer[z*8 + x*64 + y];
        }   
        else if(z %2 == 1)
        {
            value = onoff_buffer[z*8 + x*64 + 7 - y];
        }
        return value;
    }
    return 0;
}


/*..............................................................................*/

rgb_t rgb_setvoxel =  { .r = 255, .g = 0, .b = 0 };


void set_color_setvoxel(char rs, char gs, char bs)
{
    rgb_setvoxel.r = rs;
    rgb_setvoxel.g = gs;
    rgb_setvoxel.b = bs;
}
void setvoxel(uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)) {
        set_color_to_buffer(z,y,x,rgb_setvoxel);
        cube[z][y][x] = 1;
    }
}


void clrvoxel(uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)){
        set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
        cube[z][y][x] = 0;

    }
}

uint8_t getvoxel(uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)) {
        if (cube[z][y][x]==1) 
        {
            return 1;
        } 
        else
        {
            return 0;
        }
    } 
    else 
    {
        return 0;
    }
}

void setplane_x(uint8_t x) {
    uint8_t z, y;

    if (x>=0 && x<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++) {
                cube[z][y][x]=1;
                set_color_to_buffer(z,y,x,rgb_setvoxel);
            }
        }
    }
}
void clrplane_x(uint8_t x) {
    uint8_t z, y;

    if (x>=0 && x<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++) {
                cube[z][y][x]=0;
                set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
            }
        }
    }
}
void setplane_y(uint8_t y) {
    uint8_t z, x;

    if (y>=0 && y<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (x=0; x<CUBE_SIZE; x++) {
                cube[z][y][x]=1;
                set_color_to_buffer(z,y,x,rgb_setvoxel);
            }
        }
    }
}
void clrplane_y(uint8_t y) {
    uint8_t z, x;

    if (y>=0 && y<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (x=0; x<CUBE_SIZE; x++) {
                cube[z][y][x]=0x00;
                set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
            }
        }
    }
}

void fill_layer(uint8_t layer, uint8_t color) {
    uint8_t y, x;

    for (y=0; y<CUBE_SIZE; y++) {
        for (x=0; x<CUBE_SIZE; x++) {
            cube[layer][y][x]= color    ;
            set_color_to_buffer(layer,y,x,rgb_setvoxel);
        }
    }
}

void fill_cube(uint8_t colour) {
    uint8_t z, y, x;

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if(colour == 0)
                {
                     cube[z][y][x]=0;
                    set_color_to_buffer(z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
                }
                else
                {
                     cube[z][y][x]=1;
                    set_color_to_buffer(z,y,x,rgb_setvoxel);
                }
               
            }
        }
    }
}
void setplane(uint8_t axis, uint8_t i) {
    switch (axis) {
        case 1: //AXIS_Z:
            fill_layer(i, 1);
            break;
        case 2: //AXIS_Y:
            setplane_y(i);
            break;
        case 3: //AXIS_X:
            setplane_x(i);
            break;
    }
}

void clrplane(uint8_t axis, uint8_t i) {
    switch (axis) {
        case 1: //AXIS_Z:
            fill_layer(i, 0);
            break;
        case 2: //AXIS_Y:
            clrplane_y(i);
            break;
        case 3: //AXIS_X:
            clrplane_x(i);
            break;
    }
}

void altervoxel(uint8_t z, uint8_t y, uint8_t x, uint8_t state) {
    if (inrange(z, y, x)) {
        if (state==1) {
            setvoxel(z, y, x);
        }
        if (state==0) {
            clrvoxel(z, y, x);
        }
    }
}
void shift(char axis, char direction) {
    char z, y, x, ii, iii, state;

    if (axis==1) // AXIS_Z - 1
    {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) // direction=0 (-1) - ïî AXIS_Z ñïóñêàåòñÿ âíèç
                    { // direction=1 - ïî AXIS_Z ïîäûìàåòñÿ ââåðõ
                        ii=z;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - z;
                        iii=ii - 1;
                    }
                    state=getvoxel(iii, y, x);
                    altervoxel(ii, y, x, state);
                }
            }
        }
    }
    if (axis==2) // AXIS_Y - 2
    {
        for (z=0; z<CUBE_SIZE; z++) {
             for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) // direction=0 (-1) - ïî AXIS_Y äâèæåòñÿ âëåâî
                    { // direction=1 - ïî AXIS_Y äâèæåòñÿ âïðàâî
                        ii=(CUBE_SIZE - 1) - y;
                        iii=ii - 1;
                    } else {
                        ii=y;
                        iii=ii + 1;
                    }
                    state=getvoxel(z, iii, x);
                    altervoxel(z, ii, x, state);
                }
            }
        }
    }
    if (axis==3) // AXIS_X - 3
    {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) 
                    { 
                        ii=x;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - x;
                        iii=ii - 1;
                    }
                    state=getvoxel(z, y, iii);
                    altervoxel(z, y, ii, state);
                }
            }
        }
    }
    if (axis==4) // AXIS_X - 3
    {
        for (z=CUBE_SIZE-1; z>=0; z--) {
             for (y=CUBE_SIZE-1; y>=0; y--) {
                 for (x=CUBE_SIZE-1; x>=0; x--) {
                    if (direction==0) 
                    { 
                        ii=x;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - x;
                        iii=ii - 1;
                    }
                    state=getvoxel(z, y, iii);
                    altervoxel(z, y, ii, state);
                }
            }
        }
    }
}

void sendvoxel_axis(uint8_t z, uint8_t y, uint8_t x, char axis, int delay) {
    int i, ii;

    if (axis==1) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (z==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(ii + 1, y, x);
            } else {
                ii=i;
                clrvoxel(ii - 1, y, x);
            }
            setvoxel(ii, y, x);
            delay_ms(delay);
        }
    }

    if (axis==2) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (y==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(z, ii + 1, x);
            } else {
                ii=i;
                clrvoxel(z, ii - 1, x);
            }
            setvoxel(z, ii, x);
            delay_ms(delay);
        }
    }

    if (axis==3) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (x==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(z, y, ii + 1);
            } else {
                ii=i;
                clrvoxel(z, y, ii - 1);
            }
            setvoxel(z, y, ii);
             delay_ms(delay);
        }
    }
}
void argorder(uint8_t ix1, uint8_t ix2, uint8_t *ox1, uint8_t *ox2) {
    if (ix1>ix2) {
        char tmp;
        tmp=ix1;
        ix1=ix2;
        ix2=tmp;
    }
    *ox1=ix1;
    *ox2=ix2;
}
void box_wireframe(uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2) {
    uint8_t iz, iy, ix;

    argorder(x1, x2, &x1, &x2);
    argorder(y1, y2, &y1, &y2);
    argorder(z1, z2, &z1, &z2);

    for (iz=z1; iz<=z2; iz++) {
        for (iy=y1; iy<=y2; iy++) {
            for (ix=x1; ix<=x2; ix++) {
                if ((iz>=z1 && iz<=z2 && iy==y1 && ix==x1) || // ïðÿìàÿ (z1,y1,x1)-(z2,y1,x1)
                        (iz>=z1 && iz<=z2 && iy==y2 && ix==x1) || // ïðÿìàÿ (z1,y2,x1)-(z2,y2,x1)
                        (iz>=z1 && iz<=z2 && iy==y1 && ix==x2) || // ïðÿìàÿ (z1,y1,x2)-(z2,y1,x2)
                        (iz>=z1 && iz<=z2 && iy==y2 && ix==x2) || // ïðÿìàÿ (z1,y2,x2)-(z2,y2,x2)

                        (iz==z1 && iy==y1 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z1,y1,x1)-(z1,y1,x2)
                        (iz==z1 && iy==y2 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z1,y2,x1)-(z1,y2,x2)
                        (iz==z2 && iy==y1 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z2,y1,x1)-(z1,y1,x2)
                        (iz==z2 && iy==y2 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z2,y2,x1)-(z1,y2,x2)

                        (iz==z1 && iy>=y1 && iy<=y2 && ix==x1) || // ïðÿìàÿ (z1,y1,x1)-(z1,y2,x1)
                        (iz==z1 && iy>=y1 && iy<=y2 && ix==x2) || // ïðÿìàÿ (z1,y1,x2)-(z1,y2,x2)
                        (iz==z2 && iy>=y1 && iy<=y2 && ix==x1) || // ïðÿìàÿ (z2,y1,x1)-(z2,y2,x1)
                        (iz==z2 && iy>=y1 && iy<=y2 && ix==x2)) // ïðÿìàÿ (z2,y1,x2)-(z2,y2,x2)
                {
                    setvoxel(iz, iy, ix);
                    
                } else {
                    clrvoxel(iz, iy, ix);
                }
            }
        }
    }
}

void cube2buffer(void) {
    uint8_t z, y, x;

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                buffer[z][y][x]=cube[z][y][x];
            }
        }
    }
}

void mirror_z(void) {
    uint8_t z, y, x;
    cube2buffer(); // êîïèðóåò ñîäåðæèìîå êóáà â áóôåð
    fill_cube(0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (buffer[z][y][x]==1)
                    setvoxel(CUBE_SIZE - 1 - z, y, x);
            }
        }
    }
}


void mirror_y(void) {
    uint8_t z, y, x;
    cube2buffer(); 
    fill_cube(0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (buffer[z][y][x]==1)
                    setvoxel(z, CUBE_SIZE - 1 - y, x);
            }
        }
    }
}

void mirror_x(void) {
    uint8_t z, y, x;
    cube2buffer(); 
    fill_cube(0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (buffer[z][y][x]==1)
                    setvoxel(z, y, CUBE_SIZE - 1 - x);
            }
        }
    }
}

void draw_positions_axis(char axis, unsigned char positions[CUBE_SIZE*CUBE_SIZE], int invert) {
    int x, y, p;
    fill_cube(0x00);

    for (x=0; x<CUBE_SIZE; x++) {
        for (y=0; y<CUBE_SIZE; y++) {
            if (invert) {
                p=(CUBE_SIZE - 1) - positions[x * CUBE_SIZE + y];
            } else {
                p=positions[x * CUBE_SIZE + y];
            }
            if (axis==1) // AXIS_Z
                setvoxel(p, y, x); // (x,y,p)

            if (axis==2) // AXIS_Y
                setvoxel(y, p, x); // (x,p,y)

            if (axis==3) // AXIS_X
                setvoxel(x, y, p); // (p,y,x)
        }
    }
}

void line(char z1, char y1, char x1, char z2, char y2, char x2) {
    char x, y, z, last_y, last_z;
    float xy, xz; // êîëè÷åñòâî óçëîâûõ òî÷åê, ïðè ïðîõîæäåíèè îò "y" ê "x"
    // òèï float ïîâûøàåò òî÷íîñòü ðèñîâàíèÿ (íà ïðÿìûõ, ïðîõîäÿùèõ
    // íå ÷åðåç óçëû), íî èñïîëüçóåò ROM íà 7% áîëüøåì, ÷åì òèï char

    if (inrange(z1, y1, x1) & inrange(z2, y2, x2)) {
        // Ìû âñåãäà ðèñóåì ëèíèè îò x=0 ê x=7
        // Åñëè x1 áîëüøå x2, òî íåîáõîäèìî ïîìåíÿòü êîîðäèíàòû ìåñòàìè
        if (x1>x2) {
            char tmp;
            tmp=x2;
            x2=x1;
            x1=tmp;
            tmp=y2;
            y2=y1;
            y1=tmp;
            tmp=z2;
            z2=z1;
            z1=tmp;
        }

        if (y1>y2) {
            xy=(float) (y1 - y2) / (float) (x2 - x1);
            last_y=y2;
        } else {
            xy=(float) (y2 - y1) / (float) (x2 - x1);
            last_y=y1;
        }

        if (z1>z2) {
            xz=(float) (z1 - z2) / (float) (x2 - x1);
            last_z=z2;
        } else {
            xz=(float) (z2 - z1) / (float) (x2 - x1);
            last_z=z1;
        }
        for (x=x1; x<=x2; x++) {
            y=(xy * (x - x1)) + y1;
            z=(xz * (x - x1)) + z1;
            setvoxel(z, y, x);
        }
    }
}

void box_filled(uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2) {
    uint8_t iz, iy, ix;

    argorder(x1, x2, &x1, &x2);
    argorder(y1, y2, &y1, &y2);
    argorder(z1, z2, &z1, &z2);

    for (iz=z1; iz<=z2; iz++) {
        for (iy=y1; iy<=y2; iy++) {
            for (ix=x1; ix<=x2; ix++) {
                cube[iz][iy][ix]=1;
                setvoxel(iz, iy, ix);

            }
        }
    }
}


/*..................Hiệu ứng rainbow.............................................*/
rgb_t Scroll(int pos) {
	rgb_t color = { .r = 255, .g = 0, .b = 0 };
	if(pos < 85) {
		color.g = 0;
		color.r = ((float)pos / 85.0f) * 255.0f;
		color.b = 255 - color.r;
	} else if(pos < 170) {
		color.g = ((float)(pos - 85) / 85.0f) * 255.0f;
		color.r = 255 - color.g;
		color.b = 0;
	} else if(pos < 256) {
		color.b = ((float)(pos - 170) / 85.0f) * 255.0f;
		color.g = 255 - color.b;
		color.r = 1;
	}
	return color;
}
void change_rgb_color()
{
    static char scroll;
    rgb_t rgb = Scroll(scroll);
    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
    scroll++;
}
void change_rgb_color_2()
{
    static char scroll;
    rgb_t rgb = Scroll(scroll);
    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
    scroll = scroll + 30;
}

uint32_t timeStart ;
#define ENABLE 1
#define DISABLE 0
void effect_rainbow(uint8_t enordis,uint32_t timeRun, int speed)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();
        for(int k = 0; k < 10000; k++)
        {
            for(int j = 0; j < 256; j++) {
            for(int i = 0; i < 512; i++) {
                layer_buffer[i] = Scroll((i * 256 / 512 + j) % 256);   
            } 
            delay_ms(speed);
            } 
            if(sec() - timeStart > timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }
    }
   
}
/*..................Hiệu ứng mưa.............................................*/


void effect_rain(uint8_t enordis, uint32_t timeRun, int speed, int direction) {
    if(enordis == ENABLE)
    {
        int i, ii;
        int rnd_x;
        int rnd_y;
        int rnd_num;
        static char scroll = 0;
        
        clear_layer_buffer();
        timeStart = sec();
        for (ii=0; ii< 10000; ii++) {
        
            rnd_num = random_number(0,7);
            for (i=0; i<rnd_num; i++) {
                rnd_x=random_number(0,7) ;
                rnd_y=random_number(0,7) ;
                setvoxel(7, rnd_y, rnd_x);
            }
            delay_ms(speed);
            shift(1, direction);
            rgb_t rgb = Scroll(scroll);
            set_color_setvoxel(rgb.r, rgb.g, rgb.b);
            scroll++;
            if(sec() - timeStart > timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }
    }  
}


/*.................Hiệu ứng đổi màu side.....................................*/
static rgb_t eff_side_change_color_buffer[13] = {
    (rgb_t) { .r = 255, .g = 0, .b = 0 },
    (rgb_t) { .r = 255, .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 255 },
    (rgb_t) { .r = 0, .g = 0, .b = 255 } ,
    (rgb_t) { .r = 255, .g = 0, .b = 255 } ,
    (rgb_t) { .r = 255, .g = 0, .b = 0 },
    (rgb_t) { .r = 255, .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 255 },
    (rgb_t) { .r = 0, .g = 0, .b = 255 }  ,
    (rgb_t) { .r = 0, .g = 255, .b = 100 } ,
    (rgb_t) { .r = 255, .g = 255, .b = 255 }  
};
void eff_side_change_color()
{
    static uint8_t k = 0;
    if(k%3 == 0)
    {
        for(int z = 0; z <= CUBE_SIZE; z ++)
        {
            for(int x = 0; x <= CUBE_SIZE; x ++)
            {
                for(int y = 0; y <= CUBE_SIZE; y ++)
                {
                    set_color_to_buffer(z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                } 
            }
            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
    else if(k%3 == 1)
    {
        for(int z = CUBE_SIZE; z >= 0; z --)
        {
            for(int x = 0; x <= CUBE_SIZE; x ++)
            {
                for(int y = 0; y <= CUBE_SIZE; y ++)
                {
                    set_color_to_buffer(z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                } 
            }
            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
    else if(k%3 == 2)
    {
        for(int x = CUBE_SIZE; x >= 0; x --)
        {
            for(int z = CUBE_SIZE; z >= 0; z --)
            {
                for(int y = 0; y <= CUBE_SIZE; y ++)
                {
                    set_color_to_buffer(z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                } 
            }
            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
    
    k++;
    if(k > 12)  k = 0;
       
}

/*.......................Hiệu ứng planboing*/
#define AXIS_Z 1
#define AXIS_Y 2
#define AXIS_X 3
void planboing(char axis, int speed) {

    int i;
    static char scroll = 0;

    for (i=0; i<CUBE_SIZE; i++) {
         fill_cube(0x00);
        setplane(axis, i);
        delay_ms(speed);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
    }
    for (i=(CUBE_SIZE - 1); i>=0; i--) {
         fill_cube(0x00);
        setplane(axis, i);
        delay_ms(speed);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
    }
}
void effect_planboing(uint8_t enordis, uint32_t timeRun)
{
    timeStart = sec();
    if(enordis == ENABLE)
    {
        for(int i=0; i< 10000; i++)
        {
            planboing(AXIS_Z,50); 
            planboing(AXIS_X,50);
            planboing(AXIS_Y,50);
            planboing(AXIS_Z,50);
            planboing(AXIS_X,50);
            planboing(AXIS_Y,50);
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }
    }
}

/*.......................Hiệu ứng Send voxel rand..............................*/
void effect_sendvoxels_rand_axis(uint8_t enordis, uint32_t timeRun, char axis, char delay, char wait) {
    if(enordis == ENABLE)
    {
        unsigned char x, y, z, i, last_x=0, last_y=0;
        fill_cube(0);
        static char scroll;
        timeStart = sec();
        if (axis==1)
        {
            // for (x=0; x<CUBE_SIZE; x++) {
            //     for (y=0; y<CUBE_SIZE; y++) {
            //         setvoxel(((rand() % 2)*(CUBE_SIZE - 1)), y, x);
            //     }
            // }
            for (i=0; i<10000; i++) {
                x=rand() % CUBE_SIZE;
                y=rand() % CUBE_SIZE;
                if (y != last_y && x != last_x) {
                    if (getvoxel(0, y, x)) {
                        sendvoxel_axis(0, y, x, axis, delay);
                    } else {
                        sendvoxel_axis((CUBE_SIZE - 1), y, x, axis, delay);
                    }
                    delay_ms(wait);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                    scroll++;
            
                    last_y=y;
                    last_x=x;
                }
                if(sec() - timeStart >= timeRun)
                    return;
                if(restart_value == 1)
                {
                    restart_value = 0; fill_cube(0x00);delay_ms(1000);
                    return;
                }
            }
            
        }

        if (axis==2) {
            // for (x=0; x<CUBE_SIZE; x++) {
            //     for (y=0; y<CUBE_SIZE; y++) {
            //         setvoxel(y, ((rand() % 2)*(CUBE_SIZE - 1)), x);
            //     }
            // }
            for (i=0; i<10000; i++) {
                x=rand() % CUBE_SIZE;
                y=rand() % CUBE_SIZE;
                if (y != last_y && x != last_x) {

                    if (getvoxel(y, 0, x)) {
                        sendvoxel_axis(y, 0, x, axis, delay);
                    } else {
                        sendvoxel_axis(y, (CUBE_SIZE - 1), x, axis, delay);
                    }
                    delay_ms(wait);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                    scroll++;
                    last_y=y;
                    last_x=x;
                } 
                if(sec() - timeStart >= timeRun)
                    return;
                if(restart_value == 1)
                {
                    restart_value = 0; fill_cube(0x00);delay_ms(1000);
                    return;
                }
            }
           
        }

        if (axis==3) {
            // for (x=0; x<CUBE_SIZE; x++) {
            //     for (y=0; y<CUBE_SIZE; y++) {
            //         setvoxel(y, x, ((rand() % 2)*(CUBE_SIZE - 1)));
            //     }
            // }
            for (i=0; i<10000; i++) {
                x=rand() % CUBE_SIZE;
                y=rand() % CUBE_SIZE;
                if (y != last_y && x != last_x) {
                    if (getvoxel(y, x, 0)) {
                        sendvoxel_axis(y, x, 0, axis, delay);
                    } else {
                        sendvoxel_axis(y, x, (CUBE_SIZE - 1), axis, delay);
                    }
                    delay_ms(wait);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                    scroll++;
                    last_y=y;
                    last_x=x;
                }
                if(sec() - timeStart >= timeRun)
                    return;
                if(restart_value == 1)
                {
                    restart_value = 0; fill_cube(0x00);delay_ms(1000);
                    return;
                }
            }
            
        }
    }
    
}

/*............................Hiệu ứng Box Shrinkgrow and Woopwoop ..........................*/
void effect_box_shrink_grow(uint8_t enordis, uint32_t timeRun, int speed)
{
    if(enordis == ENABLE)
    {
        int x, i, ii = 0, xyz;
        int iterations=1;
        int flip = (ii & (CUBE_SIZE / 2));
        static char scroll;
        timeStart = sec();
        int k = 0;
        for (ii=0; ii<CUBE_SIZE; ii++) {
            for (x=0; x<10000; x++) {
                for (i=0; i<(CUBE_SIZE * 2); i++) {
                    xyz=(CUBE_SIZE - 1) - i; 
                    if (i>(CUBE_SIZE - 1))
                        xyz=i - CUBE_SIZE; 

                    
                    fill_cube(0x00);
                    delay_ms(2);
                    box_wireframe(0, 0, 0, xyz, xyz, xyz);

                    // if (flip>0) 
                    //     mirror_z();
                    // if (ii==(CUBE_SIZE - 3) || ii==(CUBE_SIZE - 1))
                        mirror_y();
                    // if (ii==(CUBE_SIZE - 2) || ii==(CUBE_SIZE - 1))
                    //     mirror_x();
                   
                    if(k%3 == 0) mirror_z();
                    if(k%3 == 1) mirror_y();
                    if(k%3 == 2) mirror_x();

                        
                    delay_ms(speed);
                    fill_cube(0x00);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                    scroll++;   
                } 
                 k++;
                if(sec() - timeStart >= timeRun)
                    return;
                if(restart_value == 1)
                {
                    restart_value = 0; fill_cube(0x00);delay_ms(1000);
                    return;
                }
            }
            
        }
    }
    
}
void box_woopwoop(int delay, int grow) 
{
    int i, ii;
    char ci = CUBE_SIZE / 2;
    fill_cube(0x00);

    for (i=0; i<ci; i++) {
        ii=i;
        if (grow>0)
            ii=(ci - 1) - i;

        box_wireframe(ci + ii, ci + ii, ci + ii, (ci - 1) - ii, (ci - 1) - ii, (ci - 1) - ii);
        delay_ms(delay);
        fill_cube(0x00);
    }
}
void effect_box_woopwoop(uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();
        for (int i=0; i<10000; i++) {
            box_woopwoop(50, 0);
            box_woopwoop(50, 1);
            box_woopwoop(50, 0);
            box_woopwoop(50, 1);
            box_woopwoop(50, 0);
            box_woopwoop(50, 1);
            box_woopwoop(50, 0);
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }

       
    }
}


/*....................................Hiệu ứng Axis updown rand suppend............................................*/
void axis_updown_randsuspend(char axis, char delay, int sleep, char invert) {
    unsigned char positions[CUBE_SIZE * CUBE_SIZE];
    unsigned char destinations[CUBE_SIZE * CUBE_SIZE];
    int i, px;
    static char scroll;
    fill_cube(0x00);

    // Set 64 random positions
    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        positions[i]=0; // Set all starting positions to 0
        destinations[i]=rand() % CUBE_SIZE;
    }

    // Loop 8 times to allow destination 7 to reach all the way
    for (i=0; i<CUBE_SIZE; i++) {
        // For every iteration, move all position one step closer to their destination
        for (px=0; px<(CUBE_SIZE * CUBE_SIZE); px++) {
            if (positions[px]<destinations[px]) {
                positions[px]++;
            }
        }
        // Draw the positions and take a nap
        draw_positions_axis(axis, positions, invert);
        delay_ms(delay);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
                
    }

    // Set all destinations to 7 (opposite from the side they started out)
    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        destinations[i]=CUBE_SIZE - 1;
    }

    // Suspend the positions in mid-air for a while
    delay_ms(sleep*2);

    // Then do the same thing one more time
    for (i=0; i<CUBE_SIZE; i++) {
        for (px=0; px<(CUBE_SIZE * CUBE_SIZE); px++) {
            if (positions[px]<destinations[px]) {
                positions[px]++;
            }
            if (positions[px]>destinations[px]) {
                positions[px]--;
            }
        }
        draw_positions_axis(axis, positions, invert);
        delay_ms(delay);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
        scroll++;
                
       
    }

    // Suspend the positions in top-bottom for a while
     delay_ms(sleep*2);
}

void effect_axis_z_updown_randsuspend(uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();
        for(int i = 0; i < 10000; i++)
        {
            axis_updown_randsuspend(1, 50, 0, 0);  // 9(char axis, int delay, int sleep, int invert) z
            axis_updown_randsuspend(1, 50, 0, 1);
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }
    }
}

void effect_axis_y_updown_randsuspend(uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();
        for(int i = 0; i < 10000; i++)
        {
            axis_updown_randsuspend(2, 50, 0, 0);  // 9(char axis, int delay, int sleep, int invert) z
            axis_updown_randsuspend(2, 50, 0, 1);
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }
    }
}

/*..............................Hiệu ứng ripple.............................*/
int totty_sin(unsigned char LUT[65],int sin_of)
{
    unsigned char inv=0;
    if (sin_of<0)
    {
        sin_of=-sin_of;
        inv=1;
    }
    sin_of&=0x7f;  // 127
    if (sin_of>64)
    {
        sin_of-=64;
        inv=1-inv;
    }
    if (inv)
        return -LUT[sin_of];
    else
        return LUT[sin_of];
}

int totty_cos(unsigned char LUT[65],int cos_of)
{
    unsigned char inv=0;
    cos_of+=32;    // Simply rotate by 90 degrees for COS
    cos_of&=0x7f;  // 127
    if (cos_of>64)
    {
        cos_of-=64;
        inv=1;
    }
    if (inv)
        return -LUT[cos_of];
    else
        return LUT[cos_of];
}
void init_LUT(unsigned char LUT[65])
{
  unsigned char i;
  float sin_of,sine;
  for (i=0;i<65;++i)
  {
    sin_of=i*(3.1415)/64; // Just need half a sin wave
    sine=sin(sin_of);
    // Use 181.0 as this squared is <32767, so we can multiply two sin or cos without overflowing an int.
    LUT[i]=sine*181.0;
  }
}


void effect_int_ripples(uint8_t enordis, uint32_t timeRun, int delay)
{

    if(enordis == ENABLE)
    {
        timeStart = sec();
         // 16 values for square root of a^2+b^2.  index a*4+b = 10*sqrt
        // This gives the distance to 3.5,3.5 from the point
        unsigned char sqrt_LUT[]={49,43,38,35,43,35,29,26,38,29,21,16,35,25,16,7};
        //LUT_START // Macro from new tottymath.  Commented and replaced with full code
        unsigned char LUT[65];
        init_LUT(LUT);
        int i;
        unsigned char x,y,height,distance;
        for (i=0;i<10000*4;i+=4)
        {
            fill_cube(0x00);
            for (x=0;x<4;++x)
            for(y=0;y<4;++y)
            {
                // x+y*4 gives no. from 0-15 for sqrt_LUT
                distance=sqrt_LUT[x+y*4];// distance is 0-50 roughly
                // height is sin of distance + iteration*4
                //height=4+totty_sin(LUT,distance+i)/52;
                height=(196+totty_sin(LUT,distance+i))/49;
                // Use 4-way mirroring to save on calculations
                rgb_t rgb ;
                if(height == 7)
                { 
                    rgb = Scroll(255);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 6)
                { 
                    rgb = Scroll(220);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 5)
                { 
                    rgb = Scroll(200);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 4)
                { 
                    rgb = Scroll(180);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 3)
                { 
                    rgb = Scroll(160);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 2)
                { 
                    rgb = Scroll(130);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 1)
                { 
                    rgb = Scroll(110);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 
                if(height == 0)
                { 
                    rgb = Scroll(90);
                    set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                } 

                setvoxel(height,y,x);
                setvoxel(height,y,7-x);
                setvoxel(height,7-y,x);
                setvoxel(height,7-y,7-x);
                
            }
            delay_ms(delay);
            // change_rgb_color();
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }

        }
    }
 
}

/*.........................Hiệu ứng cubix.......................................*/
void effect_cubix(uint8_t enordis, uint32_t timeRun, unsigned char cubies)
{
  if(enordis == ENABLE)
    {
        timeStart = sec();

         // cube array:
        // Stupid name to prevent confusion with cube[][]!
        // 0=pos
        // 1=dir (0 stopped 1,2 or 4 move in X, Y or Z)
        // 2=inc or dec in direction (2=inc, 0=dec)
        // 3=countdown to new movement
        // 4=x, 5=y, 6=z
        unsigned char qubes[3][7];
        int ii; // iteration counter
        unsigned char i,j,diridx,newdir;
        unsigned char runstate=2;
        //  Initialise qubes array
        for (i=0;i<cubies;++i)
        {
            qubes[i][0]=i;// position = i
            qubes[i][1]=0;// static
            qubes[i][3]=rand()&0x0f;// 0-15
            // Hack in the X,Y and Z positions
            qubes[i][4]=4*(i&0x01);
            qubes[i][5]=2*(i&0x02);
            qubes[i][6]=(i&0x04);
        }
        // Main loop
        ii=20000;
        while(runstate)
        {
            fill_cube(0x00);
            for (i=0;i<cubies;++i)
            {
            // Use a pointer to simplify array indexing
            // qube[0..7] = qubes[i][0..7]
            unsigned char *qube=&qubes[i][0];
            if (qube[1]) //moving
            {
                diridx=3+qube[1];//4,5 or 7
                if (diridx==7) diridx=6;
                qube[diridx]+=qube[2]-1;
                if ((qube[diridx]==0)||(qube[diridx]==4))
                {
                // XOR old pos and dir to get new pos.
                qube[0]=qube[0]^qube[1];
                qube[1]=0;// Stop moving!
                qube[3]=rand()&0x0f; // countdown to next move 0-15
                if(runstate==1)
                    if (qube[0]<5)
                    qube[3]*=4;// Make lower qubes move very slowly to finish
                }
            }
            else // not moving
            {
                if (qube[3])// counting down
                --qube[3];
                else // ready to move
                {
                newdir=(1<<(rand()%3));//1,2 or 4
                diridx=qube[0]^newdir;
                for (j=0;j<cubies;++j)// check newdir is safe to move to
                {
                    if ((diridx==qubes[j][0])||(diridx==(qubes[j][0]^qubes[j][1])))
                    {
                    newdir=0;
                    qube[3]=5;
                    }
                }

                if (newdir)
                {
                    diridx=3+newdir;
                    if (diridx==7) diridx=6;
                    if (qube[diridx])// should be 4 or 0
                    qube[2]=0; // dec if at 4
                    else
                    {
                    qube[2]=2; // inc if at 0
                    if(runstate==1)// Try to make qubes go home
                        if ((diridx>4)&&(qube[0]<4))
                        newdir=0;//Don't allow qubes on bottom row to move up or back
                    }
                }
                qube[1]=newdir;
                }
            }
            //if (i&0x01)//odd number
            if(i == 0)
            {
                box_filled(qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
                set_color_setvoxel(255,0,0);
            }
            if(i == 1)
            {
                box_filled(qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
                set_color_setvoxel(0,255,0);
            }
            if(i == 2)
            {
                box_filled(qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
                set_color_setvoxel(0,0,255);
            }
            

            //else
            //  box_wireframe(qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
            } // i loop
            delay_ms(50);
            if(runstate==2)// If normal running
            {
            if(!(--ii))// decrement iteration and check for zero
                runstate=1;// If zero go to homing
            }
            else//runstate at 1
            {
            diridx=0;
            for(j=0;j<cubies;++j)
                if (qubes[j][0]+1>cubies) diridx=1;// Any cube not at home
            if (!diridx)
                runstate=0;
            }
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }

        } // Main loop
    }
 
}


/*.........................Hiệu ứng sidewaves...................................*/

void effect_int_sidewaves(uint8_t enordis, uint32_t timeRun, int delay) {
    if(enordis == ENABLE)
    {
        timeStart = sec();
        unsigned char LUT[65];
        int i;
        int origin_x, origin_y, distance, height;
        int x_dist,x_dist2,y_dist;
        int x,y,x_vox;
        static char scroll;
        init_LUT(LUT);

        for (i=0; i<10000; i++)
        {	// To provide some finer control over the integer calcs the x and y
            // parameters are scaled up by a factor of 15.  This is primarily to
            // keep the sum of their squares within the scale of an integer.
            // 120^2 + 120^2=28800
            // If we scaled by 16 we would overflow at the very extremes,
            // e.g 128^2+128^2=32768.  The largest int is 32767.
            //
            // Because origin_x/y is a sin/cos pair centred at 60, the actual
            // highest distance in this effect would be at:
            // x=8,y=8, origin_x=102, origin_y=102
            //=approximate sum of 17400.
            //
            // It is probably safer to work at a scale of 15 to allow simple changes
            // to the maths to be made without risking an overflow.
            origin_x=(totty_sin(LUT,i/2)+180)/3;  // Approximately 0 to 120
            origin_y=(totty_cos(LUT,i/2)+180)/3;
            fill_cube(0x00);
            for (x=8; x<120; x+=15)// 8 steps from 8 to 113
            {
                // Everything in here happens 8 times per cycle
                x_dist=abs(x-origin_x);
                x_dist2=x_dist*x_dist;  // square of difference
                x_vox=x/15;  // Unscale x
                for (y=8; y<120; y+=15)
                {
                    // Everything in here happens 64 times per cycle
                    y_dist=abs(y-origin_y);
                    if (x_dist||y_dist)  // Either x OR y non-zero
                    {
                        // Calculate sum of squares of linear distances
                        // We use a 1st order Newton approximation:
                        // sqrt=(N/guess+guess)/2
                        distance=(x_dist2+y_dist*y_dist);
                        height=(x_dist+y_dist)/2;  // Approximate quotient
                        // We divide by 30.  1st approx would be /2
                        // but we have a factor of 15 included in our scale calcs
                        distance=(distance/height+height)/3; // 1st approx at sqrt
                    }
                    else
                        distance=0;  // x and y=origin_x and origin_y
                    height=(totty_sin(LUT,distance+i)+180)/51 ;
                    setvoxel(height,y/15,x_vox);
                    
                }
            }

            rgb_t rgb = Scroll(scroll);
            set_color_setvoxel(rgb.r, rgb.g, rgb.b);
            scroll++;
            delay_ms(delay);
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }

        }
    }
	
}


/*.........................Hiệu ứng wormsqueeze.............................*/
void effect_wormsqueeze(uint8_t enordis, uint32_t timeRun, int delay,int axis, int direction) {
    if(enordis == ENABLE)
    {
        timeStart = sec();
        int x, y, i, j, k, dx, dy, size, cube_size;
        int origin=0;
        static char scroll;

        if (CUBE_SIZE==4)
            size=1;
        if (CUBE_SIZE==8)
            size=2;

        if (direction==-1)
            origin=(CUBE_SIZE - 1);

        cube_size=CUBE_SIZE - (size - 1);

        x=rand() % CUBE_SIZE;
        y=rand() % CUBE_SIZE;

        for (i=0; i<10000; i++) {
            dx=((rand() % (CUBE_SIZE / 2 - 1)) - 1); // %3
            dy=((rand() % (CUBE_SIZE / 2 - 1)) - 1); // %3

            if ((x + dx)>0 && (x + dx)<CUBE_SIZE)
                x += dx;

            if ((y + dy)>0 && (y + dy)<CUBE_SIZE)
                y += dy;

            shift(axis, direction);

            for (j=0; j<size; j++) {
                for (k=0; k<size; k++) {
                    if (axis==1) // AXIS_Z
                    {
                        setvoxel(origin, y + k, x + j);
                        origin--;
                    } // setvoxel(x+j,y+k,origin);

                    if (axis==2) // AXIS_Y
                        setvoxel(y + k, origin, x + j); // setvoxel(x+j,origin,y+k);

                    if (axis==3) // AXIS_X
                        setvoxel(x + k, y + j, origin); // setvoxel(origin,y+j,x+k);
                    
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
                    scroll++;
                }        
            }  
            delay_ms(delay); 
            if(sec() - timeStart >= timeRun)
            {
                 fill_cube(0x00); // Blank the cube
                 return;
            }
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
                    
        }
    }
}



/*..............................Hiệu ứng boingboing..........................*/
void effect_boingboing(uint8_t enordis, uint32_t timeRun, char delay, unsigned char mode, unsigned char drawmode) {
    
    if(enordis == ENABLE)
    {
        timeStart = sec();
        uint8_t x, y, z; // Current coordinates for the point                  // mode=0x02;                 // íå ðàáîòàåò
        int dx, dy, dz; // Direction of movement                     // delay=4; mode=0x01; drawmode=0x01;  // òî÷êà, ïåðåìåùàþùàÿñÿ ïî êóáó
        int lol, i; // lol?                                      // delay=2; mode=0x01; drawmode=0x02;  // òî÷êà, ïåðåìåùàþùàÿñÿ ïî êóáó îñòÿâëÿþùàÿ ñëåä è çàïîëíÿþùàÿ âåñü êóá
        unsigned char crash_x, crash_y, crash_z; // delay=3; mode=0x01; drawmode=0x03;  // çìåéêà, ïåðåìåùàþùàÿñÿ ïî êóáó
        int snake[CUBE_SIZE][3];

        fill_cube(0x00); // Blank the cube

        y=rand() % CUBE_SIZE;
        x=rand() % CUBE_SIZE;
        z=rand() % CUBE_SIZE;

        // Coordinate array for the snake
        for (i=0; i<CUBE_SIZE; i++) {
            snake[i][0]=x;
            snake[i][1]=y;
            snake[i][2]=z;
        }

        dx=1;
        dy=1;
        dz=1;

        for(int k =0; k < 10000; k++)
        {
            crash_x=0;
            crash_y=0;
            crash_z=0;

            // Let's mix things up a little:
            if (rand() % 3==0) {
                // Pick a random axis, and set the speed to a random number.
                lol=rand() % 3;
                if (lol==0)
                    dx=rand() % 3 - 1;

                if (lol==1)
                    dy=rand() % 3 - 1;

                if (lol==2)
                    dz=rand() % 3 - 1;
            }

            // The point has reached 0 on the x-axis and is trying to go to -1
            // aka a crash
            if (dx==-1 && x==0) {
                crash_x=0x01;
                if (rand() % 3==1) {
                    dx=1;
                } else {
                    dx=0;
                }
            }

            // y axis 0 crash
            if (dy==-1 && y==0) {
                crash_y=0x01;
                if (rand() % 3==1) {
                    dy=1;
                } else {
                    dy=0;
                }
            }

            // z axis 0 crash
            if (dz==-1 && z==0) {
                crash_z=0x01;
                if (rand() % 3==1) {
                    dz=1;
                } else {
                    dz=0;
                }
            }

            // x axis 7 crash
            if (dx==1 && x==(CUBE_SIZE - 1)) {
                crash_x=0x01;
                if (rand() % 3==1) {
                    dx=-1;
                } else {
                    dx=0;
                }
            }

            // y axis 7 crash
            if (dy==1 && y==(CUBE_SIZE - 1)) {
                crash_y=0x01;
                if (rand() % 3==1) {
                    dy=-1;
                } else {
                    dy=0;
                }
            }

            // z azis 7 crash
            if (dz==1 && z==(CUBE_SIZE - 1)) {
                crash_z=0x01;
                if (rand() % 3==1) {
                    dz=-1;
                } else {
                    dz=0;
                }
            }

            // mode bit 0 sets crash action enable
            if (mode | 0x01) {
                if (crash_x) {
                    if (dy==0) {
                        if (y==(CUBE_SIZE - 1)) {
                            dy=-1;
                        } else if (y==0) {
                            dy=+1;
                        } else {
                            if (rand() % 2==0) {
                                dy=-1;
                            } else {
                                dy=1;
                            }
                        }
                    }
                    if (dz==0) {
                        if (z==(CUBE_SIZE - 1)) {
                            dz=-1;
                        } else if (z==0) {
                            dz=1;
                        } else {
                            if (rand() % 2==0) {
                                dz=-1;
                            } else {
                                dz=1;
                            }
                        }
                    }
                }

                if (crash_y) {
                    if (dx==0) {
                        if (x==(CUBE_SIZE - 1)) {
                            dx=-1;
                        } else if (x==0) {
                            dx=1;
                        } else {
                            if (rand() % 2==0) {
                                dx=-1;
                            } else {
                                dx=1;
                            }
                        }
                    }
                    if (dz==0) {
                        if (z==3) {
                            dz=-1;
                        } else if (z==0) {
                            dz=1;
                        } else {
                            if (rand() % 2==0) {
                                dz=-1;
                            } else {
                                dz=1;
                            }
                        }
                    }
                }

                if (crash_z) {
                    if (dy==0) {
                        if (y==(CUBE_SIZE - 1)) {
                            dy=-1;
                        } else if (y==0) {
                            dy=1;
                        } else {
                            if (rand() % 2==0) {
                                dy=-1;
                            } else {
                                dy=1;
                            }
                        }
                    }
                    if (dx==0) {
                        if (x==(CUBE_SIZE - 1)) {
                            dx=-1;
                        } else if (x==0) {
                            dx=1;
                        } else {
                            if (rand() % 2==0) {
                                dx=-1;
                            } else {
                                dx=1;
                            }
                        }
                    }
                }
            }

            // mode bit 1 sets corner avoid enable       (CUBE_SIZE-1)
            if (mode | 0x02) {
                if (// We are in one of 8 corner positions
                        (x==0 && y==0 && z==0) ||
                        (x==0 && y==0 && z==(CUBE_SIZE - 1)) ||
                        (x==0 && y==(CUBE_SIZE - 1) && z==0) ||
                        (x==0 && y==(CUBE_SIZE - 1) && z==(CUBE_SIZE - 1)) ||
                        (x==(CUBE_SIZE - 1) && y==0 && z==0) ||
                        (x==(CUBE_SIZE - 1) && y==0 && z==(CUBE_SIZE - 1)) ||
                        (x==(CUBE_SIZE - 1) && y==(CUBE_SIZE - 1) && z==0) ||
                        (x==(CUBE_SIZE - 1) && y==(CUBE_SIZE - 1) && z==(CUBE_SIZE - 1))
                        ) {
                    // At this point, the voxel would bounce
                    // back and forth between this corner,
                    // and the exact opposite corner
                    // We don't want that!

                    // So we alter the trajectory a bit,
                    // to avoid corner stickyness
                    lol=rand() % 3;
                    if (lol==0)
                        dx=0;

                    if (lol==1)
                        dy=0;

                    if (lol==2)
                        dz=0;
                }
            }

            // one last sanity check
            if (x==0 && dx==-1)
                dx=1;

            if (y==0 && dy==-1)
                dy=1;

            if (z==0 && dz==-1)
                dz=1;

            if (x==(CUBE_SIZE - 1) && dx==1)
                dx=-1;

            if (y==(CUBE_SIZE - 1) && dy==1)
                dy=-1;

            if (z==(CUBE_SIZE - 1) && dz==1)
                dz=-1;


            // Finally, move the voxel.
            x=x + dx;
            y=y + dy;
            z=z + dz;

            // show one voxel at time (îäèí ñâåòîäèîä)
            if (drawmode==0x01)
            {
                setvoxel(z, y, x);
                delay_ms(delay);
                clrvoxel(z, y, x);
                change_rgb_color();
            }
            // flip the voxel in question (ïåðåäåëàíî â äâèæóùèéñÿ ñâåòîäèîä, îñòàâëÿþùèé ñëåä)
            if (drawmode==0x02)
            {
                // flpvoxel(z,y,x); // èñõîäíûé âàðèàíò
                setvoxel(z, y, x);
                delay_ms(delay);
                change_rgb_color();
            }
            // draw a snake (çìåéêà)
            if (drawmode==0x03)
            {
                for (i=(CUBE_SIZE - 1); i>=0; i--) {
                    snake[i][0]=snake[i - 1][0];
                    snake[i][1]=snake[i - 1][1];
                    snake[i][2]=snake[i - 1][2];
                }
                snake[0][0]=x;
                snake[0][1]=y;
                snake[0][2]=z;

                for (i=0; i<CUBE_SIZE; i++) {
                    setvoxel(snake[i][2], snake[i][1], snake[i][0]);
                }
                delay_ms(delay);
                change_rgb_color();
                for (i=0; i<CUBE_SIZE; i++) {
                    clrvoxel(snake[i][2], snake[i][1], snake[i][0]);
                }
            }
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }

        }
    }
    
}

/*..............................Hiệu ứng filler...............................*/

void effect_random_filler(uint8_t enordis, uint32_t timeRun, int delay) {
    if(enordis == ENABLE)
    {
        timeStart = sec();
        char z,y,x;
       
        for (int i=0; i<1000; i++)  // CUBE_BYTES-3
        {
            z=rand()%CUBE_SIZE;
            y=rand()%CUBE_SIZE;
            x=rand()%CUBE_SIZE;
            if (getvoxel(z,y,x)==0x00)
                altervoxel(z,y,x,1);
            delay_ms(delay);
            change_rgb_color();
            if(sec() - timeStart >= timeRun)
                return;

            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
                

        } 
        for (int i=0; i<1000; i++)  // CUBE_BYTES-3
        {
            z=rand()%CUBE_SIZE;
            y=rand()%CUBE_SIZE;
            x=rand()%CUBE_SIZE;
            if ( getvoxel(z,y,x)==0x01) 
                altervoxel(z,y,x,0);
            delay_ms(delay);
            change_rgb_color();
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0;fill_cube(0x00);delay_ms(1000);
                return;
            }
        }
    }
}


/*...................Hiệu ứng filip filop...................................*/

void effect_plane_flip(unsigned char LUT[],unsigned char start,unsigned char end,int delay)
{
  unsigned char p1,p2;// point across and down on flip
  unsigned char x,y,z;
  unsigned char i;// rotational position
  unsigned char i1;// linear position
  unsigned char dir=0;//0-2  0=sidetrap 1=door 2=trap
  unsigned char rev=0;//0 for forward 1 for reverse
  unsigned char inv=0;//bit 0=Y bit 1=Z
  // Sort out dir, rev and inv for each start/end combo.
  //  There are 24, but with some neat combinations of
  // tests we can simplify so that only a max of 3 tests
  // are required for each type!
  if (start<2)// 0 or 1.  buh
  {
    if (end<4)
    {
      dir=0;
      if (end==3) inv=0x01;
    }
    else
    {
      dir=2;
      if (end==5) inv=0x01;
    }
    if (start==1) inv|=0x02;
  } else if (start<4)//2 or 3. Buh
  {
    if (end<2)// going to 0 or 1
    {
      rev=1;
      dir=0;
      if (start==3) inv=0x01;
      if (end==1) inv|=0x02;
    }
    else // going to 4 or 5
    {
      dir=1;// door moves
      if(start+end==7)//3 to 4 or 2 to 5
      {
        if (start==3)
          inv=0x02;
        else
          inv=0x01;
      }
      else//2 to 4 or 3 to 5
      {
        if (start==3)
          inv=0x03;
      }
    }
  }
  else //start is 4 or 5
  {
    if (end<2)// 0 or 1
    {
      dir=2;
      rev=1;// reverse trapdoor.  Yeah!
      if (start+end==5)//4 to 1 or 5 to 0
      {
        if (start==4)//4 to 1
          inv=0x02;
        else
          inv=0x01;
      }
      else // 4 to 0 or 5 to 1
        if (start==5)
          inv=0x03;
    }
    else//end is 2 or 3
    {
      rev=1;// all reverse
      dir=1;// all door
      if (start+end==7)//4 to 3 or 5 to 2
      {
        if (start==4) // 4 to 3
          inv=0x02;
        else
          inv=0x01;
      }
      else
      {
        if (start==5)//5 to 3
          inv=0x03;
      }
    }
  }
  // Do the actual plane drawing
  for(i=0;i<7;++i)
  {
    if (rev)// Reverse movement goes cos-sin
    {
      p2=totty_sin(LUT,i*4);// angle 0-45 degrees
      p1=totty_cos(LUT,i*4);
    }
    else
    {
      p1=totty_sin(LUT,i*4);// angle 0-45 degrees
      p2=totty_cos(LUT,i*4);
    }
    fill_cube(0x00);
    for (i1=0;i1<8;++i1)
    {
      z=p1*i1/168;
      if (inv&0x02) z=7-z;// invert in Z axis
      y=p2*i1/168;
      if (inv&0x01) y=7-y;// invert in Y axis
      for(x=0;x<8;++x)
      {
        if(!dir)//dir=0
        {
          setvoxel(x,y,z);
        }
        else if(dir==1)
        {
          setvoxel(y,z,x);
        }
        else//dir=2
        {
          setvoxel(y,x,z);
        }
      }
    }
    delay_ms(delay);
    change_rgb_color();
  }
}

void effect_filip_filop(uint8_t enordis, uint32_t timeRun, int FF_DELAY)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();
        unsigned char LUT[65];
        init_LUT(LUT);
        unsigned char now_plane=0;//start at top
        unsigned char next_plane;
        int delay=FF_DELAY;
        int i;//iteration counter
        for (i=20000;i;--i)
        {
            next_plane=rand()%6; //0-5
            // Check that not the same, and that:
            // 0/1 2/3 4/5 pairs do not exist.
            if ((next_plane&0x06)==(now_plane&0x06))
            next_plane=(next_plane+3)%6;
            effect_plane_flip(LUT,now_plane,next_plane,delay);
            now_plane=next_plane;
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }

        }
    }
//  LUT_START // Macro
   
}


/*............................Hiệu ứng fire work............................*/
void effect_fireworks (uint8_t enordis, uint32_t timeRun, int delay)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();  
        fill_cube(0x00);
        int i,f,e;
        int n = 50; 
        float origin_x = 3;
        float origin_y = 3;
        float origin_z = 3;

        int rand_y, rand_x, rand_z;

        float slowrate, gravity;

        // Particles and their position, x,y,z and their movement, dx, dy, dz
        float particles[50][6];

        for (i=0; i<20000; ++i)
        {

            origin_x = rand()%4;
            origin_y = rand()%4;
            origin_z = rand()%2;
            origin_z +=5;
                origin_x +=2;
                origin_y +=2;

            // shoot a particle up in the air
            rgb_t rgb = Scroll(random_number(15,255));
            for (e=0;e<origin_z;++e)
            {
            int randx = random_number(-1,1);
            int randy;
            if(randx != 0)
            {
                randy = 0;
            }
            else
            {
                randy = random_number(-1,1);
            }
            
            
            if (e == origin_z - 1)
            { 
                setvoxel(e,origin_y ,origin_x );
                set_color_setvoxel(rgb.r,rgb.g, rgb.b);
            }
            else if (e == origin_z - 2)
            {
                setvoxel(e,origin_y ,origin_x );
                set_color_setvoxel(rgb.r,rgb.g, rgb.b);
                rgb = Scroll(random_number(15,255));
                setvoxel(e-1,origin_y ,origin_x );
                set_color_setvoxel(rgb.r,rgb.g, rgb.b);
            }
            else
            {
                setvoxel(e,origin_y ,origin_x );
                set_color_setvoxel(rgb.r,rgb.g, rgb.b);
                setvoxel(e-1,origin_y ,origin_x );
                rgb = Scroll(random_number(15,255));
                set_color_setvoxel(rgb.r,rgb.g, rgb.b);
                setvoxel(e-2,origin_y ,origin_x );
                rgb = Scroll(random_number(15,255));
                set_color_setvoxel(rgb.r,rgb.g, rgb.b);
            }
            
            
            
            
            delay_ms(50+20*e);
            fill_cube(0x00);
            }

            // Fill particle array
            for (f=0; f<n; ++f)
            {
            // Position
            particles[f][0] = origin_x;
            particles[f][1] = origin_y;
            particles[f][2] = origin_z;
            
            rand_x = rand()%200;
            rand_y = rand()%200;
            rand_z = rand()%200;

            // Movement
            particles[f][3] = 1-(float)rand_x/100; // dx
            particles[f][4] = 1-(float)rand_y/100; // dy
            particles[f][5] = 1-(float)rand_z/100; // dz
            }

            // explode
            for (e=0; e<25; ++e)
            {
            slowrate = 1+tan((e+0.1)/20)*10;
            
            gravity = tan((e+0.1)/20)/2;

            for (f=0; f<n; ++f)
            {
                particles[f][0] += particles[f][3]/slowrate;
                particles[f][1] += particles[f][4]/slowrate;
                particles[f][2] += particles[f][5]/slowrate;
                particles[f][2] -= gravity;

                setvoxel(particles[f][2],particles[f][1],particles[f][0]);
                rgb_t rgb = Scroll(f*5) ;
                set_color_setvoxel(rgb.r,rgb.g,rgb.b);
                


            }

            delay_ms(delay);
            fill_cube(0x00);
            }

            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        } 

    }

}

/*..........................Hiệu ứng sinline.................................*/

void line_3d (int x1, int y1, int z1, int x2, int y2, int z2)
{
    int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc,
    err_1, err_2, dx2, dy2, dz2;
    int pixel[3];
    pixel[0] = x1;
    pixel[1] = y1;
    pixel[2] = z1;
    dx = x2 - x1;
    dy = y2 - y1;
    dz = z2 - z1;
    x_inc = (dx < 0) ? -1 : 1;
    l = abs(dx);
    y_inc = (dy < 0) ? -1 : 1;
    m = abs(dy);
    z_inc = (dz < 0) ? -1 : 1;
    n = abs(dz);
    dx2 = l << 1;
    dy2 = m << 1;
    dz2 = n << 1;
    if ((l >= m) && (l >= n)) {
    err_1 = dy2 - l;
    err_2 = dz2 - l;
    for (i = 0; i < l; ++i) {
    //PUT_PIXEL(pixel);
    setvoxel(pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    if (err_1 > 0) {
    pixel[1] += y_inc;
    err_1 -= dx2;
    }
    if (err_2 > 0) {
    pixel[2] += z_inc;
    err_2 -= dx2;
    }
    err_1 += dy2;
    err_2 += dz2;
    pixel[0] += x_inc;
    }
    } else if ((m >= l) && (m >= n)) {
    err_1 = dx2 - m;
    err_2 = dz2 - m;
    for (i = 0; i < m; ++i) {
    //PUT_PIXEL(pixel);
    setvoxel(pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    if (err_1 > 0) {
    pixel[0] += x_inc;
    err_1 -= dy2;
    }
    if (err_2 > 0) {
    pixel[2] += z_inc;
    err_2 -= dy2;
    }
    err_1 += dx2;
    err_2 += dz2;
    pixel[1] += y_inc;
    }
    } else {
    err_1 = dy2 - n;
    err_2 = dx2 - n;
    for (i = 0; i < n; ++i) {
    setvoxel(pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    //PUT_PIXEL(pixel);
    if (err_1 > 0) {
    pixel[1] += y_inc;
    err_1 -= dz2;
    }
    if (err_2 > 0) {
    pixel[0] += x_inc;
    err_2 -= dz2;
    }
    err_1 += dy2;
    err_2 += dx2;
    pixel[2] += z_inc;
    }
    }
    setvoxel(pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    //PUT_PIXEL(pixel);
}


void effect_sinelines (uint8_t enordis, uint32_t timeRun, int delay)
{
    if(enordis == ENABLE)
    {
        timeStart = sec();
        int i,x;

        float left, right, sine_base, x_dividor,ripple_height;

        for (i=0; i<20000; ++i)
        {
            for (x=0; x<8 ;++x)
            {
            x_dividor = 2 + sin((float)i/100)+1;
            ripple_height = 3 + (sin((float)i/200)+1)*6;

            sine_base = (float) i/40 + (float) x/x_dividor;

            left = 4 + sin(sine_base)*ripple_height;
            right = 4 + cos(sine_base)*ripple_height;
            right = 7-left;

            //printf("%i %i \n", (int) left, (int) right);

            line_3d(0-3, x, (int) left, 7+3, x, (int) right);
            //line_3d((int) right, 7, x);
            }
            change_rgb_color();
            delay_ms(delay);
            fill_cube(0x00);

            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }

    }
  
}

/*..........................Hiệu ứng linespin.............................*/
void effect_linespin (uint8_t enordis, uint32_t timeRun, int delay)
{

    if(enordis == ENABLE)
    {
        timeStart = sec();
        float top_x, top_y, top_z, bot_x, bot_y, bot_z, sin_base;
        float center_x, center_y;

        center_x = 4;
        center_y = 4;

        int i, z;
        for (i=0;i<20000;++i)
        {

            //printf("Sin base %f \n",sin_base);

            for (z = 0; z < 8; ++z)
            {

            sin_base = (float)i/50 + (float)z/(10+(7*sin((float)i/200)));

            top_x = center_x + sin(sin_base)*5;
            top_y = center_x + cos(sin_base)*5;
            //top_z = center_x + cos(sin_base/100)*2.5;

            bot_x = center_x + sin(sin_base+3.14)*10;
            bot_y = center_x + cos(sin_base+3.14)*10;
            //bot_z = 7-top_z;
            
            bot_z = z;
            top_z = z;

            // setvoxel((int) top_x, (int) top_y, 7);
            // setvoxel((int) bot_x, (int) bot_y, 0);

            //printf("P1: %i %i %i P2: %i %i %i \n", (int) top_x, (int) top_y, 7, (int) bot_x, (int) bot_y, 0);

            //line_3d((int) top_x, (int) top_y, (int) top_z, (int) bot_x, (int) bot_y, (int) bot_z);
            line_3d((int) top_z, (int) top_x, (int) top_y, (int) bot_z, (int) bot_x, (int) bot_y);
            }

            change_rgb_color();
            delay_ms(delay);
            fill_cube(0x00);
            
            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }
        }

    }
 
}




/*.................................boxside_randsend_parallel..........................................*/

void effect_boxside_randsend_parallel(uint8_t axis, uint8_t origin, uint8_t delay, uint8_t mode) {
    uint8_t i;
    uint8_t done;
    uint8_t cubepos[CUBE_SIZE * CUBE_SIZE];
    uint8_t pos[CUBE_SIZE * CUBE_SIZE];
    uint8_t notdone=1;
    uint8_t notdone2=1;
    uint8_t sent=0;
   // fill_cube(0x00);

    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        pos[i]=0;
    }

    while (notdone) {
        if (mode==1) {
            notdone2=1;
            while (notdone2 && sent<(CUBE_SIZE * CUBE_SIZE)) {
                i=rand() % (CUBE_SIZE * CUBE_SIZE);
                if (pos[i]==0) {
                    sent++;
                    pos[i] += 1;
                    notdone2=0;
                }
            }
        } else if (mode==2) {
            if (sent<(CUBE_SIZE * CUBE_SIZE)) {
                pos[sent] += 1;
                sent++;
            }
        }
        done=0;
        for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
            if (pos[i]>0 && pos[i]<(CUBE_SIZE - 1)) {
                pos[i] += 1;
            }
            if (pos[i]==(CUBE_SIZE - 1))
                done++;
        }
        if (done==(CUBE_SIZE * CUBE_SIZE))
            notdone=0;

        for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
            if (origin==0) {
                cubepos[i]=pos[i];
            } else {
                cubepos[i]=((CUBE_SIZE - 1) - pos[i]);
            }
        }
        delay_ms(30);
        draw_positions_axis(axis, cubepos, 0);
        // LED_PORT ^= LED_RED;
    }
    delay_ms(30);
}

/*--------------------------Hiệu ứng StringFly--------------------------------*/
unsigned char font8eng[728][8]={
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, //    0
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},// !  1
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // "
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // #
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // $
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // %
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // &
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // '
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},// (  8
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // )  9
    {0x22,0x36,0x3e,0xfc,0xfc,0x3e,0x36,0x22}, // *
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // +
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // ,
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // -
    {0x00,0x00,0x00,0x03,0x03,0x00,0x00,0x00}, // .
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},// /
    {0x00,0x00,0x3e,0x41,0x41,0x3e,0x00,0x00}, // 0
    {0x00,0x00,0x01,0x7f,0x21,0x00,0x00,0x00}, // 1
    {0x00,0x00,0x31,0x49,0x45,0x23,0x00,0x00}, // 2
    {0x00,0x00,0x36,0x49,0x49,0x22,0x00,0x00}, // 3
    {0x00,0x04,0x7f,0x24,0x14,0x0c,0x00,0x00}, // 4
    {0x00,0x00,0x46,0x49,0x49,0x79,0x00,0x00}, // 5
    {0x00,0x00,0x26,0x49,0x49,0x3e,0x00,0x00}, // 6
    {0x00,0x00,0x70,0x48,0x47,0x40,0x00,0x00}, // 7
    {0x00,0x00,0x36,0x49,0x49,0x36,0x00,0x00}, // 8
    {0x00,0x00,0x3e,0x49,0x49,0x32,0x00,0x00}, // 9
    {0x3e,0x41,0x41,0x3e,0x00,0x01,0x7f,0x21}, // 10 :
    {0x3e,0x41,0x41,0x3e,0x00,0x01,0x7f,0x21}, // ; 10
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // <
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // =
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // >
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // ?
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // @
    {0x00,0x00,0x3f,0x48,0x48,0x3f,0x00,0x00}, // A
    {0x00,0x00,0x36,0x49,0x49,0x7f,0x00,0x00}, // B
    {0x00,0x00,0x26,0x41,0x41,0x3e,0x00,0x00}, // C
    {0x00,0x00,0x3e,0x41,0x41,0x7f,0x00,0x00}, // D
    {0x00,0x00,0x49,0x49,0x49,0x7f,0x00,0x00}, // E
    {0x00,0x00,0x48,0x48,0x48,0x7f,0x00,0x00}, // F  38
    {0x00,0x2e,0x49,0x49,0x41,0x3e,0x00,0x00}, // G  39
    {0x00,0x00,0x7f,0x08,0x08,0x7f,0x00,0x00}, // H
    {0x00,0x00,0x41,0x7f,0x41,0x00,0x00,0x00}, // I
    {0x00,0x00,0x40,0x7e,0x41,0x06,0x00,0x00}, // J
    {0x00,0x00,0x63,0x14,0x08,0x7f,0x00,0x00}, // K
    {0x00,0x00,0x01,0x01,0x01,0x7f,0x00,0x00}, // L
    {0x00,0x7f,0x20,0x18,0x20,0x7f,0x00,0x00}, // M
    {0x00,0x7f,0x06,0x18,0x20,0x7f,0x00,0x00}, // N
    {0x00,0x00,0x3e,0x41,0x41,0x3e,0x00,0x00}, // O
    {0x00,0x00,0x30,0x48,0x48,0x7f,0x00,0x00}, // P
    {0x00,0x01,0x3e,0x45,0x41,0x3e,0x00,0x00}, // Q
    {0x00,0x00,0x33,0x4c,0x48,0x7f,0x00,0x00}, // R
    {0x00,0x00,0x26,0x49,0x49,0x32,0x00,0x00}, // S
    {0x00,0x40,0x40,0x7f,0x40,0x40,0x00,0x00}, // T  52
    {0x00,0x00,0x7e,0x01,0x01,0x7e,0x00,0x00}, // U  53
    {0x00,0x78,0x06,0x01,0x06,0x78,0x00,0x00}, // V  54
    {0x00,0x7e,0x01,0x06,0x01,0x7e,0x00,0x00}, // W  55
    {0x00,0x63,0x14,0x08,0x14,0x63,0x00,0x00}, // X  56
    {0x00,0x60,0x10,0x0f,0x10,0x60,0x00,0x00}, // Y  57
    {0x00,0x61,0x51,0x49,0x45,0x43,0x00,0x00}, // Z  58
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // [  59
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // \  60
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // ]  61
    {0x22,0x36,0x3e,0xfc,0xfc,0x3e,0x36,0x22}, // ^  62 0x44,0x6c,0x7c,0x3f,0x3f,0x7c,0x6c,0x44
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // _  63
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // `  64
    {0x00,0x00,0x3f,0x48,0x48,0x3f,0x00,0x00}, // A
    {0x00,0x00,0x36,0x49,0x49,0x7f,0x00,0x00}, // B
    {0x00,0x00,0x26,0x41,0x41,0x3e,0x00,0x00}, // C
    {0x00,0x00,0x3e,0x41,0x41,0x7f,0x00,0x00}, // D
    {0x00,0x00,0x49,0x49,0x49,0x7f,0x00,0x00}, // E
    {0x00,0x00,0x48,0x48,0x48,0x7f,0x00,0x00}, // F  38
    {0x00,0x2e,0x49,0x49,0x41,0x3e,0x00,0x00}, // G  39
    {0x00,0x00,0x7f,0x08,0x08,0x7f,0x00,0x00}, // H
    {0x00,0x00,0x41,0x7f,0x41,0x00,0x00,0x00}, // I
    {0x00,0x00,0x40,0x7e,0x41,0x06,0x00,0x00}, // J
    {0x00,0x00,0x63,0x14,0x08,0x7f,0x00,0x00}, // K
    {0x00,0x00,0x01,0x01,0x01,0x7f,0x00,0x00}, // L
    {0x00,0x7f,0x20,0x18,0x20,0x7f,0x00,0x00}, // M
    {0x00,0x7f,0x06,0x18,0x20,0x7f,0x00,0x00}, // N
    {0x00,0x00,0x3e,0x41,0x41,0x3e,0x00,0x00}, // O
    {0x00,0x00,0x30,0x48,0x48,0x7f,0x00,0x00}, // P
    {0x00,0x01,0x3e,0x45,0x41,0x3e,0x00,0x00}, // Q
    {0x00,0x00,0x33,0x4c,0x48,0x7f,0x00,0x00}, // R
    {0x00,0x00,0x26,0x49,0x49,0x32,0x00,0x00}, // S
    {0x00,0x40,0x40,0x7f,0x40,0x40,0x00,0x00}, // T  52
    {0x00,0x00,0x7e,0x01,0x01,0x7e,0x00,0x00}, // U  53
    {0x00,0x78,0x06,0x01,0x06,0x78,0x00,0x00}, // V  54
    {0x00,0x7e,0x01,0x06,0x01,0x7e,0x00,0x00}, // W  55
    {0x00,0x63,0x14,0x08,0x14,0x63,0x00,0x00}, // X  56
    {0x00,0x60,0x10,0x0f,0x10,0x60,0x00,0x00}, // Y  57
    {0x00,0x61,0x51,0x49,0x45,0x43,0x00,0x00}, // Z  58
}; // z  90        {0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff}



void font_getchar(int chr,unsigned char* dst) {
    chr -= 32; // our bitmap font starts at ascii char 32                   33
    /* Ìàññèâ ñêðûò äëÿ ýêîíîìèè ïàìÿòü
      if (CUBE_SIZE==4)
        {
        for (i=0; i<CUBE_SIZE; i++)
        {
          dst[i]=font4eng[chr][i];
        }
      }
     */
    if (CUBE_SIZE==8) {
         for(int i=0; i<CUBE_SIZE; i++) {
            *(dst + i) =  (unsigned char)font8eng[chr][i];
        }
    }
}

void effect_stringfly(uint8_t enordis, char *str) {
    if(enordis == ENABLE)
    {
        int x, y, z, i, ii;
        unsigned char chr[100];
        fill_cube(0x00);

        while (*str) {
            font_getchar((*str++), chr);

            for (z=(CUBE_SIZE - 1); z>=0; z--) {
                for (y=(CUBE_SIZE - 1); y>=0; y--) {
                    if (chr[z] >> y & 0x01) {
                        setvoxel(y, z, 7);
                        
                    }
                }
            }
            // Shift the entire contents of the cube forward by 6 steps
            // before placing the next character
            // for (i= 0; i<CUBE_SIZE-1; i++) {
            //     delay_ms(50); 
            //     shift(3, 1);
            //     change_rgb_color();
                
            // }

            for (i= CUBE_SIZE-1; i>0; i--) {
                delay_ms(70); 
                shift(3, 0);
                change_rgb_color_2();
                
            }
            delay_ms(500); // çàäåðæêà ìåæäó áóêâàìè
           
        }
        return;
    }
    
}


void effect_pathmove(unsigned char *path, int length) {
    int i, z;
    unsigned char state;

    // Äâèæåíèå ñïðàâî íàëåâî
    for (i=(length - 1); i>=1; i--) {
        for (z=0; z<CUBE_SIZE; z++) {
            state=getvoxel(z, CUBE_SIZE - 1 - (path[(i - 1)] >> 4 & 0x0f), CUBE_SIZE - 1 - ((path[(i - 1)])&0x0f));
            altervoxel(z, CUBE_SIZE - 1 - (path[i] >> 4 & 0x0f), CUBE_SIZE - 1 - ((path[i])&0x0f), state);
        }
    }
    for (z=0; z<CUBE_SIZE; z++)
        clrvoxel(z, CUBE_SIZE - 1 - (path[0] >> 4 & 0x0f), CUBE_SIZE - 1 - ((path[0])&0x0f));
}

unsigned char paths_8[44]={// circle, len 16, offset 28
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00, 0x10, 0x20,
    0x30, 0x40, 0x50, 0x60, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75,
    0x76, 0x77, 0x67, 0x57, 0x47, 0x37, 0x27, 0x17, 0x04, 0x03,
    0x12, 0x21, 0x30, 0x40, 0x51, 0x62, 0x73, 0x74, 0x65, 0x56,
    0x47, 0x37, 0x26, 0x15
};

void font_getpath(unsigned char path, unsigned char *destination, int length) 
{
    int i;
    int offset=0;

    if (path==1)
        offset=28;

    for (i=0; i<length; i++)
        destination[i]=paths_8[i + offset];
}

void font_getchar2(int chr,unsigned char* dst) {
    chr -= 32; // our bitmap font starts at ascii char 32                   33
    /* Ìàññèâ ñêðûò äëÿ ýêîíîìèè ïàìÿòü
      if (CUBE_SIZE==4)
        {
        for (i=0; i<CUBE_SIZE; i++)
        {
          dst[i]=font4eng[chr][i];
        }
      }
     */
    if (CUBE_SIZE==8) {
         for(int i=0; i<CUBE_SIZE; i++) {
            *(dst + i) =  (unsigned char)font8eng[chr][7-i];
        }
    }
}

void effect_path_text(uint8_t enordis, int delay, char *str) 
{
    if(enordis == ENABLE)
    {
        int z, i, ii;
        unsigned char path[28]; // 28 - ïåðèìåòð êóáà 8x8x8
        unsigned char chr[CUBE_SIZE];
        unsigned char stripe;
        fill_cube(0x00);
        font_getpath(0, path, 28);

        while (*str) {
            font_getchar2(*str++, chr);
            for (ii=0; ii<CUBE_SIZE; ii++) // ii<5
            {
                stripe=chr[ii];

                for (z=0; z<CUBE_SIZE; z++) {
                    if ((stripe >> z) & 0x01) {
                        setvoxel(z, (CUBE_SIZE - 1), 0);
                    } else {
                        clrvoxel(z, (CUBE_SIZE - 1), 0);
                    }
                }
                effect_pathmove(path, 28);
                delay_ms(delay);
                change_rgb_color();
            
            }
            effect_pathmove(path, 28);
            delay_ms(delay);
            
        
        }
        for (i=0; i<28; i++) {
            effect_pathmove(path, 28);
            delay_ms(delay);
        }
        return;

    }
    
}


/*.............................*/

float distance2d (float x1, float y1, float x2, float y2)
{	
	float dist;
	dist = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

	return dist;
}

float distance3d (float x1, float y1, float z1, float x2, float y2, float z2)
{	
	float dist;
	dist = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2));

	return dist;
}

void effect_ball_bouncing(uint8_t enordis, uint32_t timeRun, int delay)
{
	if(enordis == ENABLE)
    {
        timeStart = sec();

        fill_cube(0x00);

        float origin_x, origin_y, origin_z, distance, diameter;

        origin_x = 0;
        origin_y = 3.5;
        origin_z = 3.5;

        diameter = 3;

        uint32_t x, y, z, i;

        for (i=0; i<2000000; i++)
        {
            origin_x = 3.5+sin((float)i/50)*2.5;
            origin_y = 3.5+cos((float)i/50)*2.5;
            origin_z = 3.5+cos((float)i/30)*2;

            //diameter = 2+sin((float)i/150);
            diameter = 1;

            for (x=0; x<8; x++)
            {
                for (y=0; y<8; y++)
                {
                    for (z=0; z<8; z++)
                    {
                        distance = distance3d(x,y,z, origin_x, origin_y, origin_z);
                        //printf("Distance: %f \n", distance);

                        if (distance>diameter && distance<diameter+1)
                        {
                            setvoxel(x,y,z);
                        }
                    }
                }
            }
            if(i%6 == 0)
                change_rgb_color();
            delay_ms(delay);
            fill_cube(0x00);

            if(sec() - timeStart >= timeRun)
                return;
            if(restart_value == 1)
            {
                restart_value = 0; fill_cube(0x00);delay_ms(1000);
                return;
            }

        }
    }
	

}


void sidewaves (int iterations, int delay)
{
	float origin_x, origin_y, distance, height, ripple_interval;
	int x,y,i;

	fill_cube(0x00);

	for (i=0;i<iterations;i++)
	{

		origin_x = 3.5+sin((float)i/500)*4;
		origin_y = 3.5+cos((float)i/500)*4;
		
		for (x=0;x<8;x++)
		{
			for (y=0;y<8;y++)
			{
				distance = distance2d(origin_x,origin_y,x,y)/9.899495*8;
				ripple_interval =2;
				height = 4+sin(distance/ripple_interval+(float) i/50)*3.6;

				setvoxel((int)height,y,x);
					
			}
		}        
        change_rgb_color();
        delay_ms(delay);
        fill_cube(0x00);

	}
}


/*----------------------------------Hiệu ứng SPIN-----------------------------*/
volatile const unsigned char bitmaps[14][8]={
    {0x7e,0xff,0x81,0x18,0x18,0x00,0xc3,0xc3}, // smiley 3 small
    {0x3c, 0x42, 0x81, 0x81, 0xc3, 0x24, 0xa5, 0xe7}, // Omega
    {0x00, 0x04, 0x06, 0xff, 0xff, 0x06, 0x04, 0x00}, // Arrow
    {0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81}, // X
    {0xBD, 0xA1, 0xA1, 0xB9, 0xA1, 0xA1, 0xA1, 0x00}, // ifi
    {0xEF, 0x48, 0x4B, 0x49, 0x4F, 0x00, 0x00, 0x00}, // TG
    {0x38, 0x7f, 0xE6, 0xC0, 0xE6, 0x7f, 0x38, 0x00}, // Commodore symbol
    {0x08, 0x1c, 0x3e, 0x3e, 0x7f , 0x77 ,0x22, 0x00}, // Heart {, , , , , , , },0x08, 0x1c, 0x3e, 0x3e, 0x7f , 0x77 ,0x22, 0x00
    {0x1C, 0x22, 0x55, 0x49, 0x5d, 0x22, 0x1c, 0x00}, // face
    {0x37, 0x42, 0x22, 0x12, 0x62, 0x00, 0x7f, 0x00}, // ST
    {0x89, 0x4A, 0x2c, 0xF8, 0x1F, 0x34, 0x52, 0x91}, // STAR
    {0x18, 0x3c, 0x7e, 0xdb, 0xff, 0x24, 0x5a, 0xa5}, // Space Invader
    {0x00, 0x9c, 0xa2, 0xc5, 0xc1, 0xa2, 0x9c, 0x00},// Fish
    {0x18,0x0c,0x0e,0x0e,0x0e,0x1e,0xfc,0x78} };      //moon
    
unsigned char font_getbitmappixel(int bitmap, int x, int y) {
    char tmp=bitmaps[bitmap][x];
    return (tmp >> y) & 0x01;
}

void effect_smileyspin (int count, int delay, char bitmap)
{
	unsigned char dybde[] = {0,1,2,3,4,5,6,7,1,1,2,3,4,5,6,6,2,2,3,3,4,4,5,5,3,3,3,3,4,4,4,4};
	int d = 0;
	int flip = 0;
	int x, y, off;
	for(int i = 0; i<count; i++)
	{
		flip = 0;
		d = 0;
		off = 0;
		// front:
		for (int s=0;s<7;s++){
			if(!flip){
				off++;
				if (off == 4){
					flip = 1;
					off = 0;
				}
			} else {
				off++;
			}
		        for (x=0; x<8; x++)
        		{
				d = 0;
                		for (y=0; y<8; y++)
	                	{
					if (font_getbitmappixel ( bitmap, 7-x, y)){
						if (!flip)
							setvoxel(x,y,dybde[8 * off + d++]);
						else
							setvoxel(x,y,dybde[31 - 8 * off - d++]);
					} else {
						d++;
					}
				}
			}
			delay_ms(delay);
			fill_cube(0x00);
		}

		// side:
		off = 0;
		flip = 0;
		d = 0;
		for (int s=0;s<7;s++){
			if(!flip){
				off++;
				if (off == 4){
					flip = 1;
					off = 0;
				}
			} else {
				off++;
			}
		        for (x=0; x<8; x++)
        		{
				d = 0;
                		for (y=0; y<8; y++)
	                	{
					if (font_getbitmappixel ( bitmap, 7-x, y)){
						if (!flip)
							setvoxel(x, dybde[8 * off + d++], 7- y);
						else
							setvoxel(x,dybde[31 - 8 * off - d++], 7- y);
					} else {
						d++;
					}
				}
			}
			delay_ms(delay);
			fill_cube(0x00);
		}


		flip = 0;
		d = 0;
		off = 0;
		// back:
		for (int s=0;s<7;s++){
			if(!flip){
				off++;
				if (off == 4){
					flip = 1;
					off = 0;
				}
			} else {
				off++;
			}
		        for (x=0; x<8; x++)
        		{
				d = 0;
                		for (y=0; y<8; y++)
	                	{
					if (font_getbitmappixel ( bitmap, 7-x, 7-y)){
						if (!flip)
							setvoxel(x,y,dybde[8 * off + d++]);
						else
							setvoxel(x,y,dybde[31 - 8 * off - d++]);
					} else {
						d++;
					}
				}
			}
			delay_ms(delay);
			fill_cube(0x00);
		}

		// other side:
		off = 0;
		flip = 0;
		d = 0;
		for (int s=0;s<7;s++){
			if(!flip){
				off++;
				if (off == 4){
					flip = 1;
					off = 0;
				}
			} else {
				off++;
			}
		        for (x=0; x<8; x++)
        		{
				d = 0;
                		for (y=0; y<8; y++)
	                	{
					if (font_getbitmappixel ( bitmap, 7-x, 7-y)){
						if (!flip)
							setvoxel(x, dybde[8 * off + d++], 7-y);
						else
							setvoxel(x,dybde[31 - 8 * off - d++], 7-y);
					} else {
						d++;
					}
				}
			}
			delay_ms(delay);
			fill_cube(0x00);
		}

	}
}
/////////////////////////////////////////////////////////////

typedef struct{
    uint16_t state;
    uint16_t time_run;
    char *stringbuff;
    
}effect_config;

effect_config rain_config;
effect_config rainbow_config;
effect_config planboing_config;
effect_config sendvoxelsrandz_config;
effect_config sendvoxelsrandy_config;
effect_config boxshrinkgrow_config;
effect_config boxwoopwoop_config;
effect_config axiszupdownrs_config;
effect_config axisyupdownrs_config;
effect_config sidewaves_config;
effect_config wormsqueeze_config;
effect_config boingboing1_config;
effect_config boingboing2_config;
effect_config boingboing3_config;
effect_config randomfiller_config;

effect_config ripples_config;
effect_config cubix_config;
effect_config filip_filop_config;
effect_config sinelines_config;
effect_config stringfly_config;
effect_config fireworks_config;
effect_config path_text_config;
effect_config ball_bouncing_config;

uint8_t brightness_value;

char *stringbuffer;

void showColumm(uint8_t eff,  short columm, short height);
void cube_run(void *pvParameters)
{
    // Khởi tạo strip

   // set_brightness(brightness_value);
    set_brightness(8);
    ESP_ERROR_CHECK(led_strip_flush(&strip));


    //set_color_to_buffer(2,0,0, (rgb_t){ .r = 255, .g = 0, .b = 0 });
    
    fill_cube(0x00);
    show_led();;
    // for(int i = 0; i <=15; i++)
    // {
    //     if(i <=7)
    //         showColumm(0,i,i);
    //     else if( i <15 && i >7)
    //          showColumm(0,i,i-7);
    //     else
    //         showColumm(0,15,3);
    // }

    

    // show_led();

    while (1)
    {
        delay_ms(15);
    //    printf("hello\n");
        // effect_rain(rain_config.state, rain_config.time_run,50,0);
        // effect_rainbow(rainbow_config.state, rainbow_config.time_run,15);
        // effect_planboing(planboing_config.state, planboing_config.time_run);
        // effect_sendvoxels_rand_axis(sendvoxelsrandz_config.state, sendvoxelsrandz_config.time_run, AXIS_Z, 10,500);
        // effect_sendvoxels_rand_axis(sendvoxelsrandy_config.state, sendvoxelsrandy_config.time_run, AXIS_Y, 10,500);
        // effect_box_shrink_grow(boxshrinkgrow_config.state, boxshrinkgrow_config.time_run,50);
        // effect_box_woopwoop(boxwoopwoop_config.state,boxwoopwoop_config.time_run);
        // effect_axis_z_updown_randsuspend(axiszupdownrs_config.state,axiszupdownrs_config.time_run);
        // effect_axis_y_updown_randsuspend(axisyupdownrs_config.state,axisyupdownrs_config.time_run);
        // effect_int_sidewaves(sidewaves_config.state,sidewaves_config.time_run,10);
        // effect_wormsqueeze(wormsqueeze_config.state,(wormsqueeze_config.time_run)/2,50,2,0);
        // effect_wormsqueeze(wormsqueeze_config.state,(wormsqueeze_config.time_run)/2,50,3,1);
        // effect_random_filler(randomfiller_config.state,randomfiller_config.time_run,20);
        // effect_boingboing(boingboing1_config.state,boingboing1_config.time_run,40, 0x01,0x01);
        // effect_boingboing(boingboing2_config.state,boingboing2_config.time_run,40, 0x01,0x03);
        // effect_boingboing(boingboing3_config.state,boingboing3_config.time_run,40, 0x01,0x02);



        // effect_int_ripples(ripples_config.state,ripples_config.time_run,40); // ok
        // effect_cubix(cubix_config.state,cubix_config.time_run,3);   //ok
        // effect_filip_filop(filip_filop_config.state,filip_filop_config.time_run,50); // ok
        // effect_sinelines(sinelines_config.state,sinelines_config.time_run,10);
        // effect_stringfly(stringfly_config.state,stringfly_config.stringbuff);
        // effect_fireworks(fireworks_config.state,fireworks_config.time_run,50);
        // effect_path_text(path_text_config.state, 100,path_text_config.stringbuff);

        // effect_ball_bouncing(ball_bouncing_config.state,ball_bouncing_config.time_run,10); 
        
     

       

  


        //  effect_stringfly(1,";9876543210^");
        //  effect_fireworks(1,15,50);
        //  effect_int_ripples(1,15,40); // ok
      //  effect_smileyspin(1000,300,2);
     
       // sidewaves(10000,10); //bị chậm


//   effect_linespin(1,20,10);
      
    // effect_boxside_randsend_parallel(2, 0, 1, 2);
        // effect_boxside_randsend_parallel(2, 1, 1, 2);
        // effect_boxside_randsend_parallel(2, 0, 1, 2);
        // effect_boxside_randsend_parallel(2, 1, 1, 2);
        // effect_boxside_randsend_parallel(2, 0, 1, 2);
        // effect_boxside_randsend_parallel(2, 1, 1, 2);
        // effect_boxside_randsend_parallel(2, 0, 1, 2);
        // effect_boxside_randsend_parallel(2, 1, 1, 2);
        // // effect_boxside_randsend_parallel(3,0,1,1);
        // effect_boxside_randsend_parallel(3,1,1,1);

        // effect_boxside_randsend_parallel(3, 0, 1, 2);
        // effect_boxside_randsend_parallel(3, 1, 1, 2);
        // effect_boxside_randsend_parallel(3, 0, 1, 2);
        // effect_boxside_randsend_parallel(3, 1, 1, 2);
        // effect_boxside_randsend_parallel(3, 0, 1, 2);
        // effect_boxside_randsend_parallel(3, 1, 1, 2);
        // effect_boxside_randsend_parallel(3, 0, 1, 2);
        // effect_boxside_randsend_parallel(3, 1, 1, 2);
      
         
       
    }

}

void timer_interrupt()
{
    s_timer_queue = xQueueCreate(10, sizeof(example_timer_event_t));

    example_tg_timer_init(TIMER_GROUP_0, TIMER_0, true, 3);

    while (1) {
        example_timer_event_t evt;
        xQueueReceive(s_timer_queue, &evt, portMAX_DELAY);
        show_led();
    }
}






void get_data_config_effect(char * input_string)
{
    char *token1, *token2;
    char *rest = input_string;
    char data_eff[100][20];
    
    
    // Mảng để lưu các chuỗi con sau khi tách bằng kí tự '/'
    char substrings[20][100]; // Điều này tùy thuộc vào độ dài tối đa của chuỗi con

    int i = 0;
    int j = 0;
    while ((token1 = strtok_r(rest, "/", &rest))) {
        // Lưu chuỗi con vào mảng substrings
        strcpy(substrings[i], token1);
        
        // Tách chuỗi con thành các phần bằng kí tự ':'
        token2 = strtok(substrings[i], ":");
        
        // In ra các phần bằng kí tự ':'
        while (token2 != NULL) {
            strcpy(data_eff[j], token2);
            j++;
         //   printf("%s\n", token2);
            token2 = strtok(NULL, ":");
        }
        i++;
    }
    rain_config.state = atoi(data_eff[1]);
    rain_config.time_run = atoi(data_eff[2]);

    rainbow_config.state = atoi(data_eff[4]);
    rainbow_config.time_run = atoi(data_eff[5]);

    planboing_config.state = atoi(data_eff[7]);
    planboing_config.time_run = atoi(data_eff[8]);

    sendvoxelsrandz_config.state = atoi(data_eff[10]);
    sendvoxelsrandz_config.time_run = atoi(data_eff[11]);

    sendvoxelsrandy_config.state = atoi(data_eff[13]);
    sendvoxelsrandy_config.time_run = atoi(data_eff[14]);

    boxshrinkgrow_config.state = atoi(data_eff[16]);
    boxshrinkgrow_config.time_run = atoi(data_eff[17]);

    boxwoopwoop_config.state = atoi(data_eff[19]);
    boxwoopwoop_config.time_run = atoi(data_eff[20]);

    axiszupdownrs_config.state = atoi(data_eff[22]);
    axiszupdownrs_config.time_run = atoi(data_eff[23]);

    axisyupdownrs_config.state = atoi(data_eff[25]);
    axisyupdownrs_config.time_run = atoi(data_eff[26]);

    sidewaves_config.state = atoi(data_eff[28]);
    sidewaves_config.time_run = atoi(data_eff[29]);

    wormsqueeze_config.state = atoi(data_eff[31]);
    wormsqueeze_config.time_run = atoi(data_eff[32]);

    boingboing1_config.state = atoi(data_eff[34]);
    boingboing1_config.time_run = atoi(data_eff[35]);

    boingboing2_config.state = atoi(data_eff[37]);
    boingboing2_config.time_run = atoi(data_eff[38]);

    boingboing3_config.state = atoi(data_eff[40]);
    boingboing3_config.time_run = atoi(data_eff[41]);

    randomfiller_config.state = atoi(data_eff[43]);
    randomfiller_config.time_run = atoi(data_eff[44]);

    ripples_config.state = atoi(data_eff[46]);
    ripples_config.time_run = atoi(data_eff[47]);

    cubix_config.state = atoi(data_eff[49]);
    cubix_config.time_run = atoi(data_eff[50]);

    filip_filop_config.state = atoi(data_eff[52]);
    filip_filop_config.time_run = atoi(data_eff[53]);

    sinelines_config.state = atoi(data_eff[55]);
    sinelines_config.time_run = atoi(data_eff[56]);

    stringfly_config.state = atoi(data_eff[58]);
    stringfly_config.stringbuff = data_eff[59];

    fireworks_config.state = atoi(data_eff[61]);
    fireworks_config.time_run = atoi(data_eff[62]);

    path_text_config.state = atoi(data_eff[64]);
    path_text_config.stringbuff = data_eff[65];

    ball_bouncing_config.state = atoi(data_eff[67]);
    ball_bouncing_config.time_run = atoi(data_eff[68]);

    brightness_value = atoi(data_eff[71]);
    printf("%d\n",brightness_value);

    set_brightness(brightness_value);


    
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
        //read data
        // key : value
        // "data" : 1111
        // uint16_t read_data;
        // nvs_get_u16(nvs_handle,"data",&read_data); 
        // printf("read data : %d \n",read_data);

        //write data
        // key : value

        nvs_set_u16(nvs_handle,"1",rain_config.state);
        nvs_set_u16(nvs_handle,"2",rain_config.time_run );

        nvs_set_u16(nvs_handle,"3",rainbow_config.state);
        nvs_set_u16(nvs_handle,"4",rainbow_config.time_run );

        nvs_set_u16(nvs_handle,"5",planboing_config.state);
        nvs_set_u16(nvs_handle,"6",planboing_config.time_run );

        nvs_set_u16(nvs_handle,"7",sendvoxelsrandz_config.state);
        nvs_set_u16(nvs_handle,"8",sendvoxelsrandz_config.time_run );

        nvs_set_u16(nvs_handle,"9",sendvoxelsrandy_config.state);
        nvs_set_u16(nvs_handle,"10",sendvoxelsrandy_config.time_run );

        nvs_set_u16(nvs_handle,"11",boxshrinkgrow_config.state);
        nvs_set_u16(nvs_handle,"12",boxshrinkgrow_config.time_run );

        nvs_set_u16(nvs_handle,"13",boxwoopwoop_config.state);
        nvs_set_u16(nvs_handle,"14",boxwoopwoop_config.time_run );

        nvs_set_u16(nvs_handle,"15",axiszupdownrs_config.state);
        nvs_set_u16(nvs_handle,"16",axiszupdownrs_config.time_run );

        nvs_set_u16(nvs_handle,"17",axisyupdownrs_config.state);
        nvs_set_u16(nvs_handle,"18",axisyupdownrs_config.time_run );

        nvs_set_u16(nvs_handle,"19",sidewaves_config.state);
        nvs_set_u16(nvs_handle,"20",sidewaves_config.time_run );

        nvs_set_u16(nvs_handle,"21",wormsqueeze_config.state);
        nvs_set_u16(nvs_handle,"22",wormsqueeze_config.time_run );

        nvs_set_u16(nvs_handle,"23",boingboing1_config.state);
        nvs_set_u16(nvs_handle,"24",boingboing1_config.time_run );

        nvs_set_u16(nvs_handle,"25",boingboing2_config.state);
        nvs_set_u16(nvs_handle,"26",boingboing2_config.time_run );

        nvs_set_u16(nvs_handle,"27",boingboing3_config.state);
        nvs_set_u16(nvs_handle,"28",boingboing3_config.time_run );

        nvs_set_u16(nvs_handle,"29",randomfiller_config.state);
        nvs_set_u16(nvs_handle,"30",randomfiller_config.time_run );

        nvs_set_u16(nvs_handle,"31",ripples_config.state);
        nvs_set_u16(nvs_handle,"32",ripples_config.time_run );
        nvs_set_u16(nvs_handle,"33",cubix_config.state);
        nvs_set_u16(nvs_handle,"34",cubix_config.time_run );
        nvs_set_u16(nvs_handle,"35",filip_filop_config.state);
        nvs_set_u16(nvs_handle,"36",filip_filop_config.time_run );
        nvs_set_u16(nvs_handle,"37",sinelines_config.state);
        nvs_set_u16(nvs_handle,"38",sinelines_config.time_run );
        nvs_set_u16(nvs_handle,"39",stringfly_config.state);
        nvs_set_str(nvs_handle,"40",stringfly_config.stringbuff );
        nvs_set_u16(nvs_handle,"41",fireworks_config.state);
        nvs_set_u16(nvs_handle,"42",fireworks_config.time_run );
        nvs_set_u16(nvs_handle,"43",path_text_config.state);
        nvs_set_str(nvs_handle,"44",path_text_config.stringbuff );
        nvs_set_u16(nvs_handle,"45",ball_bouncing_config.state);
        nvs_set_u16(nvs_handle,"46",ball_bouncing_config.time_run ); 

        nvs_set_u16(nvs_handle,"47",brightness_value);


        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
  
}

void read_data_config_from_flash()
{
 
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;

    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 

        nvs_get_u16(nvs_handle,"1",&rain_config.state);
        nvs_get_u16(nvs_handle,"2",&rain_config.time_run );

        nvs_get_u16(nvs_handle,"3",&rainbow_config.state);
        nvs_get_u16(nvs_handle,"4",&rainbow_config.time_run );

        nvs_get_u16(nvs_handle,"5",&planboing_config.state);
        nvs_get_u16(nvs_handle,"6",&planboing_config.time_run );

        nvs_get_u16(nvs_handle,"7",&sendvoxelsrandz_config.state);
        nvs_get_u16(nvs_handle,"8",&sendvoxelsrandz_config.time_run );

        nvs_get_u16(nvs_handle,"9",&sendvoxelsrandy_config.state);
        nvs_get_u16(nvs_handle,"10",&sendvoxelsrandy_config.time_run );

        nvs_get_u16(nvs_handle,"11",&boxshrinkgrow_config.state);
        nvs_get_u16(nvs_handle,"12",&boxshrinkgrow_config.time_run );

        nvs_get_u16(nvs_handle,"13",&boxwoopwoop_config.state);
        nvs_get_u16(nvs_handle,"14",&boxwoopwoop_config.time_run );

        nvs_get_u16(nvs_handle,"15",&axiszupdownrs_config.state);
        nvs_get_u16(nvs_handle,"16",&axiszupdownrs_config.time_run );

        nvs_get_u16(nvs_handle,"17",&axisyupdownrs_config.state);
        nvs_get_u16(nvs_handle,"18",&axisyupdownrs_config.time_run );

        nvs_get_u16(nvs_handle,"19",&sidewaves_config.state);
        nvs_get_u16(nvs_handle,"20",&sidewaves_config.time_run );

        nvs_get_u16(nvs_handle,"21",&wormsqueeze_config.state);
        nvs_get_u16(nvs_handle,"22",&wormsqueeze_config.time_run );

        nvs_get_u16(nvs_handle,"23",&boingboing1_config.state);
        nvs_get_u16(nvs_handle,"24",&boingboing1_config.time_run );

        nvs_get_u16(nvs_handle,"25",&boingboing2_config.state);
        nvs_get_u16(nvs_handle,"26",&boingboing2_config.time_run );

        nvs_get_u16(nvs_handle,"27",&boingboing3_config.state);
        nvs_get_u16(nvs_handle,"28",&boingboing3_config.time_run );

        nvs_get_u16(nvs_handle,"29",&randomfiller_config.state);
        nvs_get_u16(nvs_handle,"30",&randomfiller_config.time_run );

        size_t required_size;
        nvs_get_u16(nvs_handle,"31",&ripples_config.state);
        nvs_get_u16(nvs_handle,"32",&ripples_config.time_run );
        nvs_get_u16(nvs_handle,"33",&cubix_config.state);
        nvs_get_u16(nvs_handle,"34",&cubix_config.time_run );
        nvs_get_u16(nvs_handle,"35",&filip_filop_config.state);
        nvs_get_u16(nvs_handle,"36",&filip_filop_config.time_run );
        nvs_get_u16(nvs_handle,"37",&sinelines_config.state);
        nvs_get_u16(nvs_handle,"38",&sinelines_config.time_run );
        
        nvs_get_u16(nvs_handle,"39",&stringfly_config.state);
        nvs_get_str(nvs_handle,"40",NULL,&required_size);
        stringfly_config.stringbuff = malloc(required_size);
        nvs_get_str(nvs_handle,"40",stringfly_config.stringbuff,&required_size);
      //  printf("read string : %s \n", stringfly_config.stringbuff);

        // nvs_get_str(nvs_handle,"40",NULL,&required_size);
        // stringfly_config.stringbuff = malloc(required_size);
        // nvs_get_str(nvs_handle,"40",stringfly_config.stringbuff,&required_size);

        nvs_get_u16(nvs_handle,"41",&fireworks_config.state);
        nvs_get_u16(nvs_handle,"42",&fireworks_config.time_run );

        nvs_get_u16(nvs_handle,"43",&path_text_config.state);

        nvs_get_str(nvs_handle,"44",NULL,&required_size);
        path_text_config.stringbuff = malloc(required_size);
        nvs_get_str(nvs_handle,"44",path_text_config.stringbuff,&required_size);
     //   printf("read string : %s \n", path_text_config.stringbuff);

        // nvs_get_str(nvs_handle,"44",NULL,&required_size);
        // path_text_config.stringbuff = malloc(required_size);
        // nvs_get_str(nvs_handle,"44",path_text_config.stringbuff,&required_size);

        nvs_get_u16(nvs_handle,"45",&ball_bouncing_config.state);
        nvs_get_u16(nvs_handle,"46",&ball_bouncing_config.time_run );



        nvs_get_u16(nvs_handle,"47",&brightness_value );

        // printf("%d\n", rain_config.state);
        // printf("%d\n", rain_config.time_run);
        // printf("%d\n", rainbow_config.state);
        // printf("%d\n", rainbow_config.time_run);
        // printf("%d\n", planboing_config.state);
        // printf("%d\n", planboing_config.time_run);
        // printf("%d\n", sendvoxelsrandz_config.state);
        // printf("%d\n", sendvoxelsrandz_config.time_run);
        // printf("%d\n", sendvoxelsrandy_config.state);
        // printf("%d\n", sendvoxelsrandy_config.time_run);
        // printf("%d\n", boxshrinkgrow_config.state);
        // printf("%d\n", boxshrinkgrow_config.time_run);
        // printf("%d\n", boxwoopwoop_config.state);
        // printf("%d\n", boxwoopwoop_config.time_run);
        // printf("%d\n", axiszupdownrs_config.state);
        // printf("%d\n", axiszupdownrs_config.time_run);
        // printf("%d\n", axisyupdownrs_config.state);
        // printf("%d\n", axisyupdownrs_config.time_run);
        // printf("%d\n", sidewaves_config.state);
        // printf("%d\n", sidewaves_config.time_run);
        // printf("%d\n", wormsqueeze_config.state);
        // printf("%d\n", wormsqueeze_config.time_run);
        // printf("%d\n", boingboing1_config.state);
        // printf("%d\n", boingboing1_config.time_run);
        // printf("%d\n", boingboing2_config.state);
        // printf("%d\n", boingboing2_config.time_run);
        // printf("%d\n", boingboing3_config.state);
        // printf("%d\n", boingboing3_config.time_run);
        // printf("%d\n", randomfiller_config.state);
        // printf("%d\n", randomfiller_config.time_run);

        // printf("%d\n", ripples_config.state);
        // printf("%d\n", ripples_config.time_run);
        // printf("%d\n", cubix_config.state);
        // printf("%d\n", cubix_config.time_run);
        // printf("%d\n", filip_filop_config.state);
        // printf("%d\n", filip_filop_config.time_run);
        // printf("%d\n", sinelines_config.state);
        // printf("%d\n", sinelines_config.time_run);
        // printf("%d\n", stringfly_config.state);
        // printf("%s\n", stringfly_config.stringbuff);
        // printf("%d\n", fireworks_config.state);
        // printf("%d\n", fireworks_config.time_run);
        // printf("%d\n", path_text_config.state);
        // printf("%s\n", path_text_config.stringbuff);
        // printf("%d\n", ball_bouncing_config.state);
        // printf("%d\n", ball_bouncing_config.time_run);


        // printf("%d\n", brightness_value);

        
        set_brightness(brightness_value);


        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
      //  printf("okok\n");

    }
}


SemaphoreHandle_t semaphorehandle_data_eff_config = NULL;
char buffer_data_eff_config[300];
#define OK   1
#define NOK  0
esp_err_t data_eff_config_post_handler(httpd_req_t* req)
{
    
    httpd_req_recv(req, buffer_data_eff_config,req->content_len);
    printf("data_eff_config : %s\n",buffer_data_eff_config);
    xSemaphoreGiveFromISR(semaphorehandle_data_eff_config, NULL);
    return ESP_OK;
}
esp_err_t html_get_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req,(char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
}


esp_err_t data_web_before_get_handler(httpd_req_t *r)
{    
    char resp_str[700];
    // gán chuỗi
 //   sprintf(resp_str,"{\"d1\":\"%d\",\"d2\":\"%d\",\"d3\":\"%d\",\"d4\":\"%d\",\"d5\":\"%d\",\"d6\":\"%d\",\"d7\":\"%d\",\"d8\":\"%d\",\"d9\":\"%d\",\"d10\":\"%d\",\"d11\":\"%d\",\"d12\":\"%d\",\"d13\":\"%d\",\"d14\":\"%d\",\"d15\":\"%d\",\"d16\":\"%d\",\"d17\":\"%d\",\"d18\":\"%d\",\"d19\":\"%d\",\"d20\":\"%d\",\"d21\":\"%d\",\"d22\":\"%d\",\"d23\":\"%d\",\"d24\":\"%d\",\"d25:\"%d\",\"d26\":\"%d\",\"d27\":\"%d\",\"d28\":\"%d\",\"d29\":\"%d\",\"d30\":\"%d\"}",rain_config.state,rain_config.time_run,rainbow_config.state,rainbow_config.time_run,planboing_config.state,planboing_config.time_run,sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,boxwoopwoop_config.state,boxwoopwoop_config.time_run,axiszupdownrs_config.state,axiszupdownrs_config.time_run,axisyupdownrs_config.state,axisyupdownrs_config.time_run,sidewaves_config.state,sidewaves_config.time_run,wormsqueeze_config.state,wormsqueeze_config.time_run,boingboing1_config.state,boingboing1_config.time_run,boingboing2_config.state,boingboing2_config.time_run,boingboing3_config.state,boingboing3_config.time_run,randomfiller_config.state,randomfiller_config.time_run);

    sprintf(resp_str,"{\"d1\":\"%d\",\"d2\":\"%d\",\"d3\":\"%d\",\"d4\":\"%d\",\"d5\":\"%d\",\"d6\":\"%d\",\"d7\":\"%d\",\"d8\":\"%d\",\"d9\":\"%d\",\"d10\":\"%d\",\"d11\":\"%d\",\"d12\":\"%d\",\"d13\":\"%d\",\"d14\":\"%d\",\"d15\":\"%d\",\"d16\":\"%d\",\"d17\":\"%d\",\"d18\":\"%d\",\"d19\":\"%d\",\"d20\":\"%d\",\"d21\":\"%d\",\"d22\":\"%d\",\"d23\":\"%d\",\"d24\":\"%d\",\"d25\":\"%d\",\"d26\":\"%d\",\"d27\":\"%d\",\"d28\":\"%d\",\"d29\":\"%d\",\"d30\":\"%d\",\"d31\":\"%d\",\"d32\":\"%d\",\"d33\":\"%d\",\"d34\":\"%d\",\"d35\":\"%d\",\"d36\":\"%d\",\"d37\":\"%d\",\"d38\":\"%d\",\"d39\":\"%d\",\"d40\":\"%s\",\"d41\":\"%d\",\"d42\":\"%d\",\"d43\":\"%d\",\"d44\":\"%s\",\"d45\":\"%d\",\"d46\":\"%d\",\"d99\":\"%d\"}",rain_config.state,rain_config.time_run,rainbow_config.state,rainbow_config.time_run,planboing_config.state,planboing_config.time_run,sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,boxwoopwoop_config.state,boxwoopwoop_config.time_run,axiszupdownrs_config.state,axiszupdownrs_config.time_run,axisyupdownrs_config.state,axisyupdownrs_config.time_run,sidewaves_config.state,sidewaves_config.time_run,wormsqueeze_config.state,wormsqueeze_config.time_run,boingboing1_config.state,boingboing1_config.time_run,boingboing2_config.state,boingboing2_config.time_run,boingboing3_config.state,boingboing3_config.time_run,randomfiller_config.state,randomfiller_config.time_run,ripples_config.state,ripples_config.time_run, cubix_config.state,cubix_config.time_run,filip_filop_config.state,filip_filop_config.time_run,sinelines_config.state,sinelines_config.time_run,stringfly_config.state,stringfly_config.stringbuff,fireworks_config.state,fireworks_config.time_run,path_text_config.state, path_text_config.stringbuff,ball_bouncing_config.state,ball_bouncing_config.time_run,brightness_value); 
    printf("%s\n",resp_str);
    // kiểu data gửi đi
    httpd_resp_set_type(r,HTTPD_TYPE_JSON);
    // gửi data
    httpd_resp_send(r,resp_str, strlen(resp_str));
    return ESP_OK;
}

httpd_uri_t data_eff_config_uri_post = {
    .handler = data_eff_config_post_handler,
    .method = HTTP_POST,
    .uri =  "/data_eff_config",
    .user_ctx = NULL
};
httpd_uri_t html_uri_get = {
    .handler = html_get_handler,
    .method = HTTP_GET,
    .uri =  "/",
    .user_ctx = NULL
};

httpd_uri_t data_web_before_uri_get = {
    .handler = data_web_before_get_handler,
    .method = HTTP_GET,
    .uri =  "/data_web_before",
    .user_ctx = NULL
};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI("SERVER", "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Registering the ws handler
        ESP_LOGI("SERVER", "Registering URI handlers");
        httpd_register_uri_handler(server, &html_uri_get);
        httpd_register_uri_handler(server, &data_eff_config_uri_post);
        httpd_register_uri_handler(server, &data_web_before_uri_get);
        return server;
    }
    ESP_LOGI("SERVER", "Error starting server!");
    return NULL;
}

TaskHandle_t timer_interrupt_handle = NULL;

void data_eff_config_check()
{
    delay_ms(3000);
    semaphorehandle_data_eff_config = xSemaphoreCreateBinary();
    
    while (1)
    {
        if(xSemaphoreTake(semaphorehandle_data_eff_config, portMAX_DELAY))
        {
             get_data_config_effect(buffer_data_eff_config);  
             read_data_config_from_flash();
             restart_value = 1;
             vTaskSuspend(timer_interrupt_handle);
             fill_cube(0x00);
             show_led();
             delay_ms(2000);
             vTaskResume(timer_interrupt_handle);
        }
    }
    
}


void flash_write_once()
{
     esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
        //read data
        // key : value
        // "data" : 1111
        // uint16_t read_data;
        // nvs_get_u16(nvs_handle,"data",&read_data); 
        // printf("read data : %d \n",read_data);

        //write data
        // key : value

        nvs_set_u16(nvs_handle,"1",1);
        nvs_set_u16(nvs_handle,"2",10 );

        nvs_set_u16(nvs_handle,"3",1);
        nvs_set_u16(nvs_handle,"4",10 );

        nvs_set_u16(nvs_handle,"5",1);
        nvs_set_u16(nvs_handle,"6",10 );

        nvs_set_u16(nvs_handle,"7",1);
        nvs_set_u16(nvs_handle,"8",10 );

        nvs_set_u16(nvs_handle,"9",1);
        nvs_set_u16(nvs_handle,"10",10 );

        nvs_set_u16(nvs_handle,"11",1);
        nvs_set_u16(nvs_handle,"12",10 );

        nvs_set_u16(nvs_handle,"13",1);
        nvs_set_u16(nvs_handle,"14",10 );

        nvs_set_u16(nvs_handle,"15",1);
        nvs_set_u16(nvs_handle,"16",10 );

        nvs_set_u16(nvs_handle,"17",1);
        nvs_set_u16(nvs_handle,"18",10 );

        nvs_set_u16(nvs_handle,"19",1);
        nvs_set_u16(nvs_handle,"20",10 );

        nvs_set_u16(nvs_handle,"21",1);
        nvs_set_u16(nvs_handle,"22",10 );

        nvs_set_u16(nvs_handle,"23",1);
        nvs_set_u16(nvs_handle,"24",10 );

        nvs_set_u16(nvs_handle,"25",1);
        nvs_set_u16(nvs_handle,"26",10 );

        nvs_set_u16(nvs_handle,"27",1);
        nvs_set_u16(nvs_handle,"28",10 );

        nvs_set_u16(nvs_handle,"29",1);
        nvs_set_u16(nvs_handle,"30",10 );

        nvs_set_u16(nvs_handle,"31",1);
        nvs_set_u16(nvs_handle,"32",10 );
        nvs_set_u16(nvs_handle,"33",1);
        nvs_set_u16(nvs_handle,"34",10);
        nvs_set_u16(nvs_handle,"35",1);
        nvs_set_u16(nvs_handle,"36",10 );
        nvs_set_u16(nvs_handle,"37",1);
        nvs_set_u16(nvs_handle,"38",10 );
        nvs_set_u16(nvs_handle,"39",1);
        nvs_set_str(nvs_handle,"40","123");
        nvs_set_u16(nvs_handle,"41",1);
        nvs_set_u16(nvs_handle,"42",10);
        nvs_set_u16(nvs_handle,"43","1");
        nvs_set_str(nvs_handle,"44","123");
        nvs_set_u16(nvs_handle,"45",1);
        nvs_set_u16(nvs_handle,"46",10 ); 

        nvs_set_u16(nvs_handle,"47",10);


        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}

void flash_test()
{
     // Khoi tao NVS Partition
    esp_err_t err = nvs_flash_init();
    
    
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    vTaskDelay(1000/portTICK_PERIOD_MS);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
         //read data
        // key : value
        // "data" : 1111
        //write data
        // key : value  
        char *write_string = "hello from esp ne..\0";
        nvs_set_str(nvs_handle,"string",write_string);
        printf("write string : %s \n",write_string);


        size_t required_size;
        nvs_get_str(nvs_handle,"string",NULL,&required_size);
         char *server_name = malloc(required_size);
       // stringfly_config.stringbuff = malloc(required_size);
        nvs_get_str(nvs_handle,"string",server_name,&required_size);
        printf("read string : %s \n", server_name);
   
        nvs_commit(nvs_handle);

        nvs_close(nvs_handle);
        
    }
}
/*---------------------------------FFT------------------------------------------*/
#include "freertos/semphr.h"
#include "esp_adc_cal.h"
#include "driver/adc.h"

SemaphoreHandle_t semaphorehandle_adc_read = NULL;
esp_adc_cal_characteristics_t   adc_char;
#define max_height 20

int valueColumm[21] = {0};

rgb_t blue_color =  { .r = 0, .g = 0, .b = 255};
rgb_t no_color =  { .r = 0, .g = 0, .b = 0 };

void drawLed(int columm, int height)
{
    if(columm%2 == 0)
    {
        for(int i = columm*20; i < columm*20 + height; i++)
        {
            layer_buffer[i] = rgb_setvoxel;
        }
        for(int i = columm*20 +height; i < columm*20 + max_height; i++)
        {
            layer_buffer[i] = no_color;
        }
    }
    if(columm%2 == 1)
    {
        for(int i = columm*20 + max_height; i > columm*20 + max_height - height - 1; i--)
        {
            layer_buffer[i] = rgb_setvoxel;
        }
        for(int i = columm*20 + max_height - height - 1; i > columm*20 ; i--)
        {
            layer_buffer[i] = no_color;
        }
    }
    rgb_t rgb = Scroll(columm*10);
    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
}


#define N_WAVE      1024    /* full length of Sinewave[] */
#define LOG2_N_WAVE 10      /* log2(N_WAVE) */

/*
  Henceforth "short" implies 16-bit word. If this is not
  the case in your architecture, please replace "short"
  with a type definition which *is* a 16-bit word.
*/

/*
  Since we only use 3/4 of N_WAVE, we define only
  this many samples, in order to conserve data space.
*/
const int16_t Sinewave[N_WAVE-N_WAVE/4] = {
      0,    201,    402,    603,    804,   1005,   1206,   1406,
   1607,   1808,   2009,   2209,   2410,   2610,   2811,   3011,
   3211,   3411,   3611,   3811,   4011,   4210,   4409,   4608,
   4807,   5006,   5205,   5403,   5601,   5799,   5997,   6195,
   6392,   6589,   6786,   6982,   7179,   7375,   7571,   7766,
   7961,   8156,   8351,   8545,   8739,   8932,   9126,   9319,
   9511,   9703,   9895,  10087,  10278,  10469,  10659,  10849,
  11038,  11227,  11416,  11604,  11792,  11980,  12166,  12353,
  12539,  12724,  12909,  13094,  13278,  13462,  13645,  13827,
  14009,  14191,  14372,  14552,  14732,  14911,  15090,  15268,
  15446,  15623,  15799,  15975,  16150,  16325,  16499,  16672,
  16845,  17017,  17189,  17360,  17530,  17699,  17868,  18036,
  18204,  18371,  18537,  18702,  18867,  19031,  19194,  19357,
  19519,  19680,  19840,  20000,  20159,  20317,  20474,  20631,
  20787,  20942,  21096,  21249,  21402,  21554,  21705,  21855,
  22004,  22153,  22301,  22448,  22594,  22739,  22883,  23027,
  23169,  23311,  23452,  23592,  23731,  23869,  24006,  24143,
  24278,  24413,  24546,  24679,  24811,  24942,  25072,  25201,
  25329,  25456,  25582,  25707,  25831,  25954,  26077,  26198,
  26318,  26437,  26556,  26673,  26789,  26905,  27019,  27132,
  27244,  27355,  27466,  27575,  27683,  27790,  27896,  28001,
  28105,  28208,  28309,  28410,  28510,  28608,  28706,  28802,
  28897,  28992,  29085,  29177,  29268,  29358,  29446,  29534,
  29621,  29706,  29790,  29873,  29955,  30036,  30116,  30195,
  30272,  30349,  30424,  30498,  30571,  30643,  30713,  30783,
  30851,  30918,  30984,  31049,  31113,  31175,  31236,  31297,
  31356,  31413,  31470,  31525,  31580,  31633,  31684,  31735,
  31785,  31833,  31880,  31926,  31970,  32014,  32056,  32097,
  32137,  32176,  32213,  32249,  32284,  32318,  32350,  32382,
  32412,  32441,  32468,  32495,  32520,  32544,  32567,  32588,
  32609,  32628,  32646,  32662,  32678,  32692,  32705,  32717,
  32727,  32736,  32744,  32751,  32757,  32761,  32764,  32766,
  32767,  32766,  32764,  32761,  32757,  32751,  32744,  32736,
  32727,  32717,  32705,  32692,  32678,  32662,  32646,  32628,
  32609,  32588,  32567,  32544,  32520,  32495,  32468,  32441,
  32412,  32382,  32350,  32318,  32284,  32249,  32213,  32176,
  32137,  32097,  32056,  32014,  31970,  31926,  31880,  31833,
  31785,  31735,  31684,  31633,  31580,  31525,  31470,  31413,
  31356,  31297,  31236,  31175,  31113,  31049,  30984,  30918,
  30851,  30783,  30713,  30643,  30571,  30498,  30424,  30349,
  30272,  30195,  30116,  30036,  29955,  29873,  29790,  29706,
  29621,  29534,  29446,  29358,  29268,  29177,  29085,  28992,
  28897,  28802,  28706,  28608,  28510,  28410,  28309,  28208,
  28105,  28001,  27896,  27790,  27683,  27575,  27466,  27355,
  27244,  27132,  27019,  26905,  26789,  26673,  26556,  26437,
  26318,  26198,  26077,  25954,  25831,  25707,  25582,  25456,
  25329,  25201,  25072,  24942,  24811,  24679,  24546,  24413,
  24278,  24143,  24006,  23869,  23731,  23592,  23452,  23311,
  23169,  23027,  22883,  22739,  22594,  22448,  22301,  22153,
  22004,  21855,  21705,  21554,  21402,  21249,  21096,  20942,
  20787,  20631,  20474,  20317,  20159,  20000,  19840,  19680,
  19519,  19357,  19194,  19031,  18867,  18702,  18537,  18371,
  18204,  18036,  17868,  17699,  17530,  17360,  17189,  17017,
  16845,  16672,  16499,  16325,  16150,  15975,  15799,  15623,
  15446,  15268,  15090,  14911,  14732,  14552,  14372,  14191,
  14009,  13827,  13645,  13462,  13278,  13094,  12909,  12724,
  12539,  12353,  12166,  11980,  11792,  11604,  11416,  11227,
  11038,  10849,  10659,  10469,  10278,  10087,   9895,   9703,
   9511,   9319,   9126,   8932,   8739,   8545,   8351,   8156,
   7961,   7766,   7571,   7375,   7179,   6982,   6786,   6589,
   6392,   6195,   5997,   5799,   5601,   5403,   5205,   5006,
   4807,   4608,   4409,   4210,   4011,   3811,   3611,   3411,
   3211,   3011,   2811,   2610,   2410,   2209,   2009,   1808,
   1607,   1406,   1206,   1005,    804,    603,    402,    201,
      0,   -201,   -402,   -603,   -804,  -1005,  -1206,  -1406,
  -1607,  -1808,  -2009,  -2209,  -2410,  -2610,  -2811,  -3011,
  -3211,  -3411,  -3611,  -3811,  -4011,  -4210,  -4409,  -4608,
  -4807,  -5006,  -5205,  -5403,  -5601,  -5799,  -5997,  -6195,
  -6392,  -6589,  -6786,  -6982,  -7179,  -7375,  -7571,  -7766,
  -7961,  -8156,  -8351,  -8545,  -8739,  -8932,  -9126,  -9319,
  -9511,  -9703,  -9895, -10087, -10278, -10469, -10659, -10849,
 -11038, -11227, -11416, -11604, -11792, -11980, -12166, -12353,
 -12539, -12724, -12909, -13094, -13278, -13462, -13645, -13827,
 -14009, -14191, -14372, -14552, -14732, -14911, -15090, -15268,
 -15446, -15623, -15799, -15975, -16150, -16325, -16499, -16672,
 -16845, -17017, -17189, -17360, -17530, -17699, -17868, -18036,
 -18204, -18371, -18537, -18702, -18867, -19031, -19194, -19357,
 -19519, -19680, -19840, -20000, -20159, -20317, -20474, -20631,
 -20787, -20942, -21096, -21249, -21402, -21554, -21705, -21855,
 -22004, -22153, -22301, -22448, -22594, -22739, -22883, -23027,
 -23169, -23311, -23452, -23592, -23731, -23869, -24006, -24143,
 -24278, -24413, -24546, -24679, -24811, -24942, -25072, -25201,
 -25329, -25456, -25582, -25707, -25831, -25954, -26077, -26198,
 -26318, -26437, -26556, -26673, -26789, -26905, -27019, -27132,
 -27244, -27355, -27466, -27575, -27683, -27790, -27896, -28001,
 -28105, -28208, -28309, -28410, -28510, -28608, -28706, -28802,
 -28897, -28992, -29085, -29177, -29268, -29358, -29446, -29534,
 -29621, -29706, -29790, -29873, -29955, -30036, -30116, -30195,
 -30272, -30349, -30424, -30498, -30571, -30643, -30713, -30783,
 -30851, -30918, -30984, -31049, -31113, -31175, -31236, -31297,
 -31356, -31413, -31470, -31525, -31580, -31633, -31684, -31735,
 -31785, -31833, -31880, -31926, -31970, -32014, -32056, -32097,
 -32137, -32176, -32213, -32249, -32284, -32318, -32350, -32382,
 -32412, -32441, -32468, -32495, -32520, -32544, -32567, -32588,
 -32609, -32628, -32646, -32662, -32678, -32692, -32705, -32717,
 -32727, -32736, -32744, -32751, -32757, -32761, -32764, -32766,
};

/*
  FIX_MPY() - fixed-point multiplication & scaling.
  Substitute inline assembly for hardware-specific
  optimization suited to a particluar DSP processor.
  Scaling ensures that result remains 16-bit.
*/
int16_t FIX_MPY(int16_t a, int16_t b)
{
	/* shift right one less bit (i.e. 15-1) */
	int32_t c = ((int32_t)a * (int32_t)b) >> 14;
	/* last bit shifted out = rounding-bit */
	b = c & 0x01;
	/* last shift + rounding bit */
	a = (c >> 1) + b;
	return a;
}

/*
  fix_fft() - perform forward/inverse fast Fourier transform.
  fr[n],fi[n] are real and imaginary arrays, both INPUT AND
  RESULT (in-place FFT), with 0 <= n < 2**m; set inverse to
  0 for forward transform (FFT), or 1 for iFFT.
*/
int32_t fix_fft(int16_t fr[], int16_t fi[], uint8_t m, uint8_t inverse)
{
	int32_t mr, nn, i, j, l, k, istep, n, scale, shift;
	int16_t qr, qi, tr, ti, wr, wi;

	n = 1 << m;

	/* max FFT size = N_WAVE */
	if (n > N_WAVE)
		return -1;

	mr = 0;
	nn = n - 1;
	scale = 0;

	/* decimation in time - re-order data */
	for (m=1; m<=nn; ++m) {
		l = n;
		do {
			l >>= 1;
		} while (mr+l > nn);
		mr = (mr & (l-1)) + l;

		if (mr <= m)
			continue;
		tr = fr[m];
		fr[m] = fr[mr];
		fr[mr] = tr;
		ti = fi[m];
		fi[m] = fi[mr];
		fi[mr] = ti;
	}

	l = 1;
	k = LOG2_N_WAVE-1;
	while (l < n) {
		if (inverse) {
			/* variable scaling, depending upon data */
			shift = 0;
			for (i=0; i<n; ++i) {
				j = fr[i];
				if (j < 0)
					j = -j;
				m = fi[i];
				if (m < 0)
					m = -m;
				if (j > 16383 || m > 16383) {
					shift = 1;
					break;
				}
			}
			if (shift)
				++scale;
		} else {
			/*
			  fixed scaling, for proper normalization --
			  there will be log2(n) passes, so this results
			  in an overall factor of 1/n, distributed to
			  maximize arithmetic accuracy.
			*/
			shift = 1;
		}
		/*
		  it may not be obvious, but the shift will be
		  performed on each data point exactly once,
		  during this pass.
		*/
		istep = l << 1;
		for (m=0; m<l; ++m) {
			j = m << k;
			/* 0 <= j < N_WAVE/2 */
			wr =  Sinewave[j+N_WAVE/4];
			wi = -Sinewave[j];
			if (inverse)
				wi = -wi;
			if (shift) {
				wr >>= 1;
				wi >>= 1;
			}
			for (i=m; i<n; i+=istep) {
				j = i + l;
				tr = FIX_MPY(wr,fr[j]) - FIX_MPY(wi,fi[j]);
				ti = FIX_MPY(wr,fi[j]) + FIX_MPY(wi,fr[j]);
				qr = fr[i];
				qi = fi[i];
				if (shift) {
					qr >>= 1;
					qi >>= 1;
				}
				fr[j] = qr - tr;
				fi[j] = qi - ti;
				fr[i] = qr + tr;
				fi[i] = qi + ti;
			}
		}
		--k;
		l = istep;
	}
	return scale;
}

/*
  fix_fftr() - forward/inverse FFT on array of real numbers.
  Real FFT/iFFT using half-size complex FFT by distributing
  even/odd samples into real/imaginary arrays respectively.
  In order to save data space (i.e. to avoid two arrays, one
  for real, one for imaginary samples), we proceed in the
  following two steps: a) samples are rearranged in the real
  array so that all even samples are in places 0-(N/2-1) and
  all imaginary samples in places (N/2)-(N-1), and b) fix_fft
  is called with fr and fi pointing to index 0 and index N/2
  respectively in the original array. The above guarantees
  that fix_fft "sees" consecutive real samples as alternating
  real and imaginary samples in the complex array.
*/
int32_t fix_fftr(int16_t f[], uint8_t m, uint8_t inverse)
{
	int32_t i, N = 1<<(m-1), scale = 0;
	int16_t tt, *fr=f, *fi=&f[N];

	if (inverse)
		scale = fix_fft(fi, fr, m-1, inverse);
	for (i=1; i<N; i+=2) {
		tt = f[N+i-1];
		f[N+i-1] = f[i];
		f[i] = tt;
	}
	if (! inverse)
		scale = fix_fft(fi, fr, m-1, inverse);
	return scale;
}
const int16_t linTable[182] = 
{
	0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8,
	8, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14,
	14, 15, 15, 15, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20, 20,
	20, 21, 21, 21, 22, 22, 22, 23, 23, 23, 24, 24, 25, 25, 25, 26, 26,
	26, 27, 27, 27, 28, 28, 28, 29, 29, 29, 30, 30, 30, 31, 31, 31, 32,
	32, 33, 33, 33, 34, 34, 34, 35, 35, 35, 36, 36, 36, 37, 37, 37, 38,
	38, 38, 39, 39, 39, 40, 40, 41, 41, 41, 42, 42, 42, 43, 43, 43, 44,
	44, 44, 45, 45, 45, 46, 46, 46, 47, 47, 47, 48, 48, 49, 49, 49, 50,
	50, 50, 51, 51, 51, 52, 52, 52, 53, 53, 53, 54, 54, 54, 55, 55, 55,
	56, 56, 57, 57, 57, 58, 58, 58, 59, 59, 59, 60, 60, 60, 61, 61, 61,
	62, 62, 62, 63, 63, 63	
};

void drawCol(int collum)
{
    for(int z = 0; z <8; z ++)
    {
        for(int y = 0; y <8; z++)
        {
            for(int x = 0; x <8; x++)
            {
                setvoxel(z,y,x);
            }
        }
    }
}

void showColumm(uint8_t eff,  short columm, short height)
{
    if(height >=0  && height <=7)
    {
        switch (columm)
        {
            case 0:   
                for(int y = 6; y <= 7; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int y = 6; y <= 7; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 1:   
                for(int y = 4; y <= 5; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int y = 4; y <= 5; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 2:   
                for(int y = 2; y <= 3; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                 for(int y = 2; y <= 3; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                
                break;
            case 3:   
                for(int y = 0; y <= 1; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                 for(int y = 0; y <= 1; y++)
                {
                    for(int x = 0; x <=1; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                 break;
            case 4:   
                for(int x = 2; x <= 3; x++)
                {
                    for(int y = 0; y <=1; y++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int x = 2; x <= 3; x++)
                {
                    for(int y = 0; y <=1; y++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 5:   
                for(int x = 4; x <= 5; x++)
                {
                    for(int y = 0; y <=1; y++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int x = 4; x <= 5; x++)
                {
                    for(int y = 0; y <=1; y++)
                    { 
                       for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 6:   
                for(int x = 6; x <= 7; x++)
                {
                    for(int y = 0; y <= 1; y++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int y = 0; y <=1; y++)
                {
                    for(int x = 6; x <= 7; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 7:   
                for(int y = 2; y <= 3; y++)
                {
                    for(int x = 6; x <=7; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int y = 2; y <= 3; y++)
                {
                    for(int x = 6; x <=7; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 8:   
                for(int y = 4; y <= 5; y++)
                {
                    for(int x = 6; x <=7; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                 for(int y = 4; y <= 5; y++)
                {
                    for(int x = 6; x <=7; x++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 9:   
                for(int y = 6; y <= 7; y++)
                {
                    for(int x = 6; x <=7; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                 for(int y = 6; y <= 7; y++)
                {
                    for(int x = 6; x <=7; x++)
                    { 
                       for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 10:   
                for(int x = 4; x <= 5; x++)
                {
                    for(int y = 6; y <=7; y++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int x = 4; x <= 5; x++)
                {
                    for(int y = 6; y <=7; y++)
                    { 
                        for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 11:   
                for(int x = 2; x <= 3; x++)
                {
                    for(int y = 6; y <=7; y++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int x = 2; x <= 3; x++)
                {
                    for(int y = 6; y <=7; y++)
                    { 
                         for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 12:   
                for(int y = 4; y <= 5; y++)
                {
                    for(int x = 2; x <=3; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                 for(int y = 4; y <= 5; y++)
                {
                    for(int x = 2; x <=3; x++)
                    { 
                         for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 13:   
                for(int y = 2; y <= 3; y++)
                {
                    for(int x = 2; x <=3; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                 for(int y = 2; y <= 3; y++)
                {
                    for(int x = 2; x <=3; x++)
                    { 
                         for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            case 14:   
                for(int y = 2; y <= 3; y++)
                {
                    for(int x = 4; x <=5; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int y = 2; y <= 3; y++)
                {
                    for(int x = 4; x <=5; x++)
                    { 
                         for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
             case 15:   
                for(int y = 4; y <= 5; y++)
                {
                    for(int x = 4; x <=5; x++)
                    { 
                        for(int z = 0; z <= height; z ++)
                        {
                            setvoxel(z,y,x);
                        }
                    }
                }
                for(int y = 4; y <= 5; y++)
                {
                    for(int x = 4; x <=5; x++)
                    { 
                         for(int z = 7; z > height; z --)
                        {
                            clrvoxel(z,y,x);
                        }
                    }
                }
                break;
            
            default:
                break;
        }
        rgb_t rgb = Scroll(columm*15);
        set_color_setvoxel(rgb.r, rgb.g, rgb.b);
    }
}


int16_t inputValue;
short  oldDisplayData[32];
short  displayData[32];
short  displayDataToCube[32];

TaskHandle_t cube_run_handle = NULL;
TaskHandle_t data_eff_handle = NULL;


int16_t  im[1024];
int16_t  data[1024];
int16_t  val;

uint32_t timeSlow = 0;
SemaphoreHandle_t mutex1;
int time1=0;
int time2=0;
void readADC()
{ 
    
    set_brightness(15);
    for(int i=0; i<= 15;i++)
    {               
         oldDisplayData[i]= 0;
    }
    ESP_LOGI("I2S ADC","Init adc task");
    int i2s_read_len = 16 * 1024;
    size_t bytes_read;
    int* i2s_read_buff = (int*) calloc(i2s_read_len, sizeof(int));


    while (1)
    {
         i2s_read(I2S_NUM_0, (void*)i2s_read_buff, 1024*sizeof(uint16_t), &bytes_read, portMAX_DELAY);
        if( I2S_EVENT_RX_DONE && bytes_read>0)
        {
           for (int i = 1; i < 512; i = i + 2) 
            {
                i2s_read_buff[i] = (float)(i2s_read_buff[i]-2048)*(1.0f/2048.0f);
                //i2s_read_buff[i] = i2s_read_buff[i] - 512;
                i2s_read_buff[i] = map(i2s_read_buff[i],600000,800000,0,4096);
                data[i] = i2s_read_buff[i] - 2700;
               // printf("%d\n", data[i]);

                // Set the imaginary number to zero
                im[i] = 0;
            }
        }
        // for(int i = 0; i < 512; i++)
        // {
        //     val = adc1_get_raw(ADC1_CHANNEL_5);
        //     data[i] = val  - 2048;

        //     // Set the imaginary number to zero
        //     im[i] = 0;
        // }
        // printf("recieve flag semaphore from task 1\n");
        fix_fft(data,im,7,0);

        for(int i=0; i< 256;i++){
            data[i] = sqrt(data[i] * data[i] + im[i] * im[i]);
            // if(data[i] > 183) data[i] = 183;
            // data[i]  =  map(data[i], 0,100,0,20);
        }
        
        int16_t inputValue;
        for (unsigned char counter = 1; counter < 20; counter++)
        {
            // Scale the input data for the display (linear) x1 or x8
            inputValue = (data[counter*2 - 1] + data[counter*2 ] +  data[counter*2 + 1])/3;
            // inputValue = data[counter];
            // for(int i = 0; i < 20; i++)
            // {
            //     if(data[counter*20 + i] > inputValue) inputValue= data[counter*20 + i];
            // }
            if (inputValue > 181) inputValue = 181;
            
            // Apply a linear or logarithmic conversion on the data
            inputValue = (short)linTable[inputValue];
            
            // Perform damping on the displayed output
            if (inputValue > displayData[counter-1]) displayData[counter-1] = inputValue ;
            else displayData[counter-1] -= 10;
            if(counter == 1)
                displayData[counter-1] =  displayData[counter-1] - 5;

            //  displayData[counter-1] =  displayData[counter-1] - 2;

            if (displayData[counter-1] < 0) displayData[counter-1] = 0;

            if ( displayData[counter-1] > 20)  displayData[counter-1] = 20;

            displayData[counter-1]  =  map(displayData[counter-1], 0,20,0,7);
        }  


        
        for(int i=0; i<= 15;i++)
        {
            if(displayData[i] >= oldDisplayData[i])
            {
                displayDataToCube[i] = displayData[i];
                oldDisplayData[i] = displayData[i];
            } 
            else if(displayData[i] < oldDisplayData[i] )
            { 
                if(oldDisplayData[i] > 0  && timeSlow < 100)
                {
                   
                    oldDisplayData[i] = oldDisplayData[i] - 1;
                    displayDataToCube[i] = oldDisplayData[i];   
                    
                }
                if(timeSlow >=100)
                    timeSlow = 0;
            }    
        }
      

        for(int i=0; i<= 15;i++)
        {               
            showColumm(0,i,displayDataToCube[i]);  
        }
        
        show_led();  
        timeSlow++;
        delay_ms(10);
        
        
        
    }
}


void app_main()
{
    // wifi_sta_connect();
    // delay_ms(1000);
    // httpd_handle_t server = NULL;
    // server = start_webserver(); 
    // delay_ms(1000);
    // xTaskCreatePinnedToCore(data_eff_config_check, "data_eff_config_check",8192, NULL, 10, NULL,0);

    



    //  led_strip_install();
    //  xTaskCreatePinnedToCore(cube_run, "cube_run",8192*2, NULL, 5, NULL,0);
    //  xTaskCreatePinnedToCore(timer_interrupt, "timer_interrupt", 8192, NULL, 5,  &timer_interrupt_handle,1);
    //  read_data_config_from_flash();

    led_strip_install();
    ESP_ERROR_CHECK(led_strip_init(&strip));

    

    
    // for(int i = 0; i< 512; i ++)
    // {
    //     led_strip_set_pixel(&strip, i,  rgb_setvoxel);
    //     led_strip_flush(&strip);
    //     delay_ms(100);
    // }
    // for(int i = 0; i< 16; i ++)
    // {
    //     showColumm(0,i,i%8); show_led();
    //     delay_ms(100);
    // }
  
    // mutex1 = xSemaphoreCreateMutex();

    i2s_config_t i2s_config = {
        .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN),  // I2S receive mode with ADC
        .sample_rate = 44100,                                               // set I2S ADC sample rate
        .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,                                 // 16 bit I2S (even though ADC is 12 bit)
        .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,                                 // handle adc data as single channel (right)
        .communication_format = (i2s_comm_format_t)I2S_COMM_FORMAT_I2S,               // I2S format
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,                                                        // 
        .dma_buf_count = 4,                                                           // number of DMA buffers >=2 for fastness
        .dma_buf_len = 1024,                                                // number of samples per buffer
        .use_apll = 0,     
        .tx_desc_auto_clear = false,
        .fixed_mclk = 0,                                                           // no Audio PLL - buggy and not well documented
    };


    i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);

    i2s_set_adc_mode(ADC_UNIT_1, ADC1_CHANNEL_5);
    i2s_adc_enable(I2S_NUM_0);
    vTaskDelay(100/portTICK_PERIOD_MS);

    xTaskCreatePinnedToCore(readADC, "readADC", 1024*16,NULL, 5, NULL, 1);
    // Khởi tạo task2 chạy core 1
    // xTaskCreatePinnedToCore(displayCubeMusic,"displayCubeMusic",1024*16,NULL,10,NULL,1);

 
 //   xTaskCreatePinnedToCore(cube_run, "cube_run",8192*2, NULL, 5, &cube_run_handle,1);
    // xTaskCreatePinnedToCore(timer_interrupt, "timer_interrupt", 8192, NULL,5,  &timer_interrupt_handle,1);

    wifi_sta_connect();
    httpd_handle_t server = NULL;
    server = start_webserver(); 



 
   // read_data_config_from_flash();
    //xTaskCreatePinnedToCore(data_eff_config_check, "data_eff_config_check",8192, NULL, 5, &data_eff_handle,1);

    



   
    // caaus hinh partion 0x9000
    // nap code flash_write_once() truoc
    // nap code con lai


   

}

