#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include <led_strip_idf.h>
#include "math.h"
#include "esp_log.h"
#include "wifi_ap.h"
#include "nvs_flash.h"
#include "esp_http_server.h"
#include "driver/i2s.h"
#include "control_led.h"
#include "effect_led.h"
#include "driver/timer.h"
#include "esp_adc_cal.h"
#include "driver/adc.h"
#include <esp_task_wdt.h> 
#include <soc/rtc_wdt.h> 
#include "soc/timer_group_struct.h"
#include "soc/timer_group_reg.h"
#include "wifi_ap_connect.h"
#include "wifi_sta_connect.h"


// Khai báo file html
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");


void flash_write_once(void);
TaskHandle_t cube_run_handle = NULL;
TaskHandle_t data_eff_handle = NULL;
TaskHandle_t timer_interrupt_handle = NULL;
TaskHandle_t readADCTaskHandle=NULL;



/*.................................Khai báo LED STRIP.............................*/ 
#define LED_TYPE LED_STRIP_WS2812
#define LED_LENGTH 512
#define LED_GPIO 5
#define CUBE_SIZE 8
control_led_cube_t  control_cube;
void init_led_cube()
{
    // cấu hình strip
    led_strip_t strip = {   
                            .type = LED_TYPE,
                            .length = LED_LENGTH,
                            .gpio = LED_GPIO,
                            .buf = NULL,
                            .brightness = 50,
                        };
    control_cube.strip = strip;
    change_rgb_color(&control_cube);
}

/*................................. Timer dùng để quét led CUBE .....................................*/
/*
    Khi timer tràn, sẽ kích hoạt hàm xQueueSendFromISR(s_timer_queue, &evt, &high_task_awoken);
    xQueueSendFromISR sẽ gửi tín hiệu đến xQueueReceive ở hàm timer_interrupt()
    hàm timer_interrupt() có nhiệm vụ show_led(&control_cube) mỗi khi có ngắt
*/

#define TIMER_DIVIDER         (16)  //  Hardware timer clock divider
#define TIMER_SCALE           1000 // convert counter value to seconds

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
    bool auto_reload;
} example_timer_info_t;

/**
 * @brief A sample structure to pass events from the timer ISR to task
 *
 */
typedef struct {
    example_timer_info_t info;
    uint64_t timer_counter_value;
} example_timer_event_t;

static xQueueHandle s_timer_queue;

/*
 * A simple helper function to print the raw timer counter value
 * and the counter value converted to seconds
 */

static void inline print_timer_counter(uint64_t counter_value)
{
    printf("Counter: 0x%08x%08x\r\n", (uint32_t) (counter_value >> 32),
           (uint32_t) (counter_value));
    printf("Time   : %.8f s\r\n", (double) counter_value / TIMER_SCALE);
}

static bool IRAM_ATTR timer_group_isr_callback(void *args)
{
    BaseType_t high_task_awoken = pdFALSE;
    example_timer_info_t *info = (example_timer_info_t *) args;

    uint64_t timer_counter_value = timer_group_get_counter_value_in_isr(info->timer_group, info->timer_idx);

    /* Prepare basic event data that will be then sent back to task */
    example_timer_event_t evt = {
        .info.timer_group = info->timer_group,
        .info.timer_idx = info->timer_idx,
        .info.auto_reload = info->auto_reload,
        .info.alarm_interval = info->alarm_interval,
        .timer_counter_value = timer_counter_value
    };
    if (!info->auto_reload) {
        timer_counter_value += info->alarm_interval * TIMER_SCALE;
        timer_group_set_alarm_value_in_isr(info->timer_group, info->timer_idx, timer_counter_value);
    }

    /* Now just send the event data back to the main program task */
    xQueueSendFromISR(s_timer_queue, &evt, &high_task_awoken);
    return high_task_awoken == pdTRUE; // return whether we need to yield at the end of ISR
}

/**
 * @brief Initialize selected timer of timer group
 *
 * @param group Timer Group number, index from 0
 * @param timer timer ID, index from 0
 * @param auto_reload whether auto-reload on alarm event
 * @param timer_interval_sec interval of alarm
 */
static void example_tg_timer_init(int group, int timer, bool auto_reload, int timer_interval_sec)
{
    /* Select and initialize basic parameters of the timer */
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = auto_reload,
    }; // default clock source is APB
    timer_init(group, timer, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(group, timer, 0);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(group, timer, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(group, timer);

    example_timer_info_t *timer_info = calloc(1, sizeof(example_timer_info_t));
    timer_info->timer_group = group;
    timer_info->timer_idx = timer;
    timer_info->auto_reload = auto_reload;
    timer_info->alarm_interval = timer_interval_sec;
    timer_isr_callback_add(group, timer, timer_group_isr_callback, timer_info, 0);

    timer_start(group, timer);
}

//------------------- Hàm đẩy data ra led CUBE mỗi khi timer ngắt -----------------------
void timer_interrupt()
{
    s_timer_queue = xQueueCreate(10, sizeof(example_timer_event_t));

    example_tg_timer_init(TIMER_GROUP_0, TIMER_0, true, 3);

    while (1) {
        example_timer_event_t evt;
        xQueueReceive(s_timer_queue, &evt, portMAX_DELAY);
        show_led(&control_cube);
    }
}

/*.................... Khai báo data config effect led ...........................*/
effect_config_t rain_config;
effect_config_t rainbow_config;
effect_config_t planboing_config;
effect_config_t sendvoxelsrandz_config;
effect_config_t sendvoxelsrandy_config;
effect_config_t boxshrinkgrow_config;
effect_config_t boxwoopwoop_config;
effect_config_t axiszupdownrs_config;
effect_config_t axisyupdownrs_config;
effect_config_t sidewaves_config;
effect_config_t wormsqueeze_config;
effect_config_t boingboing1_config;
effect_config_t boingboing2_config;
effect_config_t boingboing3_config;
effect_config_t randomfiller_config;
effect_config_t ripples_config;
effect_config_t cubix_config;
effect_config_t filip_filop_config;
effect_config_t sinelines_config;
effect_config_t stringfly_config;
effect_config_t fireworks_config;
effect_config_t path_text_config;
effect_config_t ball_bouncing_config;
uint8_t brightness_value = 50;
char *stringbuffer;

/*....................... Hàm chạy hiệu ứng LED Cube ...........................*/
void cube_run(void *pvParameters)
{
    // Hàm led_strip_init khởi tạo để điều khiển led ws2812, 1903, hàm này PHẢI được gọi trong Task mà khởi tạo chạy ở CORE 1
    // CORE 0 đang được dùng cho việc phát wifi, sẽ gây nhiễu nếu gọi ở app_main hoặc bất kì Task nào khởi tạo chạy CORE 0
    ESP_ERROR_CHECK(led_strip_init(&control_cube.strip)); 

    set_brightness(&control_cube, brightness_value);
    ESP_ERROR_CHECK(led_strip_flush(&control_cube.strip));
    fill_cube(&control_cube, 0x00);
    show_led(&control_cube);
    while (1)
    {
        delay_ms(1000);
        effect_rain(&control_cube,rain_config.state, rain_config.time_run,50,0);
        effect_rainbow(&control_cube,rainbow_config.state, rainbow_config.time_run,15);
        effect_planboing(&control_cube,planboing_config.state, planboing_config.time_run);
        effect_sendvoxels_rand_axis(&control_cube,sendvoxelsrandz_config.state, sendvoxelsrandz_config.time_run, AXIS_Z, 10,500);
        effect_sendvoxels_rand_axis(&control_cube,sendvoxelsrandy_config.state, sendvoxelsrandy_config.time_run, AXIS_Y, 10,500);
        effect_box_shrink_grow(&control_cube,boxshrinkgrow_config.state, boxshrinkgrow_config.time_run,50);
        effect_box_woopwoop(&control_cube,boxwoopwoop_config.state,boxwoopwoop_config.time_run);
        effect_axis_z_updown_randsuspend(&control_cube,axiszupdownrs_config.state,axiszupdownrs_config.time_run);
        effect_axis_y_updown_randsuspend(&control_cube,axisyupdownrs_config.state,axisyupdownrs_config.time_run);
        effect_int_sidewaves(&control_cube,sidewaves_config.state,sidewaves_config.time_run,10);
        effect_wormsqueeze(&control_cube,wormsqueeze_config.state,(wormsqueeze_config.time_run)/2,50,2,0);
        effect_wormsqueeze(&control_cube,wormsqueeze_config.state,(wormsqueeze_config.time_run)/2,50,3,1);
        effect_random_filler(&control_cube,randomfiller_config.state,randomfiller_config.time_run,20);
        effect_boingboing(&control_cube,boingboing1_config.state,boingboing1_config.time_run,40, 0x01,0x01);
        effect_boingboing(&control_cube,boingboing2_config.state,boingboing2_config.time_run,40, 0x01,0x03);
        effect_boingboing(&control_cube,boingboing3_config.state,boingboing3_config.time_run,40, 0x01,0x02);
        effect_int_ripples(&control_cube,ripples_config.state,ripples_config.time_run,10); // ok
        effect_cubix(&control_cube,cubix_config.state,cubix_config.time_run,3);   //ok
        effect_filip_filop(&control_cube,filip_filop_config.state,filip_filop_config.time_run,50); // ok
        effect_sinelines(&control_cube,sinelines_config.state,sinelines_config.time_run,10);
        effect_stringfly(&control_cube,stringfly_config.state,stringfly_config.stringbuff);
        effect_fireworks(&control_cube,fireworks_config.state,fireworks_config.time_run,50);
        effect_path_text(&control_cube,path_text_config.state, 100,path_text_config.stringbuff);
        effect_ball_bouncing(&control_cube,ball_bouncing_config.state,ball_bouncing_config.time_run,10); 
    }
}

// ---------------------------Hiệu ứng test ---------------------------------
// Hàm dùng để test hiệu ứng với các thông số được fix sẵn
void effect_test()
{
    effect_rainbow(&control_cube,1,20,15);
    effect_rain(&control_cube, 1,20,50,0);
    eff_side_change_color(&control_cube,1,5);
    effect_planboing(&control_cube,1,5 ); 
    effect_sendvoxels_rand_axis(&control_cube, 1,5,AXIS_Z, 10,500);
    effect_sendvoxels_rand_axis(&control_cube, 1,5,AXIS_Y, 10,500);
    effect_box_shrink_grow(&control_cube,1,5,50);
    effect_box_woopwoop(&control_cube,1,10);
    effect_axis_z_updown_randsuspend(&control_cube, 1,5);
    effect_axis_y_updown_randsuspend(&control_cube, 1,5);
    effect_int_sidewaves(&control_cube,1,5,10);
    effect_int_ripples(&control_cube,1,5,10);
    effect_wormsqueeze(&control_cube,1 , 5,50,2,0);
    effect_wormsqueeze(&control_cube,1 , 5,50,3,1);
    effect_cubix(&control_cube,1 , 5, 3);
    effect_boingboing(&control_cube,1,5,40, 0x01,0x01) ;
    effect_boingboing(&control_cube,1,5,40, 0x01,0x03) ;
    effect_boingboing(&control_cube,1,5,40, 0x01,0x02) ;
    effect_random_filler(&control_cube,1,10,20);
    effect_filip_filop(&control_cube,1,10,50);
    effect_sinelines(&control_cube,1,10,10);
    effect_linespin(&control_cube,1,10,10);
    for(int i=0; i<8; ++i)
        effect_boxside_randsend_parallel(&control_cube,3, i%2, 1, 1);
    for(int i=0; i<8; ++i)
        effect_boxside_randsend_parallel(&control_cube,2, i%2, 1, 2);
    effect_stringfly(&control_cube,1,";9876543210");
    effect_fireworks(&control_cube,1,10,50);
    effect_stringfly(&control_cube,1,"banlinhkien.com");
    effect_path_text(&control_cube,1,100,"banlinhkien.com");
    effect_ball_bouncing(&control_cube,1,10,10);
    effect_int_sidewaves(&control_cube,1,5,10); 
    effect_linespin(&control_cube,1,20,10);
    for(int i=0; i<14; ++i)
        effect_smileyspin(&control_cube,1,3,100,i);   
}
//----------------Hàm test LED CUBE chạy đuổi ------------------------
void testLedCUBE()
{
    for(int i = 0; i< 512; i ++)
    {
        led_strip_set_pixel(&control_cube.strip, i,  control_cube.rgb_setvoxel);
        led_strip_flush(&control_cube.strip);
        delay_ms(100);
    }
}


// ------------------------- Hàm tách data ----------------------------------------
void get_data_config_effect(char * input_string)
{
    char *token1, *token2;
    char *rest = input_string;
    char data_eff[100][20];
    // Mảng để lưu các chuỗi con sau khi tách bằng kí tự '/'
    char substrings[20][100]; // Điều này tùy thuộc vào độ dài tối đa của chuỗi con
    int i = 0;
    int j = 0;
    while ((token1 = strtok_r(rest, "/", &rest))) {
        // Lưu chuỗi con vào mảng substrings
        strcpy(substrings[i], token1);
        // Tách chuỗi con thành các phần bằng kí tự ':'
        token2 = strtok(substrings[i], ":");
        // In ra các phần bằng kí tự ':'
        while (token2 != NULL) {
            strcpy(data_eff[j], token2);
            j++;
         //   printf("%s\n", token2);
            token2 = strtok(NULL, ":");
        }
        i++;
    }
    /* Data nhận được có dạng như sau
    rain:0:5/rainbow:0:65/planboing:0:0/sendvoxelsrandz:0:0/sendvoxelsrandy:0:0/boxshrinkgrow:0:0/boxwoopwoop:0:3/axiszupdownrs:0:0/axisyupdownrs:0:0/sidewaves:0:60/wormsqueeze:0:0/boingboing1:0:0/boingboing2:1:190/boingboing3:0:0/randomfiller:0:0/ripples:0:0/cubix:0:0/filip:0:5/sinelines:0:0/stringfly:0:BANLINHKIEN.COM/fireworks:0:0/pathtext:1:BANLINHKIEN.COM/ballbouncing:0:0/brness:1:13/
    ta sẽ tiến hành tách ra bằng các kí tự đặc biệt  /   :   
    rain:0:5
    data_eff[0] = "rain";
    data_eff[1] = "0";
    data_eff[2] = "5";
    */
    rain_config.state = atoi(data_eff[1]);              rain_config.time_run = atoi(data_eff[2]);
    rainbow_config.state = atoi(data_eff[4]);           rainbow_config.time_run = atoi(data_eff[5]);
    planboing_config.state = atoi(data_eff[7]);         planboing_config.time_run = atoi(data_eff[8]);
    sendvoxelsrandz_config.state = atoi(data_eff[10]);  sendvoxelsrandz_config.time_run = atoi(data_eff[11]);
    sendvoxelsrandy_config.state = atoi(data_eff[13]);  sendvoxelsrandy_config.time_run = atoi(data_eff[14]);
    boxshrinkgrow_config.state = atoi(data_eff[16]);    boxshrinkgrow_config.time_run = atoi(data_eff[17]);
    boxwoopwoop_config.state = atoi(data_eff[19]);      boxwoopwoop_config.time_run = atoi(data_eff[20]);
    axiszupdownrs_config.state = atoi(data_eff[22]);    axiszupdownrs_config.time_run = atoi(data_eff[23]);
    axisyupdownrs_config.state = atoi(data_eff[25]);    axisyupdownrs_config.time_run = atoi(data_eff[26]);
    sidewaves_config.state = atoi(data_eff[28]);        sidewaves_config.time_run = atoi(data_eff[29]);
    wormsqueeze_config.state = atoi(data_eff[31]);      wormsqueeze_config.time_run = atoi(data_eff[32]);
    boingboing1_config.state = atoi(data_eff[34]);      boingboing1_config.time_run = atoi(data_eff[35]);
    boingboing2_config.state = atoi(data_eff[37]);      boingboing2_config.time_run = atoi(data_eff[38]);
    boingboing3_config.state = atoi(data_eff[40]);      boingboing3_config.time_run = atoi(data_eff[41]);
    randomfiller_config.state = atoi(data_eff[43]);     randomfiller_config.time_run = atoi(data_eff[44]);
    ripples_config.state = atoi(data_eff[46]);          ripples_config.time_run = atoi(data_eff[47]);
    cubix_config.state = atoi(data_eff[49]);            cubix_config.time_run = atoi(data_eff[50]);
    filip_filop_config.state = atoi(data_eff[52]);      filip_filop_config.time_run = atoi(data_eff[53]);
    sinelines_config.state = atoi(data_eff[55]);        sinelines_config.time_run = atoi(data_eff[56]);
    stringfly_config.state = atoi(data_eff[58]);        stringfly_config.stringbuff = data_eff[59];
    fireworks_config.state = atoi(data_eff[61]);        fireworks_config.time_run = atoi(data_eff[62]);
    path_text_config.state = atoi(data_eff[64]);        path_text_config.stringbuff = data_eff[65];
    ball_bouncing_config.state = atoi(data_eff[67]);    ball_bouncing_config.time_run = atoi(data_eff[68]);
    brightness_value = atoi(data_eff[71]);

    printf("%d\n",brightness_value);
    set_brightness(&control_cube, brightness_value);

    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
        // key : value
        nvs_set_u16(nvs_handle,"1",rain_config.state);              nvs_set_u16(nvs_handle,"2",rain_config.time_run );
        nvs_set_u16(nvs_handle,"3",rainbow_config.state);           nvs_set_u16(nvs_handle,"4",rainbow_config.time_run );
        nvs_set_u16(nvs_handle,"5",planboing_config.state);         nvs_set_u16(nvs_handle,"6",planboing_config.time_run );
        nvs_set_u16(nvs_handle,"7",sendvoxelsrandz_config.state);   nvs_set_u16(nvs_handle,"8",sendvoxelsrandz_config.time_run );
        nvs_set_u16(nvs_handle,"9",sendvoxelsrandy_config.state);   nvs_set_u16(nvs_handle,"10",sendvoxelsrandy_config.time_run );
        nvs_set_u16(nvs_handle,"11",boxshrinkgrow_config.state);    nvs_set_u16(nvs_handle,"12",boxshrinkgrow_config.time_run );
        nvs_set_u16(nvs_handle,"13",boxwoopwoop_config.state);      nvs_set_u16(nvs_handle,"14",boxwoopwoop_config.time_run );
        nvs_set_u16(nvs_handle,"15",axiszupdownrs_config.state);    nvs_set_u16(nvs_handle,"16",axiszupdownrs_config.time_run );
        nvs_set_u16(nvs_handle,"17",axisyupdownrs_config.state);    nvs_set_u16(nvs_handle,"18",axisyupdownrs_config.time_run );
        nvs_set_u16(nvs_handle,"19",sidewaves_config.state);        nvs_set_u16(nvs_handle,"20",sidewaves_config.time_run );
        nvs_set_u16(nvs_handle,"21",wormsqueeze_config.state);      nvs_set_u16(nvs_handle,"22",wormsqueeze_config.time_run );
        nvs_set_u16(nvs_handle,"23",boingboing1_config.state);      nvs_set_u16(nvs_handle,"24",boingboing1_config.time_run );
        nvs_set_u16(nvs_handle,"25",boingboing2_config.state);      nvs_set_u16(nvs_handle,"26",boingboing2_config.time_run );
        nvs_set_u16(nvs_handle,"27",boingboing3_config.state);      nvs_set_u16(nvs_handle,"28",boingboing3_config.time_run );
        nvs_set_u16(nvs_handle,"29",randomfiller_config.state);     nvs_set_u16(nvs_handle,"30",randomfiller_config.time_run );
        nvs_set_u16(nvs_handle,"31",ripples_config.state);          nvs_set_u16(nvs_handle,"32",ripples_config.time_run );
        nvs_set_u16(nvs_handle,"33",cubix_config.state);            nvs_set_u16(nvs_handle,"34",cubix_config.time_run );
        nvs_set_u16(nvs_handle,"35",filip_filop_config.state);      nvs_set_u16(nvs_handle,"36",filip_filop_config.time_run );
        nvs_set_u16(nvs_handle,"37",sinelines_config.state);        nvs_set_u16(nvs_handle,"38",sinelines_config.time_run );
        nvs_set_u16(nvs_handle,"39",stringfly_config.state);        nvs_set_str(nvs_handle,"40",stringfly_config.stringbuff );
        nvs_set_u16(nvs_handle,"41",fireworks_config.state);        nvs_set_u16(nvs_handle,"42",fireworks_config.time_run );
        nvs_set_u16(nvs_handle,"43",path_text_config.state);        nvs_set_str(nvs_handle,"44",path_text_config.stringbuff );
        nvs_set_u16(nvs_handle,"45",ball_bouncing_config.state);    nvs_set_u16(nvs_handle,"46",ball_bouncing_config.time_run ); 
        nvs_set_u16(nvs_handle,"47",brightness_value);

        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}
void print_data_effect_config();
// ------------------------- Hàm đọc data config effect led từ NVS----------------------------------------
void read_data_config_from_flash()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        flash_write_once();
    } else {
        printf("Done\n"); 
        size_t required_size;
        int stateDataInFlash = 0;
        if(nvs_get_u16(nvs_handle,"0",&stateDataInFlash) == ESP_OK)
        {
            printf("Data in Flash OK\n"); 
            nvs_get_u16(nvs_handle,"0",&stateDataInFlash);  
            nvs_get_u16(nvs_handle,"1",&rain_config.state);             nvs_get_u16(nvs_handle,"2",&rain_config.time_run );
            nvs_get_u16(nvs_handle,"3",&rainbow_config.state);          nvs_get_u16(nvs_handle,"4",&rainbow_config.time_run );
            nvs_get_u16(nvs_handle,"5",&planboing_config.state);        nvs_get_u16(nvs_handle,"6",&planboing_config.time_run );
            nvs_get_u16(nvs_handle,"7",&sendvoxelsrandz_config.state);  nvs_get_u16(nvs_handle,"8",&sendvoxelsrandz_config.time_run );
            nvs_get_u16(nvs_handle,"9",&sendvoxelsrandy_config.state);  nvs_get_u16(nvs_handle,"10",&sendvoxelsrandy_config.time_run );
            nvs_get_u16(nvs_handle,"11",&boxshrinkgrow_config.state);   nvs_get_u16(nvs_handle,"12",&boxshrinkgrow_config.time_run );
            nvs_get_u16(nvs_handle,"13",&boxwoopwoop_config.state);     nvs_get_u16(nvs_handle,"14",&boxwoopwoop_config.time_run );
            nvs_get_u16(nvs_handle,"15",&axiszupdownrs_config.state);   nvs_get_u16(nvs_handle,"16",&axiszupdownrs_config.time_run );
            nvs_get_u16(nvs_handle,"17",&axisyupdownrs_config.state);   nvs_get_u16(nvs_handle,"18",&axisyupdownrs_config.time_run );
            nvs_get_u16(nvs_handle,"19",&sidewaves_config.state);       nvs_get_u16(nvs_handle,"20",&sidewaves_config.time_run );
            nvs_get_u16(nvs_handle,"21",&wormsqueeze_config.state);     nvs_get_u16(nvs_handle,"22",&wormsqueeze_config.time_run );
            nvs_get_u16(nvs_handle,"23",&boingboing1_config.state);     nvs_get_u16(nvs_handle,"24",&boingboing1_config.time_run );
            nvs_get_u16(nvs_handle,"25",&boingboing2_config.state);     nvs_get_u16(nvs_handle,"26",&boingboing2_config.time_run );
            nvs_get_u16(nvs_handle,"27",&boingboing3_config.state);     nvs_get_u16(nvs_handle,"28",&boingboing3_config.time_run );
            nvs_get_u16(nvs_handle,"29",&randomfiller_config.state);    nvs_get_u16(nvs_handle,"30",&randomfiller_config.time_run );
            nvs_get_u16(nvs_handle,"31",&ripples_config.state);         nvs_get_u16(nvs_handle,"32",&ripples_config.time_run );
            nvs_get_u16(nvs_handle,"33",&cubix_config.state);           nvs_get_u16(nvs_handle,"34",&cubix_config.time_run );
            nvs_get_u16(nvs_handle,"35",&filip_filop_config.state);     nvs_get_u16(nvs_handle,"36",&filip_filop_config.time_run );
            nvs_get_u16(nvs_handle,"37",&sinelines_config.state);       nvs_get_u16(nvs_handle,"38",&sinelines_config.time_run );
            
            nvs_get_u16(nvs_handle,"39",&stringfly_config.state);
            nvs_get_str(nvs_handle,"40",NULL,&required_size);
            stringfly_config.stringbuff = malloc(required_size);
            nvs_get_str(nvs_handle,"40",stringfly_config.stringbuff,&required_size);

            nvs_get_u16(nvs_handle,"41",&fireworks_config.state);       nvs_get_u16(nvs_handle,"42",&fireworks_config.time_run );
            
            nvs_get_u16(nvs_handle,"43",&path_text_config.state);
            nvs_get_str(nvs_handle,"44",NULL,&required_size);
            path_text_config.stringbuff = malloc(required_size);
            nvs_get_str(nvs_handle,"44",path_text_config.stringbuff,&required_size);

            nvs_get_u16(nvs_handle,"45",&ball_bouncing_config.state);   nvs_get_u16(nvs_handle,"46",&ball_bouncing_config.time_run );

            nvs_get_u16(nvs_handle,"47",&brightness_value );
            set_brightness(&control_cube, brightness_value);
            print_data_effect_config();
        }
        else
        {
            printf("Init Data in Flash \n"); 
            nvs_set_u16(nvs_handle,"0",1); 
            nvs_set_u16(nvs_handle,"1",1);      nvs_set_u16(nvs_handle,"2",20 );
            nvs_set_u16(nvs_handle,"3",1);      nvs_set_u16(nvs_handle,"4",20 );
            nvs_set_u16(nvs_handle,"5",1);      nvs_set_u16(nvs_handle,"6",20 );
            nvs_set_u16(nvs_handle,"7",1);      nvs_set_u16(nvs_handle,"8",20 );
            nvs_set_u16(nvs_handle,"9",1);      nvs_set_u16(nvs_handle,"10",20 );
            nvs_set_u16(nvs_handle,"11",1);     nvs_set_u16(nvs_handle,"12",20 );
            nvs_set_u16(nvs_handle,"13",1);     nvs_set_u16(nvs_handle,"14",20 );
            nvs_set_u16(nvs_handle,"15",1);     nvs_set_u16(nvs_handle,"16",20 );
            nvs_set_u16(nvs_handle,"17",1);     nvs_set_u16(nvs_handle,"18",20 );
            nvs_set_u16(nvs_handle,"19",1);     nvs_set_u16(nvs_handle,"20",20 );
            nvs_set_u16(nvs_handle,"21",1);     nvs_set_u16(nvs_handle,"22",20 );
            nvs_set_u16(nvs_handle,"23",1);     nvs_set_u16(nvs_handle,"24",20 );
            nvs_set_u16(nvs_handle,"25",1);     nvs_set_u16(nvs_handle,"26",20 );
            nvs_set_u16(nvs_handle,"27",1);     nvs_set_u16(nvs_handle,"28",20 );
            nvs_set_u16(nvs_handle,"29",1);     nvs_set_u16(nvs_handle,"30",20 );
            nvs_set_u16(nvs_handle,"31",1);     nvs_set_u16(nvs_handle,"32",20 );
            nvs_set_u16(nvs_handle,"33",1);     nvs_set_u16(nvs_handle,"34",20);
            nvs_set_u16(nvs_handle,"35",1);     nvs_set_u16(nvs_handle,"36",20 );
            nvs_set_u16(nvs_handle,"37",1);     nvs_set_u16(nvs_handle,"38",20 );
            nvs_set_u16(nvs_handle,"39",1);     nvs_set_str(nvs_handle,"40","BANLINHKIEN.COM");
            nvs_set_u16(nvs_handle,"41",1);     nvs_set_u16(nvs_handle,"42",20);
            nvs_set_u16(nvs_handle,"43","1");   nvs_set_str(nvs_handle,"44","BANLINHKIEN.COM");
            nvs_set_u16(nvs_handle,"45",1);     nvs_set_u16(nvs_handle,"46",20 ); 
            nvs_set_u16(nvs_handle,"47",20);
            esp_restart();
        }
       
        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}

// ------------------------- Hàm in data config effect led ra Serial----------------------------------------
void print_data_effect_config()
{
    printf("rain_config.state : %d\n", rain_config.state);                          printf("rain_config.time_run : %d\n", rain_config.time_run);
    printf("rainbow_config.state : %d\n", rainbow_config.state);                    printf("rainbow_config.time_run : %d\n", rainbow_config.time_run);
    printf("planboing_config.state : %d\n", planboing_config.state);                printf("planboing_config.time_run : %d\n", planboing_config.time_run);
    printf("sendvoxelsrandz_config.state : %d\n", sendvoxelsrandz_config.state);    printf("sendvoxelsrandz_config.time_run : %d\n", sendvoxelsrandz_config.time_run);
    printf("sendvoxelsrandy_config.state : %d\n", sendvoxelsrandy_config.state);    printf("sendvoxelsrandy_config.time_run : %d\n", sendvoxelsrandy_config.time_run);
    printf("boxshrinkgrow_config.state) : %d\n", boxshrinkgrow_config.state);       printf("boxshrinkgrow_config.time_run : %d\n", boxshrinkgrow_config.time_run);
    printf("boxwoopwoop_config.state : %d\n", boxwoopwoop_config.state);            printf("boxwoopwoop_config.time_run : %d\n", boxwoopwoop_config.time_run);
    printf("axiszupdownrs_config.state : %d\n", axiszupdownrs_config.state);        printf("axiszupdownrs_config.time_run : %d\n", axiszupdownrs_config.time_run);
    printf("axisyupdownrs_config.state : %d\n", axisyupdownrs_config.state);        printf("axisyupdownrs_config.time_run : %d\n", axisyupdownrs_config.time_run);
    printf("sidewaves_config.state : %d\n", sidewaves_config.state);                printf("sidewaves_config.time_run : %d\n", sidewaves_config.time_run);
    printf("wormsqueeze_config.state : %d\n", wormsqueeze_config.state);            printf("wormsqueeze_config.time_run : %d\n", wormsqueeze_config.time_run);
    printf("boingboing1_config.state : %d\n", boingboing1_config.state);            printf("boingboing1_config.time_run : %d\n", boingboing1_config.time_run);
    printf("boingboing2_config.state : %d\n", boingboing2_config.state);            printf("boingboing2_config.time_run : %d\n", boingboing2_config.time_run);
    printf("boingboing3_config.state : %d\n", boingboing3_config.state);            printf("boingboing3_config.time_run : %d\n", boingboing3_config.time_run);
    printf("randomfiller_config.state : %d\n", randomfiller_config.state);          printf("randomfiller_config.time_run : %d\n", randomfiller_config.time_run);
    printf("ripples_config.state : %d\n", ripples_config.state);                    printf("ripples_config.time_run : %d\n", ripples_config.time_run);
    printf("cubix_config.state : %d\n", cubix_config.state);                        printf("cubix_config.time_run : %d\n", cubix_config.time_run);
    printf("filip_filop_config.state : %d\n", filip_filop_config.state);            printf("filip_filop_config.time_run : %d\n", filip_filop_config.time_run);
    printf("sinelines_config.state : %d\n", sinelines_config.state);                printf("sinelines_config.time_run : %d\n", sinelines_config.time_run);
    printf("stringfly_config.state : %d\n", stringfly_config.state);                printf("stringfly_config.stringbuff : %s\n", stringfly_config.stringbuff);
    printf("fireworks_config.state : %d\n", fireworks_config.state);                printf("fireworks_config.time_run : %d\n", fireworks_config.time_run);
    printf("path_text_config.state : %d\n", path_text_config.state);                printf("path_text_config.stringbuff : %s\n", path_text_config.stringbuff);
    printf("ball_bouncing_config.state : %d\n", ball_bouncing_config.state);        printf("ball_bouncing_config.time_run : %d\n", ball_bouncing_config.time_run);
    printf("brightness_value : %d\n", brightness_value);
}

SemaphoreHandle_t semaphorehandle_data_eff_config = NULL;

//------------------------ Hàm nhận Data từ web ----------------------------
char buffer_data_eff_config[500];
esp_err_t data_eff_config_post_handler(httpd_req_t* req)
{
    httpd_req_recv(req, buffer_data_eff_config,req->content_len);
    printf("data_eff_config : %s\n",buffer_data_eff_config);
    xSemaphoreGiveFromISR(semaphorehandle_data_eff_config, NULL);
    return ESP_OK;
}
//--------------------------- Trang HTML ---------------------------------
esp_err_t html_get_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req,(char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
}

//------------------- Hàm gửi data config led đến web ---------------------
esp_err_t data_web_before_get_handler(httpd_req_t *r)
{    
    char resp_str[700];
    sprintf(resp_str,"{\"d1\":\"%d\",\"d2\":\"%d\",\"d3\":\"%d\",\"d4\":\"%d\",\"d5\":\"%d\",\"d6\":\"%d\",\"d7\":\"%d\",\"d8\":\"%d\",\"d9\":\"%d\",\"d10\":\"%d\",\"d11\":\"%d\",\"d12\":\"%d\",\"d13\":\"%d\",\"d14\":\"%d\",\"d15\":\"%d\",\"d16\":\"%d\",\"d17\":\"%d\",\"d18\":\"%d\",\"d19\":\"%d\",\"d20\":\"%d\",\"d21\":\"%d\",\"d22\":\"%d\",\"d23\":\"%d\",\"d24\":\"%d\",\"d25\":\"%d\",\"d26\":\"%d\",\"d27\":\"%d\",\"d28\":\"%d\",\"d29\":\"%d\",\"d30\":\"%d\",\"d31\":\"%d\",\"d32\":\"%d\",\"d33\":\"%d\",\"d34\":\"%d\",\"d35\":\"%d\",\"d36\":\"%d\",\"d37\":\"%d\",\"d38\":\"%d\",\"d39\":\"%d\",\"d40\":\"%s\",\"d41\":\"%d\",\"d42\":\"%d\",\"d43\":\"%d\",\"d44\":\"%s\",\"d45\":\"%d\",\"d46\":\"%d\",\"d99\":\"%d\"}",rain_config.state,rain_config.time_run,rainbow_config.state,rainbow_config.time_run,planboing_config.state,planboing_config.time_run,sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,boxwoopwoop_config.state,boxwoopwoop_config.time_run,axiszupdownrs_config.state,axiszupdownrs_config.time_run,axisyupdownrs_config.state,axisyupdownrs_config.time_run,sidewaves_config.state,sidewaves_config.time_run,wormsqueeze_config.state,wormsqueeze_config.time_run,boingboing1_config.state,boingboing1_config.time_run,boingboing2_config.state,boingboing2_config.time_run,boingboing3_config.state,boingboing3_config.time_run,randomfiller_config.state,randomfiller_config.time_run,ripples_config.state,ripples_config.time_run, cubix_config.state,cubix_config.time_run,filip_filop_config.state,filip_filop_config.time_run,sinelines_config.state,sinelines_config.time_run,stringfly_config.state,stringfly_config.stringbuff,fireworks_config.state,fireworks_config.time_run,path_text_config.state, path_text_config.stringbuff,ball_bouncing_config.state,ball_bouncing_config.time_run,brightness_value); 
    printf("%s\n",resp_str);
    // kiểu data gửi đi
    httpd_resp_set_type(r,HTTPD_TYPE_JSON);
    // gửi data
    httpd_resp_send(r,resp_str, strlen(resp_str));
    return ESP_OK;
}
//------------------Handler APP --------------------------------------
esp_err_t esp_to_app_check_connect_handler(httpd_req_t *req)
{    
    const char resp[] = "Configured Successfullyyyy";
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}
esp_err_t esp_to_app_send_data_handler(httpd_req_t *req)
{
    char resp_str[1800];
    sprintf(resp_str,
"[{\"name\":\"rain_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"rainbow_config\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"planboing_config\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"sendvoxelsrandz_config\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"sendvoxelsrandy_config\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"boxshrinkgrow_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"boxwoopwoop_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"axiszupdownrs_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"axisyupdownrs_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"sidewaves_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"wormsqueeze_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"boingboing1_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"boingboing2_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"boingboing3_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"randomfiller_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"ripples_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"cubix_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"filip_filop_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"sinelines_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"stringfly_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"%s\"},\
{\"name\":\"fireworks_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"path_text_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"%s\"},\
{\"name\":\"ball_bouncing_config\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"brightness\": %d}]", \
    rain_config.state,rain_config.time_run,\
    rainbow_config.state,rainbow_config.time_run,\
    planboing_config.state,planboing_config.time_run,\
    sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,\
    sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,\
    boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,\
    boxwoopwoop_config.state,boxwoopwoop_config.time_run,\
    axiszupdownrs_config.state,axiszupdownrs_config.time_run,\
    axisyupdownrs_config.state,axisyupdownrs_config.time_run,\
    sidewaves_config.state,sidewaves_config.time_run,\
    wormsqueeze_config.state,wormsqueeze_config.time_run,\
    boingboing1_config.state,boingboing1_config.time_run,\
    boingboing2_config.state,boingboing2_config.time_run,\
    boingboing3_config.state,boingboing3_config.time_run,\
    randomfiller_config.state,randomfiller_config.time_run,\
    ripples_config.state,ripples_config.time_run,\
    cubix_config.state,cubix_config.time_run,\
    filip_filop_config.state,filip_filop_config.time_run,\
    sinelines_config.state,sinelines_config.time_run,\
    stringfly_config.state,stringfly_config.time_run,stringfly_config.stringbuff,\
    fireworks_config.state,fireworks_config.time_run,\
    path_text_config.state,path_text_config.time_run,path_text_config.stringbuff,\
    ball_bouncing_config.state,ball_bouncing_config.time_run,\
    brightness_value
    );

    printf("---------------------------------------------\n");
    printf("%s\n",resp_str);
    fflush(stdout);
    // kiểu data gửi đi
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    // gửi data
    httpd_resp_send(req,resp_str, strlen(resp_str));
    return ESP_OK;
}

esp_err_t app_to_esp_data_eff_config_handler(httpd_req_t* req)
{  
    
    /* Truncate if content length larger than the buffer */
    
    size_t recv_size ;
    if(req->content_len <sizeof(buffer_data_eff_config))
    {
        recv_size = req->content_len;
    }
    else
    {
        recv_size = sizeof(buffer_data_eff_config);
    }

    int ret = httpd_req_recv(req, buffer_data_eff_config, recv_size);
    if (ret <= 0) {  /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        }
        /* In case of error, returning ESP_FAIL will
         * ensure that the underlying socket is closed */
        return ESP_FAIL;
    }
    printf("%s\n",buffer_data_eff_config);
    xSemaphoreGiveFromISR(semaphorehandle_data_eff_config, NULL);
   
    /* Send a simple response */
    const char resp[] = "URI POST Response";
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

//------------------URI APP --------------------------------------
httpd_uri_t app_to_esp_data_eff_config_uri_post = {
    .handler = app_to_esp_data_eff_config_handler,
    .method = HTTP_POST,
    .uri =  "/app_to_esp_data_eff_config",
    .user_ctx = NULL
};

httpd_uri_t esp_to_app_send_data_uri_get = {
    .handler = esp_to_app_send_data_handler,
    .method = HTTP_GET,
    .uri =  "/esp_to_app_send_data",
    .user_ctx = NULL
};

httpd_uri_t esp_to_app_check_connect_uri_get = {
    .handler = esp_to_app_check_connect_handler,
    .method = HTTP_GET,
    .uri =  "/esp_to_app_check_connect",
    .user_ctx = NULL
};

//----------------Data từ Web gửi về ESP----------------------
httpd_uri_t data_eff_config_uri_post = {
    .handler = data_eff_config_post_handler,
    .method = HTTP_POST,
    .uri =  "/data_eff_config",
    .user_ctx = NULL
};
// ----------------------------Trang HTML----------------------------
httpd_uri_t html_uri_get = {
    .handler = html_get_handler,
    .method = HTTP_GET,
    .uri =  "/",
    .user_ctx = NULL
};
// -------------Gửi data từ ESP đến web trong lần đầu mở web---------
httpd_uri_t data_web_before_uri_get = {
    .handler = data_web_before_get_handler,
    .method = HTTP_GET,
    .uri =  "/data_web_before",
    .user_ctx = NULL
};


static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    // Start the httpd server
    ESP_LOGI("SERVER", "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Registering the ws handler
        ESP_LOGI("SERVER", "Registering URI handlers");
        httpd_register_uri_handler(server, &html_uri_get);
        httpd_register_uri_handler(server, &data_eff_config_uri_post);
        httpd_register_uri_handler(server, &data_web_before_uri_get);
        httpd_register_uri_handler(server, &esp_to_app_send_data_uri_get);
        httpd_register_uri_handler(server, &app_to_esp_data_eff_config_uri_post);
        httpd_register_uri_handler(server, &esp_to_app_check_connect_uri_get);
    
        return server;
    }
    ESP_LOGI("SERVER", "Error starting server!");
    return NULL;
}

// --------------------- Gửi data từ Web về ESP------------------
void data_eff_config_check()
{
    delay_ms(3000);
    semaphorehandle_data_eff_config = xSemaphoreCreateBinary();
    while (1)
    {
        if(xSemaphoreTake(semaphorehandle_data_eff_config, portMAX_DELAY))
        {
             get_data_config_effect(buffer_data_eff_config);  
             read_data_config_from_flash();
             control_cube.restart_value = 1;
             vTaskSuspend(timer_interrupt_handle);
             fill_cube(&control_cube, 0x00);
             show_led(&control_cube);
             delay_ms(2000);
             vTaskResume(timer_interrupt_handle);
        }
    }
}
// ------------------------ Hàm ghi data config led vào NSV ---------------------------
void flash_write_once()
{
     esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
        nvs_set_u16(nvs_handle,"0",1);    // check data;
        nvs_set_u16(nvs_handle,"1",1);      nvs_set_u16(nvs_handle,"2",10 );
        nvs_set_u16(nvs_handle,"3",1);      nvs_set_u16(nvs_handle,"4",10 );
        nvs_set_u16(nvs_handle,"5",1);      nvs_set_u16(nvs_handle,"6",10 );
        nvs_set_u16(nvs_handle,"7",1);      nvs_set_u16(nvs_handle,"8",10 );
        nvs_set_u16(nvs_handle,"9",1);      nvs_set_u16(nvs_handle,"10",10 );
        nvs_set_u16(nvs_handle,"11",1);     nvs_set_u16(nvs_handle,"12",10 );
        nvs_set_u16(nvs_handle,"13",1);     nvs_set_u16(nvs_handle,"14",10 );
        nvs_set_u16(nvs_handle,"15",1);     nvs_set_u16(nvs_handle,"16",10 );
        nvs_set_u16(nvs_handle,"17",1);     nvs_set_u16(nvs_handle,"18",10 );
        nvs_set_u16(nvs_handle,"19",1);     nvs_set_u16(nvs_handle,"20",10 );
        nvs_set_u16(nvs_handle,"21",1);     nvs_set_u16(nvs_handle,"22",10 );
        nvs_set_u16(nvs_handle,"23",1);     nvs_set_u16(nvs_handle,"24",10 );
        nvs_set_u16(nvs_handle,"25",1);     nvs_set_u16(nvs_handle,"26",10 );
        nvs_set_u16(nvs_handle,"27",1);     nvs_set_u16(nvs_handle,"28",10 );
        nvs_set_u16(nvs_handle,"29",1);     nvs_set_u16(nvs_handle,"30",10 );
        nvs_set_u16(nvs_handle,"31",1);     nvs_set_u16(nvs_handle,"32",10 );
        nvs_set_u16(nvs_handle,"33",1);     nvs_set_u16(nvs_handle,"34",10);
        nvs_set_u16(nvs_handle,"35",1);     nvs_set_u16(nvs_handle,"36",10 );
        nvs_set_u16(nvs_handle,"37",1);     nvs_set_u16(nvs_handle,"38",10 );
        nvs_set_u16(nvs_handle,"39",1);     nvs_set_str(nvs_handle,"40","123");
        nvs_set_u16(nvs_handle,"41",1);     nvs_set_u16(nvs_handle,"42",10);
        nvs_set_u16(nvs_handle,"43","1");   nvs_set_str(nvs_handle,"44","123");
        nvs_set_u16(nvs_handle,"45",1);     nvs_set_u16(nvs_handle,"46",10 ); 
        nvs_set_u16(nvs_handle,"47",10);
        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}

/*---------------------------------FFT------------------------------------------*/

#define N_WAVE      1024    /* full length of Sinewave[] */
#define LOG2_N_WAVE 10      /* log2(N_WAVE) */
/*
  Henceforth "short" implies 16-bit word. If this is not
  the case in your architecture, please replace "short"
  with a type definition which *is* a 16-bit word.
*/

/*
  Since we only use 3/4 of N_WAVE, we define only
  this many samples, in order to conserve data space.
*/
const int16_t Sinewave[N_WAVE-N_WAVE/4] = {
      0,    201,    402,    603,    804,   1005,   1206,   1406,
   1607,   1808,   2009,   2209,   2410,   2610,   2811,   3011,
   3211,   3411,   3611,   3811,   4011,   4210,   4409,   4608,
   4807,   5006,   5205,   5403,   5601,   5799,   5997,   6195,
   6392,   6589,   6786,   6982,   7179,   7375,   7571,   7766,
   7961,   8156,   8351,   8545,   8739,   8932,   9126,   9319,
   9511,   9703,   9895,  10087,  10278,  10469,  10659,  10849,
  11038,  11227,  11416,  11604,  11792,  11980,  12166,  12353,
  12539,  12724,  12909,  13094,  13278,  13462,  13645,  13827,
  14009,  14191,  14372,  14552,  14732,  14911,  15090,  15268,
  15446,  15623,  15799,  15975,  16150,  16325,  16499,  16672,
  16845,  17017,  17189,  17360,  17530,  17699,  17868,  18036,
  18204,  18371,  18537,  18702,  18867,  19031,  19194,  19357,
  19519,  19680,  19840,  20000,  20159,  20317,  20474,  20631,
  20787,  20942,  21096,  21249,  21402,  21554,  21705,  21855,
  22004,  22153,  22301,  22448,  22594,  22739,  22883,  23027,
  23169,  23311,  23452,  23592,  23731,  23869,  24006,  24143,
  24278,  24413,  24546,  24679,  24811,  24942,  25072,  25201,
  25329,  25456,  25582,  25707,  25831,  25954,  26077,  26198,
  26318,  26437,  26556,  26673,  26789,  26905,  27019,  27132,
  27244,  27355,  27466,  27575,  27683,  27790,  27896,  28001,
  28105,  28208,  28309,  28410,  28510,  28608,  28706,  28802,
  28897,  28992,  29085,  29177,  29268,  29358,  29446,  29534,
  29621,  29706,  29790,  29873,  29955,  30036,  30116,  30195,
  30272,  30349,  30424,  30498,  30571,  30643,  30713,  30783,
  30851,  30918,  30984,  31049,  31113,  31175,  31236,  31297,
  31356,  31413,  31470,  31525,  31580,  31633,  31684,  31735,
  31785,  31833,  31880,  31926,  31970,  32014,  32056,  32097,
  32137,  32176,  32213,  32249,  32284,  32318,  32350,  32382,
  32412,  32441,  32468,  32495,  32520,  32544,  32567,  32588,
  32609,  32628,  32646,  32662,  32678,  32692,  32705,  32717,
  32727,  32736,  32744,  32751,  32757,  32761,  32764,  32766,
  32767,  32766,  32764,  32761,  32757,  32751,  32744,  32736,
  32727,  32717,  32705,  32692,  32678,  32662,  32646,  32628,
  32609,  32588,  32567,  32544,  32520,  32495,  32468,  32441,
  32412,  32382,  32350,  32318,  32284,  32249,  32213,  32176,
  32137,  32097,  32056,  32014,  31970,  31926,  31880,  31833,
  31785,  31735,  31684,  31633,  31580,  31525,  31470,  31413,
  31356,  31297,  31236,  31175,  31113,  31049,  30984,  30918,
  30851,  30783,  30713,  30643,  30571,  30498,  30424,  30349,
  30272,  30195,  30116,  30036,  29955,  29873,  29790,  29706,
  29621,  29534,  29446,  29358,  29268,  29177,  29085,  28992,
  28897,  28802,  28706,  28608,  28510,  28410,  28309,  28208,
  28105,  28001,  27896,  27790,  27683,  27575,  27466,  27355,
  27244,  27132,  27019,  26905,  26789,  26673,  26556,  26437,
  26318,  26198,  26077,  25954,  25831,  25707,  25582,  25456,
  25329,  25201,  25072,  24942,  24811,  24679,  24546,  24413,
  24278,  24143,  24006,  23869,  23731,  23592,  23452,  23311,
  23169,  23027,  22883,  22739,  22594,  22448,  22301,  22153,
  22004,  21855,  21705,  21554,  21402,  21249,  21096,  20942,
  20787,  20631,  20474,  20317,  20159,  20000,  19840,  19680,
  19519,  19357,  19194,  19031,  18867,  18702,  18537,  18371,
  18204,  18036,  17868,  17699,  17530,  17360,  17189,  17017,
  16845,  16672,  16499,  16325,  16150,  15975,  15799,  15623,
  15446,  15268,  15090,  14911,  14732,  14552,  14372,  14191,
  14009,  13827,  13645,  13462,  13278,  13094,  12909,  12724,
  12539,  12353,  12166,  11980,  11792,  11604,  11416,  11227,
  11038,  10849,  10659,  10469,  10278,  10087,   9895,   9703,
   9511,   9319,   9126,   8932,   8739,   8545,   8351,   8156,
   7961,   7766,   7571,   7375,   7179,   6982,   6786,   6589,
   6392,   6195,   5997,   5799,   5601,   5403,   5205,   5006,
   4807,   4608,   4409,   4210,   4011,   3811,   3611,   3411,
   3211,   3011,   2811,   2610,   2410,   2209,   2009,   1808,
   1607,   1406,   1206,   1005,    804,    603,    402,    201,
      0,   -201,   -402,   -603,   -804,  -1005,  -1206,  -1406,
  -1607,  -1808,  -2009,  -2209,  -2410,  -2610,  -2811,  -3011,
  -3211,  -3411,  -3611,  -3811,  -4011,  -4210,  -4409,  -4608,
  -4807,  -5006,  -5205,  -5403,  -5601,  -5799,  -5997,  -6195,
  -6392,  -6589,  -6786,  -6982,  -7179,  -7375,  -7571,  -7766,
  -7961,  -8156,  -8351,  -8545,  -8739,  -8932,  -9126,  -9319,
  -9511,  -9703,  -9895, -10087, -10278, -10469, -10659, -10849,
 -11038, -11227, -11416, -11604, -11792, -11980, -12166, -12353,
 -12539, -12724, -12909, -13094, -13278, -13462, -13645, -13827,
 -14009, -14191, -14372, -14552, -14732, -14911, -15090, -15268,
 -15446, -15623, -15799, -15975, -16150, -16325, -16499, -16672,
 -16845, -17017, -17189, -17360, -17530, -17699, -17868, -18036,
 -18204, -18371, -18537, -18702, -18867, -19031, -19194, -19357,
 -19519, -19680, -19840, -20000, -20159, -20317, -20474, -20631,
 -20787, -20942, -21096, -21249, -21402, -21554, -21705, -21855,
 -22004, -22153, -22301, -22448, -22594, -22739, -22883, -23027,
 -23169, -23311, -23452, -23592, -23731, -23869, -24006, -24143,
 -24278, -24413, -24546, -24679, -24811, -24942, -25072, -25201,
 -25329, -25456, -25582, -25707, -25831, -25954, -26077, -26198,
 -26318, -26437, -26556, -26673, -26789, -26905, -27019, -27132,
 -27244, -27355, -27466, -27575, -27683, -27790, -27896, -28001,
 -28105, -28208, -28309, -28410, -28510, -28608, -28706, -28802,
 -28897, -28992, -29085, -29177, -29268, -29358, -29446, -29534,
 -29621, -29706, -29790, -29873, -29955, -30036, -30116, -30195,
 -30272, -30349, -30424, -30498, -30571, -30643, -30713, -30783,
 -30851, -30918, -30984, -31049, -31113, -31175, -31236, -31297,
 -31356, -31413, -31470, -31525, -31580, -31633, -31684, -31735,
 -31785, -31833, -31880, -31926, -31970, -32014, -32056, -32097,
 -32137, -32176, -32213, -32249, -32284, -32318, -32350, -32382,
 -32412, -32441, -32468, -32495, -32520, -32544, -32567, -32588,
 -32609, -32628, -32646, -32662, -32678, -32692, -32705, -32717,
 -32727, -32736, -32744, -32751, -32757, -32761, -32764, -32766,
};

const int16_t linTable[182] = 
{
	0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8,
	8, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14,
	14, 15, 15, 15, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20, 20,
	20, 21, 21, 21, 22, 22, 22, 23, 23, 23, 24, 24, 25, 25, 25, 26, 26,
	26, 27, 27, 27, 28, 28, 28, 29, 29, 29, 30, 30, 30, 31, 31, 31, 32,
	32, 33, 33, 33, 34, 34, 34, 35, 35, 35, 36, 36, 36, 37, 37, 37, 38,
	38, 38, 39, 39, 39, 40, 40, 41, 41, 41, 42, 42, 42, 43, 43, 43, 44,
	44, 44, 45, 45, 45, 46, 46, 46, 47, 47, 47, 48, 48, 49, 49, 49, 50,
	50, 50, 51, 51, 51, 52, 52, 52, 53, 53, 53, 54, 54, 54, 55, 55, 55,
	56, 56, 57, 57, 57, 58, 58, 58, 59, 59, 59, 60, 60, 60, 61, 61, 61,
	62, 62, 62, 63, 63, 63	
};

/*
  FIX_MPY() - fixed-point multiplication & scaling.
  Substitute inline assembly for hardware-specific
  optimization suited to a particluar DSP processor.
  Scaling ensures that result remains 16-bit.
*/
int16_t FIX_MPY(int16_t a, int16_t b)
{
	/* shift right one less bit (i.e. 15-1) */
	int32_t c = ((int32_t)a * (int32_t)b) >> 14;
	/* last bit shifted out = rounding-bit */
	b = c & 0x01;
	/* last shift + rounding bit */
	a = (c >> 1) + b;
	return a;
}

/*
  fix_fft() - perform forward/inverse fast Fourier transform.
  fr[n],fi[n] are real and imaginary arrays, both INPUT AND
  RESULT (in-place FFT), with 0 <= n < 2**m; set inverse to
  0 for forward transform (FFT), or 1 for iFFT.
*/
int32_t fix_fft(int16_t fr[], int16_t fi[], uint8_t m, uint8_t inverse)
{
	int32_t mr, nn, i, j, l, k, istep, n, scale, shift;
	int16_t qr, qi, tr, ti, wr, wi;

	n = 1 << m;

	/* max FFT size = N_WAVE */
	if (n > N_WAVE)
		return -1;

	mr = 0;
	nn = n - 1;
	scale = 0;

	/* decimation in time - re-order data */
	for (m=1; m<=nn; ++m) {
		l = n;
		do {
			l >>= 1;
		} while (mr+l > nn);
		mr = (mr & (l-1)) + l;

		if (mr <= m)
			continue;
		tr = fr[m];
		fr[m] = fr[mr];
		fr[mr] = tr;
		ti = fi[m];
		fi[m] = fi[mr];
		fi[mr] = ti;
	}

	l = 1;
	k = LOG2_N_WAVE-1;
	while (l < n) {
		if (inverse) {
			/* variable scaling, depending upon data */
			shift = 0;
			for (i=0; i<n; ++i) {
				j = fr[i];
				if (j < 0)
					j = -j;
				m = fi[i];
				if (m < 0)
					m = -m;
				if (j > 16383 || m > 16383) {
					shift = 1;
					break;
				}
			}
			if (shift)
				++scale;
		} else {
			/*
			  fixed scaling, for proper normalization there will be log2(n) passes, so this results
			  in an overall factor of 1/n, distributed to maximize arithmetic accuracy.
			*/
			shift = 1;
		}
		/*
		  it may not be obvious, but the shift will be performed on each data point exactly once,
		  during this pass.
		*/
		istep = l << 1;
		for (m=0; m<l; ++m) {
			j = m << k;
			/* 0 <= j < N_WAVE/2 */
			wr =  Sinewave[j+N_WAVE/4];
			wi = -Sinewave[j];
			if (inverse)
				wi = -wi;
			if (shift) {
				wr >>= 1;
				wi >>= 1;
			}
			for (i=m; i<n; i+=istep) {
				j = i + l;
				tr = FIX_MPY(wr,fr[j]) - FIX_MPY(wi,fi[j]);
				ti = FIX_MPY(wr,fi[j]) + FIX_MPY(wi,fr[j]);
				qr = fr[i];
				qi = fi[i];
				if (shift) {
					qr >>= 1;
					qi >>= 1;
				}
				fr[j] = qr - tr;
				fi[j] = qi - ti;
				fr[i] = qr + tr;
				fi[i] = qi + ti;
			}
		}
		--k;
		l = istep;
	}
	return scale;
}

/*
  fix_fftr() - forward/inverse FFT on array of real numbers.
  Real FFT/iFFT using half-size complex FFT by distributing even/odd samples into real/imaginary arrays respectively.
  In order to save data space (i.e. to avoid two arrays, one for real, one for imaginary samples), we proceed in the
  following two steps: a) samples are rearranged in the real array so that all even samples are in places 0-(N/2-1) and
  all imaginary samples in places (N/2)-(N-1), and b) fix_fft is called with fr and fi pointing to index 0 and index N/2
  respectively in the original array. The above guarantees that fix_fft "sees" consecutive real samples as alternating
  real and imaginary samples in the complex array.
*/
int32_t fix_fftr(int16_t f[], uint8_t m, uint8_t inverse)
{
	int32_t i, N = 1<<(m-1), scale = 0;
	int16_t tt, *fr=f, *fi=&f[N];

	if (inverse)
		scale = fix_fft(fi, fr, m-1, inverse);
	for (i=1; i<N; i+=2) {
		tt = f[N+i-1];
		f[N+i-1] = f[i];
		f[i] = tt;
	}
	if (! inverse)
		scale = fix_fft(fi, fr, m-1, inverse);
	return scale;
}

esp_adc_cal_characteristics_t   adc_char;
int16_t inputValue;

short  displayData[20];
short  oldDisplayData[20];
short  displayDataToCube[20];

int16_t  im[1024];
int16_t  data[1024];
int16_t  val;
int timeSlow = 0;

void feedTheDog(){
  // feed dog 0
   TIMERG0.wdt_wprotect=TIMG_WDT_WKEY_VALUE; // write enable
   TIMERG0.wdt_feed=1;                       // feed dog
   TIMERG0.wdt_wprotect=0;                   // write protect
  // feed dog 1
  // TIMERG1.wdt_wprotect=TIMG_WDT_WKEY_VALUE; // write enable
  // TIMERG1.wdt_feed=1;                       // feed dog
  // TIMERG1.wdt_wprotect=0;                   // write protect
}

void fft_display()
{ 
    ESP_ERROR_CHECK(led_strip_init(&control_cube.strip)); 
    // Hiệu chỉnh ADC
    esp_adc_cal_characterize(ADC_UNIT_1,ADC_ATTEN_DB_11,ADC_WIDTH_BIT_12, 0, &adc_char);
    // cấu hình độ rộng ADC
    adc1_config_width(ADC_WIDTH_BIT_12);
    // cấu hình dải điện áp đầu vào cho adc 
    adc1_config_channel_atten(ADC1_CHANNEL_5 , ADC_ATTEN_DB_11);  // 33
    delay_ms(1000);
    set_brightness(&control_cube, 15);
   // esp_task_wdt_init(30, false);

    while (1)
    {
        feedTheDog();
        for(int i = 0; i < 512; i++)
        {
            val = adc1_get_raw(ADC1_CHANNEL_5);
            data[i] = val  - 2048;;
			im[i] = 0;
        }
        fix_fft(data,im,7,0);
        for(int i=0; i< 256;i++){
            data[i] = sqrt(data[i] * data[i] + im[i] * im[i]);
            // if(data[i] > 183) data[i] = 183;
            // data[i]  =  map(data[i], 0,100,0,20);
        }
        
        int16_t inputValue;
        for (unsigned char counter = 1; counter <20; counter++)
        {
            // Scale the input data for the display (linear) x1 or x8
            inputValue = (data[counter*2 - 1] + data[counter*2 ] +  data[counter*2 + 1])/3;
            if (inputValue > 181) inputValue = 181;
            
            // Apply a linear or logarithmic conversion on the data
            inputValue = (short)linTable[inputValue];
            
            // Perform damping on the displayed output
            if (inputValue > displayData[counter-1]) displayData[counter-1] = inputValue ;
            else displayData[counter-1] -= 10;

            displayData[counter-1] =  displayData[counter-1] - 5;
            if (displayData[counter-1] < 0) displayData[counter-1] = 0;

            if ( displayData[counter-1] > 20)  displayData[counter-1] = 20;
            displayData[counter-1]  =  map(displayData[counter-1], 0,20,0,8);
        }

        // Cài đặt tốc độ nháy, tốc độ cột LED xuống
        for(int i=0; i <= 15; ++i)
        {               
            if(displayData[i] >= oldDisplayData[i])
            {
                displayDataToCube[i] = displayData[i];
                oldDisplayData[i]    = displayData[i];
            } 
        }
        
        uint8_t speedSlow = 0;
        if(timeSlow > speedSlow)
        {
            for(int i=0; i <= 15; ++i)
            {
                if(oldDisplayData[i] > 0 )
                {
                    oldDisplayData[i]    = oldDisplayData[i] - 1;
                    displayDataToCube[i] = oldDisplayData[i]; 
                    timeSlow = 0;  
                } 
            }    
        }
        // Show data ra LED CUBE
        showAllCollumm(&control_cube,displayDataToCube);
        show_led(&control_cube);
        timeSlow++;
    }
}


void app_main()
{
    init_led_cube();
    led_strip_install();
    
    xTaskCreatePinnedToCore(cube_run, "cube_run",8192*2, NULL, 5, &cube_run_handle,1);
    xTaskCreatePinnedToCore(timer_interrupt, "timer_interrupt", 8192, NULL,5,  &timer_interrupt_handle,1);

    wifi_ap_start("LED Cube 8x8x8","12345678",1,5);
    httpd_handle_t server = NULL;
    server = start_webserver(); 

    read_data_config_from_flash();
    xTaskCreatePinnedToCore(data_eff_config_check, "data_eff_config_check",8192, NULL, 5, &data_eff_handle,0);
    
   // disableCore0WDT();
   // xTaskCreatePinnedToCore(fft_display, "readADC",  1024*8, NULL, configMAX_PRIORITIES-1, &readADCTaskHandle, 1);
}
// cấu hình partion 0x9000
