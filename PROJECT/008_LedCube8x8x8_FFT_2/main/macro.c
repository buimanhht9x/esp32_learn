#include "macro.h"

void delay_ms(int time_delay)
{
    vTaskDelay(time_delay/portTICK_RATE_MS);
}

int random_number(int minN, int maxN){
	return minN + rand() % (maxN + 1 - minN);
}

uint32_t map(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

