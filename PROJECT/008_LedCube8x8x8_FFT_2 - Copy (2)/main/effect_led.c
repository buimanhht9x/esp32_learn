#include "effect_led.h"

/*.............................Hiệu ứng rainbow...............................*/




void effect_rainbow(control_led_cube_t *control_led_cube, uint8_t enordis,uint32_t timeRun, int speed)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        while(1)
        {
            for(int j = 0; j < 256; j++) {
            for(int i = 0; i < 512; i++) {
                control_led_cube->layerRGB_buffer[i] = Scroll((i * 256 / 512 + j) % 256);   
            } 
            delay_ms(speed);
            } 
            if(sec() - control_led_cube->time_start > timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; 
                fill_cube(control_led_cube, 0x00);
                delay_ms(1000);
                return;
            }
        }
    }
   
}

/*.............................Hiệu ứng mưa..................................*/
void effect_rain(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int speed, int direction) {
    if(enordis == ENABLE)
    {
        int i, ii;
        int rnd_x;
        int rnd_y;
        int rnd_num;
        static char scroll = 0;
        
        clear_layer_buffer(control_led_cube);
        control_led_cube->time_start = sec();
        while(1) {
        
            rnd_num = random_number(0,7);
            for (i=0; i<rnd_num; i++) {
                rnd_x=random_number(0,7) ;
                rnd_y=random_number(0,7) ;
                setvoxel(control_led_cube, 7, rnd_y, rnd_x);
            }
            delay_ms(speed);
            shift(control_led_cube, 1, direction);
            rgb_t rgb = Scroll(scroll);
            set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
            scroll++;
            if(sec() - control_led_cube->time_start > timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; 
                fill_cube(control_led_cube, 0x00);
                delay_ms(1000);
                return;
            }
        }
    }  
}

/*......................Hiệu ứng đổi màu side................................*/
static rgb_t eff_side_change_color_buffer[13] = {
    (rgb_t) { .r = 255, .g = 0, .b = 0 },
    (rgb_t) { .r = 255, .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 255 },
    (rgb_t) { .r = 0, .g = 0, .b = 255 } ,
    (rgb_t) { .r = 255, .g = 0, .b = 255 } ,
    (rgb_t) { .r = 255, .g = 0, .b = 0 },
    (rgb_t) { .r = 255, .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 0 },
    (rgb_t) { .r = 0  , .g = 255, .b = 255 },
    (rgb_t) { .r = 0, .g = 0, .b = 255 }  ,
    (rgb_t) { .r = 0, .g = 255, .b = 100 } ,
    (rgb_t) { .r = 255, .g = 255, .b = 255 }  
};
void eff_side_change_color(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        while(1)
        {
            static uint8_t k = 0;
            if(k%3 == 0)
            {
                for(int z = 0; z <= CUBE_SIZE; z ++)
                {
                    for(int x = 0; x <= CUBE_SIZE; x ++)
                    {
                        for(int y = 0; y <= CUBE_SIZE; y ++)
                        {
                            set_color_to_buffer(control_led_cube, z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                        } 
                    }
                    vTaskDelay(pdMS_TO_TICKS(50));
                }
            }
            else if(k%3 == 1)
            {
                for(int z = CUBE_SIZE; z >= 0; z --)
                {
                    for(int x = 0; x <= CUBE_SIZE; x ++)
                    {
                        for(int y = 0; y <= CUBE_SIZE; y ++)
                        {
                            set_color_to_buffer(control_led_cube, z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                        } 
                    }
                    vTaskDelay(pdMS_TO_TICKS(50));
                }
            }
            else if(k%3 == 2)
            {
                for(int x = CUBE_SIZE; x >= 0; x --)
                {
                    for(int z = CUBE_SIZE; z >= 0; z --)
                    {
                        for(int y = 0; y <= CUBE_SIZE; y ++)
                        {
                            set_color_to_buffer(control_led_cube, z,y,x, (rgb_t)eff_side_change_color_buffer[k]); 
                        } 
                    }
                    vTaskDelay(pdMS_TO_TICKS(50));
                }
            }
            
            k++;
            if(k > 12)  k = 0;
            if(sec() - control_led_cube->time_start > timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; 
                fill_cube(control_led_cube, 0x00);
                delay_ms(1000);
                return;
            } 
        }   
    }
}


/*.......................Hiệu ứng planboing.............................*/

void planboing(control_led_cube_t *control_led_cube, char axis, int speed) {

    int i;
    static char scroll = 0;

    for (i=0; i<CUBE_SIZE; i++) {
        fill_cube(control_led_cube, 0x00);
        setplane(control_led_cube, axis, i);
        delay_ms(speed);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
        scroll++;
    }
    for (i=(CUBE_SIZE - 1); i>=0; i--) {
        fill_cube(control_led_cube, 0x00);
        setplane(control_led_cube, axis, i);
        delay_ms(speed);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
        scroll++;
    }
}
void effect_planboing(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun)
{
    control_led_cube->time_start = sec();
    if(enordis == ENABLE)
    {
        while(1)
        {
            planboing(control_led_cube, AXIS_Z,50); 
            planboing(control_led_cube, AXIS_X,50);
            planboing(control_led_cube,AXIS_Y,50);
            planboing(control_led_cube,AXIS_Z,50);
            planboing(control_led_cube,AXIS_X,50);
            planboing(control_led_cube,AXIS_Y,50);
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; 
                fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

/*.......................Hiệu ứng Send voxel rand..............................*/
void effect_sendvoxels_rand_axis(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, char axis, char delay, char wait) {
    if(enordis == ENABLE)
    {
        unsigned char x, y, z, i, last_x=0, last_y=0;
        fill_cube(control_led_cube, 0);
        static char scroll;
        control_led_cube->time_start  = sec();
        if (axis==1)
        {
            // for (x=0; x<CUBE_SIZE; x++) {
            //     for (y=0; y<CUBE_SIZE; y++) {
            //         setvoxel(((rand() % 2)*(CUBE_SIZE - 1)), y, x);
            //     }
            // }
            while(1) {
                x=rand() % CUBE_SIZE;
                y=rand() % CUBE_SIZE;
                if (y != last_y && x != last_x) {
                    if (getvoxel(control_led_cube, 0, y, x)) {
                        sendvoxel_axis(control_led_cube, 0, y, x, axis, delay);
                    } else {
                        sendvoxel_axis(control_led_cube, (CUBE_SIZE - 1), y, x, axis, delay);
                    }
                    delay_ms(wait);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
                    scroll++;
            
                    last_y=y;
                    last_x=x;
                }
                if(sec() - control_led_cube->time_start  >= timeRun)
                    return;
                if(control_led_cube->restart_value == 1)
                {
                    control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                    return;
                }
            }
        }

        if (axis==2) {
            // for (x=0; x<CUBE_SIZE; x++) {
            //     for (y=0; y<CUBE_SIZE; y++) {
            //         setvoxel(y, ((rand() % 2)*(CUBE_SIZE - 1)), x);
            //     }
            // }
            while(1) {
                x=rand() % CUBE_SIZE;
                y=rand() % CUBE_SIZE;
                if (y != last_y && x != last_x) {

                    if (getvoxel(control_led_cube, y, 0, x)) {
                        sendvoxel_axis(control_led_cube,y, 0, x, axis, delay);
                    } else {
                        sendvoxel_axis(control_led_cube, y, (CUBE_SIZE - 1), x, axis, delay);
                    }
                    delay_ms(wait);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
                    scroll++;
                    last_y=y;
                    last_x=x;
                } 
                if(sec() - control_led_cube->time_start  >= timeRun)
                    return;
                if(control_led_cube->restart_value == 1)
                {
                    control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                    return;
                }
            }
        }

        if (axis==3) {
            // for (x=0; x<CUBE_SIZE; x++) {
            //     for (y=0; y<CUBE_SIZE; y++) {
            //         setvoxel(y, x, ((rand() % 2)*(CUBE_SIZE - 1)));
            //     }
            // }
            while(1) {
                x=rand() % CUBE_SIZE;
                y=rand() % CUBE_SIZE;
                if (y != last_y && x != last_x) {
                    if (getvoxel(control_led_cube, y, x, 0)) {
                        sendvoxel_axis(control_led_cube, y, x, 0, axis, delay);
                    } else {
                        sendvoxel_axis(control_led_cube, y, x, (CUBE_SIZE - 1), axis, delay);
                    }
                    delay_ms(wait);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
                    scroll++;
                    last_y=y;
                    last_x=x;
                }
                if(sec() - control_led_cube->time_start  >= timeRun)
                    return;
                if(control_led_cube->restart_value == 1)
                {
                    control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                    return;
                }
            }
        }
    }  
}

/*............................Hiệu ứng Box Shrinkgrow and Woopwoop ..........................*/
void effect_box_shrink_grow(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int speed)
{
    if(enordis == ENABLE)
    {
        int x, i, ii = 0, xyz;
        int iterations=1;
        int flip = (ii & (CUBE_SIZE / 2));
        static char scroll;
        control_led_cube->time_start = sec();
        int k = 0;
        for (ii=0; ii<CUBE_SIZE; ii++) {
            while(1) {
                for (i=0; i<(CUBE_SIZE * 2); i++) {
                    xyz=(CUBE_SIZE - 1) - i; 
                    if (i>(CUBE_SIZE - 1))
                        xyz=i - CUBE_SIZE; 

                    
                    fill_cube(control_led_cube, 0x00);
                    delay_ms(2);
                    box_wireframe(control_led_cube, 0, 0, 0, xyz, xyz, xyz);

                    // if (flip>0) 
                    //     mirror_z();
                    // if (ii==(CUBE_SIZE - 3) || ii==(CUBE_SIZE - 1))
                    mirror_y(control_led_cube);
                    // if (ii==(CUBE_SIZE - 2) || ii==(CUBE_SIZE - 1))
                    //     mirror_x();
                   
                    if(k%3 == 0) mirror_z(control_led_cube);
                    if(k%3 == 1) mirror_y(control_led_cube);
                    if(k%3 == 2) mirror_x(control_led_cube);
   
                    delay_ms(speed);
                    fill_cube(control_led_cube, 0x00);
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
                    scroll++;   
                } 
                 k++;
                if(sec() - control_led_cube->time_start >= timeRun)
                    return;
                if(control_led_cube->restart_value == 1)
                {
                    control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                    return;
                }
            }    
        }
    }
}

void box_woopwoop(control_led_cube_t *control_led_cube, int delay, int grow) 
{
    int i, ii;
    char ci = CUBE_SIZE / 2;
    fill_cube(control_led_cube, 0x00);

    for (i=0; i<ci; i++) {
        ii=i;
        if (grow>0)
            ii=(ci - 1) - i;

        box_wireframe(control_led_cube, ci + ii, ci + ii, ci + ii, (ci - 1) - ii, (ci - 1) - ii, (ci - 1) - ii);
        delay_ms(delay);
        fill_cube(control_led_cube, 0x00);
    }
}

void effect_box_woopwoop(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        while(1) {
            box_woopwoop(control_led_cube, 50, 0);
            box_woopwoop(control_led_cube, 50, 1);
            box_woopwoop(control_led_cube,50, 0);
            box_woopwoop(control_led_cube, 50, 1);
            box_woopwoop(control_led_cube, 50, 0);
            box_woopwoop(control_led_cube, 50, 1);
            box_woopwoop(control_led_cube, 50, 0);
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}


/*....................................Hiệu ứng Axis updown rand suppend............................................*/
void axis_updown_randsuspend(control_led_cube_t *control_led_cube, char axis, char delay, int sleep, char invert) {
    unsigned char positions[CUBE_SIZE * CUBE_SIZE];
    unsigned char destinations[CUBE_SIZE * CUBE_SIZE];
    int i, px;
    static char scroll;
    fill_cube(control_led_cube, 0x00);

    // Set 64 random positions
    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        positions[i]=0; // Set all starting positions to 0
        destinations[i]=rand() % CUBE_SIZE;
    }

    // Loop 8 times to allow destination 7 to reach all the way
    for (i=0; i<CUBE_SIZE; i++) {
        // For every iteration, move all position one step closer to their destination
        for (px=0; px<(CUBE_SIZE * CUBE_SIZE); px++) {
            if (positions[px]<destinations[px]) {
                positions[px]++;
            }
        }
        // Draw the positions and take a nap
        draw_positions_axis(control_led_cube, axis, positions, invert);
        delay_ms(delay);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
        scroll++;
                
    }

    // Set all destinations to 7 (opposite from the side they started out)
    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        destinations[i]=CUBE_SIZE - 1;
    }

    // Suspend the positions in mid-air for a while
    delay_ms(sleep*2);

    // Then do the same thing one more time
    for (i=0; i<CUBE_SIZE; i++) {
        for (px=0; px<(CUBE_SIZE * CUBE_SIZE); px++) {
            if (positions[px]<destinations[px]) {
                positions[px]++;
            }
            if (positions[px]>destinations[px]) {
                positions[px]--;
            }
        }
        draw_positions_axis(control_led_cube, axis, positions, invert);
        delay_ms(delay);
        rgb_t rgb = Scroll(scroll);
        set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
        scroll++;
                
       
    }

    // Suspend the positions in top-bottom for a while
     delay_ms(sleep*2);
}

void effect_axis_z_updown_randsuspend(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        while(1)
        {
            axis_updown_randsuspend(control_led_cube, 1, 50, 0, 0);  // 9(char axis, int delay, int sleep, int invert) z
            axis_updown_randsuspend(control_led_cube, 1, 50, 0, 1);
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

void effect_axis_y_updown_randsuspend(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        while(1)
        {
            axis_updown_randsuspend(control_led_cube, 2, 50, 0, 0);  // 9(char axis, int delay, int sleep, int invert) z
            axis_updown_randsuspend(control_led_cube, 2, 50, 0, 1);
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

// /*.........................Hiệu ứng sidewaves...................................*/
int totty_sin(unsigned char LUT[65],int sin_of)
{
    unsigned char inv=0;
    if (sin_of<0)
    {
        sin_of=-sin_of;
        inv=1;
    }
    sin_of&=0x7f;  // 127
    if (sin_of>64)
    {
        sin_of-=64;
        inv=1-inv;
    }
    if (inv)
        return -LUT[sin_of];
    else
        return LUT[sin_of];
}

int totty_cos(unsigned char LUT[65],int cos_of)
{
    unsigned char inv=0;
    cos_of+=32;    // Simply rotate by 90 degrees for COS
    cos_of&=0x7f;  // 127
    if (cos_of>64)
    {
        cos_of-=64;
        inv=1;
    }
    if (inv)
        return -LUT[cos_of];
    else
        return LUT[cos_of];
}
void init_LUT(unsigned char LUT[65])
{
  unsigned char i;
  float sin_of,sine;
  for (i=0;i<65;++i)
  {
    sin_of=i*(3.1415)/64; // Just need half a sin wave
    sine=sin(sin_of);
    // Use 181.0 as this squared is <32767, so we can multiply two sin or cos without overflowing an int.
    LUT[i]=sine*181.0;
  }
}


void effect_int_sidewaves(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay) {
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        unsigned char LUT[65];
        int i = 0;
        int origin_x, origin_y, distance, height;
        int x_dist,x_dist2,y_dist;
        int x,y,x_vox;
        static char scroll;
        init_LUT(LUT);

        while(1)
        {	// To provide some finer control over the integer calcs the x and y
            // parameters are scaled up by a factor of 15.  This is primarily to
            // keep the sum of their squares within the scale of an integer.
            // 120^2 + 120^2=28800
            // If we scaled by 16 we would overflow at the very extremes,
            // e.g 128^2+128^2=32768.  The largest int is 32767.
            //
            // Because origin_x/y is a sin/cos pair centred at 60, the actual
            // highest distance in this effect would be at:
            // x=8,y=8, origin_x=102, origin_y=102
            //=approximate sum of 17400.
            //
            // It is probably safer to work at a scale of 15 to allow simple changes
            // to the maths to be made without risking an overflow.
            origin_x=(totty_sin(LUT,i/2)+180)/3;  // Approximately 0 to 120
            origin_y=(totty_cos(LUT,i/2)+180)/3;
            fill_cube(control_led_cube, 0x00);
            for (x=8; x<120; x+=15)// 8 steps from 8 to 113
            {
                // Everything in here happens 8 times per cycle
                x_dist=abs(x-origin_x);
                x_dist2=x_dist*x_dist;  // square of difference
                x_vox=x/15;  // Unscale x
                for (y=8; y<120; y+=15)
                {
                    // Everything in here happens 64 times per cycle
                    y_dist=abs(y-origin_y);
                    if (x_dist||y_dist)  // Either x OR y non-zero
                    {
                        // Calculate sum of squares of linear distances
                        // We use a 1st order Newton approximation:
                        // sqrt=(N/guess+guess)/2
                        distance=(x_dist2+y_dist*y_dist);
                        height=(x_dist+y_dist)/2;  // Approximate quotient
                        // We divide by 30.  1st approx would be /2
                        // but we have a factor of 15 included in our scale calcs
                        distance=(distance/height+height)/3; // 1st approx at sqrt
                    }
                    else
                        distance=0;  // x and y=origin_x and origin_y
                    height=(totty_sin(LUT,distance+i)+180)/51 ;
                    setvoxel(control_led_cube, height,y/15,x_vox);
                    
                }
            }
            i++;
            rgb_t rgb = Scroll(scroll);
            set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
            scroll++;
            delay_ms(delay);
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

/*..............................Hiệu ứng ripple.............................*/
void effect_int_ripples(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
         // 16 values for square root of a^2+b^2.  index a*4+b = 10*sqrt
        // This gives the distance to 3.5,3.5 from the point
        unsigned char sqrt_LUT[]={49,43,38,35,43,35,29,26,38,29,21,16,35,25,16,7};
        //LUT_START // Macro from new tottymath.  Commented and replaced with full code
        unsigned char LUT[65];
        init_LUT(LUT);
        int i = 0;
        unsigned char x,y,height,distance;
        while(1)
        {
            i++;
            fill_cube(control_led_cube, 0x00);
            for (x=0;x<4;++x)
            for(y=0;y<4;++y)
            {
                // x+y*4 gives no. from 0-15 for sqrt_LUT
                distance=sqrt_LUT[x+y*4];// distance is 0-50 roughly
                // height is sin of distance + iteration*4
                //height=4+totty_sin(LUT,distance+i)/52;
                height=(196+totty_sin(LUT,distance+i))/49;
                // Use 4-way mirroring to save on calculations
                rgb_t rgb ;
                if(height == 7)
                { 
                    rgb = Scroll(255);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 6)
                { 
                    rgb = Scroll(220);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 5)
                { 
                    rgb = Scroll(200);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 4)
                { 
                    rgb = Scroll(180);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 3)
                { 
                    rgb = Scroll(160);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 2)
                { 
                    rgb = Scroll(130);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 1)
                { 
                    rgb = Scroll(110);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 
                if(height == 0)
                { 
                    rgb = Scroll(90);
                    set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
                } 

                setvoxel(control_led_cube, height,y,x);
                setvoxel(control_led_cube, height,y,7-x);
                setvoxel(control_led_cube, height,7-y,x);
                setvoxel(control_led_cube, height,7-y,7-x);
                
            }
            delay_ms(delay);
            // change_rgb_color(control_led_cube);
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

// /*.........................Hiệu ứng wormsqueeze.............................*/
void effect_wormsqueeze(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay,int axis, int direction) {
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        int x, y, i, j, k, dx, dy, size, cube_size;
        int origin=0;
        static char scroll;

        if (CUBE_SIZE==4)
            size=1;
        if (CUBE_SIZE==8)
            size=2;

        if (direction==-1)
            origin=(CUBE_SIZE - 1);

        cube_size=CUBE_SIZE - (size - 1);

        x=rand() % CUBE_SIZE;
        y=rand() % CUBE_SIZE;

        for (i=0; i<10000; i++) {
            dx=((rand() % (CUBE_SIZE / 2 - 1)) - 1); // %3
            dy=((rand() % (CUBE_SIZE / 2 - 1)) - 1); // %3

            if ((x + dx)>0 && (x + dx)<CUBE_SIZE)
                x += dx;

            if ((y + dy)>0 && (y + dy)<CUBE_SIZE)
                y += dy;

             shift(control_led_cube, axis, direction);

            for (j=0; j<size; j++) {
                for (k=0; k<size; k++) {
                    if (axis==1) // AXIS_Z
                    {
                        setvoxel(control_led_cube, origin, y + k, x + j);
                        origin--;
                    } // setvoxel(x+j,y+k,origin);

                    if (axis==2) // AXIS_Y
                        setvoxel(control_led_cube, y + k, origin, x + j); // setvoxel(control_led_cube, x+j,origin,y+k);

                    if (axis==3) // AXIS_X
                        setvoxel(control_led_cube, x + k, y + j, origin); // setvoxel(origin,y+j,x+k);
                    
                    rgb_t rgb = Scroll(scroll);
                    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
                    scroll++;
                }        
            }  
            delay_ms(delay); 
            if(sec() - control_led_cube->time_start >= timeRun)
            {
                 fill_cube(control_led_cube, 0x00); // Blank the cube
                 return;
            }
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }              
        }
    }
}

/*.........................Hiệu ứng cubix.......................................*/
void effect_cubix(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, unsigned char cubies)
{
  if(enordis == ENABLE)
    {
         control_led_cube->time_start = sec();

         // cube array:
        // Stupid name to prevent confusion with cube[][]!
        // 0=pos
        // 1=dir (0 stopped 1,2 or 4 move in X, Y or Z)
        // 2=inc or dec in direction (2=inc, 0=dec)
        // 3=countdown to new movement
        // 4=x, 5=y, 6=z
        unsigned char qubes[3][7];
        int ii; // iteration counter
        unsigned char i,j,diridx,newdir;
        unsigned char runstate=2;
        //  Initialise qubes array
        for (i=0;i<cubies;++i)
        {
            qubes[i][0]=i;// position = i
            qubes[i][1]=0;// static
            qubes[i][3]=rand()&0x0f;// 0-15
            // Hack in the X,Y and Z positions
            qubes[i][4]=4*(i&0x01);
            qubes[i][5]=2*(i&0x02);
            qubes[i][6]=(i&0x04);
        }
        // Main loop
        ii=20000;
        while(runstate)
        {
            fill_cube(control_led_cube, 0x00);
            for (i=0;i<cubies;++i)
            {
                // Use a pointer to simplify array indexing
                // qube[0..7] = qubes[i][0..7]
                unsigned char *qube=&qubes[i][0];
                if (qube[1]) //moving
                {
                    diridx=3+qube[1];//4,5 or 7
                    if (diridx==7) diridx=6;
                    qube[diridx]+=qube[2]-1;
                    if ((qube[diridx]==0)||(qube[diridx]==4))
                    {
                    // XOR old pos and dir to get new pos.
                    qube[0]=qube[0]^qube[1];
                    qube[1]=0;// Stop moving!
                    qube[3]=rand()&0x0f; // countdown to next move 0-15
                    if(runstate==1)
                        if (qube[0]<5)
                        qube[3]*=4;// Make lower qubes move very slowly to finish
                    }
                }
                else // not moving
                {
                    if (qube[3])// counting down
                    --qube[3];
                    else // ready to move
                    {
                        newdir=(1<<(rand()%3));//1,2 or 4
                        diridx=qube[0]^newdir;
                        for (j=0;j<cubies;++j)// check newdir is safe to move to
                        {
                            if ((diridx==qubes[j][0])||(diridx==(qubes[j][0]^qubes[j][1])))
                            {
                                newdir=0;
                                qube[3]=5;
                            }
                        }
                        if (newdir)
                        {
                            diridx=3+newdir;
                            if (diridx==7) diridx=6;
                            if (qube[diridx])// should be 4 or 0
                            qube[2]=0; // dec if at 4
                            else
                            {
                            qube[2]=2; // inc if at 0
                            if(runstate==1)// Try to make qubes go home
                                if ((diridx>4)&&(qube[0]<4))
                                newdir=0;//Don't allow qubes on bottom row to move up or back
                            }
                        }
                        qube[1]=newdir;
                    }
                }
                //if (i&0x01)//odd number
                if(i == 0){
                    box_filled(control_led_cube, qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
                    set_color_setvoxel(control_led_cube, 255,0,0);
                }
                if(i == 1){
                    box_filled(control_led_cube, qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
                    set_color_setvoxel(control_led_cube, 0,255,0);
                }
                if(i == 2){
                    box_filled(control_led_cube, qube[4],qube[5],qube[6],qube[4]+3,qube[5]+3,qube[6]+3);
                    set_color_setvoxel(control_led_cube, 0,0,255);
                }
            }
            delay_ms(50);
            if(runstate==2){    // If normal running
                if(!(--ii))// decrement iteration and check for zero
                     runstate=1;// If zero go to homing
            }
            else{       //runstate at 1
                diridx=0;
                for(j=0;j<cubies;++j)
                    if (qubes[j][0]+1>cubies) diridx=1;// Any cube not at home
                if (!diridx)
                    runstate=0;
            }
            if(sec() -  control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        } // Main loop
    }
}


/*..............................Hiệu ứng boingboing..........................*/
void effect_boingboing(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, char delay, unsigned char mode, unsigned char drawmode) {
    
    if(enordis == ENABLE)
    {
         control_led_cube->time_start = sec();
        uint8_t x, y, z; // Current coordinates for the point                  // mode=0x02;                 // íå ðàáîòàåò
        int dx, dy, dz; // Direction of movement                     // delay=4; mode=0x01; drawmode=0x01;  // òî÷êà, ïåðåìåùàþùàÿñÿ ïî êóáó
        int lol, i; // lol?                                      // delay=2; mode=0x01; drawmode=0x02;  // òî÷êà, ïåðåìåùàþùàÿñÿ ïî êóáó îñòÿâëÿþùàÿ ñëåä è çàïîëíÿþùàÿ âåñü êóá
        unsigned char crash_x, crash_y, crash_z; // delay=3; mode=0x01; drawmode=0x03;  // çìåéêà, ïåðåìåùàþùàÿñÿ ïî êóáó
        int snake[CUBE_SIZE][3];

        fill_cube(control_led_cube, 0x00); // Blank the cube

        y=rand() % CUBE_SIZE;
        x=rand() % CUBE_SIZE;
        z=rand() % CUBE_SIZE;

        // Coordinate array for the snake
        for (i=0; i<CUBE_SIZE; i++) {
            snake[i][0]=x;
            snake[i][1]=y;
            snake[i][2]=z;
        }

        dx=1;
        dy=1;
        dz=1;

        for(int k =0; k < 10000; k++)
        {
            crash_x=0;
            crash_y=0;
            crash_z=0;

            // Let's mix things up a little:
            if (rand() % 3==0) {
                // Pick a random axis, and set the speed to a random number.
                lol=rand() % 3;
                if (lol==0)
                    dx=rand() % 3 - 1;

                if (lol==1)
                    dy=rand() % 3 - 1;

                if (lol==2)
                    dz=rand() % 3 - 1;
            }

            // The point has reached 0 on the x-axis and is trying to go to -1
            // aka a crash
            if (dx==-1 && x==0) {
                crash_x=0x01;
                if (rand() % 3==1) {
                    dx=1;
                } else {
                    dx=0;
                }
            }

            // y axis 0 crash
            if (dy==-1 && y==0) {
                crash_y=0x01;
                if (rand() % 3==1) {
                    dy=1;
                } else {
                    dy=0;
                }
            }

            // z axis 0 crash
            if (dz==-1 && z==0) {
                crash_z=0x01;
                if (rand() % 3==1) {
                    dz=1;
                } else {
                    dz=0;
                }
            }

            // x axis 7 crash
            if (dx==1 && x==(CUBE_SIZE - 1)) {
                crash_x=0x01;
                if (rand() % 3==1) {
                    dx=-1;
                } else {
                    dx=0;
                }
            }

            // y axis 7 crash
            if (dy==1 && y==(CUBE_SIZE - 1)) {
                crash_y=0x01;
                if (rand() % 3==1) {
                    dy=-1;
                } else {
                    dy=0;
                }
            }

            // z azis 7 crash
            if (dz==1 && z==(CUBE_SIZE - 1)) {
                crash_z=0x01;
                if (rand() % 3==1) {
                    dz=-1;
                } else {
                    dz=0;
                }
            }

            // mode bit 0 sets crash action enable
            if (mode | 0x01) {
                if (crash_x) {
                    if (dy==0) {
                        if (y==(CUBE_SIZE - 1)) {
                            dy=-1;
                        } else if (y==0) {
                            dy=+1;
                        } else {
                            if (rand() % 2==0) {
                                dy=-1;
                            } else {
                                dy=1;
                            }
                        }
                    }
                    if (dz==0) {
                        if (z==(CUBE_SIZE - 1)) {
                            dz=-1;
                        } else if (z==0) {
                            dz=1;
                        } else {
                            if (rand() % 2==0) {
                                dz=-1;
                            } else {
                                dz=1;
                            }
                        }
                    }
                }

                if (crash_y) {
                    if (dx==0) {
                        if (x==(CUBE_SIZE - 1)) {
                            dx=-1;
                        } else if (x==0) {
                            dx=1;
                        } else {
                            if (rand() % 2==0) {
                                dx=-1;
                            } else {
                                dx=1;
                            }
                        }
                    }
                    if (dz==0) {
                        if (z==3) {
                            dz=-1;
                        } else if (z==0) {
                            dz=1;
                        } else {
                            if (rand() % 2==0) {
                                dz=-1;
                            } else {
                                dz=1;
                            }
                        }
                    }
                }

                if (crash_z) {
                    if (dy==0) {
                        if (y==(CUBE_SIZE - 1)) {
                            dy=-1;
                        } else if (y==0) {
                            dy=1;
                        } else {
                            if (rand() % 2==0) {
                                dy=-1;
                            } else {
                                dy=1;
                            }
                        }
                    }
                    if (dx==0) {
                        if (x==(CUBE_SIZE - 1)) {
                            dx=-1;
                        } else if (x==0) {
                            dx=1;
                        } else {
                            if (rand() % 2==0) {
                                dx=-1;
                            } else {
                                dx=1;
                            }
                        }
                    }
                }
            }

            // mode bit 1 sets corner avoid enable       (CUBE_SIZE-1)
            if (mode | 0x02) {
                if (// We are in one of 8 corner positions
                        (x==0 && y==0 && z==0) ||
                        (x==0 && y==0 && z==(CUBE_SIZE - 1)) ||
                        (x==0 && y==(CUBE_SIZE - 1) && z==0) ||
                        (x==0 && y==(CUBE_SIZE - 1) && z==(CUBE_SIZE - 1)) ||
                        (x==(CUBE_SIZE - 1) && y==0 && z==0) ||
                        (x==(CUBE_SIZE - 1) && y==0 && z==(CUBE_SIZE - 1)) ||
                        (x==(CUBE_SIZE - 1) && y==(CUBE_SIZE - 1) && z==0) ||
                        (x==(CUBE_SIZE - 1) && y==(CUBE_SIZE - 1) && z==(CUBE_SIZE - 1))
                        ) {
                    // At this point, the voxel would bounce
                    // back and forth between this corner,
                    // and the exact opposite corner
                    // We don't want that!

                    // So we alter the trajectory a bit,
                    // to avoid corner stickyness
                    lol=rand() % 3;
                    if (lol==0)
                        dx=0;

                    if (lol==1)
                        dy=0;

                    if (lol==2)
                        dz=0;
                }
            }

            // one last sanity check
            if (x==0 && dx==-1)
                dx=1;

            if (y==0 && dy==-1)
                dy=1;

            if (z==0 && dz==-1)
                dz=1;

            if (x==(CUBE_SIZE - 1) && dx==1)
                dx=-1;

            if (y==(CUBE_SIZE - 1) && dy==1)
                dy=-1;

            if (z==(CUBE_SIZE - 1) && dz==1)
                dz=-1;


            // Finally, move the voxel.
            x=x + dx;
            y=y + dy;
            z=z + dz;

            // show one voxel at time (îäèí ñâåòîäèîä)
            if (drawmode==0x01)
            {
                setvoxel(control_led_cube, z, y, x);
                delay_ms(delay);
                clrvoxel(control_led_cube, z, y, x);
                change_rgb_color(control_led_cube);
            }
            // flip the voxel in question (ïåðåäåëàíî â äâèæóùèéñÿ ñâåòîäèîä, îñòàâëÿþùèé ñëåä)
            if (drawmode==0x02)
            {
                // flpvoxel(z,y,x); // èñõîäíûé âàðèàíò
                setvoxel(control_led_cube, z, y, x);
                delay_ms(delay);
                change_rgb_color(control_led_cube);
            }
            // draw a snake (çìåéêà)
            if (drawmode==0x03)
            {
                for (i=(CUBE_SIZE - 1); i>=0; i--) {
                    snake[i][0]=snake[i - 1][0];
                    snake[i][1]=snake[i - 1][1];
                    snake[i][2]=snake[i - 1][2];
                }
                snake[0][0]=x;
                snake[0][1]=y;
                snake[0][2]=z;

                for (i=0; i<CUBE_SIZE; i++) {
                    setvoxel(control_led_cube, snake[i][2], snake[i][1], snake[i][0]);
                }
                delay_ms(delay);
                change_rgb_color(control_led_cube);
                for (i=0; i<CUBE_SIZE; i++) {
                    clrvoxel(control_led_cube, snake[i][2], snake[i][1], snake[i][0]);
                }
            }
            if(sec() -  control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}


/*..............................Hiệu ứng filler...............................*/
void effect_random_filler(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay) {
    if(enordis == ENABLE)
    {
        control_led_cube->time_start  = sec();
        char z,y,x;
       
        for (int i=0; i<1000; i++)  // CUBE_BYTES-3
        {
            z=rand()%CUBE_SIZE;
            y=rand()%CUBE_SIZE;
            x=rand()%CUBE_SIZE;
            if (getvoxel(control_led_cube, z,y,x)==0x00)
                 altervoxel(control_led_cube, z,y,x,1);
            delay_ms(delay);
            change_rgb_color(control_led_cube);
            if(sec() - control_led_cube->time_start  >= timeRun)
                return;

            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
                

        } 
        for (int i=0; i<1000; i++)  // CUBE_BYTES-3
        {
            z=rand()%CUBE_SIZE;
            y=rand()%CUBE_SIZE;
            x=rand()%CUBE_SIZE;
            if ( getvoxel(control_led_cube, z,y,x)==0x01) 
                 altervoxel(control_led_cube, z,y,x,0);
            delay_ms(delay);
            change_rgb_color(control_led_cube);
            if(sec() - control_led_cube->time_start  >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0;fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}


// /*...................Hiệu ứng filip filop...................................*/

void effect_plane_flip(control_led_cube_t *control_led_cube, unsigned char LUT[],unsigned char start,unsigned char end,int delay)
{
  unsigned char p1,p2;// point across and down on flip
  unsigned char x,y,z;
  unsigned char i;// rotational position
  unsigned char i1;// linear position
  unsigned char dir=0;//0-2  0=sidetrap 1=door 2=trap
  unsigned char rev=0;//0 for forward 1 for reverse
  unsigned char inv=0;//bit 0=Y bit 1=Z
  // Sort out dir, rev and inv for each start/end combo.
  //  There are 24, but with some neat combinations of
  // tests we can simplify so that only a max of 3 tests
  // are required for each type!
  if (start<2)// 0 or 1.  buh
  {
    if (end<4)
    {
      dir=0;
      if (end==3) inv=0x01;
    }
    else
    {
      dir=2;
      if (end==5) inv=0x01;
    }
    if (start==1) inv|=0x02;
  } else if (start<4)//2 or 3. Buh
  {
    if (end<2)// going to 0 or 1
    {
      rev=1;
      dir=0;
      if (start==3) inv=0x01;
      if (end==1) inv|=0x02;
    }
    else // going to 4 or 5
    {
      dir=1;// door moves
      if(start+end==7)//3 to 4 or 2 to 5
      {
        if (start==3)
          inv=0x02;
        else
          inv=0x01;
      }
      else//2 to 4 or 3 to 5
      {
        if (start==3)
          inv=0x03;
      }
    }
  }
  else //start is 4 or 5
  {
    if (end<2)// 0 or 1
    {
      dir=2;
      rev=1;// reverse trapdoor.  Yeah!
      if (start+end==5)//4 to 1 or 5 to 0
      {
        if (start==4)//4 to 1
          inv=0x02;
        else
          inv=0x01;
      }
      else // 4 to 0 or 5 to 1
        if (start==5)
          inv=0x03;
    }
    else//end is 2 or 3
    {
      rev=1;// all reverse
      dir=1;// all door
      if (start+end==7)//4 to 3 or 5 to 2
      {
        if (start==4) // 4 to 3
          inv=0x02;
        else
          inv=0x01;
      }
      else
      {
        if (start==5)//5 to 3
          inv=0x03;
      }
    }
  }
  // Do the actual plane drawing
  for(i=0;i<7;++i)
  {
    if (rev)// Reverse movement goes cos-sin
    {
      p2=totty_sin(LUT,i*4);// angle 0-45 degrees
      p1=totty_cos(LUT,i*4);
    }
    else
    {
      p1=totty_sin(LUT,i*4);// angle 0-45 degrees
      p2=totty_cos(LUT,i*4);
    }
    fill_cube(control_led_cube, 0x00);
    for (i1=0;i1<8;++i1)
    {
      z=p1*i1/168;
      if (inv&0x02) z=7-z;// invert in Z axis
      y=p2*i1/168;
      if (inv&0x01) y=7-y;// invert in Y axis
      for(x=0;x<8;++x)
      {
        if(!dir)//dir=0
        {
          setvoxel(control_led_cube, x,y,z);
        }
        else if(dir==1)
        {
          setvoxel(control_led_cube, y,z,x);
        }
        else//dir=2
        {
          setvoxel(control_led_cube, y,x,z);
        }
      }
    }
    delay_ms(delay);
    change_rgb_color(control_led_cube);
  }
}

void effect_filip_filop(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int FF_DELAY)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start  = sec();
        unsigned char LUT[65];
        init_LUT(LUT);
        unsigned char now_plane=0;//start at top
        unsigned char next_plane;
        int delay=FF_DELAY;
        int i;//iteration counter
        for (i=20000;i;--i)
        {
            next_plane=rand()%6; //0-5
            // Check that not the same, and that:
            // 0/1 2/3 4/5 pairs do not exist.
            if ((next_plane&0x06)==(now_plane&0x06))
            next_plane=(next_plane+3)%6;
            effect_plane_flip(control_led_cube,LUT,now_plane,next_plane,delay);
            now_plane=next_plane;
            if(sec() - control_led_cube->time_start  >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }

        }
    }
//  LUT_START // Macro
}

/*............................Hiệu ứng fire work............................*/
void effect_fireworks (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();  
        fill_cube(control_led_cube, 0x00);
        int i,f,e;
        int n = 50; 
        float origin_x = 3;
        float origin_y = 3;
        float origin_z = 3;

        int rand_y, rand_x, rand_z;

        float slowrate, gravity;

        // Particles and their position, x,y,z and their movement, dx, dy, dz
        float particles[50][6];

        for (i=0; i<20000; ++i)
        {

            origin_x = rand()%4;
            origin_y = rand()%4;
            origin_z = rand()%2;
            origin_z +=5;
                origin_x +=2;
                origin_y +=2;

            // shoot a particle up in the air
            rgb_t rgb = Scroll(random_number(15,255));
            for (e=0;e<origin_z;++e)
            {
            int randx = random_number(-1,1);
            int randy;
            if(randx != 0)
            {
                randy = 0;
            }
            else
            {
                randy = random_number(-1,1);
            }
            
            
            if (e == origin_z - 1)
            { 
                setvoxel(control_led_cube, e,origin_y ,origin_x );
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g, rgb.b);
            }
            else if (e == origin_z - 2)
            {
                setvoxel(control_led_cube, e,origin_y ,origin_x );
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g, rgb.b);
                rgb = Scroll(random_number(15,255));
                setvoxel(control_led_cube, e-1,origin_y ,origin_x );
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g, rgb.b);
            }
            else
            {
                setvoxel(control_led_cube, e,origin_y ,origin_x );
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g, rgb.b);
                setvoxel(control_led_cube, e-1,origin_y ,origin_x );
                rgb = Scroll(random_number(15,255));
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g, rgb.b);
                setvoxel(control_led_cube, e-2,origin_y ,origin_x );
                rgb = Scroll(random_number(15,255));
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g, rgb.b);
            }
            
            
            
            
            delay_ms(50+20*e);
            fill_cube(control_led_cube, 0x00);
            }

            // Fill particle array
            for (f=0; f<n; ++f)
            {
            // Position
            particles[f][0] = origin_x;
            particles[f][1] = origin_y;
            particles[f][2] = origin_z;
            
            rand_x = rand()%200;
            rand_y = rand()%200;
            rand_z = rand()%200;

            // Movement
            particles[f][3] = 1-(float)rand_x/100; // dx
            particles[f][4] = 1-(float)rand_y/100; // dy
            particles[f][5] = 1-(float)rand_z/100; // dz
            }

            // explode
            for (e=0; e<25; ++e)
            {
            slowrate = 1+tan((e+0.1)/20)*10;
            
            gravity = tan((e+0.1)/20)/2;

            for (f=0; f<n; ++f)
            {
                particles[f][0] += particles[f][3]/slowrate;
                particles[f][1] += particles[f][4]/slowrate;
                particles[f][2] += particles[f][5]/slowrate;
                particles[f][2] -= gravity;

                setvoxel(control_led_cube, particles[f][2],particles[f][1],particles[f][0]);
                rgb_t rgb = Scroll(f*5) ;
                set_color_setvoxel(control_led_cube, rgb.r,rgb.g,rgb.b);
            }
            delay_ms(delay);
            fill_cube(control_led_cube, 0x00);
            }

            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        } 
    }
}

/*..........................Hiệu ứng sinline.................................*/
void line_3d (control_led_cube_t *control_led_cube, int x1, int y1, int z1, int x2, int y2, int z2)
{
    int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc,
    err_1, err_2, dx2, dy2, dz2;
    int pixel[3];
    pixel[0] = x1;
    pixel[1] = y1;
    pixel[2] = z1;
    dx = x2 - x1;
    dy = y2 - y1;
    dz = z2 - z1;
    x_inc = (dx < 0) ? -1 : 1;
    l = abs(dx);
    y_inc = (dy < 0) ? -1 : 1;
    m = abs(dy);
    z_inc = (dz < 0) ? -1 : 1;
    n = abs(dz);
    dx2 = l << 1;
    dy2 = m << 1;
    dz2 = n << 1;
    if ((l >= m) && (l >= n)) {
    err_1 = dy2 - l;
    err_2 = dz2 - l;
    for (i = 0; i < l; ++i) {
    //PUT_PIXEL(pixel);
    setvoxel(control_led_cube, pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    if (err_1 > 0) {
    pixel[1] += y_inc;
    err_1 -= dx2;
    }
    if (err_2 > 0) {
    pixel[2] += z_inc;
    err_2 -= dx2;
    }
    err_1 += dy2;
    err_2 += dz2;
    pixel[0] += x_inc;
    }
    } else if ((m >= l) && (m >= n)) {
    err_1 = dx2 - m;
    err_2 = dz2 - m;
    for (i = 0; i < m; ++i) {
    //PUT_PIXEL(pixel);
    setvoxel(control_led_cube, pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    if (err_1 > 0) {
    pixel[0] += x_inc;
    err_1 -= dy2;
    }
    if (err_2 > 0) {
    pixel[2] += z_inc;
    err_2 -= dy2;
    }
    err_1 += dx2;
    err_2 += dz2;
    pixel[1] += y_inc;
    }
    } else {
    err_1 = dy2 - n;
    err_2 = dx2 - n;
    for (i = 0; i < n; ++i) {
    setvoxel(control_led_cube, pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    //PUT_PIXEL(pixel);
    if (err_1 > 0) {
    pixel[1] += y_inc;
    err_1 -= dz2;
    }
    if (err_2 > 0) {
    pixel[0] += x_inc;
    err_2 -= dz2;
    }
    err_1 += dy2;
    err_2 += dx2;
    pixel[2] += z_inc;
    }
    }
    setvoxel(control_led_cube, pixel[0],pixel[1],pixel[2]);
    //printf("Setting %i %i %i \n", pixel[0],pixel[1],pixel[2]);
    //PUT_PIXEL(pixel);
}


void effect_sinelines (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        int i,x;

        float left, right, sine_base, x_dividor,ripple_height;

        for (i=0; i<20000; ++i)
        {
            for (x=0; x<8 ;++x)
            {
            x_dividor = 2 + sin((float)i/100)+1;
            ripple_height = 3 + (sin((float)i/200)+1)*6;

            sine_base = (float) i/40 + (float) x/x_dividor;

            left = 4 + sin(sine_base)*ripple_height;
            right = 4 + cos(sine_base)*ripple_height;
            right = 7-left;

            //printf("%i %i \n", (int) left, (int) right);

            line_3d(control_led_cube, 0-3, x, (int) left, 7+3, x, (int) right);
            //line_3d((int) right, 7, x);
            }
            change_rgb_color(control_led_cube);
            delay_ms(delay);
            fill_cube(control_led_cube, 0x00);

            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

/*..........................Hiệu ứng linespin.............................*/
void effect_linespin (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        float top_x, top_y, top_z, bot_x, bot_y, bot_z, sin_base;
        float center_x;

        center_x = 4;

        int i, z;
        for (i=0;i<50000;++i)
        {

            //printf("Sin base %f \n",sin_base);

            for (z = 0; z < 8; ++z)
            {

            sin_base = (float)i/50 + (float)z/(10+(7*sin((float)i/200)));

            top_x = center_x + sin(sin_base)*5;
            top_y = center_x + cos(sin_base)*5;
            //top_z = center_x + cos(sin_base/100)*2.5;

            bot_x = center_x + sin(sin_base+3.14)*10;
            bot_y = center_x + cos(sin_base+3.14)*10;
            //bot_z = 7-top_z;
            
            bot_z = z;
            top_z = z;

            // setvoxel((int) top_x, (int) top_y, 7);
            // setvoxel((int) bot_x, (int) bot_y, 0);

            //printf("P1: %i %i %i P2: %i %i %i \n", (int) top_x, (int) top_y, 7, (int) bot_x, (int) bot_y, 0);

            //line_3d((int) top_x, (int) top_y, (int) top_z, (int) bot_x, (int) bot_y, (int) bot_z);
            line_3d(control_led_cube,(int) top_z, (int) top_x, (int) top_y, (int) bot_z, (int) bot_x, (int) bot_y);
            }

            change_rgb_color(control_led_cube);
            delay_ms(delay);
            fill_cube(control_led_cube, 0x00);
            
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}


/*.................................boxside_randsend_parallel..........................................*/

void effect_boxside_randsend_parallel(control_led_cube_t *control_led_cube, uint8_t axis, uint8_t origin, uint8_t delay, uint8_t mode) {
    uint8_t i;
    uint8_t done;
    uint8_t cubepos[CUBE_SIZE * CUBE_SIZE];
    uint8_t pos[CUBE_SIZE * CUBE_SIZE];
    uint8_t notdone=1;
    uint8_t notdone2=1;
    uint8_t sent=0;
    int k = 0;
   // fill_cube(layer_buffer, cube, rgb_setvoxel, 0x00);

    for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
        pos[i]=0;
    }

    while (notdone) {
        if (mode==1) {
            notdone2=1;
            while (notdone2 && sent<(CUBE_SIZE * CUBE_SIZE)) {
                i=rand() % (CUBE_SIZE * CUBE_SIZE);
                if (pos[i]==0) {
                    sent++;
                    pos[i] += 1;
                    notdone2=0;
                }
            }
        } else if (mode==2) {
            if (sent<(CUBE_SIZE * CUBE_SIZE)) {
                pos[sent] += 1;
                sent++;
            }
        }
        done=0;
        for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
            if (pos[i]>0 && pos[i]<(CUBE_SIZE - 1)) {
                pos[i] += 1;
            }
            if (pos[i]==(CUBE_SIZE - 1))
                done++;
        }
        if (done==(CUBE_SIZE * CUBE_SIZE))
            notdone=0;

        for (i=0; i<(CUBE_SIZE * CUBE_SIZE); i++) {
            if (origin==0) {
                cubepos[i]=pos[i];
            } else {
                cubepos[i]=((CUBE_SIZE - 1) - pos[i]);
            }
        }
        delay_ms(30);
        if(++k > 10)
        {
            change_rgb_color(control_led_cube);
            k = 0;
        }
            
        draw_positions_axis(control_led_cube, axis, cubepos, 0);
        // LED_PORT ^= LED_RED;
    }
    delay_ms(30);
}


/*--------------------------Hiệu ứng StringFly--------------------------------*/
unsigned char font8eng[728][8]={
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, //    0
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // !  1
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // "
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // #
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // $
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // %
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // &
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // '
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // (  8
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // )  9
    {0x22,0x36,0x3e,0xfc,0xfc,0x3e,0x36,0x22}, // *
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // +
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // ,
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // -
    {0x00,0x00,0x00,0x03,0x03,0x00,0x00,0x00}, // .
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // /
    {0x00,0x00,0x3e,0x41,0x41,0x3e,0x00,0x00}, // 0
    {0x00,0x00,0x01,0x7f,0x21,0x00,0x00,0x00}, // 1
    {0x00,0x00,0x31,0x49,0x45,0x23,0x00,0x00}, // 2
    {0x00,0x00,0x36,0x49,0x49,0x22,0x00,0x00}, // 3
    {0x00,0x04,0x7f,0x24,0x14,0x0c,0x00,0x00}, // 4
    {0x00,0x00,0x46,0x49,0x49,0x79,0x00,0x00}, // 5
    {0x00,0x00,0x26,0x49,0x49,0x3e,0x00,0x00}, // 6
    {0x00,0x00,0x70,0x48,0x47,0x40,0x00,0x00}, // 7
    {0x00,0x00,0x36,0x49,0x49,0x36,0x00,0x00}, // 8
    {0x00,0x00,0x3e,0x49,0x49,0x32,0x00,0x00}, // 9
    {0x3e,0x41,0x41,0x3e,0x00,0x01,0x7f,0x21}, // 10 :
    {0x3e,0x41,0x41,0x3e,0x00,0x01,0x7f,0x21}, // ; 10
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // <
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // =
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // >
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // ?
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // @
    {0x00,0x00,0x3f,0x48,0x48,0x3f,0x00,0x00}, // A
    {0x00,0x00,0x36,0x49,0x49,0x7f,0x00,0x00}, // B
    {0x00,0x00,0x26,0x41,0x41,0x3e,0x00,0x00}, // C
    {0x00,0x00,0x3e,0x41,0x41,0x7f,0x00,0x00}, // D
    {0x00,0x00,0x49,0x49,0x49,0x7f,0x00,0x00}, // E
    {0x00,0x00,0x48,0x48,0x48,0x7f,0x00,0x00}, // F  38
    {0x00,0x2e,0x49,0x49,0x41,0x3e,0x00,0x00}, // G  39
    {0x00,0x00,0x7f,0x08,0x08,0x7f,0x00,0x00}, // H
    {0x00,0x00,0x41,0x7f,0x41,0x00,0x00,0x00}, // I
    {0x00,0x00,0x40,0x7e,0x41,0x06,0x00,0x00}, // J
    {0x00,0x00,0x63,0x14,0x08,0x7f,0x00,0x00}, // K
    {0x00,0x00,0x01,0x01,0x01,0x7f,0x00,0x00}, // L
    {0x00,0x7f,0x20,0x18,0x20,0x7f,0x00,0x00}, // M
    {0x00,0x7f,0x06,0x18,0x20,0x7f,0x00,0x00}, // N
    {0x00,0x00,0x3e,0x41,0x41,0x3e,0x00,0x00}, // O
    {0x00,0x00,0x30,0x48,0x48,0x7f,0x00,0x00}, // P
    {0x00,0x01,0x3e,0x45,0x41,0x3e,0x00,0x00}, // Q
    {0x00,0x00,0x33,0x4c,0x48,0x7f,0x00,0x00}, // R
    {0x00,0x00,0x26,0x49,0x49,0x32,0x00,0x00}, // S
    {0x00,0x40,0x40,0x7f,0x40,0x40,0x00,0x00}, // T  52
    {0x00,0x00,0x7e,0x01,0x01,0x7e,0x00,0x00}, // U  53
    {0x00,0x78,0x06,0x01,0x06,0x78,0x00,0x00}, // V  54
    {0x00,0x7e,0x01,0x06,0x01,0x7e,0x00,0x00}, // W  55
    {0x00,0x63,0x14,0x08,0x14,0x63,0x00,0x00}, // X  56
    {0x00,0x60,0x10,0x0f,0x10,0x60,0x00,0x00}, // Y  57
    {0x00,0x61,0x51,0x49,0x45,0x43,0x00,0x00}, // Z  58
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // [  59
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // \  60
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // ]  61
    {0x22,0x36,0x3e,0xfc,0xfc,0x3e,0x36,0x22}, // ^  62 0x44,0x6c,0x7c,0x3f,0x3f,0x7c,0x6c,0x44
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // _  63
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}, // `  64
    {0x00,0x00,0x3f,0x48,0x48,0x3f,0x00,0x00}, // A
    {0x00,0x00,0x36,0x49,0x49,0x7f,0x00,0x00}, // B
    {0x00,0x00,0x26,0x41,0x41,0x3e,0x00,0x00}, // C
    {0x00,0x00,0x3e,0x41,0x41,0x7f,0x00,0x00}, // D
    {0x00,0x00,0x49,0x49,0x49,0x7f,0x00,0x00}, // E
    {0x00,0x00,0x48,0x48,0x48,0x7f,0x00,0x00}, // F  38
    {0x00,0x2e,0x49,0x49,0x41,0x3e,0x00,0x00}, // G  39
    {0x00,0x00,0x7f,0x08,0x08,0x7f,0x00,0x00}, // H
    {0x00,0x00,0x41,0x7f,0x41,0x00,0x00,0x00}, // I
    {0x00,0x00,0x40,0x7e,0x41,0x06,0x00,0x00}, // J
    {0x00,0x00,0x63,0x14,0x08,0x7f,0x00,0x00}, // K
    {0x00,0x00,0x01,0x01,0x01,0x7f,0x00,0x00}, // L
    {0x00,0x7f,0x20,0x18,0x20,0x7f,0x00,0x00}, // M
    {0x00,0x7f,0x06,0x18,0x20,0x7f,0x00,0x00}, // N
    {0x00,0x00,0x3e,0x41,0x41,0x3e,0x00,0x00}, // O
    {0x00,0x00,0x30,0x48,0x48,0x7f,0x00,0x00}, // P
    {0x00,0x01,0x3e,0x45,0x41,0x3e,0x00,0x00}, // Q
    {0x00,0x00,0x33,0x4c,0x48,0x7f,0x00,0x00}, // R
    {0x00,0x00,0x26,0x49,0x49,0x32,0x00,0x00}, // S
    {0x00,0x40,0x40,0x7f,0x40,0x40,0x00,0x00}, // T  52
    {0x00,0x00,0x7e,0x01,0x01,0x7e,0x00,0x00}, // U  53
    {0x00,0x78,0x06,0x01,0x06,0x78,0x00,0x00}, // V  54
    {0x00,0x7e,0x01,0x06,0x01,0x7e,0x00,0x00}, // W  55
    {0x00,0x63,0x14,0x08,0x14,0x63,0x00,0x00}, // X  56
    {0x00,0x60,0x10,0x0f,0x10,0x60,0x00,0x00}, // Y  57
    {0x00,0x61,0x51,0x49,0x45,0x43,0x00,0x00}, // Z  58
}; // z  90        {0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff}
void font_getchar(int chr,unsigned char* dst) {
    chr -= 32; // our bitmap font starts at ascii char 32    
    /* 
      if (CUBE_SIZE==4)
        {
        for (i=0; i<CUBE_SIZE; i++)
        {
          dst[i]=font4eng[chr][i];
        }
      }
     */
    if (CUBE_SIZE==8) {
         for(int i=0; i<CUBE_SIZE; i++) {
            *(dst + i) =  (unsigned char)font8eng[chr][i];
        }
    }
}

void effect_stringfly(control_led_cube_t *control_led_cube, uint8_t enordis, char *str) {
    if(enordis == ENABLE)
    {
        int  y, z, i;
        unsigned char chr[100];
        fill_cube(control_led_cube, 0x00);

        while (*str) {
            font_getchar((*str++), chr);

            for (z=(CUBE_SIZE - 1); z>=0; z--) {
                for (y=(CUBE_SIZE - 1); y>=0; y--) {
                    if (chr[z] >> y & 0x01) {
                        setvoxel(control_led_cube, y, z, 7);
                        
                    }
                }
            }
            // Shift the entire contents of the cube forward by 6 steps
            // before placing the next character
            // for (i= 0; i<CUBE_SIZE-1; i++) {
            //     delay_ms(50); 
            //     shift(3, 1);
            //     change_rgb_color(control_led_cube);
                
            // }

            for (i= CUBE_SIZE-1; i>0; i--) {
                delay_ms(70); 
                 shift(control_led_cube, 3, 0);
                change_rgb_color_2(control_led_cube);
                
            }
            delay_ms(500);    
        }
        return;
    }
}

/*--------------------------Hiệu ứng String Path Move--------------------------------*/
void effect_pathmove(control_led_cube_t *control_led_cube, unsigned char *path, int length) {
    int i, z;
    unsigned char state;

    for (i=(length - 1); i>=1; i--) {
        for (z=0; z<CUBE_SIZE; z++) {
            state=getvoxel(control_led_cube, z, CUBE_SIZE - 1 - (path[(i - 1)] >> 4 & 0x0f), CUBE_SIZE - 1 - ((path[(i - 1)])&0x0f));
             altervoxel(control_led_cube, z, CUBE_SIZE - 1 - (path[i] >> 4 & 0x0f), CUBE_SIZE - 1 - ((path[i])&0x0f), state);
        }
    }
    for (z=0; z<CUBE_SIZE; z++)
        clrvoxel(control_led_cube, z, CUBE_SIZE - 1 - (path[0] >> 4 & 0x0f), CUBE_SIZE - 1 - ((path[0])&0x0f));
}

unsigned char paths_8[44]={// circle, len 16, offset 28
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00, 0x10, 0x20,
    0x30, 0x40, 0x50, 0x60, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75,
    0x76, 0x77, 0x67, 0x57, 0x47, 0x37, 0x27, 0x17, 0x04, 0x03,
    0x12, 0x21, 0x30, 0x40, 0x51, 0x62, 0x73, 0x74, 0x65, 0x56,
    0x47, 0x37, 0x26, 0x15
};

void font_getpath(unsigned char path, unsigned char *destination, int length) 
{
    int i;
    int offset=0;

    if (path==1)
        offset=28;

    for (i=0; i<length; i++)
        destination[i]=paths_8[i + offset];
}

void font_getchar2(int chr,unsigned char* dst) {
    chr -= 32; // our bitmap font starts at ascii char 32              
    /*
      if (CUBE_SIZE==4)
        {
        for (i=0; i<CUBE_SIZE; i++)
        {
          dst[i]=font4eng[chr][i];
        }
      }
     */
    if (CUBE_SIZE==8) {
         for(int i=0; i<CUBE_SIZE; i++) {
            *(dst + i) =  (unsigned char)font8eng[chr][7-i];
        }
    }
}

void effect_path_text(control_led_cube_t *control_led_cube, uint8_t enordis, int delay, char *str) 
{
    if(enordis == ENABLE)
    {
        int z, i, ii;
        unsigned char path[28]; 
        unsigned char chr[CUBE_SIZE];
        unsigned char stripe;
        fill_cube(control_led_cube, 0x00);
        font_getpath(0, path, 28);

        while (*str) {
            font_getchar2(*str++, chr);
            for (ii=0; ii<CUBE_SIZE; ii++) // ii<5
            {
                stripe=chr[ii];
                for (z=0; z<CUBE_SIZE; z++) {
                    if ((stripe >> z) & 0x01) {
                        setvoxel(control_led_cube, z, (CUBE_SIZE - 1), 0);
                    } else {
                        clrvoxel(control_led_cube, z, (CUBE_SIZE - 1), 0);
                    }
                }
                effect_pathmove(control_led_cube, path, 28);
                delay_ms(delay);
                change_rgb_color(control_led_cube);        
            }
            effect_pathmove(control_led_cube, path, 28);
            delay_ms(delay);              
        }
        for (i=0; i<28; i++) {
            effect_pathmove(control_led_cube, path, 28);
            delay_ms(delay);
        }
        return;
    }
}

/*.................... Hiệu ứng Ball Bouncing ..........................*/

float distance2d (float x1, float y1, float x2, float y2)
{	
	float dist;
	dist = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	return dist;
}

float distance3d (float x1, float y1, float z1, float x2, float y2, float z2)
{	
	float dist;
	dist = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2));
	return dist;
}

void effect_ball_bouncing(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay)
{
	if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        fill_cube(control_led_cube, 0x00);
        float origin_x, origin_y, origin_z, distance, diameter;
        origin_x = 0;
        origin_y = 3.5;
        origin_z = 3.5;
        diameter = 3;
        uint32_t x, y, z, i;

        for (i=0; i<2000000; i++)
        {
            origin_x = 3.5+sin((float)i/50)*2.5;
            origin_y = 3.5+cos((float)i/50)*2.5;
            origin_z = 3.5+cos((float)i/30)*2;
            //diameter = 2+sin((float)i/150);
            diameter = 1;

            for (x=0; x<8; x++)
            {
                for (y=0; y<8; y++)
                {
                    for (z=0; z<8; z++)
                    {
                        distance = distance3d(x,y,z, origin_x, origin_y, origin_z);
                        //printf("Distance: %f \n", distance);
                        if (distance>diameter && distance<diameter+1)
                        {
                            setvoxel(control_led_cube, x,y,z);
                        }
                    }
                }
            }
            if(i%6 == 0)
                change_rgb_color(control_led_cube);
            delay_ms(delay);
            fill_cube(control_led_cube, 0x00);

            if(sec() - control_led_cube->time_start >= timeRun)
                return;
            if(control_led_cube->restart_value == 1)
            {
                control_led_cube->restart_value = 0; fill_cube(control_led_cube, 0x00);delay_ms(1000);
                return;
            }
        }
    }
}

/*----------------------------------Hiệu ứng SPIN-----------------------------*/
volatile const unsigned char bitmaps[14][8]={
    {0x7e,0xff,0x81,0x18,0x18,0x00,0xc3,0xc3}, // smiley 3 small
    {0x3c, 0x42, 0x81, 0x81, 0xc3, 0x24, 0xa5, 0xe7}, // Omega
    {0x00, 0x04, 0x06, 0xff, 0xff, 0x06, 0x04, 0x00}, // Arrow
    {0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81}, // X
    {0xBD, 0xA1, 0xA1, 0xB9, 0xA1, 0xA1, 0xA1, 0x00}, // ifi
    {0xEF, 0x48, 0x4B, 0x49, 0x4F, 0x00, 0x00, 0x00}, // TG
    {0x38, 0x7f, 0xE6, 0xC0, 0xE6, 0x7f, 0x38, 0x00}, // Commodore symbol
    {0x08, 0x1c, 0x3e, 0x3e, 0x7f , 0x77 ,0x22, 0x00}, // Heart {, , , , , , , },0x08, 0x1c, 0x3e, 0x3e, 0x7f , 0x77 ,0x22, 0x00
    {0x1C, 0x22, 0x55, 0x49, 0x5d, 0x22, 0x1c, 0x00}, // face
    {0x37, 0x42, 0x22, 0x12, 0x62, 0x00, 0x7f, 0x00}, // ST
    {0x89, 0x4A, 0x2c, 0xF8, 0x1F, 0x34, 0x52, 0x91}, // STAR
    {0x18, 0x3c, 0x7e, 0xdb, 0xff, 0x24, 0x5a, 0xa5}, // Space Invader
    {0x00, 0x9c, 0xa2, 0xc5, 0xc1, 0xa2, 0x9c, 0x00},// Fish
    {0x18,0x0c,0x0e,0x0e,0x0e,0x1e,0xfc,0x78} };      //moon
    
unsigned char font_getbitmappixel(int bitmap, int x, int y) {
    char tmp=bitmaps[bitmap][x];
    return (tmp >> y) & 0x01;
}

void effect_smileyspin (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay, char bitmap)
{
    if(enordis == ENABLE)
    {
        control_led_cube->time_start = sec();
        unsigned char dybde[] = {0,1,2,3,4,5,6,7,1,1,2,3,4,5,6,6,2,2,3,3,4,4,5,5,3,3,3,3,4,4,4,4};
        int d = 0;
        int flip = 0;
        int x, y, off;
        while(1)
        {
            flip = 0;
            d = 0;
            off = 0;
            // front:
            for (int s=0;s<7;s++){
                if(!flip){
                    off++;
                    if (off == 4){
                        flip = 1;
                        off = 0;
                    }
                } else {
                    off++;
                }
                    for (x=0; x<8; x++)
                    {
                    d = 0;
                            for (y=0; y<8; y++)
                            {
                        if (font_getbitmappixel ( bitmap, 7-x, y)){
                            if (!flip)
                                setvoxel(control_led_cube, x,y,dybde[8 * off + d++]);
                            else
                                setvoxel(control_led_cube, x,y,dybde[31 - 8 * off - d++]);
                        } else {
                            d++;
                        }
                    }
                }
                delay_ms(delay);
                fill_cube(control_led_cube, 0x00);
            }

            // side:
            off = 0;
            flip = 0;
            d = 0;
            for (int s=0;s<7;s++){
                if(!flip){
                    off++;
                    if (off == 4){
                        flip = 1;
                        off = 0;
                    }
                } else {
                    off++;
                }
                    for (x=0; x<8; x++)
                    {
                    d = 0;
                            for (y=0; y<8; y++)
                            {
                        if (font_getbitmappixel ( bitmap, 7-x, y)){
                            if (!flip)
                                setvoxel(control_led_cube, x, dybde[8 * off + d++], 7- y);
                            else
                                setvoxel(control_led_cube, x,dybde[31 - 8 * off - d++], 7- y);
                        } else {
                            d++;
                        }
                    }
                }
                delay_ms(delay);
                fill_cube(control_led_cube, 0x00);
            }


            flip = 0;
            d = 0;
            off = 0;
            // back:
            for (int s=0;s<7;s++){
                if(!flip){
                    off++;
                    if (off == 4){
                        flip = 1;
                        off = 0;
                    }
                } else {
                    off++;
                }
                    for (x=0; x<8; x++)
                    {
                    d = 0;
                            for (y=0; y<8; y++)
                            {
                        if (font_getbitmappixel ( bitmap, 7-x, 7-y)){
                            if (!flip)
                                setvoxel(control_led_cube, x,y,dybde[8 * off + d++]);
                            else
                                setvoxel(control_led_cube, x,y,dybde[31 - 8 * off - d++]);
                        } else {
                            d++;
                        }
                    }
                }
                delay_ms(delay);
                fill_cube(control_led_cube, 0x00);
            }

            // other side:
            off = 0;
            flip = 0;
            d = 0;
            for (int s=0;s<7;s++){
                if(!flip){
                    off++;
                    if (off == 4){
                        flip = 1;
                        off = 0;
                    }
                } else {
                    off++;
                }
                    for (x=0; x<8; x++)
                    {
                    d = 0;
                            for (y=0; y<8; y++)
                            {
                        if (font_getbitmappixel ( bitmap, 7-x, 7-y)){
                            if (!flip)
                                setvoxel(control_led_cube, x, dybde[8 * off + d++], 7-y);
                            else
                                setvoxel(control_led_cube, x,dybde[31 - 8 * off - d++], 7-y);
                        } else {
                            d++;
                        }
                    }
                }
                delay_ms(delay);
                fill_cube(control_led_cube, 0x00);
            }
            if(sec() - control_led_cube->time_start >= timeRun)
                return;
        }
    }
	
}

