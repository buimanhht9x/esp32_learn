#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include <led_strip_idf.h>
#include "math.h"
#include "esp_log.h"
#include "wifi_ap.h"
#include "nvs_flash.h"
#include "esp_http_server.h"
#include "driver/i2s.h"
#include "control_led.h"
#include "effect_led.h"
#include "driver/timer.h"
#include "esp_adc_cal.h"
#include "driver/adc.h"
#include "fft.h"
#include <esp_task_wdt.h> 
#include <soc/rtc_wdt.h> 
#include "soc/timer_group_struct.h"
#include "soc/timer_group_reg.h"

// Khai báo file html
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");


void flash_write_once(void);
TaskHandle_t cube_run_handle = NULL;
TaskHandle_t data_eff_handle = NULL;
TaskHandle_t timer_interrupt_handle = NULL;
TaskHandle_t readADCTaskHandle=NULL;
SemaphoreHandle_t semaphorehandle_adc_read = NULL;


/*.................................Khai báo LED STRIP.............................*/ 
#define LED_TYPE LED_STRIP_WS2812
#define LED_LENGTH 512
#define LED_GPIO 5
#define CUBE_SIZE 8
control_led_cube_t  control_cube;
void init_led_cube()
{
    // cấu hình strip
    led_strip_t strip = {   
                            .type = LED_TYPE,
                            .length = LED_LENGTH,
                            .gpio = LED_GPIO,
                            .buf = NULL,
                            .brightness = 50,
                        };
    control_cube.strip = strip;
    change_rgb_color(&control_cube);
}

/*........................................Timer.......................................*/

#define TIMER_DIVIDER         (16)  //  Hardware timer clock divider
#define TIMER_SCALE           1000 // convert counter value to seconds

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
    bool auto_reload;
} example_timer_info_t;

/**
 * @brief A sample structure to pass events from the timer ISR to task
 *
 */
typedef struct {
    example_timer_info_t info;
    uint64_t timer_counter_value;
} example_timer_event_t;

static xQueueHandle s_timer_queue;

/*
 * A simple helper function to print the raw timer counter value
 * and the counter value converted to seconds
 */

static void inline print_timer_counter(uint64_t counter_value)
{
    printf("Counter: 0x%08x%08x\r\n", (uint32_t) (counter_value >> 32),
           (uint32_t) (counter_value));
    printf("Time   : %.8f s\r\n", (double) counter_value / TIMER_SCALE);
}

static bool IRAM_ATTR timer_group_isr_callback(void *args)
{
    BaseType_t high_task_awoken = pdFALSE;
    example_timer_info_t *info = (example_timer_info_t *) args;

    uint64_t timer_counter_value = timer_group_get_counter_value_in_isr(info->timer_group, info->timer_idx);

    /* Prepare basic event data that will be then sent back to task */
    example_timer_event_t evt = {
        .info.timer_group = info->timer_group,
        .info.timer_idx = info->timer_idx,
        .info.auto_reload = info->auto_reload,
        .info.alarm_interval = info->alarm_interval,
        .timer_counter_value = timer_counter_value
    };
    if (!info->auto_reload) {
        timer_counter_value += info->alarm_interval * TIMER_SCALE;
        timer_group_set_alarm_value_in_isr(info->timer_group, info->timer_idx, timer_counter_value);
    }

    /* Now just send the event data back to the main program task */
    xQueueSendFromISR(s_timer_queue, &evt, &high_task_awoken);
    return high_task_awoken == pdTRUE; // return whether we need to yield at the end of ISR
}

/**
 * @brief Initialize selected timer of timer group
 *
 * @param group Timer Group number, index from 0
 * @param timer timer ID, index from 0
 * @param auto_reload whether auto-reload on alarm event
 * @param timer_interval_sec interval of alarm
 */
static void example_tg_timer_init(int group, int timer, bool auto_reload, int timer_interval_sec)
{
    /* Select and initialize basic parameters of the timer */
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = auto_reload,
    }; // default clock source is APB
    timer_init(group, timer, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(group, timer, 0);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(group, timer, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(group, timer);

    example_timer_info_t *timer_info = calloc(1, sizeof(example_timer_info_t));
    timer_info->timer_group = group;
    timer_info->timer_idx = timer;
    timer_info->auto_reload = auto_reload;
    timer_info->alarm_interval = timer_interval_sec;
    timer_isr_callback_add(group, timer, timer_group_isr_callback, timer_info, 0);

    timer_start(group, timer);
}

void timer_interrupt()
{
    s_timer_queue = xQueueCreate(10, sizeof(example_timer_event_t));

    example_tg_timer_init(TIMER_GROUP_0, TIMER_0, true, 3);

    while (1) {
        example_timer_event_t evt;
        xQueueReceive(s_timer_queue, &evt, portMAX_DELAY);
        show_led(&control_cube);
    }
}

/*.................... Khai báo data config effect led ...........................*/
effect_config_t rain_config;
effect_config_t rainbow_config;
effect_config_t planboing_config;
effect_config_t sendvoxelsrandz_config;
effect_config_t sendvoxelsrandy_config;
effect_config_t boxshrinkgrow_config;
effect_config_t boxwoopwoop_config;
effect_config_t axiszupdownrs_config;
effect_config_t axisyupdownrs_config;
effect_config_t sidewaves_config;
effect_config_t wormsqueeze_config;
effect_config_t boingboing1_config;
effect_config_t boingboing2_config;
effect_config_t boingboing3_config;
effect_config_t randomfiller_config;
effect_config_t ripples_config;
effect_config_t cubix_config;
effect_config_t filip_filop_config;
effect_config_t sinelines_config;
effect_config_t stringfly_config;
effect_config_t fireworks_config;
effect_config_t path_text_config;
effect_config_t ball_bouncing_config;
uint8_t brightness_value = 50;
char *stringbuffer;

/*....................... Hàm chạy hiệu ứng LED Cube ...........................*/
void cube_run(void *pvParameters)
{
    // Khởi tạo strip
    ESP_ERROR_CHECK(led_strip_init(&control_cube.strip));
    set_brightness(&control_cube, brightness_value);
    ESP_ERROR_CHECK(led_strip_flush(&control_cube.strip));
    fill_cube(&control_cube, 0x00);
    show_led(&control_cube);
    while (1)
    {
        effect_rain(&control_cube,rain_config.state, rain_config.time_run,50,0);
        effect_rainbow(&control_cube,rainbow_config.state, rainbow_config.time_run,15);
        effect_planboing(&control_cube,planboing_config.state, planboing_config.time_run);
        effect_sendvoxels_rand_axis(&control_cube,sendvoxelsrandz_config.state, sendvoxelsrandz_config.time_run, AXIS_Z, 10,500);
        effect_sendvoxels_rand_axis(&control_cube,sendvoxelsrandy_config.state, sendvoxelsrandy_config.time_run, AXIS_Y, 10,500);
        effect_box_shrink_grow(&control_cube,boxshrinkgrow_config.state, boxshrinkgrow_config.time_run,50);
        effect_box_woopwoop(&control_cube,boxwoopwoop_config.state,boxwoopwoop_config.time_run);
        effect_axis_z_updown_randsuspend(&control_cube,axiszupdownrs_config.state,axiszupdownrs_config.time_run);
        effect_axis_y_updown_randsuspend(&control_cube,axisyupdownrs_config.state,axisyupdownrs_config.time_run);
        effect_int_sidewaves(&control_cube,sidewaves_config.state,sidewaves_config.time_run,10);
        effect_wormsqueeze(&control_cube,wormsqueeze_config.state,(wormsqueeze_config.time_run)/2,50,2,0);
        effect_wormsqueeze(&control_cube,wormsqueeze_config.state,(wormsqueeze_config.time_run)/2,50,3,1);
        effect_random_filler(&control_cube,randomfiller_config.state,randomfiller_config.time_run,20);
        effect_boingboing(&control_cube,boingboing1_config.state,boingboing1_config.time_run,40, 0x01,0x01);
        effect_boingboing(&control_cube,boingboing2_config.state,boingboing2_config.time_run,40, 0x01,0x03);
        effect_boingboing(&control_cube,boingboing3_config.state,boingboing3_config.time_run,40, 0x01,0x02);
        effect_int_ripples(&control_cube,ripples_config.state,ripples_config.time_run,10); // ok
        effect_cubix(&control_cube,cubix_config.state,cubix_config.time_run,3);   //ok
        effect_filip_filop(&control_cube,filip_filop_config.state,filip_filop_config.time_run,50); // ok
        effect_sinelines(&control_cube,sinelines_config.state,sinelines_config.time_run,10);
        effect_stringfly(&control_cube,stringfly_config.state,stringfly_config.stringbuff);
        effect_fireworks(&control_cube,fireworks_config.state,fireworks_config.time_run,50);
        effect_path_text(&control_cube,path_text_config.state, 100,path_text_config.stringbuff);
        effect_ball_bouncing(&control_cube,ball_bouncing_config.state,ball_bouncing_config.time_run,10); 
    }
}

// ---------------------------Hiệu ứng test ---------------------------------
void effect_test()
{
    effect_rainbow(&control_cube,1,20,15);
    effect_rain(&control_cube, 1,20,50,0);
    eff_side_change_color(&control_cube,1,5);
    effect_planboing(&control_cube,1,5 ); 
    effect_sendvoxels_rand_axis(&control_cube, 1,5,AXIS_Z, 10,500);
    effect_sendvoxels_rand_axis(&control_cube, 1,5,AXIS_Y, 10,500);
    effect_box_shrink_grow(&control_cube,1,5,50);
    effect_box_woopwoop(&control_cube,1,10);
    effect_axis_z_updown_randsuspend(&control_cube, 1,5);
    effect_axis_y_updown_randsuspend(&control_cube, 1,5);
    effect_int_sidewaves(&control_cube,1,5,10);
    effect_int_ripples(&control_cube,1,5,10);
    effect_wormsqueeze(&control_cube,1 , 5,50,2,0);
    effect_wormsqueeze(&control_cube,1 , 5,50,3,1);
    effect_cubix(&control_cube,1 , 5, 3);
    effect_boingboing(&control_cube,1,5,40, 0x01,0x01) ;
    effect_boingboing(&control_cube,1,5,40, 0x01,0x03) ;
    effect_boingboing(&control_cube,1,5,40, 0x01,0x02) ;
    effect_random_filler(&control_cube,1,10,20);
    effect_filip_filop(&control_cube,1,10,50);
    effect_sinelines(&control_cube,1,10,10);
    effect_linespin(&control_cube,1,10,10);
    for(int i=0; i<8; ++i)
        effect_boxside_randsend_parallel(&control_cube,3, i%2, 1, 1);
    for(int i=0; i<8; ++i)
        effect_boxside_randsend_parallel(&control_cube,2, i%2, 1, 2);
    effect_stringfly(&control_cube,1,";9876543210^");
    effect_fireworks(&control_cube,1,10,50);
    effect_stringfly(&control_cube,1,"banlinhkien.com");
    effect_path_text(&control_cube,1,100,"banlinhkien.com");
    effect_ball_bouncing(&control_cube,1,10,10);
    effect_int_sidewaves(&control_cube,1,5,10); 
    effect_linespin(&control_cube,1,20,10);
    for(int i=0; i<14; ++i)
        effect_smileyspin(&control_cube,1,3,100,i);   
}
//----------------Hàm test LED CUBE chạy đuổi ------------------------
void testLedCUBE()
{
    for(int i = 0; i< 512; i ++)
    {
        led_strip_set_pixel(&control_cube.strip, i,  control_cube.rgb_setvoxel);
        led_strip_flush(&control_cube.strip);
        delay_ms(100);
    }
}
// ------------------------- Hàm tách data ----------------------------------------
void get_data_config_effect(char * input_string)
{
    char *token1, *token2;
    char *rest = input_string;
    char data_eff[100][20];
    // Mảng để lưu các chuỗi con sau khi tách bằng kí tự '/'
    char substrings[20][100]; // Điều này tùy thuộc vào độ dài tối đa của chuỗi con
    int i = 0;
    int j = 0;
    while ((token1 = strtok_r(rest, "/", &rest))) {
        // Lưu chuỗi con vào mảng substrings
        strcpy(substrings[i], token1);
        // Tách chuỗi con thành các phần bằng kí tự ':'
        token2 = strtok(substrings[i], ":");
        // In ra các phần bằng kí tự ':'
        while (token2 != NULL) {
            strcpy(data_eff[j], token2);
            j++;
         //   printf("%s\n", token2);
            token2 = strtok(NULL, ":");
        }
        i++;
    }
    rain_config.state = atoi(data_eff[1]);              rain_config.time_run = atoi(data_eff[2]);
    rainbow_config.state = atoi(data_eff[4]);           rainbow_config.time_run = atoi(data_eff[5]);
    planboing_config.state = atoi(data_eff[7]);         planboing_config.time_run = atoi(data_eff[8]);
    sendvoxelsrandz_config.state = atoi(data_eff[10]);  sendvoxelsrandz_config.time_run = atoi(data_eff[11]);
    sendvoxelsrandy_config.state = atoi(data_eff[13]);  sendvoxelsrandy_config.time_run = atoi(data_eff[14]);
    boxshrinkgrow_config.state = atoi(data_eff[16]);    boxshrinkgrow_config.time_run = atoi(data_eff[17]);
    boxwoopwoop_config.state = atoi(data_eff[19]);      boxwoopwoop_config.time_run = atoi(data_eff[20]);
    axiszupdownrs_config.state = atoi(data_eff[22]);    axiszupdownrs_config.time_run = atoi(data_eff[23]);
    axisyupdownrs_config.state = atoi(data_eff[25]);    axisyupdownrs_config.time_run = atoi(data_eff[26]);
    sidewaves_config.state = atoi(data_eff[28]);        sidewaves_config.time_run = atoi(data_eff[29]);
    wormsqueeze_config.state = atoi(data_eff[31]);      wormsqueeze_config.time_run = atoi(data_eff[32]);
    boingboing1_config.state = atoi(data_eff[34]);      boingboing1_config.time_run = atoi(data_eff[35]);
    boingboing2_config.state = atoi(data_eff[37]);      boingboing2_config.time_run = atoi(data_eff[38]);
    boingboing3_config.state = atoi(data_eff[40]);      boingboing3_config.time_run = atoi(data_eff[41]);
    randomfiller_config.state = atoi(data_eff[43]);     randomfiller_config.time_run = atoi(data_eff[44]);
    ripples_config.state = atoi(data_eff[46]);          ripples_config.time_run = atoi(data_eff[47]);
    cubix_config.state = atoi(data_eff[49]);            cubix_config.time_run = atoi(data_eff[50]);
    filip_filop_config.state = atoi(data_eff[52]);      filip_filop_config.time_run = atoi(data_eff[53]);
    sinelines_config.state = atoi(data_eff[55]);        sinelines_config.time_run = atoi(data_eff[56]);
    stringfly_config.state = atoi(data_eff[58]);        stringfly_config.stringbuff = data_eff[59];
    fireworks_config.state = atoi(data_eff[61]);        fireworks_config.time_run = atoi(data_eff[62]);
    path_text_config.state = atoi(data_eff[64]);        path_text_config.stringbuff = data_eff[65];
    ball_bouncing_config.state = atoi(data_eff[67]);    ball_bouncing_config.time_run = atoi(data_eff[68]);
    brightness_value = atoi(data_eff[71]);

    printf("%d\n",brightness_value);
    set_brightness(&control_cube, brightness_value);

    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
        // key : value
        nvs_set_u16(nvs_handle,"1",rain_config.state);              nvs_set_u16(nvs_handle,"2",rain_config.time_run );
        nvs_set_u16(nvs_handle,"3",rainbow_config.state);           nvs_set_u16(nvs_handle,"4",rainbow_config.time_run );
        nvs_set_u16(nvs_handle,"5",planboing_config.state);         nvs_set_u16(nvs_handle,"6",planboing_config.time_run );
        nvs_set_u16(nvs_handle,"7",sendvoxelsrandz_config.state);   nvs_set_u16(nvs_handle,"8",sendvoxelsrandz_config.time_run );
        nvs_set_u16(nvs_handle,"9",sendvoxelsrandy_config.state);   nvs_set_u16(nvs_handle,"10",sendvoxelsrandy_config.time_run );
        nvs_set_u16(nvs_handle,"11",boxshrinkgrow_config.state);    nvs_set_u16(nvs_handle,"12",boxshrinkgrow_config.time_run );
        nvs_set_u16(nvs_handle,"13",boxwoopwoop_config.state);      nvs_set_u16(nvs_handle,"14",boxwoopwoop_config.time_run );
        nvs_set_u16(nvs_handle,"15",axiszupdownrs_config.state);    nvs_set_u16(nvs_handle,"16",axiszupdownrs_config.time_run );
        nvs_set_u16(nvs_handle,"17",axisyupdownrs_config.state);    nvs_set_u16(nvs_handle,"18",axisyupdownrs_config.time_run );
        nvs_set_u16(nvs_handle,"19",sidewaves_config.state);        nvs_set_u16(nvs_handle,"20",sidewaves_config.time_run );
        nvs_set_u16(nvs_handle,"21",wormsqueeze_config.state);      nvs_set_u16(nvs_handle,"22",wormsqueeze_config.time_run );
        nvs_set_u16(nvs_handle,"23",boingboing1_config.state);      nvs_set_u16(nvs_handle,"24",boingboing1_config.time_run );
        nvs_set_u16(nvs_handle,"25",boingboing2_config.state);      nvs_set_u16(nvs_handle,"26",boingboing2_config.time_run );
        nvs_set_u16(nvs_handle,"27",boingboing3_config.state);      nvs_set_u16(nvs_handle,"28",boingboing3_config.time_run );
        nvs_set_u16(nvs_handle,"29",randomfiller_config.state);     nvs_set_u16(nvs_handle,"30",randomfiller_config.time_run );
        nvs_set_u16(nvs_handle,"31",ripples_config.state);          nvs_set_u16(nvs_handle,"32",ripples_config.time_run );
        nvs_set_u16(nvs_handle,"33",cubix_config.state);            nvs_set_u16(nvs_handle,"34",cubix_config.time_run );
        nvs_set_u16(nvs_handle,"35",filip_filop_config.state);      nvs_set_u16(nvs_handle,"36",filip_filop_config.time_run );
        nvs_set_u16(nvs_handle,"37",sinelines_config.state);        nvs_set_u16(nvs_handle,"38",sinelines_config.time_run );
        nvs_set_u16(nvs_handle,"39",stringfly_config.state);        nvs_set_str(nvs_handle,"40",stringfly_config.stringbuff );
        nvs_set_u16(nvs_handle,"41",fireworks_config.state);        nvs_set_u16(nvs_handle,"42",fireworks_config.time_run );
        nvs_set_u16(nvs_handle,"43",path_text_config.state);        nvs_set_str(nvs_handle,"44",path_text_config.stringbuff );
        nvs_set_u16(nvs_handle,"45",ball_bouncing_config.state);    nvs_set_u16(nvs_handle,"46",ball_bouncing_config.time_run ); 
        nvs_set_u16(nvs_handle,"47",brightness_value);

        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}

// ------------------------- Hàm đọc data config effect led từ NVS----------------------------------------
void read_data_config_from_flash()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        flash_write_once();
    } else {
        printf("Done\n"); 
        size_t required_size;
        nvs_get_u16(nvs_handle,"1",&rain_config.state);             nvs_get_u16(nvs_handle,"2",&rain_config.time_run );
        nvs_get_u16(nvs_handle,"3",&rainbow_config.state);          nvs_get_u16(nvs_handle,"4",&rainbow_config.time_run );
        nvs_get_u16(nvs_handle,"5",&planboing_config.state);        nvs_get_u16(nvs_handle,"6",&planboing_config.time_run );
        nvs_get_u16(nvs_handle,"7",&sendvoxelsrandz_config.state);  nvs_get_u16(nvs_handle,"8",&sendvoxelsrandz_config.time_run );
        nvs_get_u16(nvs_handle,"9",&sendvoxelsrandy_config.state);  nvs_get_u16(nvs_handle,"10",&sendvoxelsrandy_config.time_run );
        nvs_get_u16(nvs_handle,"11",&boxshrinkgrow_config.state);   nvs_get_u16(nvs_handle,"12",&boxshrinkgrow_config.time_run );
        nvs_get_u16(nvs_handle,"13",&boxwoopwoop_config.state);     nvs_get_u16(nvs_handle,"14",&boxwoopwoop_config.time_run );
        nvs_get_u16(nvs_handle,"15",&axiszupdownrs_config.state);   nvs_get_u16(nvs_handle,"16",&axiszupdownrs_config.time_run );
        nvs_get_u16(nvs_handle,"17",&axisyupdownrs_config.state);   nvs_get_u16(nvs_handle,"18",&axisyupdownrs_config.time_run );
        nvs_get_u16(nvs_handle,"19",&sidewaves_config.state);       nvs_get_u16(nvs_handle,"20",&sidewaves_config.time_run );
        nvs_get_u16(nvs_handle,"21",&wormsqueeze_config.state);     nvs_get_u16(nvs_handle,"22",&wormsqueeze_config.time_run );
        nvs_get_u16(nvs_handle,"23",&boingboing1_config.state);     nvs_get_u16(nvs_handle,"24",&boingboing1_config.time_run );
        nvs_get_u16(nvs_handle,"25",&boingboing2_config.state);     nvs_get_u16(nvs_handle,"26",&boingboing2_config.time_run );
        nvs_get_u16(nvs_handle,"27",&boingboing3_config.state);     nvs_get_u16(nvs_handle,"28",&boingboing3_config.time_run );
        nvs_get_u16(nvs_handle,"29",&randomfiller_config.state);    nvs_get_u16(nvs_handle,"30",&randomfiller_config.time_run );
        nvs_get_u16(nvs_handle,"31",&ripples_config.state);         nvs_get_u16(nvs_handle,"32",&ripples_config.time_run );
        nvs_get_u16(nvs_handle,"33",&cubix_config.state);           nvs_get_u16(nvs_handle,"34",&cubix_config.time_run );
        nvs_get_u16(nvs_handle,"35",&filip_filop_config.state);     nvs_get_u16(nvs_handle,"36",&filip_filop_config.time_run );
        nvs_get_u16(nvs_handle,"37",&sinelines_config.state);       nvs_get_u16(nvs_handle,"38",&sinelines_config.time_run );
        
        nvs_get_u16(nvs_handle,"39",&stringfly_config.state);
        nvs_get_str(nvs_handle,"40",NULL,&required_size);
        stringfly_config.stringbuff = malloc(required_size);
        nvs_get_str(nvs_handle,"40",stringfly_config.stringbuff,&required_size);

        nvs_get_u16(nvs_handle,"41",&fireworks_config.state);       nvs_get_u16(nvs_handle,"42",&fireworks_config.time_run );
        
        nvs_get_u16(nvs_handle,"43",&path_text_config.state);
        nvs_get_str(nvs_handle,"44",NULL,&required_size);
        path_text_config.stringbuff = malloc(required_size);
        nvs_get_str(nvs_handle,"44",path_text_config.stringbuff,&required_size);

        nvs_get_u16(nvs_handle,"45",&ball_bouncing_config.state);   nvs_get_u16(nvs_handle,"46",&ball_bouncing_config.time_run );

        nvs_get_u16(nvs_handle,"47",&brightness_value );
        set_brightness(&control_cube, brightness_value);
        // print_data_effect_config();
        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}

// ------------------------- Hàm in data config effect led ra Serial----------------------------------------
void print_data_effect_config()
{
    printf("rain_config.state : %d\n", rain_config.state);                          printf("rain_config.time_run : %d\n", rain_config.time_run);
    printf("rainbow_config.state : %d\n", rainbow_config.state);                    printf("rainbow_config.time_run : %d\n", rainbow_config.time_run);
    printf("planboing_config.state : %d\n", planboing_config.state);                printf("planboing_config.time_run : %d\n", planboing_config.time_run);
    printf("sendvoxelsrandz_config.state : %d\n", sendvoxelsrandz_config.state);    printf("sendvoxelsrandz_config.time_run : %d\n", sendvoxelsrandz_config.time_run);
    printf("sendvoxelsrandy_config.state : %d\n", sendvoxelsrandy_config.state);    printf("sendvoxelsrandy_config.time_run : %d\n", sendvoxelsrandy_config.time_run);
    printf("boxshrinkgrow_config.state) : %d\n", boxshrinkgrow_config.state);       printf("boxshrinkgrow_config.time_run : %d\n", boxshrinkgrow_config.time_run);
    printf("boxwoopwoop_config.state : %d\n", boxwoopwoop_config.state);            printf("boxwoopwoop_config.time_run : %d\n", boxwoopwoop_config.time_run);
    printf("axiszupdownrs_config.state : %d\n", axiszupdownrs_config.state);        printf("axiszupdownrs_config.time_run : %d\n", axiszupdownrs_config.time_run);
    printf("axisyupdownrs_config.state : %d\n", axisyupdownrs_config.state);        printf("axisyupdownrs_config.time_run : %d\n", axisyupdownrs_config.time_run);
    printf("sidewaves_config.state : %d\n", sidewaves_config.state);                printf("sidewaves_config.time_run : %d\n", sidewaves_config.time_run);
    printf("wormsqueeze_config.state : %d\n", wormsqueeze_config.state);            printf("wormsqueeze_config.time_run : %d\n", wormsqueeze_config.time_run);
    printf("boingboing1_config.state : %d\n", boingboing1_config.state);            printf("boingboing1_config.time_run : %d\n", boingboing1_config.time_run);
    printf("boingboing2_config.state : %d\n", boingboing2_config.state);            printf("boingboing2_config.time_run : %d\n", boingboing2_config.time_run);
    printf("boingboing3_config.state : %d\n", boingboing3_config.state);            printf("boingboing3_config.time_run : %d\n", boingboing3_config.time_run);
    printf("randomfiller_config.state : %d\n", randomfiller_config.state);          printf("randomfiller_config.time_run : %d\n", randomfiller_config.time_run);
    printf("ripples_config.state : %d\n", ripples_config.state);                    printf("ripples_config.time_run : %d\n", ripples_config.time_run);
    printf("cubix_config.state : %d\n", cubix_config.state);                        printf("cubix_config.time_run : %d\n", cubix_config.time_run);
    printf("filip_filop_config.state : %d\n", filip_filop_config.state);            printf("filip_filop_config.time_run : %d\n", filip_filop_config.time_run);
    printf("sinelines_config.state : %d\n", sinelines_config.state);                printf("sinelines_config.time_run : %d\n", sinelines_config.time_run);
    printf("stringfly_config.state : %d\n", stringfly_config.state);                printf(" stringfly_config.stringbuff : %s\n", stringfly_config.stringbuff);
    printf("fireworks_config.state : %d\n", fireworks_config.state);                printf("fireworks_config.time_run : %d\n", fireworks_config.time_run);
    printf("path_text_config.state : %d\n", path_text_config.state);                printf("path_text_config.stringbuff : %s\n", path_text_config.stringbuff);
    printf("ball_bouncing_config.state : %d\n", ball_bouncing_config.state);        printf("ball_bouncing_config.time_run : %d\n", ball_bouncing_config.time_run);
    printf("brightness_value : %d\n", brightness_value);
}

SemaphoreHandle_t semaphorehandle_data_eff_config = NULL;

//------------------------ Hàm nhận Data từ web ----------------------------
char buffer_data_eff_config[300];
esp_err_t data_eff_config_post_handler(httpd_req_t* req)
{
    httpd_req_recv(req, buffer_data_eff_config,req->content_len);
    printf("data_eff_config : %s\n",buffer_data_eff_config);
    xSemaphoreGiveFromISR(semaphorehandle_data_eff_config, NULL);
    return ESP_OK;
}
//--------------------------- Trang HTML ---------------------------------
esp_err_t html_get_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req,(char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
}

//------------------- Hàm gửi data config led đến web ---------------------
esp_err_t data_web_before_get_handler(httpd_req_t *r)
{    
    char resp_str[700];
    sprintf(resp_str,"{\"d1\":\"%d\",\"d2\":\"%d\",\"d3\":\"%d\",\"d4\":\"%d\",\"d5\":\"%d\",\"d6\":\"%d\",\"d7\":\"%d\",\"d8\":\"%d\",\"d9\":\"%d\",\"d10\":\"%d\",\"d11\":\"%d\",\"d12\":\"%d\",\"d13\":\"%d\",\"d14\":\"%d\",\"d15\":\"%d\",\"d16\":\"%d\",\"d17\":\"%d\",\"d18\":\"%d\",\"d19\":\"%d\",\"d20\":\"%d\",\"d21\":\"%d\",\"d22\":\"%d\",\"d23\":\"%d\",\"d24\":\"%d\",\"d25\":\"%d\",\"d26\":\"%d\",\"d27\":\"%d\",\"d28\":\"%d\",\"d29\":\"%d\",\"d30\":\"%d\",\"d31\":\"%d\",\"d32\":\"%d\",\"d33\":\"%d\",\"d34\":\"%d\",\"d35\":\"%d\",\"d36\":\"%d\",\"d37\":\"%d\",\"d38\":\"%d\",\"d39\":\"%d\",\"d40\":\"%s\",\"d41\":\"%d\",\"d42\":\"%d\",\"d43\":\"%d\",\"d44\":\"%s\",\"d45\":\"%d\",\"d46\":\"%d\",\"d99\":\"%d\"}",rain_config.state,rain_config.time_run,rainbow_config.state,rainbow_config.time_run,planboing_config.state,planboing_config.time_run,sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,boxwoopwoop_config.state,boxwoopwoop_config.time_run,axiszupdownrs_config.state,axiszupdownrs_config.time_run,axisyupdownrs_config.state,axisyupdownrs_config.time_run,sidewaves_config.state,sidewaves_config.time_run,wormsqueeze_config.state,wormsqueeze_config.time_run,boingboing1_config.state,boingboing1_config.time_run,boingboing2_config.state,boingboing2_config.time_run,boingboing3_config.state,boingboing3_config.time_run,randomfiller_config.state,randomfiller_config.time_run,ripples_config.state,ripples_config.time_run, cubix_config.state,cubix_config.time_run,filip_filop_config.state,filip_filop_config.time_run,sinelines_config.state,sinelines_config.time_run,stringfly_config.state,stringfly_config.stringbuff,fireworks_config.state,fireworks_config.time_run,path_text_config.state, path_text_config.stringbuff,ball_bouncing_config.state,ball_bouncing_config.time_run,brightness_value); 
    printf("%s\n",resp_str);
    // kiểu data gửi đi
    httpd_resp_set_type(r,HTTPD_TYPE_JSON);
    // gửi data
    httpd_resp_send(r,resp_str, strlen(resp_str));
    return ESP_OK;
}

//----------------Data từ Web gửi về ESP----------------------
httpd_uri_t data_eff_config_uri_post = {
    .handler = data_eff_config_post_handler,
    .method = HTTP_POST,
    .uri =  "/data_eff_config",
    .user_ctx = NULL
};
// ----------------------------Trang HTML----------------------------
httpd_uri_t html_uri_get = {
    .handler = html_get_handler,
    .method = HTTP_GET,
    .uri =  "/",
    .user_ctx = NULL
};
// -------------Gửi data từ ESP đến web trong lần đầu mở web---------
httpd_uri_t data_web_before_uri_get = {
    .handler = data_web_before_get_handler,
    .method = HTTP_GET,
    .uri =  "/data_web_before",
    .user_ctx = NULL
};


static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    // Start the httpd server
    ESP_LOGI("SERVER", "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Registering the ws handler
        ESP_LOGI("SERVER", "Registering URI handlers");
        httpd_register_uri_handler(server, &html_uri_get);
        httpd_register_uri_handler(server, &data_eff_config_uri_post);
        httpd_register_uri_handler(server, &data_web_before_uri_get);
        return server;
    }
    ESP_LOGI("SERVER", "Error starting server!");
    return NULL;
}

// --------------------- Gửi data từ Web về ESP------------------
void data_eff_config_check()
{
    delay_ms(3000);
    semaphorehandle_data_eff_config = xSemaphoreCreateBinary();
    while (1)
    {
        if(xSemaphoreTake(semaphorehandle_data_eff_config, portMAX_DELAY))
        {
             get_data_config_effect(buffer_data_eff_config);  
             read_data_config_from_flash();
             control_cube.restart_value = 1;
             vTaskSuspend(timer_interrupt_handle);
             fill_cube(&control_cube, 0x00);
             show_led(&control_cube);
             delay_ms(2000);
             vTaskResume(timer_interrupt_handle);
        }
    }
}
// ------------------------ Hàm ghi data config led vào NSV ---------------------------
void flash_write_once()
{
     esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    // Đợi khởi tạo bộ nhớ
    delay_ms(1000);
    nvs_handle_t nvs_handle;
    err = nvs_open("storage",NVS_READWRITE, &nvs_handle);  
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n"); 
        nvs_set_u16(nvs_handle,"1",1);      nvs_set_u16(nvs_handle,"2",10 );
        nvs_set_u16(nvs_handle,"3",1);      nvs_set_u16(nvs_handle,"4",10 );
        nvs_set_u16(nvs_handle,"5",1);      nvs_set_u16(nvs_handle,"6",10 );
        nvs_set_u16(nvs_handle,"7",1);      nvs_set_u16(nvs_handle,"8",10 );
        nvs_set_u16(nvs_handle,"9",1);      nvs_set_u16(nvs_handle,"10",10 );
        nvs_set_u16(nvs_handle,"11",1);     nvs_set_u16(nvs_handle,"12",10 );
        nvs_set_u16(nvs_handle,"13",1);     nvs_set_u16(nvs_handle,"14",10 );
        nvs_set_u16(nvs_handle,"15",1);     nvs_set_u16(nvs_handle,"16",10 );
        nvs_set_u16(nvs_handle,"17",1);     nvs_set_u16(nvs_handle,"18",10 );
        nvs_set_u16(nvs_handle,"19",1);     nvs_set_u16(nvs_handle,"20",10 );
        nvs_set_u16(nvs_handle,"21",1);     nvs_set_u16(nvs_handle,"22",10 );
        nvs_set_u16(nvs_handle,"23",1);     nvs_set_u16(nvs_handle,"24",10 );
        nvs_set_u16(nvs_handle,"25",1);     nvs_set_u16(nvs_handle,"26",10 );
        nvs_set_u16(nvs_handle,"27",1);     nvs_set_u16(nvs_handle,"28",10 );
        nvs_set_u16(nvs_handle,"29",1);     nvs_set_u16(nvs_handle,"30",10 );
        nvs_set_u16(nvs_handle,"31",1);     nvs_set_u16(nvs_handle,"32",10 );
        nvs_set_u16(nvs_handle,"33",1);     nvs_set_u16(nvs_handle,"34",10);
        nvs_set_u16(nvs_handle,"35",1);     nvs_set_u16(nvs_handle,"36",10 );
        nvs_set_u16(nvs_handle,"37",1);     nvs_set_u16(nvs_handle,"38",10 );
        nvs_set_u16(nvs_handle,"39",1);     nvs_set_str(nvs_handle,"40","123");
        nvs_set_u16(nvs_handle,"41",1);     nvs_set_u16(nvs_handle,"42",10);
        nvs_set_u16(nvs_handle,"43","1");   nvs_set_str(nvs_handle,"44","123");
        nvs_set_u16(nvs_handle,"45",1);     nvs_set_u16(nvs_handle,"46",10 ); 
        nvs_set_u16(nvs_handle,"47",10);
        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
}

/*---------------------------------FFT------------------------------------------*/

esp_adc_cal_characteristics_t   adc_char;
int16_t inputValue;

short  displayData[20];
short  oldDisplayData[20];
short  displayDataToCube[20];

int16_t  im[1024];
int16_t  data[1024];
int16_t  val;
int timeSlow = 0;

void feedTheDog(){
  // feed dog 0
   TIMERG0.wdt_wprotect=TIMG_WDT_WKEY_VALUE; // write enable
   TIMERG0.wdt_feed=1;                       // feed dog
   TIMERG0.wdt_wprotect=0;                   // write protect
  // feed dog 1
  // TIMERG1.wdt_wprotect=TIMG_WDT_WKEY_VALUE; // write enable
  // TIMERG1.wdt_feed=1;                       // feed dog
  // TIMERG1.wdt_wprotect=0;                   // write protect
}

void fft_display()
{ 
    // Hiệu chỉnh ADC
    esp_adc_cal_characterize(ADC_UNIT_1,ADC_ATTEN_DB_11,ADC_WIDTH_BIT_12, 0, &adc_char);
    // cấu hình độ rộng ADC
    adc1_config_width(ADC_WIDTH_BIT_12);
    // cấu hình dải điện áp đầu vào cho adc 
    adc1_config_channel_atten(ADC1_CHANNEL_5 , ADC_ATTEN_DB_11); // 33
   // adc1_config_channel_atten(ADC2_CHANNEL_0 , ADC_ATTEN_DB_11);  //4
    semaphorehandle_adc_read = xSemaphoreCreateBinary();
    delay_ms(1000);
    set_brightness(&control_cube, 15);
   // esp_task_wdt_init(30, false);

    while (1)
    {
        feedTheDog();
        for(int i = 0; i < 512; i++)
        {
            val = adc1_get_raw(ADC1_CHANNEL_5);
            data[i] = val  - 2048;;
			im[i] = 0;
        }
        fix_fft(data,im,7,0);
        for(int i=0; i< 256;i++){
            data[i] = sqrt(data[i] * data[i] + im[i] * im[i]);
            // if(data[i] > 183) data[i] = 183;
            // data[i]  =  map(data[i], 0,100,0,20);
        }
        
        int16_t inputValue;
        for (unsigned char counter = 1; counter <20; counter++)
        {
            // Scale the input data for the display (linear) x1 or x8
            inputValue = (data[counter*2 - 1] + data[counter*2 ] +  data[counter*2 + 1])/3;
            if (inputValue > 181) inputValue = 181;
            
            // Apply a linear or logarithmic conversion on the data
            inputValue = (short)linTable[inputValue];
            
            // Perform damping on the displayed output
            if (inputValue > displayData[counter-1]) displayData[counter-1] = inputValue ;
            else displayData[counter-1] -= 10;

            displayData[counter-1] =  displayData[counter-1] - 5;
            if (displayData[counter-1] < 0) displayData[counter-1] = 0;

            if ( displayData[counter-1] > 20)  displayData[counter-1] = 20;
            displayData[counter-1]  =  map(displayData[counter-1], 0,20,0,8);
        }

        // Cài đặt tốc độ nháy, tốc độ cột LED xuống
        for(int i=0; i <= 15; ++i)
        {               
            if(displayData[i] >= oldDisplayData[i])
            {
                displayDataToCube[i] = displayData[i];
                oldDisplayData[i]    = displayData[i];
            } 
        }
        
        uint8_t speedSlow = 0;
        if(timeSlow > speedSlow)
        {
            for(int i=0; i <= 15; ++i)
            {
                if(oldDisplayData[i] > 0 )
                {
                    oldDisplayData[i]    = oldDisplayData[i] - 1;
                    displayDataToCube[i] = oldDisplayData[i]; 
                    timeSlow = 0;  
                } 
            }    
        }
        // Show data ra LED CUBE
        showAllCollumm(&control_cube,displayDataToCube);
        show_led(&control_cube);
        timeSlow++;
    }
}



//--------------- KissFFT ---------------------------------------------
// #include "kiss_fftr.h"
// #include "cq_kernel.h"
// #include "driver/i2s.h"
// #include "driver/adc.h"
// #include "math.h"

// // End-user constants, adjust depending on your electrical configuration
// const int dB_min = 25; // dB, minimum value to display
// const int dB_max = 45; // dB, maximum value to display
// const int clip_pin = 25; // Connect LED to this pin to get a clipping indicator (TODO: reimplement)
// const adc1_channel_t adc_channel = ADC1_CHANNEL_5; // Connect DC-biased line signal to this, see IDF docs for pin nums
// const float fft_mag_cutoff = 15.0; // factor used for cutting off noise in raw spectrum, raise if noise is in the output
// const int screen_width = 128; // px, width of screen
// const int screen_height = 64; // px, height of screen
// // #define SPI_SSD1306 // Uncomment if using a SPI SSD1306 OLED, also injects an interp routine for 3x the "frame rate"

// // Critical constants, not intended for end-user modification
// const int N_samples = 6144; // FFT length, prime factorication should contain as many 2s as possible, affects performance
// const int sampling_frequency = 44100; // Hz, I2S sampling frequency
// const int max_freq = 14000; // Hz, last CQT center freq to display, ensure CQT kernels aren't degenerated when changing
// const int min_freq = 40; // Hz, first CQT center freq to display, ensure CQT kernels aren't degenerated when changing
// const enum window_type window_type = GAUSSIAN; // shape of CQT kernels
// const float min_val = 0.02; // see Brown CQT paper for explanation
// const int calc_rate = 120; // Hz, calcs pinned to this rate, artifacts on tone tests and fails to meet calc_rate if too high
// const int N_columns = 16; // number of columns to display
// const int col_width = 1; // px, width of each column

// // global variables, accessed during execution
// struct cq_kernel_cfg cq_cfg = { // accessed before all other tasks are started, so its global
//     .samples = N_samples,
//     .bands = N_columns,
//     .fmin = min_freq,
//     .fmax = max_freq,
//     .fs = sampling_frequency,
//     .window_type = window_type,
//     .min_val = min_val
// };
// cq_kernels_t kernels; // will point to kernels allocated in dynamic memory

// ///////////////////////////////////////////////
// typedef struct {
//     float *readBuffer;
//     float *writeBuffer;
// 	int SIZE;
// } doubleBuffer;

// //doubleBuffer<float, N_columns> colBuffer; // for CQT out before post-processing
// doubleBuffer colBuffer={
// 	.readBuffer = NULL,
//   	.writeBuffer = NULL,
//   	.SIZE = N_columns,
// };

// void doubleBuffer_swap(doubleBuffer *db) {
//     float *temp = db->readBuffer;
//     db->readBuffer = db->writeBuffer;
//     db->writeBuffer = temp;
// }
// void doubleBuffer_alloc(doubleBuffer *db) {
//     db->readBuffer = malloc(db->SIZE * sizeof(float));
//     db->writeBuffer = malloc(db->SIZE * sizeof(float));
// }



// typedef struct {
//     float *buffer;
//     int end_index;
// 	int SIZE ;
// } fftBuffer;
// fftBuffer analogBuffer = {
// 	.buffer = NULL,
//   	.end_index = 0,
//   	.SIZE = N_samples,
// };

// void fftBuffer_write(fftBuffer *fb, float *data, int w_size ) {
//     int i_start = fb->end_index;
//     for (int j = 0, i = fb->end_index; j < w_size; j++, i = (i + 1) % fb->SIZE) {
//         fb->buffer[i] = data[j];
//     }
//     fb->end_index = (i_start + w_size) % fb->SIZE;
// }

// void fftBuffer_read(fftBuffer *fb, float *data) {

//     int i_start = fb->end_index - fb->SIZE;
//     if (i_start < 0) {
//         i_start += fb->SIZE;
//     }
//     for (int j = 0, i = i_start; j < fb->SIZE; j++, i = (i + 1) % fb->SIZE) {
//         data[j] = fb->buffer[i];
//     }
// }

// void fftBuffer_alloc(fftBuffer *fb) {
//     fb->buffer = calloc(fb->SIZE, sizeof(float));
// }

// volatile bool colBuffer_swap_ready = false; 

// /*--------------------------------------------------------------------*/
// void screen_Task_routine(void *pvParameters){
//     doubleBuffer_alloc(&colBuffer);

// 	ESP_LOGI("TAG", "init FFT SHOW 128x64");
	

//     float   *y = (float*)calloc(N_columns, sizeof(float)),
//             *y_1 = (float*)calloc(N_columns, sizeof(float)),
//             *y_2 = (float*)calloc(N_columns, sizeof(float)),
//             *x_1 = (float*)calloc(N_columns, sizeof(float)),
//             *x_2 = (float*)calloc(N_columns, sizeof(float));

//     delay_ms(1000); // give time for the other tasks to allocate memory

//     while(true){

//         if(colBuffer_swap_ready){
//             doubleBuffer_swap(&colBuffer);
//             colBuffer_swap_ready = false;
//         }

//         for(int i = 0; i < N_columns; i++){
//             float x = colBuffer.readBuffer[i]-dB_min;
//             if(x < 0) x = 0;
            
//             // 2nd-order Butterworth IIR with cutoff at 10Hz (89Hz "sampling") as a filter
//             y[i] = 0.081926471866054*x+0.163852943732109*x_1[i]+0.081926471866054*x_2[i] 
//                 +1.043326781704508*y_1[i]-0.371032669168726*y_2[i];

//             x_2[i] = x_1[i];
//             x_1[i] = x;
//             y_2[i] = y_1[i];
//             y_1[i] = y[i];
//         }
		
// 		fill_cube(layer_buffer, cube, rgb_setvoxel, 0);
      
// 		//_ssd1306_line(&dev,0, screen_height-1,screen_width-1, screen_height-1,0);
//         const int col_px = screen_width/N_columns;
//         for(int i = 0; i < N_columns; i++){
//             int length = y[i]*((float)screen_height/(dB_max-dB_min));
//            // display.fillRect(i*col_px-col_width, screen_height-length, col_width, length, WHITE);
// 			//ssd1306_drawbar(&dev, i*col_px-col_width, 8,col_width,length,0 );
//             showColumm(0,i*col_px-col_width,length);
//         }
//       //  ssd1306_show_buffer(&dev);
//         delay_ms(10);
//     }
// }


// void comp_Task_routine(void *pvParameters){
//     // Allocate some large arrays
//     float *in = (float*)malloc(N_samples*sizeof(float));
//     kiss_fft_cpx *out = (kiss_fft_cpx*)malloc(N_samples*sizeof(kiss_fft_cpx));
//     kiss_fftr_cfg cfg = kiss_fftr_alloc(N_samples, 0, NULL, NULL);
//     kiss_fft_cpx *bands_cpx = (kiss_fft_cpx*)malloc(N_columns*sizeof(kiss_fft_cpx));

//     // Initialize I2S sampling
//     const int samples_to_read = sampling_frequency/calc_rate+1; // better to want a sample more than to want too fast
//     const int i2s_buffer_size = (1 << ((int)(log2(samples_to_read))+1));
//     const i2s_config_t i2s_cfg = {
//         .mode = (i2s_mode_t)( I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN ),
//         .sample_rate = sampling_frequency,
//         .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
//         .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
//         .communication_format = (i2s_comm_format_t)1,                      // I2S_COMM_FORMAT_I2S
//         .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
//         .dma_buf_count = 2,
//         .dma_buf_len = i2s_buffer_size,
//         .use_apll = false,
//         .tx_desc_auto_clear = false,
//         .fixed_mclk = 0,
//     };

//     fftBuffer_alloc(&analogBuffer);
//     int16_t *samples_raw = (int16_t*)malloc(sizeof(int16_t)*i2s_buffer_size);
//     float *samples = (float*)malloc(sizeof(float)*i2s_buffer_size);
//     i2s_driver_install(I2S_NUM_0, &i2s_cfg, 0, NULL);
//     i2s_set_adc_mode(ADC_UNIT_1, ADC1_CHANNEL_5);
//     i2s_adc_enable(I2S_NUM_0);

//     unsigned long last_clipped = 0;
//     delay_ms(1000); // give time for the other tasks to allocate memory

   

//     while(true){
		
//         size_t bytes_read = 0;
//         i2s_read(I2S_NUM_0, samples_raw, sizeof(int16_t)*samples_to_read, &bytes_read, portMAX_DELAY); // blocking call

//         bool clipped = false;
//         int samples_read = bytes_read/sizeof(int16_t);
//         for(int i = 0; i < samples_read; i++){
//             if(samples_raw[i] > 3873 || samples_raw[i] < 223) clipped = true;
//             samples[i] = (float)(samples_raw[i]-2048)*(1.0f/2048.0f);
//            // Serial.println(samples_raw[i]);
//         }

//         if(clipped && !last_clipped){
//             gpio_set_level(25,1);
//             last_clipped = millis();
//         }
//         else if(!clipped && last_clipped && millis()-last_clipped > 100){
//             gpio_set_level(25,0);
//             last_clipped = 0;
//         }

//         for(int i = 0; i < samples_read; i += 2){ // even and odd samples are switched for some reason
//             float temp = samples[i];
//             samples[i] = samples[i+1];
//             samples[i+1] = temp;
//         }

//         //analogBuffer.write(samples, samples_read); // write only 308 samples to the buffer...
//         fftBuffer_write(&analogBuffer,samples, samples_read);

//         float sum = 0, avg;
//         //analogBuffer.read(in); // ...and read the past N_samples out!
// 		fftBuffer_read(&analogBuffer,in);
//         for(int i = 0; i < N_samples; i++) sum += in[i];
//         avg = sum*(1.0f/N_samples);
//         for(int i = 0; i < N_samples; i++){
//             in[i] -= avg;
//             out[i] = (kiss_fft_cpx){0, 0}; // necessary before calling kiss_fftr
//         }

     
      
//         kiss_fftr(cfg, in, out);

        
        
//         // Cutting off noise with a threshold inversely proportional to N_samples
//         const float minimum_mag = 2048*fft_mag_cutoff/N_samples;
//         const float minimum_mag_squared = minimum_mag*minimum_mag;
//         for(int i = 0; i < N_samples; i++)
//             if(out[i].r*out[i].r+out[i].i*out[i].i < minimum_mag_squared)
//                 out[i] = (kiss_fft_cpx){0, 0};

//         // Convert FFT output to Constant Q output using cq_kernel
//         for(int i = 0; i < N_columns; i++) bands_cpx[i] = (kiss_fft_cpx){0, 0};
//         apply_kernels(out, bands_cpx, kernels, cq_cfg);

//         for(int i = 0; i < N_columns; i++){
//             // Finds decibel value of complex magnitude (relative to 1<<14, apparent maximum)
//             float mag_squared = bands_cpx[i].r*bands_cpx[i].r+bands_cpx[i].i*bands_cpx[i].i;
//             float x = 10.0f*log10(mag_squared); // dB, (squared in ==> 10*log10, not 20*log10), reference level is arbitrary
//             colBuffer.writeBuffer[i] = x;
//         }
//         colBuffer_swap_ready = true;   // Raises flag to indicate buffer is ready to push    
//     }
// }
//--------------------------------------------------------------------


void app_main()
{
    init_led_cube();
    led_strip_install();
    
 
    xTaskCreatePinnedToCore(cube_run, "cube_run",8192*2, NULL, 5, &cube_run_handle,1);
    xTaskCreatePinnedToCore(timer_interrupt, "timer_interrupt", 8192, NULL,5,  &timer_interrupt_handle,1);

    wifi_ap_start("LED Cube 8x8x8","12345678",1,5);
    httpd_handle_t server = NULL;
    server = start_webserver(); 

    read_data_config_from_flash();
    xTaskCreatePinnedToCore(data_eff_config_check, "data_eff_config_check",8192, NULL, 5, &data_eff_handle,0);
    

   // disableCore0WDT();
    xTaskCreatePinnedToCore(fft_display, "readADC",  1024*8, NULL, configMAX_PRIORITIES-1, &readADCTaskHandle, 1);
	
    vTaskSuspend(NULL); // suspend the arduino loop
    // flash_write_once();
   
    // cấu hình partion 0x9000
    // nap code flash_write_once() truoc
    // nap code con lai
}

