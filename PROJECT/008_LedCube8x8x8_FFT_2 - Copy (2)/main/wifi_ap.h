#ifndef WIFI_AP_H
#define WIFI_AP_H

void wifi_ap_start(const char *WIFI_AP_SSID, const char *WIFI_AP_PASS, const char WIFI_AP_CHANNEL, const char WIFI_AP_MAX_STA_CONN);

#endif
