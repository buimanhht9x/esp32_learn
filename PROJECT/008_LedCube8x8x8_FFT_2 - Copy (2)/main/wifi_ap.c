#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "wifi_ap.h"

static const char *TAG = "wifi softAP";

// hàm xử lí sự kiện wifi AP
static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    // sự kiện có station kết nối vào
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } 
    // sự kiện có station ngắt kết nối 
    else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
}

/*  Hàm khởi tạo wifi AP
    WIFI_AP_SSID    : tên wifi AP
    WIFI_AP_PASS    : mật khẩu AP
    WIFI_AP_CHANNEL : channel kết nối
    WIFI_AP_MAX_STA_CONN : số station tối đa kết nối được
*/
void wifi_ap_start(const char *WIFI_AP_SSID, const char *WIFI_AP_PASS, const char WIFI_AP_CHANNEL, const char WIFI_AP_MAX_STA_CONN)
{
    // khởi tạo NVS trên ESP32 và nếu có lỗi cụ thể liên quan đến việc khởi tạo NVS, 
    // mã này sẽ cố gắng xóa dữ liệu hiện có và khởi tạo lại. Điều này có thể hữu ích 
    // để xử lý các tình huống trong đó việc khởi tạo NVS không thành công do không đủ 
    // trang trống hoặc phát hiện thấy phiên bản NVS mới.
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_LOGI(TAG, "ESP_WIFI_MODE_AP");

    // Initialize the underlying TCP/IP stack
    ESP_ERROR_CHECK(esp_netif_init());

    // Hàm này tạo một vòng lặp sự kiện mặc định để xử lý các sự kiện ESP-IDF. 
    // Các sự kiện trong ESP-IDF được sử dụng để xử lý các sự kiện không đồng bộ 
    // như thay đổi kết nối Wi-Fi, thu thập địa chỉ IP và các sự kiện hệ thống khác.
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // tạo giao diện mạng Wi-Fi mặc định ( esp_netif) cho ESP32 và định cấu hình nó 
    // để sử dụng làm Điểm truy cập Wi-Fi (AP). Nó thiết lập các tham số giao diện 
    // mạng cần thiết.
    esp_netif_create_default_wifi_ap();

    // khởi tạo cấu trúc cấu hình ( wifi_init_config_t) cho mô-đun Wi-Fi bằng các 
    // giá trị mặc định. Macro WIFI_INIT_CONFIG_DEFAULT()thiết lập cấu hình mặc định.
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

    // khởi chạy mô-đun Wi-Fi với cấu hình được cung cấp trong tệp cfg.
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    // đăng ký trình xử lý sự kiện cho các sự kiện Wi-Fi. Nó liên kết hàm wifi_event_handler
    // với việc xử lý các sự kiện thuộc loại WIFI_EVENT. Trình xử lý sự kiện chịu trách nhiệm 
    // phản ứng với các sự kiện khác nhau liên quan đến Wi-Fi. 
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    // struct để định cấu hình AP Wi-Fi. Nó bao gồm các tham số như SSID (tên mạng), mật khẩu, 
    // kênh, số lượng kết nối tối đa và chế độ xác thực.
    wifi_config_t wifi_config = {
        .ap = {
            .ssid = "",
            .ssid_len = 0,
            .channel  = WIFI_AP_CHANNEL,
            .password = "",
            .max_connection = WIFI_AP_MAX_STA_CONN,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK
        },
    };
    strncpy((char *)wifi_config.ap.ssid, WIFI_AP_SSID, sizeof(wifi_config.ap.ssid));
    strncpy((char *)wifi_config.ap.password, WIFI_AP_PASS, sizeof(wifi_config.ap.password));
    wifi_config.ap.ssid_len = strlen((char *)wifi_config.ap.ssid);

    // nếu EXAMPLE_ESP_WIFI_PASS để trống thì wifi không có mật khẩu
    if (strlen(WIFI_AP_PASS) == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    // đặt chế độ Wi-Fi thành chế độ AP.
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));

    // đặt cấu hình Wi-Fi đã đặt trước wifi_config đó cho AP.
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));

    // khởi động mô-đun Wi-Fi ở chế độ AP.
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
             WIFI_AP_SSID, WIFI_AP_PASS, WIFI_AP_CHANNEL);
}