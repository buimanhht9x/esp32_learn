#ifndef EFFECT_LED_H
#define EFFECT_LED_H
#include "control_led.h"
#include <led_strip_idf.h>
#include "math.h"


#define ENABLE 1
#define DISABLE 0
#define AXIS_Z 1
#define AXIS_Y 2
#define AXIS_X 3

typedef struct{
    uint16_t state;
    uint16_t time_run;
    char *stringbuff; 
}effect_config_t;





rgb_t Scroll(int pos) ;
void change_rgb_color(control_led_cube_t *control_led_cube);
void change_rgb_color_2(control_led_cube_t *control_led_cube);
void effect_rainbow(control_led_cube_t *control_led_cube,uint8_t enordis,uint32_t timeRun, int speed);
void effect_rain(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int speed, int direction);
void eff_side_change_color(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun);
void planboing(control_led_cube_t *control_led_cube, char axis, int speed);
void effect_planboing(control_led_cube_t *control_led_cube,uint8_t enordis, uint32_t timeRun);
void effect_sendvoxels_rand_axis(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, char axis, char delay, char wait);
void effect_box_shrink_grow(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int speed);
void box_woopwoop(control_led_cube_t *control_led_cube, int delay, int grow) ;
void effect_box_woopwoop(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun);
void axis_updown_randsuspend(control_led_cube_t *control_led_cube, char axis, char delay, int sleep, char invert);
void effect_axis_z_updown_randsuspend(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun);
void effect_axis_y_updown_randsuspend(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun);
void effect_int_sidewaves(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_int_ripples(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_wormsqueeze(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay,int axis, int direction);
void effect_cubix(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, unsigned char cubies);
void effect_boingboing(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, char delay, unsigned char mode, unsigned char drawmode);
void effect_random_filler(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_filip_filop(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int FF_DELAY);
void effect_fireworks (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_sinelines (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_linespin (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_boxside_randsend_parallel(control_led_cube_t *control_led_cube, uint8_t axis, uint8_t origin, uint8_t delay, uint8_t mode);
void effect_stringfly(control_led_cube_t *control_led_cube, uint8_t enordis, char *str);
void effect_path_text(control_led_cube_t *control_led_cube, uint8_t enordis, int delay, char *str);
void effect_ball_bouncing(control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay);
void effect_smileyspin (control_led_cube_t *control_led_cube, uint8_t enordis, uint32_t timeRun, int delay, char bitmap);


#endif
