#ifndef TIMER_H
#define TIMER_H
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include <led_strip_idf.h>
#include "math.h"
#include "esp_log.h"
#include "driver/timer.h"
#define TIMER_DIVIDER         (16)  //  Hardware timer clock divider
#define TIMER_SCALE           1000  // convert counter value to seconds

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
    bool auto_reload;
} example_timer_info_t;

/**
 * @brief A sample structure to pass events from the timer ISR to task
 *
 */
typedef struct {
    example_timer_info_t info;
    uint64_t timer_counter_value;
} example_timer_event_t;



#endif
