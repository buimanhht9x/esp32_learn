#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "math.h"
#include "esp_log.h"
#include "wifi_sta_connect.h"

#include "esp_http_server.h"
// Khai báo file html
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");


esp_err_t led1_post_handler(httpd_req_t* req)
{
    char buf[100];
    httpd_req_recv(req, buf,req->content_len);
    printf("led : %s\n",buf);
    return ESP_OK;
}
esp_err_t html_get_handler(httpd_req_t *req)
{

    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req,(char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
}


httpd_uri_t led1_uri_post = {
    .handler = led1_post_handler,
    .method = HTTP_POST,
    .uri =  "/led1",
    .user_ctx = NULL
};
httpd_uri_t html_uri_get = {
    .handler = html_get_handler,
    .method = HTTP_GET,
    .uri =  "/",
    .user_ctx = NULL
};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI("SERVER", "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Registering the ws handler
        ESP_LOGI("SERVER", "Registering URI handlers");
        httpd_register_uri_handler(server, &html_uri_get);
        httpd_register_uri_handler(server, &led1_uri_post);
        return server;
    }
    ESP_LOGI("SERVER", "Error starting server!");
    return NULL;
}


void app_main(void)
{ 
    wifi_sta_connect();
    httpd_handle_t server = NULL;
    server = start_webserver();
}