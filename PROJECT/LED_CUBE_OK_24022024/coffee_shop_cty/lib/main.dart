import 'package:coffee_shop_cty/components/MyNavBar.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:coffee_shop_cty/pages/brightness_page.dart';
import 'package:coffee_shop_cty/pages/home_page.dart';
import 'package:coffee_shop_cty/pages/music_page.dart';
import 'package:coffee_shop_cty/pages/setup_page.dart';
import 'package:coffee_shop_cty/pages/welcome_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


void main() {
  runApp(const MyApp()); 
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return   ChangeNotifierProvider(
      create: (context) => EffectShop(),
      builder:(context, child) => MaterialApp(
            debugShowCheckedModeBanner: false,
            home:  WelcomePage(),
          ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  void navigateBottomBar(index)
  {
      setState(() {
        _selectedIndex = index;
      });
  }

  // Pages
  final List<Widget> _pages= [
      // home page
      HomePage(),
      // cartpage
      BrightnessPage(),
      //Music
      MusicPage(),
      // setup page
      SetupPage(),
  ];
 
  
  @override
  Widget build(BuildContext context) {
    

    return Scaffold(
       
       bottomNavigationBar: MyNavBar(
          onTabChange: (value) {
            navigateBottomBar(value);
          } ,
       ),
       body: _pages[_selectedIndex],

    );
  }
}
