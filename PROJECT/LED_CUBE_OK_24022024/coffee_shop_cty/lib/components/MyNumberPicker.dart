import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:numberpicker/numberpicker.dart';

import 'package:coffee_shop_cty/models/effect.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';




class MyNumberPicker extends StatefulWidget {
  final Effect effect;
  final int index;
  final int timeRun;
  MyNumberPicker({required this.effect, required this.index, required this.timeRun});

  @override
  State<MyNumberPicker> createState() => _MyNumberPickerState(effect, index, timeRun);
}

class _MyNumberPickerState extends State<MyNumberPicker> {
  Effect effect;
  int index;
  int timeRun;
  _MyNumberPickerState(this.effect, this.index, this.timeRun);
  int _timeSec = 5;
  int _timeMin = 5;
  @override
   void initState() {
     // TODO: implement initState
     super.initState();
     setState(() {
       _timeMin = (timeRun/60).toInt();
       _timeSec = (timeRun%60).toInt();
     }); 
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, valueEffectShop, child) =>  
      Container(
      width: 180,
      
      // decoration: BoxDecoration(
      //     color: Color.fromARGB(255, 255, 255, 255),
      //     borderRadius: BorderRadius.circular(20),
      // ),
      child: Column(
        children: [
          SizedBox(height: 5,),
          Text(' Time run', style: TextStyle(
                fontSize: 15,
              ),),
          SizedBox(height: 5,),
          Row(
            children: [
              SizedBox(width: 15,), 
              NumberPicker(
              itemWidth: 50,
              value: _timeMin,
              minValue: 0,
              maxValue: 59,
              onChanged: (value) => setState(() {
                  _timeMin = value;
                  int timeSec =  _timeMin*60 + _timeSec;
                  Provider.of<EffectShop>(context, listen: false).setTimeEffect(index, timeSec);
                }, ),
              ),
              SizedBox(width: 40,), 
              NumberPicker(
                itemWidth: 50,
                value: _timeSec ,
                minValue: 0,
                maxValue: 59,
                onChanged: (value) => setState(() {
                  _timeSec = value;
                  int timeSec =  _timeMin*60 + _timeSec;
                  Provider.of<EffectShop>(context, listen: false).setTimeEffect(index, timeSec);
                }, ),
              ),
            ],
          ),
          const SizedBox(height: 15,),
          Row(
            children: [
              Text(_timeMin < 10 ? '0'+_timeMin.toString(): _timeMin.toString(), style: TextStyle(
                fontSize: 25,
              ),),
              Text(" : "),
              Text(_timeSec < 10 ? '0'+_timeSec.toString(): _timeSec.toString(), style: TextStyle(
                fontSize: 25,
              ),),
              Text(" s"),
            ],
          ),
          SizedBox(height: 10,),
        ],
      ),
    ),);

  }
  

}