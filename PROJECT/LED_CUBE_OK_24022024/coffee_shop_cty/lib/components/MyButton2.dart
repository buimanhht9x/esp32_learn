import 'package:flutter/material.dart';

class MyButton2 extends StatelessWidget {
  final onTap;
  final bool isLiked;
  const MyButton2({super.key,required this.isLiked, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AnimatedContainer(
          duration: Duration(milliseconds: 3000),
          height: 120,
          width:  120,
          child: Icon(
              Icons.favorite,
              size: 120,
              color:isLiked? Colors.red: Colors.red[200],
          ),
      ),
    );
  }
}