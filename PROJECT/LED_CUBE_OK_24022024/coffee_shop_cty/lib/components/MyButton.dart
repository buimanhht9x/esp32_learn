import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final onTap;
  final bool isLiked;
  const MyButton({super.key,required this.isLiked, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AnimatedContainer(
          duration: Duration(milliseconds: 200),
          height: 80,
          width:  80,
          child: Icon(
              Icons.favorite,
              size: 40,
              color:isLiked? Colors.red: Colors.red[200],
          ),
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(12),
            border: Border.all(
                color: isLiked? Colors.grey.shade300: Colors.grey.shade200
            ) ,
            boxShadow:isLiked?  [
              BoxShadow(
                  color: Colors.grey.shade500,
                  offset: Offset(3,3),
                  blurRadius: 10,
                  spreadRadius: 1
              ),
              BoxShadow(
                  color: Colors.white,
                  offset: Offset(-3,-3),
                  blurRadius: 10,
                  spreadRadius: 1
              )
            ] :[],
          ),
      ),
    );
  }
}