import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class MyNavBar extends StatelessWidget {
  final void Function(int)? onTabChange;
  const MyNavBar({super.key, required this.onTabChange});

  @override
  Widget build(BuildContext context) {
    return GNav(
       // tabBackgroundColor  : Color.fromARGB(255, 162, 96, 175),
        tabBackgroundGradient: LinearGradient(colors: [Color.fromARGB(255, 131, 49, 224), Colors.brown]),
        mainAxisAlignment: MainAxisAlignment.center,
        tabMargin: EdgeInsets.only(bottom: 5,top: 5),
        padding : EdgeInsets.all(20),

        onTabChange:(value) => onTabChange!(value),
        tabs: [
            GButton(
              icon: Icons.home_filled,
              text: 'Home', 
              textColor :Colors.white,

                     
            ),
            GButton(
              icon: Icons.brightness_medium_outlined,
              text: 'Brightness',
              textColor :Colors.white,
            ),
             GButton(
              icon: Icons.music_note_sharp,
              text: 'Music',
              textColor :Colors.white,
            ),
            GButton(
              icon: Icons.settings,
              text: 'Setup',
              textColor :Colors.white,
            ),
        ],
    );
  }
}