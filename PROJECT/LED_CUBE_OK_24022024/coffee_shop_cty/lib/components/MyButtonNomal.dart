import 'package:flutter/material.dart';

class MyButtonNomal extends StatelessWidget {
  final onTap;
  final String text;
  const MyButtonNomal({super.key, required this.text,required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AnimatedContainer(
          margin: EdgeInsets.all(15),
          duration: Duration(milliseconds: 3000),
          height: 100,
          width:  double.infinity,
          child: Center(child: Text(text, style: TextStyle(
             fontSize: 15,
             color:Colors.white
          ),)),
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 130, 50, 235),
            borderRadius: BorderRadius.circular(40),
          ),
      ),
    );
  }
}