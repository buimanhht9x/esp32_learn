import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MySlider extends StatefulWidget {
  final double valueSlider;
  MySlider({required this.valueSlider});

  @override
  State<MySlider> createState() => _MySliderState(valueSlider);
}

class _MySliderState extends State<MySlider> {
  double valueSlider;
  _MySliderState(this.valueSlider);
  double _sliderValue = 5;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(valueSlider<5 || valueSlider >100) valueSlider = 5;
    _sliderValue = valueSlider;
  }
  
  @override
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, valueEffectShop, child) => 
    GestureDetector(
        child: Container(
            child: Slider(
                      min: 5,
                      max: 100,
                      divisions: 95,
                      value: _sliderValue, 
                    
                      label: (_sliderValue.toInt()).toString(),
                      thumbColor :Color.fromARGB(255, 77, 52, 105), 
                      activeColor :const Color.fromARGB(255, 102, 72, 121),
                      onChanged: (value) {
                          setState(() {
                              _sliderValue = value;
                              print(valueSlider);
                              Provider.of<EffectShop>(context, listen: false).setBrightness(_sliderValue.toInt());
                          });
                      },),
        ),
    ),);
  }
}