import 'package:coffee_shop_cty/models/effect.dart';
import 'package:flutter/material.dart';

class EffectShop extends ChangeNotifier{

  // list coffee
  List<Effect> _shop = [
    // hot coffee
    Effect(name: 'Rain', isLiked: true, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Rain Bow', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Plan Boing', isLiked: true, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Send Voxels Random Y', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Send Voxels Random Z', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Shrink Grow', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Woopwoop', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'AxisZ Updown Suspend', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'AxisY Updown Suspend', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Sidewaves', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Worm Squeeze', isLiked: true, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Boingboing 1', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Boingboing 2', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Boingboing 3', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Filler', isLiked: true, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Ripples', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Cubix', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Filip Filop', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Sinelines', isLiked: false, timeRun: 12,imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Stringfly', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif', string: "BANLINHKIEN.COM"),
    Effect(name: 'Fireworks', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
    Effect(name: 'Path Text', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif', string: "BANLINHKIEN.COM"),
    Effect(name: 'Ball Bouncing', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
  //  Effect(name: 'Worm Squeeze 2', isLiked: false, timeRun: 12, imgPath: 'lib/image/dribbble-final.gif'),
  ];

  int _brightness = 10;

  int get brightness => _brightness;

   
  void setBrightness(int brightness)
  {
    _brightness = brightness;
    notifyListeners();
  } 

  void printBrightness()
  {
      print(_brightness);
  } 

 
  // user cart
  List<Effect> _userLike = [];

  // get coffee list
  List<Effect> get effectShop => _shop;

  // get user cart
  List<Effect> get userCart => _userLike;


  
 
  // set like effect
  void setLikeEffect(int index)
  {
      _shop[index].isLiked = true;
      notifyListeners();
  } 
  void setUnLikeEffect(int index)
  {
      _shop[index].isLiked = false;
       notifyListeners();
  } 
  // set time effect
  void setTimeEffect(int index, int time)
  {
     _shop[index].timeRun = time;
      notifyListeners();
  } 

  // set String effect
  void setStringEffect(int index, String str)
  {
     _shop[index].string = str;
      notifyListeners();
  } 


  // add item to cart
  void addItemToCart(Effect effect)
  {
     _userLike.add(effect);
     notifyListeners();
  }
  // remove item from cart
   void removeItemFromCart(Effect effect)
  {
     _userLike.remove(effect);
     notifyListeners();
  }

  void printEffectShop()
  {
     for (Effect effect in effectShop) {
      print('Name: ${effect.name}');
      print('Is Liked: ${effect.isLiked}');
      print('Time Run: ${effect.timeRun}');
      print('Image Path: ${effect.imgPath}');
      print('String: ${effect.string}');
      print('---');
    }
  }

}