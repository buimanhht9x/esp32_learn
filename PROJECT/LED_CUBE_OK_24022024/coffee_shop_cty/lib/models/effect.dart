class Effect {
  String name;
  bool isLiked;
  int timeRun;
  String imgPath;
  String? string;

  Effect({required this.name, required this.isLiked, required this.timeRun, required this.imgPath, this.string});
}