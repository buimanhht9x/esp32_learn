import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MusicPage extends StatefulWidget {
  const MusicPage({super.key});

  @override
  State<MusicPage> createState() => _MusicPageState();
}

class _MusicPageState extends State<MusicPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, valueEffectShop, child) => SafeArea(
        child: Center(
          child: Column(
              children: [
                 Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text('MUSIC', style: TextStyle(
                        fontSize: 25,
                        letterSpacing: 4
                    ),),
                  ),
                  Text("Hello"),
              ],
          ),
        ),
      ),
    );
  }
}

