


import 'package:coffee_shop_cty/models/effect.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../components/MyButton.dart';

class EffectTile extends StatelessWidget {
  final Function()? pressLike;
  final Function()? pressDetailPage;
  final Effect effect;
  final int index;
  const EffectTile({super.key, required this.effect, required this.index, required this.pressDetailPage, required this.pressLike});

  @override
  
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, value, child) => GestureDetector(
      onTap: pressDetailPage,
      child: Container(
        padding:  EdgeInsets.all(40),
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // image, name, time
            Row(
               children: [
                //  Image.asset(effect.imgPath, height: 30,),
                  SizedBox(width: 10,),
                  Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                          Text(effect.name),
                          SizedBox(height: 20,),
                          Row(
                            children: [
                              Text(((effect.timeRun)/60).toInt() < 10 ? '0'+(((effect.timeRun)/60).toInt()).toString(): (((effect.timeRun)/60).toInt()).toString()),
                              Text(" : "),
                              Text(((effect.timeRun)%60).toInt() < 10 ? '0'+(((effect.timeRun)%60).toInt()).toString(): (((effect.timeRun)%60).toInt()).toString()),
                              Text(" s"),
                            ],
                          ),
                      ],
                  ),
               ],
            ),
            // islike
            MyButton(
              onTap: pressLike,
              isLiked: effect.isLiked,
            ),
          ],
            
        )
      ),
    ),);
  }
}