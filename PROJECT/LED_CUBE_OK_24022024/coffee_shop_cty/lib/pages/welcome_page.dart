
import 'package:coffee_shop_cty/components/MyButtonNomal.dart';
import 'package:coffee_shop_cty/main.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {

  List<dynamic> jsonData = []; // To store the received JSON array data
  List<String> nameList = [];
  List<int> isLikedList = [];
  List<int> timeRunList = [];
  List<String> strList = [];
  int brightness = 10;

  Future<void> fetchDataFromESP32() async {
    final response = await http.get(Uri.parse('http://192.168.4.1/esp_to_app_send_data')); // Replace with your ESP32 API endpoint
    if (response.statusCode == 200) {
      // If the server returns a 200 OK response, parse the JSON data
      setState(() {
        jsonData = json.decode(response.body);
      });
      print(jsonData);
      nameList.clear();
      isLikedList.clear();
      timeRunList.clear();
      strList.clear();

      for (Map<String, dynamic> item in jsonData) {
        if (item.containsKey('name')) {
          nameList.add(item['name']);
        }
        if (item.containsKey('isLiked')) {
          isLikedList.add(item['isLiked']);
        }
        if (item.containsKey('timeRun')) {
          timeRunList.add(item['timeRun']);
        }
        if (item.containsKey('string')) {
          strList.add(item['string']);
        }
         if (item.containsKey('brightness')) {
          brightness = item['brightness'];
        }
      }

      // Now you have separate lists for each property
      print('Name List: $nameList');
      print('Is Enable List: $isLikedList');
      print('Time Run List: $timeRunList');
      print('Str List: $strList');
      
      print(jsonData.length);
    } else {
      // If the server did not return a 200 OK response, handle the error here
      print('Failed to fetch data from ESP32');
    } 
  }
  
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchDataFromESP32();
  }
  
  @override
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, value, child) => SafeArea(
      child: Scaffold(
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.end,
              children: [
                  Text("LED Cube 8x8x8", style: TextStyle(
                     fontSize:30,

                  )),
                   SizedBox(height: 10,),
                  Text("Banlinhkien.com", style: TextStyle(
                     fontSize:20,
                  )),
                  SizedBox(height: 30,),
                  Image.asset('lib/image/blk_logo.jpg', height: 200,),
                  SizedBox(height: 250,),
                  MyButtonNomal(text: 'Get Started', onTap: (){
                      fetchDataFromESP32();
                      fetchDataFromESP32();
                      if(jsonData.length > 10)
                    //  if(1>0)
                      {
                          jsonData.clear();
                          Provider.of<EffectShop>(context, listen: false).printBrightness();
                          for(int i = 0;  i <= nameList.length - 1; i++)
                          {
                              isLikedList[i] >=1 ? 
                                  Provider.of<EffectShop>(context, listen: false).setLikeEffect(i):
                                  Provider.of<EffectShop>(context, listen: false).setUnLikeEffect(i);
                              print(i);
                              Provider.of<EffectShop>(context, listen: false).setTimeEffect(i,timeRunList[i]);
                              Provider.of<EffectShop>(context, listen: false).setStringEffect(i,strList[i]);
                              Provider.of<EffectShop>(context, listen: false).setBrightness(brightness);
                          }
                          Navigator.push(context , MaterialPageRoute(builder:(context) => MyHomePage())); 
                      }
                      else
                      {
                          showDialog(context: context, builder: (context) => AlertDialog(
                            title: Text('Error'),
                            content: Text('Please connect to wifi "LED Cube 8x8x8"'),
                          ),);
                      }
                  }
                  ),
                  SizedBox(height: 30,),
              ],
          ),
      ),
    ),);
  }
}
