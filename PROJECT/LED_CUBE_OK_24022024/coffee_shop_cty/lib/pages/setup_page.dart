
import 'package:coffee_shop_cty/components/MyButtonNomal.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class SetupPage extends StatefulWidget {
  const SetupPage({super.key});

  @override
  State<SetupPage> createState() => _SetupPageState();
}


class _SetupPageState extends State<SetupPage> {
  List<dynamic> jsonData = []; // To store the received JSON array data
  List<String> nameList = [];
  List<int> isLikedList = [];
  List<int> timeRunList = [];
  List<String> strList = [];
  bool _transmitESP32OK = false;
  int timeout = 2;

  Future<void> fetchDataFromESP32() async {
    final response = await http.get(Uri.parse('http://192.168.4.1/esp_to_app_check_connect')).timeout(Duration(seconds: timeout)); // Replace with your ESP32 API endpoint
    if (response.statusCode == 200) {
      // If the server returns a 200 OK response, parse the JSON data
      
      setState(() {
        jsonData = json.decode(response.body);
        
      });
      print(jsonData);

      nameList.clear();
      isLikedList.clear();
      timeRunList.clear();
      strList.clear();

      for (Map<String, dynamic> item in jsonData) {
        if (item.containsKey('name')) {
          nameList.add(item['name']);
        }
        if (item.containsKey('isLiked')) {
          isLikedList.add(item['isLiked']);
        }
        if (item.containsKey('timeRun')) {
          timeRunList.add(item['timeRun']);
        }
        if (item.containsKey('string')) {
          strList.add(item['string']);
        }
      }

      // Now you have separate lists for each property
      print('Name List: $nameList');
      print('Is Enable List: $isLikedList');
      print('Time Run List: $timeRunList');
      print('Str List: $strList');
      
    } else {
      // If the server did not return a 200 OK response, handle the error here
      print('Failed to fetch data from ESP32');
    }
  }
  int checkConnectOK = 3;
  Future<void> fetchDataResponse() async {
    final response = await http.get(Uri.parse('http://192.168.4.1/esp_to_app_check_connect')); // Replace with your ESP32 API endpoint
    if (response.statusCode == 200) {
      // If the server returns a 200 OK response, parse the JSON data
 
      print('fetch Data Response');
      checkConnectOK = 1;
      jsonData.clear();
      
    } else {
      // If the server did not return a 200 OK response, handle the error here
      print('Failed to fetch data from ESP32');
      checkConnectOK = 0;
    }
   
    
  }


  Future<void> sendJsonArrayToESP(String jsonArray) async {
  var client = http.Client();
  try {
    final response = await client.post(
      Uri.parse('http://192.168.4.1/app_to_esp_data_eff_config'), // Replace with your ESP32 API endpoint
      headers: <String, String>{
          'Content-Type': 'text/html',
      },
      body: jsonArray ,
    );

  //  print('JSON array sent successfully');
    if (response.statusCode == 200) {
      // Successful request, handle the response here if needed
      print('JSON array sent successfully');
      setState(() {
         _transmitESP32OK = true;
      });
     
     
    } else {
      // Handle the error if the request was not successful
      print('Failed to send JSON array to ESP32');
      _transmitESP32OK = false;
    }
  } catch (e) {
    // Handle any exceptions or network errors
    print('Error: $e');
    _transmitESP32OK = false;
  } finally {
    client.close(); 
   
  }
 
 
}

  @override
  Widget build(BuildContext context) {
   
    return Consumer<EffectShop>(builder: (context, valueEffectShop, child) => SafeArea(
      child: Center(
        child: Center(
    
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Text('SET UP', style: TextStyle(
                    fontSize: 25,
                    letterSpacing: 4
                ),),
              ),
              SizedBox(height: 55,),
              MyButtonNomal(
                text: 'CHECK CONNECTION',
                onTap: (){
                  print('press fetch data');
                  fetchDataResponse();
                  if(checkConnectOK == 1)
                  { 
                      jsonData.clear();
                      showDialog(context: context, builder: (context) => AlertDialog(
                            title: Text('Connected'),
                            content: Text('Connected to wifi "LED Cube 8x8x8"'),
                          ),);
                  }
                  else if(checkConnectOK == 0){
                    jsonData.clear();
                    showDialog(context: context, builder: (context) => AlertDialog(
                            title: Text('Error'),
                            content: Text('Please connect to wifi "LED Cube 8x8x8"'),
                          ),);
                  }
                  else
                  {
                      checkConnectOK = 3;
                  }
                  
                  
                  
                  

              },),
              // MyButtonNomal(
              //   text: 'CONFIG',
              //   onTap: (){
              //     print('press CONFIG');
              //     Provider.of<EffectShop>(context, listen: false).printBrightness();
              //     for(int i = 0;  i < nameList.length; i++)
              //     {
              //         isLikedList[i] >=1 ? 
              //             Provider.of<EffectShop>(context, listen: false).setLikeEffect(i):
              //             Provider.of<EffectShop>(context, listen: false).setUnLikeEffect(i);
              //         print(i);
              //         Provider.of<EffectShop>(context, listen: false).setTimeEffect(i,timeRunList[i]);
              //     }
              // },),
              // MyButtonNomal(
              //   text: 'CHECK DATA',
              //   onTap: (){
              //     print('press CHECK');
              //     Provider.of<EffectShop>(context, listen: false).printEffectShop();
              // },),


              MyButtonNomal(
                text: 'CONFIG DATA',
                onTap: (){
                  String jsonArrayData = 'rain:${valueEffectShop.effectShop[0].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[0].timeRun}' +
                                        '/rainbow:${valueEffectShop.effectShop[1].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[1].timeRun}' +
                                        '/planboing:${valueEffectShop.effectShop[2].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[2].timeRun}' + 
                                        '/sendvoxelsrandz:${valueEffectShop.effectShop[3].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[3].timeRun}' +
                                        '/sendvoxelsrandy:${valueEffectShop.effectShop[4].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[4].timeRun}' +
                                        '/boxshrinkgrow:${valueEffectShop.effectShop[5].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[5].timeRun}' +
                                        '/boxwoopwoop:${valueEffectShop.effectShop[6].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[6].timeRun}' +
                                        '/axiszupdownrs:${valueEffectShop.effectShop[7].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[7].timeRun}' +
                                        '/axisyupdownrs:${valueEffectShop.effectShop[8].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[8].timeRun}' +
                                        '/sidewaves:${valueEffectShop.effectShop[9].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[9].timeRun}' +
                                        '/wormsqueeze:${valueEffectShop.effectShop[10].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[10].timeRun}' +
                                        '/boingboing1:${valueEffectShop.effectShop[11].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[11].timeRun}' +
                                        '/boingboing2:${valueEffectShop.effectShop[12].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[12].timeRun}' +
                                        '/boingboing3:${valueEffectShop.effectShop[13].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[13].timeRun}' +
                                        '/randomfiller:${valueEffectShop.effectShop[14].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[14].timeRun}' +
                                        '/ripples:${valueEffectShop.effectShop[15].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[15].timeRun}' +
                                        '/cubix:${valueEffectShop.effectShop[16].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[16].timeRun}' +
                                        '/filip:${valueEffectShop.effectShop[17].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[17].timeRun}' +
                                        '/sinelines:${valueEffectShop.effectShop[18].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[18].timeRun}' +
                                        '/stringfly:${valueEffectShop.effectShop[19].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[19].string}' +
                                        '/fireworks:${valueEffectShop.effectShop[20].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[20].timeRun}' +
                                        '/pathtext:${valueEffectShop.effectShop[21].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[21].string}'+
                                        '/ballbouncing:${valueEffectShop.effectShop[22].isLiked == true ? 1 : 0}:${valueEffectShop.effectShop[22].timeRun}' +
                                        '/brness:1:${valueEffectShop.brightness}' +
                                        '/';

                  print('press CONFIG DATA $_transmitESP32OK');
                  
                  sendJsonArrayToESP(jsonArrayData);
                  sendJsonArrayToESP(jsonArrayData);
                //  fetchDataResponse();
                   if( _transmitESP32OK  == true)
                    {
                        showDialog(context: context, builder: (context) => AlertDialog(
                                content: Text('Configured Successfully'),
                        ),);
                        _transmitESP32OK == false;
                    }
                    // else if( _transmitESP32OK  == false)
                    // {
                    //     showDialog(context: context, builder: (context) => AlertDialog(
                    //             title: Text('Configuration Failed'),
                    //             content: Text('Please connect to wifi "Cube 8x8x8 Banlinhkien"'),
                    //     ),);
                    // }
                  

                  
                  // 
              },),
              
            ],
          ),
        )
      ),
    ),);
  }
}