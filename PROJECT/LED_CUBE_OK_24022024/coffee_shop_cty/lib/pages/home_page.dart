import 'package:coffee_shop_cty/models/effect.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:coffee_shop_cty/pages/effect_detail_page.dart';
import 'package:coffee_shop_cty/pages/effect_detail_page_string.dart';
import 'package:coffee_shop_cty/pages/effect_tile.dart';
import 'package:coffee_shop_cty/pages/effect_tile_string.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, value, child) => SafeArea(
      child: Column(
        children: [
            Padding(
              padding: const EdgeInsets.all(15),
             child: Text('EFFECT CONFIG', style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 4
              ),),
            ),

            SizedBox(height: 10,),
            Expanded(
              child: ListView.builder(
                itemCount: value.effectShop.length,
                itemBuilder: (context, index) {
                Effect eachCoffee = value.effectShop[index];
                
                return index != 19 && index != 21 ? EffectTile(
                  effect: eachCoffee,
                  index: index,
                  pressDetailPage: () {
                    Navigator.push(context , MaterialPageRoute(builder:(context) => EffectDetailPage(
                      effect : eachCoffee, 
                      index: index,
                    )));
                  },
                  pressLike: () {
                    eachCoffee.isLiked = ! eachCoffee.isLiked;
                    print(eachCoffee.isLiked);
                    if( eachCoffee.isLiked == true)
                      Provider.of<EffectShop>(context as BuildContext, listen: false).setLikeEffect(index);
                    else
                      Provider.of<EffectShop>(context as BuildContext, listen: false).setUnLikeEffect(index);
                  },
                ) :  EffectTileString(
                  effect: eachCoffee,
                  index: index,
                  pressDetailPage: () {
                    Navigator.push(context , MaterialPageRoute(builder:(context) => EffectDetailPageString(
                      effect : eachCoffee, 
                      index: index,
                    )));
                  },
                  pressLike: () {
                    eachCoffee.isLiked = ! eachCoffee.isLiked;
                    print(eachCoffee.isLiked);
                    if( eachCoffee.isLiked == true)
                      Provider.of<EffectShop>(context as BuildContext, listen: false).setLikeEffect(index);
                    else
                      Provider.of<EffectShop>(context as BuildContext, listen: false).setUnLikeEffect(index);
                  },
                )
                
                ;
                
              },),
            )
          ],
        )
      )
    );
  }
}