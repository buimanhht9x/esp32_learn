
import 'package:coffee_shop_cty/components/MyButton2.dart';
import 'package:coffee_shop_cty/models/effect.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EffectDetailPageString extends StatefulWidget {
  final Effect effect;
  final int index;
  const EffectDetailPageString({super.key, required this.effect, required this.index});

  @override
  State<EffectDetailPageString> createState() => _EffectDetailPageStringState(index);
}

class _EffectDetailPageStringState extends State<EffectDetailPageString> {
  TextEditingController _controller = TextEditingController();
  int index;

  _EffectDetailPageStringState(this.index);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller.text = widget.effect.string!;
  }
  @override
  Widget build(BuildContext context) {
     return Consumer<EffectShop>(builder: (context, valueEffectShop, child) => 
     Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            foregroundColor: Colors.black,
            title: Text(widget.effect.name, style: TextStyle(
              
            ),),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
              Expanded(
                child: Image.asset(
                  widget.effect.imgPath,
                ),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                
                child: TextField(
                  controller: _controller,

                  onChanged: (value) {
                    print(value);
                     _controller.text = value;
                    //widget.effect.string = value;
                    Provider.of<EffectShop>(context as BuildContext, listen: false).setStringEffect(index,value);
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: widget.effect.string,
                  ),
                ),
              ),
              SizedBox(height: 40,),
              MyButton2(
                isLiked: widget.effect.isLiked,
                onTap: (){
                  widget.effect.isLiked = !widget.effect.isLiked;
                  print(widget.effect.name);
                  print(widget.effect.isLiked );
                  
                  if( widget.effect.isLiked  == true)
                      Provider.of<EffectShop>(context as BuildContext, listen: false).setLikeEffect(index);
                    else
                      Provider.of<EffectShop>(context as BuildContext, listen: false).setUnLikeEffect(index);
                },
              
              ),
              SizedBox(height: 40,),


          ],
        )
    ),);
  }
}

