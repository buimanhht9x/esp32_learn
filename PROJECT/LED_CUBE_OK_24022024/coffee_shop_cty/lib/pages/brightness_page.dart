import 'package:coffee_shop_cty/components/MySlider.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BrightnessPage extends StatefulWidget {
  const BrightnessPage({super.key});

  @override
  State<BrightnessPage> createState() => _BrightnessPageState();
}

class _BrightnessPageState extends State<BrightnessPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<EffectShop>(builder: (context, valueEffectShop, child) => SafeArea(

      child: Column(
        children: [
             Padding(
              padding: const EdgeInsets.all(15),
              child: Text('BRIGHTNESS', style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 4
              ),),
            ),
            SizedBox(height: 150,),
              Text((valueEffectShop.brightness >= 5 && valueEffectShop.brightness <= 100) ? (
                valueEffectShop.brightness).toString() : '5',
                style: TextStyle(
                  fontSize: 100,
                ),
              
              ),
              SizedBox(height: 220,),
              MySlider(valueSlider: (valueEffectShop.brightness).toDouble()),
          ],
        )
      )
    );
  }
}