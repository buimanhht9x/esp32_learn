import 'package:coffee_shop_cty/components/MyButton2.dart';
import 'package:coffee_shop_cty/components/MyNumberPicker.dart';
import 'package:coffee_shop_cty/models/effect.dart';
import 'package:coffee_shop_cty/models/effect_shop.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EffectDetailPage extends StatefulWidget {
  final Effect effect;
  final int index;
  const EffectDetailPage({super.key, required this.effect, required this.index});

  @override
  State<EffectDetailPage> createState() => _EffectDetailPageState(index);
}

class _EffectDetailPageState extends State<EffectDetailPage> {
  int index;
  _EffectDetailPageState(this.index);
  @override
  Widget build(BuildContext context) {
     return Consumer<EffectShop>(builder: (context, valueEffectShop, child) => 
     Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            foregroundColor: Colors.black,
            title: Text(widget.effect.name, style: TextStyle(
              
            ),),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
              Expanded(
                child: Image.asset(
                  widget.effect.imgPath,
                ),
              ),
               SizedBox(height: 40,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  MyButton2(
                    isLiked: widget.effect.isLiked,
                    onTap: (){
                      widget.effect.isLiked = !widget.effect.isLiked;
                      print(widget.effect.name);
                      print(widget.effect.isLiked );
                      
                      if( widget.effect.isLiked  == true)
                          Provider.of<EffectShop>(context as BuildContext, listen: false).setLikeEffect(index);
                        else
                          Provider.of<EffectShop>(context as BuildContext, listen: false).setUnLikeEffect(index);
                    },
                  
                  ),
                  SizedBox(width: 20,),
                  MyNumberPicker(
                    effect:widget.effect ,
                    index: index,
                    timeRun:widget.effect.timeRun,
                    
                  ),
                  
                ],
              ), SizedBox(height: 40,),

          ],
        )
    ),);
  }
}

