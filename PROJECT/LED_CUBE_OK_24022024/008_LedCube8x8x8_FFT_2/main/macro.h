#ifndef MACRO_H
#define MACRO_H

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include <led_strip_idf.h>
#include "math.h"
#include "esp_log.h"
#define OK   1
#define NOK  0

#define millis()  esp_log_timestamp()
#define sec()  esp_log_timestamp()/1000

void delay_ms(int time_delay);
int random_number(int minN, int maxN);
uint32_t map(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max);
#endif
