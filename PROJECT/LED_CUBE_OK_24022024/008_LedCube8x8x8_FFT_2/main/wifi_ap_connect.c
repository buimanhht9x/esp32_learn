/*
 *                            default handler              user handler
 *  -------------             ---------------             ---------------
 *  |           |   event     |             | callback or |             |
 *  |   tcpip   | --------->  |    event    | ----------> | application |
 *  |   stack   |             |     task    |  event      |    task     |
 *  |-----------|             |-------------|             |-------------|
 *                                  /|\                          |
 *                                   |                           |
 *                            event  |                           |
 *                                   |                           |
 *                                   |                           |
 *                             ---------------                   |
 *                             |             |                   |
 *                             | WiFi Driver |/__________________|
 *                             |             |\     API call
 *                             |             |
 *                             |-------------|
 *
 */


/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*  WiFi softAP Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "wifi_ap_connect.h"

/* The examples use WiFi configuration that you can set via project configuration menu.

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/


static const char *TAG = "wifi softAP";

static void event_handler(void* arg, esp_event_base_t event_base,int32_t event_id, void* event_data)
{
    // esp_wifi_types.h
    if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STACONNECTED )
    {
        ESP_LOGI(TAG, "device connected to esp");
    }
    else if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STADISCONNECTED )
    {
        ESP_LOGI(TAG, "device disconnected to esp");
    }
}

void wifi_sta_connect()
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ESP_LOGI(TAG, "ESP_WIFI_MODE_AP");

//                                         Wi-Fi/LwIP Init Phase
// s1.1: The main task calls esp_netif_init() to create an LwIP core task and initialize LwIP-related work6
   ESP_ERROR_CHECK(esp_netif_init());
// s1.2: The main task calls esp_event_loop_init() to create a system Event task and initialize an application event’s callback function. In the scenario above, the application event’s callback function does nothing but relaying the event to the application task.
   ESP_ERROR_CHECK(esp_event_loop_create_default());
// 1.3: The main task calls esp_netif_create_default_wifi_ap() or esp_netif_create_default_wifi_sta() to create default network interface instance binding station or AP with TCP/IP stack.
   esp_netif_create_default_wifi_ap();
// s1.4: The main task calls esp_wifi_init() to create the Wi-Fi driver task and initialize the Wi-Fi driver.
   wifi_init_config_t wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
   ESP_ERROR_CHECK(esp_wifi_init(&wifi_init_config));

  //.........................phase 2: Wi-Fi Configuration Phase................................
  
  // đăng kí event cho wifi
  ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,
                            ESP_EVENT_ANY_ID,
                            event_handler,
                            NULL));

  wifi_config_t wifi_config = {
        .ap = {
              .ssid = SSID,
              .password = PASS,
              .max_connection = AP_CONNNECT_MAX,
              .channel = CHANNEL,
              .authmode = WIFI_AUTH_WPA_WPA2_PSK,
              .ssid_len = strlen(SSID)
              }
  };


  if(strlen((char *)wifi_config.sta.password) == 0 )
  {
      wifi_config.ap.authmode = WIFI_AUTH_OPEN;
  }
  
  


   ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP,&wifi_config));

// ............................phase 3:   Wi-Fi Start Phase............................
//  s3.1: Call esp_wifi_start to start the Wi-Fi driver.
   ESP_ERROR_CHECK(esp_wifi_start());

   ESP_LOGI(TAG,"wifi have ssid: %s, pass: %s",(char *)wifi_config.sta.ssid,(char *)wifi_config.sta.password);


  // hủy đăng ký các trình xử lý sự kiện khi chúng không còn cần thiết nữa hoặc khi thành phần 
  // sử dụng chúng sắp bị hủy tải hoặc vô hiệu hóa. Việc hủy đăng ký trình xử lý sự kiện đảm
  // bảo rằng mọi tài nguyên được phân bổ liên kết với trình xử lý sự kiện đều được giải phóng
  // và tránh rò rỉ bộ nhớ tiềm ẩn.
  esp_event_handler_unregister(WIFI_EVENT,ESP_EVENT_ANY_ID,event_handler);

//  s3.2: The Wi-Fi driver posts <WIFI_EVENT_STA_START> to the event task; then, the event task will do some common things and will call the application event callback function.

//  s3.3: The application event callback function relays the <WIFI_EVENT_STA_START> to the application task. We recommend that you call esp_wifi_connect(). However, you can also call esp_wifi_connect() in other phrases after the <WIFI_EVENT_STA_START> arises.
    //Initialize NVS

}
