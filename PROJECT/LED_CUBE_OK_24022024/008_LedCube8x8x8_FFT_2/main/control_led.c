#include "control_led.h"

// -------------- Hàm kiểm tra tọa độ z,y,x có hợp lệ k -----------------
// return 1 nếu hợp lệ
uint8_t inrange(int z, int y, int x) 
{
    if (x>=0 && x<CUBE_SIZE && y>=0 && y<CUBE_SIZE && z>=0 && z<CUBE_SIZE) 
        return 1;
    else 
        return 0;
}

// ---------------- Hàm show 1 điểm ảnh tọa độ z,y,x màu rgb ra khối CUBE ------------
void set_pixel_zyx(control_led_cube_t *control_led_cube, uint8_t z,uint8_t y, uint8_t x, rgb_t rgb)
{
    if(inrange(z,y,x)){
        if(z %2 == 0)
            led_strip_set_pixel(&control_led_cube->strip, z*8 + x*64 + y,  rgb);
        else if(z %2 == 1)
            led_strip_set_pixel(&control_led_cube->strip, z*8 + x*64 + 7 - y,  rgb);
    }  
}

// ---------------- Hàm clear 1 điểm ảnh tọa độ z,y,x màu rgb ra khối CUBE ------------
void clear_pixel_zyx(control_led_cube_t *control_led_cube, uint8_t z,uint8_t y, uint8_t x)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            led_strip_set_pixel(&control_led_cube->strip, z*8 + x*64 + y,       (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 });
        else if(z %2 == 1)
            led_strip_set_pixel(&control_led_cube->strip, z*8 + x*64 + 7 - y,   (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 });
    }
}

// ------- Hàm đẩy data RGB của 1 điểm ảnh có tọa độ x,y,z vào mảng layer_buffer----------------------------
void set_color_to_buffer(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x, rgb_t data)
{
    if(inrange(z,y,x))
    {
        if(z %2 == 0)
            control_led_cube->layerRGB_buffer[z*8 + x*64 + y] =  data;
        else if(z %2 == 1)
            control_led_cube->layerRGB_buffer[z*8 + x*64 + 7 - y] =  data;
    }
}

// ------------------ Hàm clear khối CUBE ----------------------------
void clear_layer_buffer(control_led_cube_t *control_led_cube)
{
    for(int i = 0; i<512; i++) 
        control_led_cube->layerRGB_buffer[i] = (rgb_t){ .r = 0x00, .g = 0x00, .b = 0x00 };
}

void set_color_setvoxel(control_led_cube_t *control_led_cube, char rs, char gs, char bs)
{
    (control_led_cube->rgb_setvoxel).r = rs;
    (control_led_cube->rgb_setvoxel).g = gs;
    (control_led_cube->rgb_setvoxel).b = bs;
}


void setvoxel(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)) {
        set_color_to_buffer(control_led_cube, z,y,x,control_led_cube->rgb_setvoxel);
        control_led_cube->layerONOFF_buffer[z][y][x] = 1;
    }
}
void clrvoxel(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)){
        set_color_to_buffer(control_led_cube, z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
        control_led_cube->layerONOFF_buffer[z][y][x] = 0;
    }
}
uint8_t getvoxel(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x) {
    if (inrange(z, y, x)) {
        if (control_led_cube->layerONOFF_buffer[z][y][x]==1) 
            return 1;
        else
            return 0;
    } 
    else 
        return 0;
}

void setplane_x(control_led_cube_t *control_led_cube, uint8_t x) {
    uint8_t z, y;

    if (x>=0 && x<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++) {
                control_led_cube->layerONOFF_buffer[z][y][x]=1;
                set_color_to_buffer(control_led_cube, z,y,x,control_led_cube->rgb_setvoxel);
            }
        }
    }
}
void clrplane_x(control_led_cube_t *control_led_cube, uint8_t x) {
    uint8_t z, y;

    if (x>=0 && x<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++) {
                control_led_cube->layerONOFF_buffer[z][y][x]=0;
                set_color_to_buffer(control_led_cube, z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
            }
        }
    }
}

void setplane_y(control_led_cube_t *control_led_cube, uint8_t y) {
    uint8_t z, x;
    if (y>=0 && y<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (x=0; x<CUBE_SIZE; x++) {
                control_led_cube->layerONOFF_buffer[z][y][x]=1;
                set_color_to_buffer(control_led_cube, z,y,x,control_led_cube->rgb_setvoxel);
            }
        }
    }
}
void clrplane_y(control_led_cube_t *control_led_cube, uint8_t y) {
    uint8_t z, x;
    if (y>=0 && y<CUBE_SIZE) {
        for (z=0; z<CUBE_SIZE; z++) {
            for (x=0; x<CUBE_SIZE; x++) {
                control_led_cube->layerONOFF_buffer[z][y][x]=0x00;
                set_color_to_buffer(control_led_cube, z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
            }
        }
    }
}

void fill_layer(control_led_cube_t *control_led_cube, uint8_t layer, uint8_t onoff) {
    uint8_t y, x;

    for (y=0; y<CUBE_SIZE; y++) {
        for (x=0; x<CUBE_SIZE; x++) {
            control_led_cube->layerONOFF_buffer[layer][y][x]= onoff;
            set_color_to_buffer(control_led_cube, layer, y, x, control_led_cube->rgb_setvoxel);
        }
    }
}

void fill_cube(control_led_cube_t *control_led_cube, uint8_t onoff) {
    uint8_t z, y, x;
    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if(onoff == 0)
                {
                    control_led_cube->layerONOFF_buffer[z][y][x]=0;
                    set_color_to_buffer(control_led_cube, z,y,x,(rgb_t){ .r = 0, .g = 0, .b = 0 });
                }
                else
                {
                    control_led_cube->layerONOFF_buffer[z][y][x]=1;
                    set_color_to_buffer(control_led_cube, z,y,x,control_led_cube->rgb_setvoxel);
                }
               
            }
        }
    }
}

void setplane(control_led_cube_t *control_led_cube, uint8_t axis, uint8_t i) {
    switch (axis) {
        case 1: //AXIS_Z:
            fill_layer(control_led_cube, i, 1);
            break;
        case 2: //AXIS_Y:
            setplane_y(control_led_cube, i);
            break;
        case 3: //AXIS_X:
            setplane_x(control_led_cube, i);
            break;
    }
}

void clrplane(control_led_cube_t *control_led_cube, uint8_t axis, uint8_t i) {
    switch (axis) {
        case 1: //AXIS_Z:
            fill_layer(control_led_cube, i, 0);
            break;
        case 2: //AXIS_Y:
            clrplane_y(control_led_cube, i);
            break;
        case 3: //AXIS_X:
            clrplane_x(control_led_cube, i);
            break;
    }
}
void altervoxel(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x, uint8_t state) {
    if (inrange(z, y, x)) {
        if (state==1) {
            setvoxel(control_led_cube, z, y, x);
        }
        if (state==0) {
            clrvoxel(control_led_cube, z, y, x);
        }
    }
}
void shift(control_led_cube_t *control_led_cube, char axis, char direction) {
    char z, y, x, ii, iii, state;

    if (axis==1) // AXIS_Z - 1
    {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0){
                        ii=z;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - z;
                        iii=ii - 1;
                    }
                    state=getvoxel(control_led_cube, iii, y, x);
                    altervoxel(control_led_cube, ii, y, x, state);
                }
            }
        }
    }
    if (axis==2) // AXIS_Y - 2
    {
        for (z=0; z<CUBE_SIZE; z++) {
             for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) { 
                        ii=(CUBE_SIZE - 1) - y;
                        iii=ii - 1;
                    } else {
                        ii=y;
                        iii=ii + 1;
                    }
                    state=getvoxel(control_led_cube, z, iii, x);
                     altervoxel(control_led_cube,z, ii, x, state);
                }
            }
        }
    }
    if (axis==3) // AXIS_X - 3
    {
        for (z=0; z<CUBE_SIZE; z++) {
            for (y=0; y<CUBE_SIZE; y++){
                for (x=0; x<CUBE_SIZE; x++) {
                    if (direction==0) { 
                        ii=x;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - x;
                        iii=ii - 1;
                    }
                    state=getvoxel(control_led_cube, z, y, iii);
                     altervoxel(control_led_cube, z, y, ii, state);
                }
            }
        }
    }
    if (axis==4) // AXIS_X - 3
    {
        for (z=CUBE_SIZE-1; z>=0; z--) {
             for (y=CUBE_SIZE-1; y>=0; y--) {
                 for (x=CUBE_SIZE-1; x>=0; x--) {
                    if (direction==0) { 
                        ii=x;
                        iii=ii + 1;
                    } else {
                        ii=(CUBE_SIZE - 1) - x;
                        iii=ii - 1;
                    }
                    state=getvoxel(control_led_cube, z, y, iii);
                    altervoxel(control_led_cube,z, y, ii, state);
                }
            }
        }
    }
}

void sendvoxel_axis(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x, char axis, int delay) {
    int i, ii;

    if (axis==1) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (z==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(control_led_cube, ii + 1, y, x);
            } else {
                ii=i;
                clrvoxel(control_led_cube, ii - 1, y, x);
            }
            setvoxel(control_led_cube , ii, y, x);
            delay_ms(delay);
        }
    }

    if (axis==2) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (y==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(control_led_cube, z, ii + 1, x);
            } else {
                ii=i;
                clrvoxel(control_led_cube, z, ii - 1, x);
            }
            setvoxel(control_led_cube, z, ii, x);
            delay_ms(delay);
        }
    }

    if (axis==3) {
        for (i=0; i<CUBE_SIZE; i++) {
            if (x==(CUBE_SIZE - 1)) {
                ii=(CUBE_SIZE - 1) - i;
                clrvoxel(control_led_cube, z, y, ii + 1);
            } else {
                ii=i;
                clrvoxel(control_led_cube, z, y, ii - 1);
            }
            setvoxel(control_led_cube, z, y, ii);
            delay_ms(delay);
        }
    }
}
void argorder(uint8_t ix1, uint8_t ix2, uint8_t *ox1, uint8_t *ox2) {
    if (ix1>ix2) {
        char tmp;
        tmp=ix1;
        ix1=ix2;
        ix2=tmp;
    }
    *ox1=ix1;
    *ox2=ix2;
}


void box_wireframe(control_led_cube_t *control_led_cube, uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2) {
    uint8_t iz, iy, ix;

    argorder(x1, x2, &x1, &x2);
    argorder(y1, y2, &y1, &y2);
    argorder(z1, z2, &z1, &z2);

    for (iz=z1; iz<=z2; iz++) {
        for (iy=y1; iy<=y2; iy++) {
            for (ix=x1; ix<=x2; ix++) {
                if ((iz>=z1 && iz<=z2 && iy==y1 && ix==x1) || // ïðÿìàÿ (z1,y1,x1)-(z2,y1,x1)
                        (iz>=z1 && iz<=z2 && iy==y2 && ix==x1) || // ïðÿìàÿ (z1,y2,x1)-(z2,y2,x1)
                        (iz>=z1 && iz<=z2 && iy==y1 && ix==x2) || // ïðÿìàÿ (z1,y1,x2)-(z2,y1,x2)
                        (iz>=z1 && iz<=z2 && iy==y2 && ix==x2) || // ïðÿìàÿ (z1,y2,x2)-(z2,y2,x2)

                        (iz==z1 && iy==y1 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z1,y1,x1)-(z1,y1,x2)
                        (iz==z1 && iy==y2 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z1,y2,x1)-(z1,y2,x2)
                        (iz==z2 && iy==y1 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z2,y1,x1)-(z1,y1,x2)
                        (iz==z2 && iy==y2 && ix>=x1 && ix<=x2) || // ïðÿìàÿ (z2,y2,x1)-(z1,y2,x2)

                        (iz==z1 && iy>=y1 && iy<=y2 && ix==x1) || // ïðÿìàÿ (z1,y1,x1)-(z1,y2,x1)
                        (iz==z1 && iy>=y1 && iy<=y2 && ix==x2) || // ïðÿìàÿ (z1,y1,x2)-(z1,y2,x2)
                        (iz==z2 && iy>=y1 && iy<=y2 && ix==x1) || // ïðÿìàÿ (z2,y1,x1)-(z2,y2,x1)
                        (iz==z2 && iy>=y1 && iy<=y2 && ix==x2)) // ïðÿìàÿ (z2,y1,x2)-(z2,y2,x2)
                {
                    setvoxel(control_led_cube, iz, iy, ix);
                    
                } else {
                    clrvoxel(control_led_cube, iz, iy, ix);
                }
            }
        }
    }
}

void cube2buffer(control_led_cube_t *control_led_cube) {
    uint8_t z, y, x;

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                control_led_cube->layerONOFF_miror[z][y][x] = control_led_cube->layerONOFF_buffer[z][y][x];
            }
        }
    }
}


void mirror_z(control_led_cube_t *control_led_cube) {
    uint8_t z, y, x;
    cube2buffer(control_led_cube); 
    fill_cube(control_led_cube, 0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (control_led_cube->layerONOFF_miror[z][y][x]==1)
                    setvoxel(control_led_cube, CUBE_SIZE - 1 - z, y, x);
            }
        }
    }
}

void mirror_y(control_led_cube_t *control_led_cube) {
    uint8_t z, y, x;
    cube2buffer(control_led_cube); 
    fill_cube(control_led_cube, 0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (control_led_cube->layerONOFF_miror[z][y][x]==1)
                    setvoxel(control_led_cube, z, CUBE_SIZE - 1 - y, x);
            }
        }
    }
}

void mirror_x(control_led_cube_t *control_led_cube) {
    uint8_t z, y, x;
    cube2buffer(control_led_cube); 
    fill_cube(control_led_cube, 0x00);

    for (z=0; z<CUBE_SIZE; z++) {
        for (y=0; y<CUBE_SIZE; y++) {
            for (x=0; x<CUBE_SIZE; x++) {
                if (control_led_cube->layerONOFF_miror[z][y][x]==1)
                    setvoxel(control_led_cube, z, y, CUBE_SIZE - 1 - x);
            }
        }
    }
}

void draw_positions_axis(control_led_cube_t *control_led_cube, char axis, unsigned char positions[CUBE_SIZE*CUBE_SIZE], int invert) {
    int x, y, p;
    fill_cube(control_led_cube, 0x00);

    for (x=0; x<CUBE_SIZE; x++) {
        for (y=0; y<CUBE_SIZE; y++) {
            if (invert) {
                p=(CUBE_SIZE - 1) - positions[x * CUBE_SIZE + y];
            } else {
                p=positions[x * CUBE_SIZE + y];
            }
            if (axis==1) // AXIS_Z
                setvoxel(control_led_cube, p, y, x); // (x,y,p)

            if (axis==2) // AXIS_Y
                setvoxel(control_led_cube, y, p, x); // (x,p,y)

            if (axis==3) // AXIS_X
                setvoxel(control_led_cube, x, y, p); // (p,y,x)
           // change_rgb_color(control_led_cube);
        }
    }
}

void line(control_led_cube_t *control_led_cube, char z1, char y1, char x1, char z2, char y2, char x2) {
    char x, y, z, last_y, last_z;
    float xy, xz; 

    if (inrange(z1, y1, x1) & inrange(z2, y2, x2)) {
        if (x1>x2) {
            char tmp;
            tmp=x2;
            x2=x1;
            x1=tmp;
            tmp=y2;
            y2=y1;
            y1=tmp;
            tmp=z2;
            z2=z1;
            z1=tmp;
        }
        if (y1>y2) {
            xy=(float) (y1 - y2) / (float) (x2 - x1);
            last_y=y2;
        } else {
            xy=(float) (y2 - y1) / (float) (x2 - x1);
            last_y=y1;
        }

        if (z1>z2) {
            xz=(float) (z1 - z2) / (float) (x2 - x1);
            last_z=z2;
        } else {
            xz=(float) (z2 - z1) / (float) (x2 - x1);
            last_z=z1;
        }
        for (x=x1; x<=x2; x++) {
            y=(xy * (x - x1)) + y1;
            z=(xz * (x - x1)) + z1;
            setvoxel(control_led_cube, z, y, x);
        }
    }
}

void box_filled(control_led_cube_t *control_led_cube, uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2) {
    uint8_t iz, iy, ix;

    argorder(x1, x2, &x1, &x2);
    argorder(y1, y2, &y1, &y2);
    argorder(z1, z2, &z1, &z2);

    for (iz=z1; iz<=z2; iz++) {
        for (iy=y1; iy<=y2; iy++) {
            for (ix=x1; ix<=x2; ix++) {
                control_led_cube->layerONOFF_buffer[iz][iy][ix] = 1;
                setvoxel(control_led_cube, iz, iy, ix);
            }
        }
    }
}

void set_brightness(control_led_cube_t *control_led_cube, int percentage)
{
    if(percentage < 0 ) percentage = 0;
    else if( percentage > 100) percentage = 100;
    int value = map(percentage, 0,100,0,255);
    control_led_cube->strip.brightness = value;
}

rgb_t Scroll(int pos) {
	rgb_t color = { .r = 255, .g = 0, .b = 0 };
	if(pos < 85) {
		color.g = 0;
		color.r = ((float)pos / 85.0f) * 255.0f;
		color.b = 255 - color.r;
	} 
    else if(pos < 170) {
		color.g = ((float)(pos - 85) / 85.0f) * 255.0f;
		color.r = 255 - color.g;
		color.b = 0;
	} 
    else if(pos < 256) {
		color.b = ((float)(pos - 170) / 85.0f) * 255.0f;
		color.g = 255 - color.b;
		color.r = 1;
	}
	return color;
}

void change_rgb_color(control_led_cube_t *control_led_cube)
{
    static char scroll;
    rgb_t rgb = Scroll(scroll);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    scroll++;
}

void change_rgb_color_2(control_led_cube_t *control_led_cube)
{
    static char scroll;
    rgb_t rgb = Scroll(scroll);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    scroll = scroll + 30;
}

void showAllCollumm(control_led_cube_t *control_led_cube, short colummHeight[])
{  
    // columm 0
    for(int y = 6; y <= 7; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 0; z <= colummHeight[0]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 6; y <= 7; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 7; z > colummHeight[0]; z --)
                clrvoxel(control_led_cube, z,y,x);
    rgb_t rgb = Scroll(0*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 1
    for(int y = 4; y <= 5; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 0; z <= colummHeight[1]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 4; y <= 5; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 7; z > colummHeight[1]; z --)
                clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(1*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 2 
    for(int y = 2; y <= 3; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 0; z <= colummHeight[2]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 2; y <= 3; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 7; z > colummHeight[2]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(2*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 3 
    for(int y = 0; y <= 1; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 0; z <= colummHeight[3]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 0; y <= 1; y++)
        for(int x = 0; x <=1; x++)
            for(int z = 7; z > colummHeight[3]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(3*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 4
    for(int x = 2; x <= 3; x++)
        for(int y = 0; y <=1; y++)
            for(int z = 0; z <= colummHeight[4]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int x = 2; x <= 3; x++)
        for(int y = 0; y <=1; y++)
            for(int z = 7; z > colummHeight[4]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(4*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 5  
    for(int x = 4; x <= 5; x++)
        for(int y = 0; y <=1; y++)
            for(int z = 0; z <= colummHeight[5]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int x = 4; x <= 5; x++)
        for(int y = 0; y <=1; y++)
            for(int z = 7; z > colummHeight[5]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(5*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 6 
    for(int x = 6; x <= 7; x++)
        for(int y = 0; y <= 1; y++)
            for(int z = 0; z <= colummHeight[6]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 0; y <=1; y++)
        for(int x = 6; x <= 7; x++)
            for(int z = 7; z > colummHeight[6]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(6*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 7 
    for(int y = 2; y <= 3; y++)
        for(int x = 6; x <=7; x++)
            for(int z = 0; z <= colummHeight[7]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 2; y <= 3; y++)
        for(int x = 6; x <=7; x++)
            for(int z = 7; z > colummHeight[7]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(7*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 8 
    for(int y = 4; y <= 5; y++)
        for(int x = 6; x <=7; x++)
            for(int z = 0; z <= colummHeight[8]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 4; y <= 5; y++)
        for(int x = 6; x <=7; x++)
            for(int z = 7; z > colummHeight[8]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(8*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 9 
    for(int y = 6; y <= 7; y++)
        for(int x = 6; x <=7; x++)
            for(int z = 0; z <= colummHeight[9]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 6; y <= 7; y++)
        for(int x = 6; x <=7; x++)
            for(int z = 7; z > colummHeight[9]; z --)
                clrvoxel(control_led_cube, z,y,x);
     rgb = Scroll(9*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 10 
    for(int x = 4; x <= 5; x++)
        for(int y = 6; y <=7; y++)
            for(int z = 0; z <= colummHeight[10]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int x = 4; x <= 5; x++)
        for(int y = 6; y <=7; y++)
            for(int z = 7; z > colummHeight[10]; z --)
                clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(10*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 11
    for(int x = 2; x <= 3; x++)
        for(int y = 6; y <=7; y++)
            for(int z = 0; z <= colummHeight[11]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int x = 2; x <= 3; x++)
        for(int y = 6; y <=7; y++)
            for(int z = 7; z > colummHeight[11]; z --)
                clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(11*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 12   
    for(int y = 4; y <= 5; y++)
        for(int x = 2; x <=3; x++)
            for(int z = 0; z <= colummHeight[12]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 4; y <= 5; y++)
        for(int x = 2; x <=3; x++)
            for(int z = 7; z > colummHeight[12]; z --)
                clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(12*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 13
    for(int y = 2; y <= 3; y++)
        for(int x = 2; x <=3; x++)
            for(int z = 0; z <= colummHeight[13]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 2; y <= 3; y++)
        for(int x = 2; x <=3; x++)
            for(int z = 7; z > colummHeight[13]; z --)
                clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(13*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 14  
    for(int y = 2; y <= 3; y++)
        for(int x = 4; x <=5; x++)
            for(int z = 0; z <= colummHeight[14]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 2; y <= 3; y++)
        for(int x = 4; x <=5; x++)
                for(int z = 7; z > colummHeight[14]; z --)
                    clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(14*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
    // columm 15
    for(int y = 4; y <= 5; y++)
        for(int x = 4; x <=5; x++)
            for(int z = 0; z <= colummHeight[15]; z ++)
                setvoxel(control_led_cube, z,y,x);
    for(int y = 4; y <= 5; y++)
        for(int x = 4; x <=5; x++)
                for(int z = 7; z > colummHeight[15]; z --)
                    clrvoxel(control_led_cube, z,y,x);
    rgb = Scroll(15*15);
    set_color_setvoxel(control_led_cube, rgb.r, rgb.g, rgb.b);
}

// ------------------ Hàm show led ra khối CUBE -----------------------
void show_led(control_led_cube_t *control_led_cube)
{
    ESP_ERROR_CHECK(led_strip_set_pixels(&control_led_cube->strip, 0, 512, control_led_cube->layerRGB_buffer));
    ESP_ERROR_CHECK(led_strip_flush(&control_led_cube->strip));
}
