#ifndef CONTROL_LED_H
#define CONTROL_LED_H

#include "macro.h"
#include <led_strip_idf.h>

#define CUBE_SIZE 8

typedef struct 
{
    led_strip_t strip;
    rgb_t layerRGB_buffer[512];
    uint8_t layerONOFF_buffer[8][8][8];
    uint8_t layerONOFF_miror[8][8][8];
    rgb_t rgb_setvoxel;
    uint32_t time_start ;
    uint8_t restart_value;
}control_led_cube_t;

uint8_t inrange(int z, int y, int x) ;
void set_color_to_buffer(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x, rgb_t rgb);
void clear_layer_buffer(control_led_cube_t *control_led_cube);
void set_color_setvoxel(control_led_cube_t *control_led_cube, char rs, char gs, char bs);
void setvoxel(control_led_cube_t *control_led_cube,  uint8_t z, uint8_t y, uint8_t x);
void clrvoxel(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x) ;
uint8_t getvoxel(control_led_cube_t *control_led_cube,uint8_t z, uint8_t y, uint8_t x);
void setplane_x(control_led_cube_t *control_led_cube, uint8_t x);
void clrplane_x(control_led_cube_t *control_led_cube, uint8_t x) ;
void setplane_y(control_led_cube_t *control_led_cube, uint8_t y);
void clrplane_y(control_led_cube_t *control_led_cube, uint8_t y);
void fill_layer(control_led_cube_t *control_led_cube, uint8_t layer, uint8_t onoff);
void fill_cube(control_led_cube_t *control_led_cube, uint8_t onoff);
void setplane(control_led_cube_t *control_led_cube, uint8_t axis, uint8_t i);
void clrplane(control_led_cube_t *control_led_cube, uint8_t axis, uint8_t i);
void altervoxel(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x, uint8_t state);
void shift(control_led_cube_t *control_led_cube, char axis, char direction);
void sendvoxel_axis(control_led_cube_t *control_led_cube, uint8_t z, uint8_t y, uint8_t x, char axis, int delay);
void argorder(uint8_t ix1, uint8_t ix2, uint8_t *ox1, uint8_t *ox2);
void box_wireframe(control_led_cube_t *control_led_cube, uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2);
void cube2buffer(control_led_cube_t *control_led_cube);
void mirror_z(control_led_cube_t *control_led_cube);
void mirror_y(control_led_cube_t *control_led_cube);
void mirror_x(control_led_cube_t *control_led_cube);
void draw_positions_axis(control_led_cube_t *control_led_cube, char axis, unsigned char positions[CUBE_SIZE*CUBE_SIZE], int invert);
void line(control_led_cube_t *control_led_cube, char z1, char y1, char x1, char z2, char y2, char x2);
void box_filled(control_led_cube_t *control_led_cube, uint8_t z1, uint8_t y1, uint8_t x1, uint8_t z2, uint8_t y2, uint8_t x2);
void change_rgb_color(control_led_cube_t *control_led_cube);
void change_rgb_color_2(control_led_cube_t *control_led_cube);
void set_brightness(control_led_cube_t *control_led_cube, int percentage);
void showAllCollumm(control_led_cube_t *control_led_cube, short colummHeight[]);
void show_led(control_led_cube_t *control_led_cube);

#endif
