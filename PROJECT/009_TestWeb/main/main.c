#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include <led_strip_idf.h>
#include "math.h"
#include "esp_log.h"
#include "wifi_sta_connect.h"
#include "nvs_flash.h"
#include "esp_http_server.h"
#include "macro.h"
wifi_ap_connect(void);

void get_data_config_effect(char * input_string);
void print_data_effect_config();

// Khai báo file html
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");
SemaphoreHandle_t semaphorehandle_data_eff_config = NULL;

char buffer_data_eff_config[1400];
#define OK   1
#define NOK  0
char content[500];
esp_err_t app_to_esp_data_eff_config_handler(httpd_req_t* req)
{  
    
    /* Truncate if content length larger than the buffer */
    
    size_t recv_size ;
    if(req->content_len <sizeof(content))
    {
        recv_size = req->content_len;
    }
    else
    {
        recv_size = sizeof(content);
    }

    int ret = httpd_req_recv(req, content, recv_size);
    if (ret <= 0) {  /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        }
        /* In case of error, returning ESP_FAIL will
         * ensure that the underlying socket is closed */
        return ESP_FAIL;
    }
    printf("%s\n",content);
    xSemaphoreGiveFromISR(semaphorehandle_data_eff_config, NULL);
   
    /* Send a simple response */
    const char resp[] = "URI POST Response";
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}


typedef struct{
    int state;
    int time_run;
    char *stringbuff; 
}effect_config_t;
effect_config_t rain_config;
effect_config_t rainbow_config;
effect_config_t planboing_config;
effect_config_t sendvoxelsrandz_config;
effect_config_t sendvoxelsrandy_config;
effect_config_t boxshrinkgrow_config;
effect_config_t boxwoopwoop_config;
effect_config_t axiszupdownrs_config;
effect_config_t axisyupdownrs_config;
effect_config_t sidewaves_config;
effect_config_t wormsqueeze_config;
effect_config_t boingboing1_config;
effect_config_t boingboing2_config;
effect_config_t boingboing3_config;
effect_config_t randomfiller_config;
effect_config_t ripples_config;
effect_config_t cubix_config;
effect_config_t filip_filop_config;
effect_config_t sinelines_config;
effect_config_t stringfly_config;
effect_config_t fireworks_config;
effect_config_t path_text_config;
effect_config_t ball_bouncing_config;
#include <string.h>
void init_effect_config()
{
    rain_config.state = 1 ;rain_config.time_run = 1 ; 
    rainbow_config.state = 1 ;rainbow_config.time_run = 1 ; 
    planboing_config.state = 1 ;planboing_config.time_run = 1 ; 
    sendvoxelsrandz_config.state = 1 ;sendvoxelsrandz_config.time_run = 1 ; 
    sendvoxelsrandy_config.state = 1 ;sendvoxelsrandy_config.time_run = 1 ; 
    boxshrinkgrow_config.state = 1 ;boxshrinkgrow_config.time_run = 1 ; 
    boxwoopwoop_config.state = 1 ;boxwoopwoop_config.time_run = 1 ; 
    axiszupdownrs_config.state = 1 ;axiszupdownrs_config.time_run = 1 ; 
    axisyupdownrs_config.state = 1 ;axisyupdownrs_config.time_run = 1 ; 
    sidewaves_config.state = 1 ;sidewaves_config.time_run = 1 ; 
    wormsqueeze_config.state = 1 ;wormsqueeze_config.time_run = 1 ; 
    wormsqueeze_config.state = 1 ;wormsqueeze_config.time_run = 1 ; 
    boingboing1_config.state = 1 ;boingboing1_config.time_run = 1 ; 
    boingboing2_config.state = 1 ;boingboing2_config.time_run = 1 ; 
    boingboing3_config.state = 1 ;boingboing3_config.time_run = 1 ; 
    randomfiller_config.state = 1 ;randomfiller_config.time_run = 1 ; 
    ripples_config.state = 1 ;ripples_config.time_run = 1 ; 
    cubix_config.state = 1 ;cubix_config.time_run = 1 ; 
    filip_filop_config.state = 1 ;filip_filop_config.time_run = 1 ; 
    sinelines_config.state = 1 ;sinelines_config.time_run = 1 ; 
    stringfly_config.state = 1 ;stringfly_config.time_run = 1 ; 
   // strcpy(stringfly_config.stringbuff,"banlinhkien");
    fireworks_config.state = 1 ;fireworks_config.time_run = 1 ; 
    path_text_config.state = 1 ;path_text_config.time_run = 1 ;
   // strcpy(path_text_config.stringbuff,"banlinhkien");
    ball_bouncing_config.state = 1 ;ball_bouncing_config.time_run = 1 ; 
}

uint8_t brightness_value = 50;




void get_data_config_effect(char * input_string)
{
    char *token1, *token2;
    char *rest = input_string;
    char data_eff[100][20];
    // Mảng để lưu các chuỗi con sau khi tách bằng kí tự '/'
    char substrings[20][100]; // Điều này tùy thuộc vào độ dài tối đa của chuỗi con
    int i = 0;
    int j = 0;
    while ((token1 = strtok_r(rest, "/", &rest))) {
        // Lưu chuỗi con vào mảng substrings
        strcpy(substrings[i], token1);
        // Tách chuỗi con thành các phần bằng kí tự ':'
        token2 = strtok(substrings[i], ":");
        // In ra các phần bằng kí tự ':'
        while (token2 != NULL) {
            strcpy(data_eff[j], token2);
            j++;
         //   printf("%s\n", token2);
            token2 = strtok(NULL, ":");
        }
        i++;
    }
    rain_config.state = atoi(data_eff[1]);              rain_config.time_run = atoi(data_eff[2]);
    rainbow_config.state = atoi(data_eff[4]);           rainbow_config.time_run = atoi(data_eff[5]);
    planboing_config.state = atoi(data_eff[7]);         planboing_config.time_run = atoi(data_eff[8]);
    sendvoxelsrandz_config.state = atoi(data_eff[10]);  sendvoxelsrandz_config.time_run = atoi(data_eff[11]);
    sendvoxelsrandy_config.state = atoi(data_eff[13]);  sendvoxelsrandy_config.time_run = atoi(data_eff[14]);
    boxshrinkgrow_config.state = atoi(data_eff[16]);    boxshrinkgrow_config.time_run = atoi(data_eff[17]);
    boxwoopwoop_config.state = atoi(data_eff[19]);      boxwoopwoop_config.time_run = atoi(data_eff[20]);
    axiszupdownrs_config.state = atoi(data_eff[22]);    axiszupdownrs_config.time_run = atoi(data_eff[23]);
    axisyupdownrs_config.state = atoi(data_eff[25]);    axisyupdownrs_config.time_run = atoi(data_eff[26]);
    sidewaves_config.state = atoi(data_eff[28]);        sidewaves_config.time_run = atoi(data_eff[29]);
    wormsqueeze_config.state = atoi(data_eff[31]);      wormsqueeze_config.time_run = atoi(data_eff[32]);
    boingboing1_config.state = atoi(data_eff[34]);      boingboing1_config.time_run = atoi(data_eff[35]);
    boingboing2_config.state = atoi(data_eff[37]);      boingboing2_config.time_run = atoi(data_eff[38]);
    boingboing3_config.state = atoi(data_eff[40]);      boingboing3_config.time_run = atoi(data_eff[41]);
    randomfiller_config.state = atoi(data_eff[43]);     randomfiller_config.time_run = atoi(data_eff[44]);
    ripples_config.state = atoi(data_eff[46]);          ripples_config.time_run = atoi(data_eff[47]);
    cubix_config.state = atoi(data_eff[49]);            cubix_config.time_run = atoi(data_eff[50]);
    filip_filop_config.state = atoi(data_eff[52]);      filip_filop_config.time_run = atoi(data_eff[53]);
    sinelines_config.state = atoi(data_eff[55]);        sinelines_config.time_run = atoi(data_eff[56]);
    stringfly_config.state = atoi(data_eff[58]);        stringfly_config.stringbuff = data_eff[59];
    fireworks_config.state = atoi(data_eff[61]);        fireworks_config.time_run = atoi(data_eff[62]);
    path_text_config.state = atoi(data_eff[64]);        path_text_config.stringbuff = data_eff[65];
    ball_bouncing_config.state = atoi(data_eff[67]);    ball_bouncing_config.time_run = atoi(data_eff[68]);
    brightness_value = atoi(data_eff[71]);
}
// ------------------------- Hàm in data config effect led ra Serial----------------------------------------
void print_data_effect_config()
{
    printf("rain_config.state : %d\n", rain_config.state);                          printf("rain_config.time_run : %d\n", rain_config.time_run);
    printf("rainbow_config.state : %d\n", rainbow_config.state);                    printf("rainbow_config.time_run : %d\n", rainbow_config.time_run);
    printf("planboing_config.state : %d\n", planboing_config.state);                printf("planboing_config.time_run : %d\n", planboing_config.time_run);
    printf("sendvoxelsrandz_config.state : %d\n", sendvoxelsrandz_config.state);    printf("sendvoxelsrandz_config.time_run : %d\n", sendvoxelsrandz_config.time_run);
    printf("sendvoxelsrandy_config.state : %d\n", sendvoxelsrandy_config.state);    printf("sendvoxelsrandy_config.time_run : %d\n", sendvoxelsrandy_config.time_run);
    printf("boxshrinkgrow_config.state) : %d\n", boxshrinkgrow_config.state);       printf("boxshrinkgrow_config.time_run : %d\n", boxshrinkgrow_config.time_run);
    printf("boxwoopwoop_config.state : %d\n", boxwoopwoop_config.state);            printf("boxwoopwoop_config.time_run : %d\n", boxwoopwoop_config.time_run);
    printf("axiszupdownrs_config.state : %d\n", axiszupdownrs_config.state);        printf("axiszupdownrs_config.time_run : %d\n", axiszupdownrs_config.time_run);
    printf("axisyupdownrs_config.state : %d\n", axisyupdownrs_config.state);        printf("axisyupdownrs_config.time_run : %d\n", axisyupdownrs_config.time_run);
    printf("sidewaves_config.state : %d\n", sidewaves_config.state);                printf("sidewaves_config.time_run : %d\n", sidewaves_config.time_run);
    printf("wormsqueeze_config.state : %d\n", wormsqueeze_config.state);            printf("wormsqueeze_config.time_run : %d\n", wormsqueeze_config.time_run);
    printf("boingboing1_config.state : %d\n", boingboing1_config.state);            printf("boingboing1_config.time_run : %d\n", boingboing1_config.time_run);
    printf("boingboing2_config.state : %d\n", boingboing2_config.state);            printf("boingboing2_config.time_run : %d\n", boingboing2_config.time_run);
    printf("boingboing3_config.state : %d\n", boingboing3_config.state);            printf("boingboing3_config.time_run : %d\n", boingboing3_config.time_run);
    printf("randomfiller_config.state : %d\n", randomfiller_config.state);          printf("randomfiller_config.time_run : %d\n", randomfiller_config.time_run);
    printf("ripples_config.state : %d\n", ripples_config.state);                    printf("ripples_config.time_run : %d\n", ripples_config.time_run);
    printf("cubix_config.state : %d\n", cubix_config.state);                        printf("cubix_config.time_run : %d\n", cubix_config.time_run);
    printf("filip_filop_config.state : %d\n", filip_filop_config.state);            printf("filip_filop_config.time_run : %d\n", filip_filop_config.time_run);
    printf("sinelines_config.state : %d\n", sinelines_config.state);                printf("sinelines_config.time_run : %d\n", sinelines_config.time_run);
    printf("stringfly_config.state : %d\n", stringfly_config.state);                printf(" stringfly_config.stringbuff : %s\n", stringfly_config.stringbuff);
    printf("fireworks_config.state : %d\n", fireworks_config.state);                printf("fireworks_config.time_run : %d\n", fireworks_config.time_run);
    printf("path_text_config.state : %d\n", path_text_config.state);                printf("path_text_config.stringbuff : %s\n", path_text_config.stringbuff);
    printf("ball_bouncing_config.state : %d\n", ball_bouncing_config.state);        printf("ball_bouncing_config.time_run : %d\n", ball_bouncing_config.time_run);
    printf("brightness_value : %d\n", brightness_value);
}
esp_err_t esp_to_app_send_data_handler(httpd_req_t *req)
{
    char resp_str[1400];
    sprintf(resp_str,
"[{\"name\":\"name0\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name1\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"name2\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"name3\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"name4\",\"isLiked\":%d,\"timeRun\":%d ,\"string\":\"\"},\
{\"name\":\"name5\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name6\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name7\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name8\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name9\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name10\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name11\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name12\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name13\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name14\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name15\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name16\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name17\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name18\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name19\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name20\",\"isLiked\":%d,\"timeRun\":%d,\"string\":%s},\
{\"name\":\"name21\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"name\":\"name22\",\"isLiked\":%d,\"timeRun\":%d,\"string\":%s},\
{\"name\":\"name23\",\"isLiked\":%d,\"timeRun\":%d,\"string\":\"\"},\
{\"brightness\": %d}]", \
    rain_config.state,rain_config.time_run,\
    rainbow_config.state,rainbow_config.time_run,\
    planboing_config.state,planboing_config.time_run,\
    sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,\
    sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,\
    boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,\
    boxwoopwoop_config.state,boxwoopwoop_config.time_run,\
    axiszupdownrs_config.state,axiszupdownrs_config.time_run,\
    axisyupdownrs_config.state,axisyupdownrs_config.time_run,\
    sidewaves_config.state,sidewaves_config.time_run,\
    wormsqueeze_config.state,wormsqueeze_config.time_run,\
    wormsqueeze_config.state,wormsqueeze_config.time_run,\
    boingboing1_config.state,boingboing1_config.time_run,\
    boingboing2_config.state,boingboing2_config.time_run,\
    boingboing3_config.state,boingboing3_config.time_run,\
    randomfiller_config.state,randomfiller_config.time_run,\
    ripples_config.state,ripples_config.time_run,\
    cubix_config.state,cubix_config.time_run,\
    filip_filop_config.state,filip_filop_config.time_run,\
    sinelines_config.state,sinelines_config.time_run,\
    stringfly_config.state,stringfly_config.time_run,"\"hello\"",\
    fireworks_config.state,fireworks_config.time_run,\
    path_text_config.state,path_text_config.time_run,"\"hello\"",\
    ball_bouncing_config.state,ball_bouncing_config.time_run,\
    brightness_value
    );


    printf("---------------------------------------------\n");
    printf("%s\n",resp_str);
    fflush(stdout);
    // kiểu data gửi đi
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    // gửi data
    httpd_resp_send(req,resp_str, strlen(resp_str));
    return ESP_OK;
}

//------------------- Hàm gửi data config led đến web ---------------------
/*
esp_err_t data_web_before_get_handler(httpd_req_t *r)
{    
    char resp_str[700];
    sprintf(resp_str,"{\"d1\":\"%d\",\"d2\":\"%d\",\"d3\":\"%d\",\"d4\":\"%d\",\"d5\":\"%d\",\
\"d6\":\"%d\",\"d7\":\"%d\",\"d8\":\"%d\",\"d9\":\"%d\",\"d10\":\"%d\",\"d11\":\"%d\",\"d12\":\"%d\",\
\"d13\":\"%d\",\"d14\":\"%d\",\"d15\":\"%d\",\"d16\":\"%d\",\"d17\":\"%d\",\"d18\":\"%d\",\"d19\":\"%d\",\
\"d20\":\"%d\",\"d21\":\"%d\",\"d22\":\"%d\",\"d23\":\"%d\",\"d24\":\"%d\",\"d25\":\"%d\",\"d26\":\"%d\",\
\"d27\":\"%d\",\"d28\":\"%d\",\"d29\":\"%d\",\"d30\":\"%d\",\"d31\":\"%d\",\"d32\":\"%d\",\"d33\":\"%d\",\
\"d34\":\"%d\",\"d35\":\"%d\",\"d36\":\"%d\",\"d37\":\"%d\",\"d38\":\"%d\",\"d39\":\"%d\",\"d40\":\"%s\",\
\"d41\":\"%d\",\"d42\":\"%d\",\"d43\":\"%d\",\"d44\":\"%s\",\"d45\":\"%d\",\"d46\":\"%d\",\"d99\":\"%d\"}",\
rain_config.state,rain_config.time_run,rainbow_config.state,rainbow_config.time_run,planboing_config.state,\
planboing_config.time_run,sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,sendvoxelsrandy_config.state,\
sendvoxelsrandy_config.time_run,boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,boxwoopwoop_config.state,\
boxwoopwoop_config.time_run,axiszupdownrs_config.state,axiszupdownrs_config.time_run,axisyupdownrs_config.state,\
axisyupdownrs_config.time_run,sidewaves_config.state,sidewaves_config.time_run,wormsqueeze_config.state,\
wormsqueeze_config.time_run,boingboing1_config.state,boingboing1_config.time_run,boingboing2_config.state,\
boingboing2_config.time_run,boingboing3_config.state,boingboing3_config.time_run,randomfiller_config.state,\
randomfiller_config.time_run,ripples_config.state,ripples_config.time_run, cubix_config.state,cubix_config.time_run,\
filip_filop_config.state,filip_filop_config.time_run,sinelines_config.state,sinelines_config.time_run,stringfly_config.state,\
stringfly_config.stringbuff,fireworks_config.state,fireworks_config.time_run,path_text_config.state, path_text_config.stringbuff,\
ball_bouncing_config.state,ball_bouncing_config.time_run,brightness_value); 
    printf("%s\n",resp_str);
    // kiểu data gửi đi
    httpd_resp_set_type(r,HTTPD_TYPE_JSON);
    // gửi data
    httpd_resp_send(r,resp_str, strlen(resp_str));
    return ESP_OK;
}
*/
//------------------- Hàm gửi data config led đến web ---------------------
esp_err_t data_web_before_get_handler(httpd_req_t *r)
{    
    char resp_str[700];
    sprintf(resp_str,"{\"d1\":\"%d\",\"d2\":\"%d\",\"d3\":\"%d\",\"d4\":\"%d\",\"d5\":\"%d\",\"d6\":\"%d\",\"d7\":\"%d\",\"d8\":\"%d\",\"d9\":\"%d\",\"d10\":\"%d\",\"d11\":\"%d\",\"d12\":\"%d\",\"d13\":\"%d\",\"d14\":\"%d\",\"d15\":\"%d\",\"d16\":\"%d\",\"d17\":\"%d\",\"d18\":\"%d\",\"d19\":\"%d\",\"d20\":\"%d\",\"d21\":\"%d\",\"d22\":\"%d\",\"d23\":\"%d\",\"d24\":\"%d\",\"d25\":\"%d\",\"d26\":\"%d\",\"d27\":\"%d\",\"d28\":\"%d\",\"d29\":\"%d\",\"d30\":\"%d\",\"d31\":\"%d\",\"d32\":\"%d\",\"d33\":\"%d\",\"d34\":\"%d\",\"d35\":\"%d\",\"d36\":\"%d\",\"d37\":\"%d\",\"d38\":\"%d\",\"d39\":\"%d\",\"d40\":\"%s\",\"d41\":\"%d\",\"d42\":\"%d\",\"d43\":\"%d\",\"d44\":\"%s\",\"d45\":\"%d\",\"d46\":\"%d\",\"d99\":\"%d\"}",rain_config.state,rain_config.time_run,rainbow_config.state,rainbow_config.time_run,planboing_config.state,planboing_config.time_run,sendvoxelsrandz_config.state,sendvoxelsrandz_config.time_run,sendvoxelsrandy_config.state,sendvoxelsrandy_config.time_run,boxshrinkgrow_config.state,boxshrinkgrow_config.time_run,boxwoopwoop_config.state,boxwoopwoop_config.time_run,axiszupdownrs_config.state,axiszupdownrs_config.time_run,axisyupdownrs_config.state,axisyupdownrs_config.time_run,sidewaves_config.state,sidewaves_config.time_run,wormsqueeze_config.state,wormsqueeze_config.time_run,boingboing1_config.state,boingboing1_config.time_run,boingboing2_config.state,boingboing2_config.time_run,boingboing3_config.state,boingboing3_config.time_run,randomfiller_config.state,randomfiller_config.time_run,ripples_config.state,ripples_config.time_run, cubix_config.state,cubix_config.time_run,filip_filop_config.state,filip_filop_config.time_run,sinelines_config.state,sinelines_config.time_run,stringfly_config.state,stringfly_config.stringbuff,fireworks_config.state,fireworks_config.time_run,path_text_config.state, path_text_config.stringbuff,ball_bouncing_config.state,ball_bouncing_config.time_run,brightness_value); 
    printf("%s\n",resp_str);
    // kiểu data gửi đi
    httpd_resp_set_type(r,HTTPD_TYPE_JSON);
    // gửi data
    httpd_resp_send(r,resp_str, strlen(resp_str));
    return ESP_OK;
}


esp_err_t esp_to_app_check_connect_handler(httpd_req_t *req)
{    
    const char resp[] = "Configured Successfully";
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}
//--------------------------- Trang HTML ---------------------------------
esp_err_t html_get_handler(httpd_req_t *req)
{
    httpd_resp_set_type(req,HTTPD_TYPE_TEXT);
    httpd_resp_send(req,(char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
}


httpd_uri_t app_to_esp_data_eff_config_uri_post = {
    .handler = app_to_esp_data_eff_config_handler,
    .method = HTTP_POST,
    .uri =  "/app_to_esp_data_eff_config",
    .user_ctx = NULL
};

httpd_uri_t esp_to_app_send_data_uri_get = {
    .handler = esp_to_app_send_data_handler,
    .method = HTTP_GET,
    .uri =  "/esp_to_app_send_data",
    .user_ctx = NULL
};

httpd_uri_t esp_to_app_check_connect_uri_get = {
    .handler = esp_to_app_check_connect_handler,
    .method = HTTP_GET,
    .uri =  "/esp_to_app_check_connect",
    .user_ctx = NULL
};
// ----------------------------Trang HTML----------------------------
httpd_uri_t html_uri_get = {
    .handler = html_get_handler,
    .method = HTTP_GET,
    .uri =  "/",
    .user_ctx = NULL
};
// -------------Gửi data từ ESP đến web trong lần đầu mở web---------
httpd_uri_t data_web_before_uri_get = {
    .handler = data_web_before_get_handler,
    .method = HTTP_GET,
    .uri =  "/data_web_before",
    .user_ctx = NULL
};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI("SERVER", "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Registering the ws handler
        ESP_LOGI("SERVER", "Registering URI handlers");
        httpd_register_uri_handler(server, &html_uri_get);
        httpd_register_uri_handler(server, &esp_to_app_send_data_uri_get);
        httpd_register_uri_handler(server, &app_to_esp_data_eff_config_uri_post);
        httpd_register_uri_handler(server, &esp_to_app_check_connect_uri_get);
        httpd_register_uri_handler(server, &data_web_before_uri_get);
        return server;
    }
    ESP_LOGI("SERVER", "Error starting server!");
    return NULL;
}
void data_eff_config_check()
{
    delay_ms(3000);
    semaphorehandle_data_eff_config = xSemaphoreCreateBinary();
    while (1)
    {
        if(xSemaphoreTake(semaphorehandle_data_eff_config, portMAX_DELAY))
        {
            get_data_config_effect(content);
            print_data_effect_config();
            delay_ms(2000);

        }
    }
}

void app_main()
{
    init_effect_config();
    wifi_sta_connect();
    httpd_handle_t server = NULL;
    server = start_webserver();  
    xTaskCreatePinnedToCore(data_eff_config_check, "data_eff_config_check",8192, NULL, 5, NULL,0);

}

