
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "soc/uart_struct.h"
#include <math.h>
#include "driver/i2s.h"
#include "esp_log.h"
#include "driver/adc.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_adc_cal.h"
#include <led_strip_idf.h>



int buffAudio[1024];
SemaphoreHandle_t semaphorehandle_adc_read = NULL;
esp_adc_cal_characteristics_t   adc_char;
void delay_ms(int time_delay)
{
    vTaskDelay(time_delay/portTICK_RATE_MS);
}

#define LED_TYPE LED_STRIP_WS2812
#define LED_LENGTH 512
#define LED_GPIO 5
#define LED_STRIP_BRIGHTNESS 50

rgb_t layer_buffer[512];
// cấu hình strip
led_strip_t strip = {
        .type = LED_TYPE,
        .length = LED_LENGTH,
        .gpio = LED_GPIO,
        .buf = NULL,
        .brightness = 50,
};

void show_led()
{
    ESP_ERROR_CHECK(led_strip_set_pixels(&strip, 0, 512, layer_buffer));
    ESP_ERROR_CHECK(led_strip_flush(&strip));
}



rgb_t green_color =  { .r = 0, .g = 255, .b = 0 };
rgb_t blue_color =  { .r = 0, .g = 0, .b = 255};
rgb_t no_color =  { .r = 0, .g = 0, .b = 0 };
rgb_t rgb_setvoxel =  { .r = 255, .g = 0, .b = 0 };
rgb_t Scroll(int pos) {
	rgb_t color = { .r = 255, .g = 0, .b = 0 };
	if(pos < 85) {
		color.g = 0;
		color.r = ((float)pos / 85.0f) * 255.0f;
		color.b = 255 - color.r;
	} else if(pos < 170) {
		color.g = ((float)(pos - 85) / 85.0f) * 255.0f;
		color.r = 255 - color.g;
		color.b = 0;
	} else if(pos < 256) {
		color.b = ((float)(pos - 170) / 85.0f) * 255.0f;
		color.g = 255 - color.b;
		color.r = 1;
	}
	return color;
}
void set_color_setvoxel(char rs, char gs, char bs)
{
    rgb_setvoxel.r = rs;
    rgb_setvoxel.g = gs;
    rgb_setvoxel.b = bs;
}
void change_rgb_color()
{
    static char scroll;
    rgb_t rgb = Scroll(scroll);
    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
    scroll++;
}

#define max_height 20

int valueColumm[21] = {0};

void drawLed(int columm, int height)
{
    if(columm%2 == 0)
    {
        for(int i = columm*20; i < columm*20 + height; i++)
        {
            layer_buffer[i] = rgb_setvoxel;
        }
        for(int i = columm*20 +height; i < columm*20 + max_height; i++)
        {
            layer_buffer[i] = no_color;
        }
    }
    if(columm%2 == 1)
    {
        for(int i = columm*20 + max_height; i > columm*20 + max_height - height - 1; i--)
        {
            layer_buffer[i] = rgb_setvoxel;
        }
        for(int i = columm*20 + max_height - height - 1; i > columm*20 ; i--)
        {
            layer_buffer[i] = no_color;
        }
    }
    rgb_t rgb = Scroll(columm*10);
    set_color_setvoxel(rgb.r, rgb.g, rgb.b);
}

void drawLed2(int columm, int height)
{
    if(columm%2 == 0)
    {
        for(int i = columm*20; i < columm*20 + height; i++)
        {
            layer_buffer[i] = green_color;
        }

    }
    else
    {
        for(int i = columm*20 + max_height; i > columm*20 + max_height - height - 1; i--)
        {
            layer_buffer[i] = green_color;
        }
    }
   
}

void dropLed()
{
    for(int collum = 0; collum < 21; collum++)
    {
        if(collum%2 == 0)
        {
            layer_buffer[collum*20 + valueColumm[collum] - 1] = no_color;
            valueColumm[collum]--;
        }
        if(collum%2 == 1)
        {
           layer_buffer[collum*20 + max_height - valueColumm[collum] + 1] = no_color;
           valueColumm[collum]++;
        }

    }
   
}

/*----------------------------------------------------------------*/

#define N_WAVE      1024    /* full length of Sinewave[] */
#define LOG2_N_WAVE 10      /* log2(N_WAVE) */

/*
  Henceforth "short" implies 16-bit word. If this is not
  the case in your architecture, please replace "short"
  with a type definition which *is* a 16-bit word.
*/

/*
  Since we only use 3/4 of N_WAVE, we define only
  this many samples, in order to conserve data space.
*/
const int16_t Sinewave[N_WAVE-N_WAVE/4] = {
      0,    201,    402,    603,    804,   1005,   1206,   1406,
   1607,   1808,   2009,   2209,   2410,   2610,   2811,   3011,
   3211,   3411,   3611,   3811,   4011,   4210,   4409,   4608,
   4807,   5006,   5205,   5403,   5601,   5799,   5997,   6195,
   6392,   6589,   6786,   6982,   7179,   7375,   7571,   7766,
   7961,   8156,   8351,   8545,   8739,   8932,   9126,   9319,
   9511,   9703,   9895,  10087,  10278,  10469,  10659,  10849,
  11038,  11227,  11416,  11604,  11792,  11980,  12166,  12353,
  12539,  12724,  12909,  13094,  13278,  13462,  13645,  13827,
  14009,  14191,  14372,  14552,  14732,  14911,  15090,  15268,
  15446,  15623,  15799,  15975,  16150,  16325,  16499,  16672,
  16845,  17017,  17189,  17360,  17530,  17699,  17868,  18036,
  18204,  18371,  18537,  18702,  18867,  19031,  19194,  19357,
  19519,  19680,  19840,  20000,  20159,  20317,  20474,  20631,
  20787,  20942,  21096,  21249,  21402,  21554,  21705,  21855,
  22004,  22153,  22301,  22448,  22594,  22739,  22883,  23027,
  23169,  23311,  23452,  23592,  23731,  23869,  24006,  24143,
  24278,  24413,  24546,  24679,  24811,  24942,  25072,  25201,
  25329,  25456,  25582,  25707,  25831,  25954,  26077,  26198,
  26318,  26437,  26556,  26673,  26789,  26905,  27019,  27132,
  27244,  27355,  27466,  27575,  27683,  27790,  27896,  28001,
  28105,  28208,  28309,  28410,  28510,  28608,  28706,  28802,
  28897,  28992,  29085,  29177,  29268,  29358,  29446,  29534,
  29621,  29706,  29790,  29873,  29955,  30036,  30116,  30195,
  30272,  30349,  30424,  30498,  30571,  30643,  30713,  30783,
  30851,  30918,  30984,  31049,  31113,  31175,  31236,  31297,
  31356,  31413,  31470,  31525,  31580,  31633,  31684,  31735,
  31785,  31833,  31880,  31926,  31970,  32014,  32056,  32097,
  32137,  32176,  32213,  32249,  32284,  32318,  32350,  32382,
  32412,  32441,  32468,  32495,  32520,  32544,  32567,  32588,
  32609,  32628,  32646,  32662,  32678,  32692,  32705,  32717,
  32727,  32736,  32744,  32751,  32757,  32761,  32764,  32766,
  32767,  32766,  32764,  32761,  32757,  32751,  32744,  32736,
  32727,  32717,  32705,  32692,  32678,  32662,  32646,  32628,
  32609,  32588,  32567,  32544,  32520,  32495,  32468,  32441,
  32412,  32382,  32350,  32318,  32284,  32249,  32213,  32176,
  32137,  32097,  32056,  32014,  31970,  31926,  31880,  31833,
  31785,  31735,  31684,  31633,  31580,  31525,  31470,  31413,
  31356,  31297,  31236,  31175,  31113,  31049,  30984,  30918,
  30851,  30783,  30713,  30643,  30571,  30498,  30424,  30349,
  30272,  30195,  30116,  30036,  29955,  29873,  29790,  29706,
  29621,  29534,  29446,  29358,  29268,  29177,  29085,  28992,
  28897,  28802,  28706,  28608,  28510,  28410,  28309,  28208,
  28105,  28001,  27896,  27790,  27683,  27575,  27466,  27355,
  27244,  27132,  27019,  26905,  26789,  26673,  26556,  26437,
  26318,  26198,  26077,  25954,  25831,  25707,  25582,  25456,
  25329,  25201,  25072,  24942,  24811,  24679,  24546,  24413,
  24278,  24143,  24006,  23869,  23731,  23592,  23452,  23311,
  23169,  23027,  22883,  22739,  22594,  22448,  22301,  22153,
  22004,  21855,  21705,  21554,  21402,  21249,  21096,  20942,
  20787,  20631,  20474,  20317,  20159,  20000,  19840,  19680,
  19519,  19357,  19194,  19031,  18867,  18702,  18537,  18371,
  18204,  18036,  17868,  17699,  17530,  17360,  17189,  17017,
  16845,  16672,  16499,  16325,  16150,  15975,  15799,  15623,
  15446,  15268,  15090,  14911,  14732,  14552,  14372,  14191,
  14009,  13827,  13645,  13462,  13278,  13094,  12909,  12724,
  12539,  12353,  12166,  11980,  11792,  11604,  11416,  11227,
  11038,  10849,  10659,  10469,  10278,  10087,   9895,   9703,
   9511,   9319,   9126,   8932,   8739,   8545,   8351,   8156,
   7961,   7766,   7571,   7375,   7179,   6982,   6786,   6589,
   6392,   6195,   5997,   5799,   5601,   5403,   5205,   5006,
   4807,   4608,   4409,   4210,   4011,   3811,   3611,   3411,
   3211,   3011,   2811,   2610,   2410,   2209,   2009,   1808,
   1607,   1406,   1206,   1005,    804,    603,    402,    201,
      0,   -201,   -402,   -603,   -804,  -1005,  -1206,  -1406,
  -1607,  -1808,  -2009,  -2209,  -2410,  -2610,  -2811,  -3011,
  -3211,  -3411,  -3611,  -3811,  -4011,  -4210,  -4409,  -4608,
  -4807,  -5006,  -5205,  -5403,  -5601,  -5799,  -5997,  -6195,
  -6392,  -6589,  -6786,  -6982,  -7179,  -7375,  -7571,  -7766,
  -7961,  -8156,  -8351,  -8545,  -8739,  -8932,  -9126,  -9319,
  -9511,  -9703,  -9895, -10087, -10278, -10469, -10659, -10849,
 -11038, -11227, -11416, -11604, -11792, -11980, -12166, -12353,
 -12539, -12724, -12909, -13094, -13278, -13462, -13645, -13827,
 -14009, -14191, -14372, -14552, -14732, -14911, -15090, -15268,
 -15446, -15623, -15799, -15975, -16150, -16325, -16499, -16672,
 -16845, -17017, -17189, -17360, -17530, -17699, -17868, -18036,
 -18204, -18371, -18537, -18702, -18867, -19031, -19194, -19357,
 -19519, -19680, -19840, -20000, -20159, -20317, -20474, -20631,
 -20787, -20942, -21096, -21249, -21402, -21554, -21705, -21855,
 -22004, -22153, -22301, -22448, -22594, -22739, -22883, -23027,
 -23169, -23311, -23452, -23592, -23731, -23869, -24006, -24143,
 -24278, -24413, -24546, -24679, -24811, -24942, -25072, -25201,
 -25329, -25456, -25582, -25707, -25831, -25954, -26077, -26198,
 -26318, -26437, -26556, -26673, -26789, -26905, -27019, -27132,
 -27244, -27355, -27466, -27575, -27683, -27790, -27896, -28001,
 -28105, -28208, -28309, -28410, -28510, -28608, -28706, -28802,
 -28897, -28992, -29085, -29177, -29268, -29358, -29446, -29534,
 -29621, -29706, -29790, -29873, -29955, -30036, -30116, -30195,
 -30272, -30349, -30424, -30498, -30571, -30643, -30713, -30783,
 -30851, -30918, -30984, -31049, -31113, -31175, -31236, -31297,
 -31356, -31413, -31470, -31525, -31580, -31633, -31684, -31735,
 -31785, -31833, -31880, -31926, -31970, -32014, -32056, -32097,
 -32137, -32176, -32213, -32249, -32284, -32318, -32350, -32382,
 -32412, -32441, -32468, -32495, -32520, -32544, -32567, -32588,
 -32609, -32628, -32646, -32662, -32678, -32692, -32705, -32717,
 -32727, -32736, -32744, -32751, -32757, -32761, -32764, -32766,
};

/*
  FIX_MPY() - fixed-point multiplication & scaling.
  Substitute inline assembly for hardware-specific
  optimization suited to a particluar DSP processor.
  Scaling ensures that result remains 16-bit.
*/
int16_t FIX_MPY(int16_t a, int16_t b)
{
	/* shift right one less bit (i.e. 15-1) */
	int32_t c = ((int32_t)a * (int32_t)b) >> 14;
	/* last bit shifted out = rounding-bit */
	b = c & 0x01;
	/* last shift + rounding bit */
	a = (c >> 1) + b;
	return a;
}

/*
  fix_fft() - perform forward/inverse fast Fourier transform.
  fr[n],fi[n] are real and imaginary arrays, both INPUT AND
  RESULT (in-place FFT), with 0 <= n < 2**m; set inverse to
  0 for forward transform (FFT), or 1 for iFFT.
*/
int32_t fix_fft(int16_t fr[], int16_t fi[], uint8_t m, uint8_t inverse)
{
	int32_t mr, nn, i, j, l, k, istep, n, scale, shift;
	int16_t qr, qi, tr, ti, wr, wi;

	n = 1 << m;

	/* max FFT size = N_WAVE */
	if (n > N_WAVE)
		return -1;

	mr = 0;
	nn = n - 1;
	scale = 0;

	/* decimation in time - re-order data */
	for (m=1; m<=nn; ++m) {
		l = n;
		do {
			l >>= 1;
		} while (mr+l > nn);
		mr = (mr & (l-1)) + l;

		if (mr <= m)
			continue;
		tr = fr[m];
		fr[m] = fr[mr];
		fr[mr] = tr;
		ti = fi[m];
		fi[m] = fi[mr];
		fi[mr] = ti;
	}

	l = 1;
	k = LOG2_N_WAVE-1;
	while (l < n) {
		if (inverse) {
			/* variable scaling, depending upon data */
			shift = 0;
			for (i=0; i<n; ++i) {
				j = fr[i];
				if (j < 0)
					j = -j;
				m = fi[i];
				if (m < 0)
					m = -m;
				if (j > 16383 || m > 16383) {
					shift = 1;
					break;
				}
			}
			if (shift)
				++scale;
		} else {
			/*
			  fixed scaling, for proper normalization --
			  there will be log2(n) passes, so this results
			  in an overall factor of 1/n, distributed to
			  maximize arithmetic accuracy.
			*/
			shift = 1;
		}
		/*
		  it may not be obvious, but the shift will be
		  performed on each data point exactly once,
		  during this pass.
		*/
		istep = l << 1;
		for (m=0; m<l; ++m) {
			j = m << k;
			/* 0 <= j < N_WAVE/2 */
			wr =  Sinewave[j+N_WAVE/4];
			wi = -Sinewave[j];
			if (inverse)
				wi = -wi;
			if (shift) {
				wr >>= 1;
				wi >>= 1;
			}
			for (i=m; i<n; i+=istep) {
				j = i + l;
				tr = FIX_MPY(wr,fr[j]) - FIX_MPY(wi,fi[j]);
				ti = FIX_MPY(wr,fi[j]) + FIX_MPY(wi,fr[j]);
				qr = fr[i];
				qi = fi[i];
				if (shift) {
					qr >>= 1;
					qi >>= 1;
				}
				fr[j] = qr - tr;
				fi[j] = qi - ti;
				fr[i] = qr + tr;
				fi[i] = qi + ti;
			}
		}
		--k;
		l = istep;
	}
	return scale;
}

/*
  fix_fftr() - forward/inverse FFT on array of real numbers.
  Real FFT/iFFT using half-size complex FFT by distributing
  even/odd samples into real/imaginary arrays respectively.
  In order to save data space (i.e. to avoid two arrays, one
  for real, one for imaginary samples), we proceed in the
  following two steps: a) samples are rearranged in the real
  array so that all even samples are in places 0-(N/2-1) and
  all imaginary samples in places (N/2)-(N-1), and b) fix_fft
  is called with fr and fi pointing to index 0 and index N/2
  respectively in the original array. The above guarantees
  that fix_fft "sees" consecutive real samples as alternating
  real and imaginary samples in the complex array.
*/
int32_t fix_fftr(int16_t f[], uint8_t m, uint8_t inverse)
{
	int32_t i, N = 1<<(m-1), scale = 0;
	int16_t tt, *fr=f, *fi=&f[N];

	if (inverse)
		scale = fix_fft(fi, fr, m-1, inverse);
	for (i=1; i<N; i+=2) {
		tt = f[N+i-1];
		f[N+i-1] = f[i];
		f[i] = tt;
	}
	if (! inverse)
		scale = fix_fft(fi, fr, m-1, inverse);
	return scale;
}


int16_t  im[1024];
int16_t  data[1024];
int16_t  val;
void readADC()
{ 
    // Hiệu chỉnh ADC
    esp_adc_cal_characterize(ADC_UNIT_1,ADC_ATTEN_DB_11,ADC_WIDTH_BIT_12, 0, &adc_char);
    // cấu hình độ rộng ADC
    adc1_config_width(ADC_WIDTH_BIT_12);
    // cấu hình dải điện áp đầu vào cho adc 
    adc1_config_channel_atten(ADC1_CHANNEL_5 , ADC_ATTEN_DB_11);
    semaphorehandle_adc_read = xSemaphoreCreateBinary();
    delay_ms(1000);
    while (1)
    {
       

        uint32_t time =  esp_log_timestamp();
        for(int i = 0; i < 512; i++)
        {
            val = adc1_get_raw(ADC1_CHANNEL_5);
            data[i] = val  - 2048;;

			// Set the imaginary number to zero
			im[i] = 0;
        }
     //   printf("%d\n", esp_log_timestamp()  - time);
        
        delay_ms(10);
        xSemaphoreGive(semaphorehandle_adc_read);
    }
}
uint32_t map(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
int random_number(int minN, int maxN){
	return minN + rand() % (maxN + 1 - minN);
}

void set_brightness(int percentage)
{
    if(percentage < 0 ) percentage = 0;
    else if( percentage > 100) percentage = 100;
    int value = map(percentage, 0,100,0,255);
    strip.brightness = value;
}




const int16_t linTable[182] = 
{
	0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8,
	8, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14,
	14, 15, 15, 15, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20, 20,
	20, 21, 21, 21, 22, 22, 22, 23, 23, 23, 24, 24, 25, 25, 25, 26, 26,
	26, 27, 27, 27, 28, 28, 28, 29, 29, 29, 30, 30, 30, 31, 31, 31, 32,
	32, 33, 33, 33, 34, 34, 34, 35, 35, 35, 36, 36, 36, 37, 37, 37, 38,
	38, 38, 39, 39, 39, 40, 40, 41, 41, 41, 42, 42, 42, 43, 43, 43, 44,
	44, 44, 45, 45, 45, 46, 46, 46, 47, 47, 47, 48, 48, 49, 49, 49, 50,
	50, 50, 51, 51, 51, 52, 52, 52, 53, 53, 53, 54, 54, 54, 55, 55, 55,
	56, 56, 57, 57, 57, 58, 58, 58, 59, 59, 59, 60, 60, 60, 61, 61, 61,
	62, 62, 62, 63, 63, 63	
};


int16_t inputValue;;
short  displayData[32];


void handleDataFFT()
{
    set_brightness(10);
    while (1)
    {
        if(xSemaphoreTake(semaphorehandle_adc_read, portMAX_DELAY))
        {
           // printf("recieve flag semaphore from task 1\n");
            fix_fft(data,im,7,0);

            for(int i=0; i< 256;i++){
                data[i] = sqrt(data[i] * data[i] + im[i] * im[i]);
               // if(data[i] > 183) data[i] = 183;
               // data[i]  =  map(data[i], 0,100,0,20);
            }
            
           
            int16_t inputValue;
            for (unsigned char counter = 1; counter < 22; counter++)
            {
                // Scale the input data for the display (linear) x1 or x8
                // inputValue = (data[counter*2 - 1] + data[counter*2 ])/2;
                 inputValue = data[counter];
                // for(int i = 0; i < 20; i++)
                // // {
                // //     if(data[counter*20 + i] > inputValue) inputValue= data[counter*20 + i];
                // // }
                if (inputValue > 181) inputValue = 181;
                
                // Apply a linear or logarithmic conversion on the data
                inputValue = (short)linTable[inputValue];
                
                // Perform damping on the displayed output
                if (inputValue > displayData[counter-1]) displayData[counter-1] = inputValue ;
                else displayData[counter-1] -= 10;
                if (displayData[counter-1] < 0) displayData[counter-1] = 0;
                if ( displayData[counter-1] > 50)  displayData[counter-1] = 50;

                displayData[counter-1]  =  map(displayData[counter-1], 0,50,0,20);
            }
   
            for(int i=0; i< 21;i++)
            {               
             //   printf("%d  ",displayData[i] );
                
                // if(displayData[i] >= valueColumm[i])
                // { 
                //     valueColumm[i] = displayData[i];
                //     drawLed2(i ,displayData[i] );
                // }  
                // else
                // {
                //     if(i%2 == 0)
                //     {  
                //         if(valueColumm[i] > 0) 
                //         { 
                //             layer_buffer[i*20 + valueColumm[i] ] = no_color;   
                //             valueColumm[i]--;
                //         }                
                //     }
                //     else
                //     {   
                //         if(valueColumm[i] > 0)
                //         { 
                //             layer_buffer[i*20  + (20 - valueColumm[i]) ] = no_color;
                //             valueColumm[i]--;
                //         }      
                //     }
                // }
                
                drawLed(i ,displayData[i] );
            }
           // change_rgb_color();
          
            show_led();
        //  printf(".........\n");
        }
    }
    
}




void app_main() 
{
    xTaskCreatePinnedToCore(readADC, "readADC", 1024*8,NULL, 10, NULL, 0);
    xTaskCreatePinnedToCore(handleDataFFT, "handleDataFFT", 1024*8,NULL, 10, NULL, 0);

  


    led_strip_install();

    ESP_ERROR_CHECK(led_strip_init(&strip));


    


}
