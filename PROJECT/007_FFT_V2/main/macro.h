#ifndef _MACRO_H_H
#define _MACRO_H_H


#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


#define millis()  esp_log_timestamp()

void delay_ms(int time_delay)
{
    vTaskDelay(time_delay/portTICK_RATE_MS);
}

#endif