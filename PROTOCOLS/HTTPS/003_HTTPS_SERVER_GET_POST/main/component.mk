#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

# COMPONENT_EMBED_TXTFILES := ../html/index.html

COMPONENT_EMBED_TXTFILES := ../certificate/ServerCert.pem
COMPONENT_EMBED_TXTFILES := ../certificate/ServerKey.pem

