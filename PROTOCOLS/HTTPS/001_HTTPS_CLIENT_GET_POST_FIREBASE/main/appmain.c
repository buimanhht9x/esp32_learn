#include <stdio.h>
#include"string.h"
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_http_client.h"
#include "cJSON.h"

#include <esp_event.h>
#include <esp_system.h>




#define SSID "Tplink 2.4 ghz"
#define PASS "hoianhson"

// Define client certificate
// openssl s_client -showcerts -connect console.firebase.google.com:443
extern const uint8_t ClientCert_pem_start[] asm("_binary_certificate_pem_start");
extern const uint8_t ClientCert_pem_end[]   asm("_binary_certificate_pem_end");


// WiFi
static void wifi_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    switch (event_id)
    {
    case WIFI_EVENT_STA_START:
        printf("WiFi connecting ... \n");
        break;
    case WIFI_EVENT_STA_CONNECTED:
        printf("WiFi connected ... \n");
        break;
    case WIFI_EVENT_STA_DISCONNECTED:
        printf("WiFi lost connection ... \n");
        esp_wifi_connect();
        break;
    case IP_EVENT_STA_GOT_IP:
        printf("WiFi got IP ... \n\n");
        break;
    default:
        break;
    }
}

void wifi_connection()
{
    // 1 - Wi-Fi/LwIP Init Phase
    esp_netif_init();                    // TCP/IP initiation 					s1.1
    esp_event_loop_create_default();     // event loop 			                s1.2
    esp_netif_create_default_wifi_sta(); // WiFi station 	                    s1.3
    wifi_init_config_t wifi_initiation = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wifi_initiation); // 					                    s1.4
    // 2 - Wi-Fi Configuration Phase
    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_event_handler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, wifi_event_handler, NULL);
    wifi_config_t wifi_configuration = {
        .sta = {
            .ssid = SSID,
            .password = PASS}};
    esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_configuration);
    // 3 - Wi-Fi Start Phase
    esp_wifi_start();
    // 4- Wi-Fi Connect Phase
    esp_wifi_connect();
}



//.........................................................................................................
//..................................... Client post firestore...............................................
esp_err_t client_event_post_firestore_handler(esp_http_client_event_handle_t evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ON_DATA:
        printf("Client HTTP_EVENT_ON_DATA: %.*s\n", evt->data_len, (char *)evt->data);
        break;

    default:
        break;
    }
    return ESP_OK;
}

static void client_post_firestore_rest_function()
{
    esp_http_client_config_t config_post = {
        .url = "https://firestore.googleapis.com/v1/projects/esp32-project-d3087/databases/(default)/documents/mycollection",
        .method = HTTP_METHOD_POST,
        .cert_pem = (const char *)ClientCert_pem_start,
        .event_handler = client_event_post_firestore_handler};
        
    esp_http_client_handle_t client = esp_http_client_init(&config_post);

    /*
    {
        "fields":
        {
            "temp": {"stringValue":"35"},
            "humi": {"stringValue":"11"}
        }    
    }
    */
    char  *post_data = "{\"fields\":{\"temp\":{\"stringValue\":\"35\"},\"humi\":{\"stringValue\":\"11\"}}}";
    esp_http_client_set_post_field(client, post_data, strlen(post_data));
    esp_http_client_set_header(client, "Content-Type", "application/json");

    esp_http_client_perform(client);
    esp_http_client_cleanup(client);
}


//.........................................................................................................
//..................................... Client get firestore...............................................
esp_err_t client_event_get_firestore_handler(esp_http_client_event_handle_t evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ON_DATA:  
         printf("Client HTTP_EVENT_ON_DATA: %.*s\n", evt->data_len, (char *)evt->data);
        break;

    default:
        break;
    }
    return ESP_OK;
}

static void client_get_firestore_rest_function()
{
    esp_http_client_config_t config_post = {
        .url = "https://firestore.googleapis.com/v1/projects/esp32-project-d3087/databases/(default)/documents/mycollection",
        .method = HTTP_METHOD_GET,
        .cert_pem = (const char *)ClientCert_pem_start,
        .event_handler = client_event_get_firestore_handler};
        
    esp_http_client_handle_t client = esp_http_client_init(&config_post);
    esp_http_client_perform(client);
    esp_http_client_cleanup(client);
}

//.........................................................................................................
void app_main(void)
{
    nvs_flash_init();
    wifi_connection();

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    printf("WIFI was initiated ...........\n\n");


    vTaskDelay(2000 / portTICK_PERIOD_MS);
    printf("Start client:\n\n");
 //   client_post_firestore_rest_function();
    client_get_firestore_rest_function();
}