#include "http_server_app.h"

/* Simple HTTP Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_eth.h"
#include "driver/gpio.h"
#include <esp_http_server.h>
extern char buffTemHum[500];
#define LED_GPIO 2
/* A simple example that demonstrates how to create GET and POST
 * handlers for the web server.
 */
httpd_handle_t server = NULL;
static const char *TAG = "HTTP_SERVER";
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");


static http_post_callback_t http_post_switch_callback=NULL;
static http_get_callback_t http_get_dht11_callback=NULL;

/* An HTTP GET handler */
static esp_err_t get_html(httpd_req_t *req)
{
  //  const char* resp_str = (const char*) "hELOOO";  
    httpd_resp_set_type(req, HTTPD_TYPE_TEXT);
    httpd_resp_send(req,(const char*)index_html_start ,index_html_end - index_html_start);
  //  httpd_resp_send(req, resp_str, strlen(resp_str));
    return ESP_OK;
}
static const httpd_uri_t gethtml = {
    .uri       = "/dht11",
    .method    = HTTP_GET,
    .handler   = get_html,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};
/* An HTTP GET handler */
static esp_err_t get_data_dht11(httpd_req_t *req)
{
   
    const char* resp_str = (const char*) buffTemHum;  
    httpd_resp_send(req, resp_str, strlen(resp_str));
    return ESP_OK;
}
static const httpd_uri_t getdatadht11 = {
    .uri       = "/getdatadht11",
    .method    = HTTP_GET,
    .handler   = get_data_dht11,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

///////////////////////////////////////
/* An HTTP POST handler */
static esp_err_t data_post_handler(httpd_req_t *req)
{
    char buf[100];

    httpd_req_recv(req, buf,req->content_len);
    printf("DATA: %s\n",buf);

    /* Send back the same data */
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t postData = {
    .uri       = "/data",
    .method    = HTTP_POST,
    .handler   = data_post_handler,
    .user_ctx  = NULL
};

///////////////////////////////////////
/* An HTTP POST handler */
static esp_err_t switch1_post_handler(httpd_req_t *req)
{
    char buf[100];
    gpio_pad_select_gpio(LED_GPIO);
    gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);
    httpd_req_recv(req, buf,req->content_len);
    if(strstr((char*)buf,"1"))
                gpio_set_level(LED_GPIO, 1); 
    if(strstr((char*)buf,"0"))
                gpio_set_level(LED_GPIO, 0);
    printf("DATA: %s\n",buf);

    /* Send back the same data */
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t switch1posthandler = {
    .uri       = "/switch1",
    .method    = HTTP_POST,
    .handler   = switch1_post_handler,
    .user_ctx  = NULL
};

///////////////////////////////////////

/* An HTTP POST handler */
static esp_err_t switch2_post_handler(httpd_req_t *req)
{
    char buf[100];
    gpio_pad_select_gpio(LED_GPIO);
    gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);
    httpd_req_recv(req, buf,req->content_len);
    if(strstr((char*)buf,"1"))
                gpio_set_level(LED_GPIO, 1); 
    if(strstr((char*)buf,"0"))
                gpio_set_level(LED_GPIO, 0);
    printf("DATA: %s\n",buf);

    /* Send back the same data */
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t switch2posthandler = {
    .uri       = "/switch2",
    .method    = HTTP_POST,
    .handler   = switch2_post_handler,
    .user_ctx  = NULL
};

////////////////////////////////////////////////////////////////
esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    if (strcmp("/hello", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/hello URI is not available");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    } else if (strcmp("/echo", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/echo URI is not available");
        
        /* Return ESP_FAIL to close underlying socket */
        return ESP_FAIL;
    }
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Some 404 error message");
    return ESP_FAIL;
}

void start_webserver(void)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &getdatadht11);
        httpd_register_uri_handler(server, &gethtml);
        httpd_register_uri_handler(server, &switch1posthandler);
        httpd_register_uri_handler(server, &switch2posthandler);
        httpd_register_uri_handler(server, &postData);
        httpd_register_err_handler(server, HTTPD_404_NOT_FOUND, http_404_error_handler);
    }
    else
    ESP_LOGI(TAG, "Error starting server!");

}

void stop_webserver(void)
{
    // Stop the httpd server

    httpd_stop(server);
}