#include <stdio.h>
#include"string.h"
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_http_client.h"
#include "cJSON.h"
#include "driver/gpio.h"

#include <esp_event.h>
#include <esp_system.h>




#define SSID "Tplink 2.4 ghz"
#define PASS "hoianhson"
#define FIREBASE_HOST       "https://esp32-project-d3087-default-rtdb.firebaseio.com"
#define FIREBASE_DATABASE   "DHT11"
//#define FIREBASE_API_KEY    "AIzaSyAbVY9bhJNid6wtzH7IvXoi7U7NVt9U7dU"


// Define client certificate
// openssl s_client -showcerts -connect console.firebase.google.com:443
extern const uint8_t ClientCert_pem_start[] asm("_binary_certificate_pem_start");
extern const uint8_t ClientCert_pem_end[]   asm("_binary_certificate_pem_end");


// WiFi
static void wifi_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    switch (event_id)
    {
    case WIFI_EVENT_STA_START:
        printf("WiFi connecting ... \n");
        break;
    case WIFI_EVENT_STA_CONNECTED:
        printf("WiFi connected ... \n");
        break;
    case WIFI_EVENT_STA_DISCONNECTED:
        printf("WiFi lost connection ... \n");
        esp_wifi_connect();
        break;
    case IP_EVENT_STA_GOT_IP:
        printf("WiFi got IP ... \n\n");
        break;
    default:
        break;
    }
}

void wifi_connection()
{
    // 1 - Wi-Fi/LwIP Init Phase
    esp_netif_init();                    // TCP/IP initiation 					s1.1
    esp_event_loop_create_default();     // event loop 			                s1.2
    esp_netif_create_default_wifi_sta(); // WiFi station 	                    s1.3
    wifi_init_config_t wifi_initiation = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wifi_initiation); // 					                    s1.4
    // 2 - Wi-Fi Configuration Phase
    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_event_handler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, wifi_event_handler, NULL);
    wifi_config_t wifi_configuration = {
        .sta = {
            .ssid = SSID,
            .password = PASS}};
    esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_configuration);
    // 3 - Wi-Fi Start Phase
    esp_wifi_start();
    // 4- Wi-Fi Connect Phase
    esp_wifi_connect();
}



//.........................................................................................................
//..................................... Client put realtime firebase.......................................
esp_err_t client_event_put_realtime_firebase_handler(esp_http_client_event_handle_t evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ON_DATA:
        printf("Client HTTP_EVENT_ON_DATA: %.*s\n", evt->data_len, (char *)evt->data);
        break;

    default:
        break;
    }
    return ESP_OK;
}

int randomm(int minN, int maxN){
	return minN + rand() % (maxN + 1 - minN);
}
static void client_put_realtime_firebase_rest_function()
{
    char url[128];
 //   snprintf(url, sizeof(url), "%s/%s.json?auth=%s", FIREBASE_HOST, FIREBASE_DATABASE, FIREBASE_API_KEY);
    snprintf(url, sizeof(url), "%s/%s.json", FIREBASE_HOST, FIREBASE_DATABASE);
    esp_http_client_config_t config_put = {
        .url = url,
        .method = HTTP_METHOD_PUT,
        .cert_pem = (const char *)ClientCert_pem_start,
        .event_handler = client_event_put_realtime_firebase_handler};
        
    esp_http_client_handle_t client = esp_http_client_init(&config_put);

    /*
    {
        "temp": "35",
        "humi": "11"  
    }
    */
    char  put_data[30] ;
    sprintf(put_data, "{\"temp\":\"%d\",\"humi\":\"%d\"}",randomm(20,30),randomm(60,90));
    esp_http_client_set_post_field(client, put_data, strlen(put_data));
    esp_http_client_set_header(client, "Content-Type", "application/json");

    esp_http_client_perform(client);
    esp_http_client_cleanup(client);
}

char my_json_string[100];
void readJSON(char *dataIN, char* dataFIND , char* dataOUT)
{
    cJSON *root2 = cJSON_Parse(dataIN);
    if (cJSON_GetObjectItem(root2,dataFIND)) 
    {
        char *temp = cJSON_GetObjectItem(root2,dataFIND)->valuestring; // data kieu string
        // int cores = cJSON_GetObjectItem(root2,"cores")->valueint;   // data kiểu int
        ESP_LOGI("TAG", "%s=%s",dataFIND,temp);  
        strcpy(dataOUT,temp); 
        ESP_LOGI("TAG", "dataOUT=%s",dataOUT);  
    }
    cJSON_Delete(root2);
}

//.........................................................................................................
//..................................... Client Get firestore...............................................

esp_err_t client_event_get_realtime_firebase_handler(esp_http_client_event_handle_t evt)
{
    switch (evt->event_id)
    {
        case HTTP_EVENT_ON_DATA:  
            printf("Client HTTP_EVENT_ON_DATA: %.*s\n", evt->data_len, (char *)evt->data);
            // copy data nhận được vào chuỗi
            strcpy(my_json_string,(char *)evt->data);
            
            break;

        default:
            break;
    }
    return ESP_OK;
}

static void client_get_realtime_firebase_rest_function()
{
    char url[128];
    sprintf(url, "%s/%s.json", FIREBASE_HOST, FIREBASE_DATABASE);
    esp_http_client_config_t config_post = {
      //  .url = "https://esp32-project-d3087-default-rtdb.firebaseio.com/DHT11.json",
        .url = url,
        .method = HTTP_METHOD_GET,
        .cert_pem = (const char *)ClientCert_pem_start,
        .event_handler = client_event_get_realtime_firebase_handler};
        
    esp_http_client_handle_t client = esp_http_client_init(&config_post);
    esp_http_client_perform(client);
    esp_http_client_cleanup(client);
}

//.........................................................................................................
void gpio_init()
{
    gpio_pad_select_gpio(2);
    gpio_set_direction(2,GPIO_MODE_OUTPUT);
}

void app_main(void)
{
    nvs_flash_init();
    wifi_connection();
    gpio_init();

    char dataTemp[20];
    char led[20];

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    printf("WIFI was initiated ...........\n\n");

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    printf("Start client:\n\n");

    client_get_realtime_firebase_rest_function();
   while (1)
   {
       // chú ý setup kiểu data string trên firebase
       client_get_realtime_firebase_rest_function();
       readJSON(my_json_string,"temp", dataTemp);
       printf("%s\n",dataTemp);
       readJSON(my_json_string,"led", led);
       printf("%s\n",led);
       if(strcmp(led,"0") == 0)
          gpio_set_level(2,1);
       else
          gpio_set_level(2,0);
       vTaskDelay(1000/portTICK_PERIOD_MS);
   }
  
}