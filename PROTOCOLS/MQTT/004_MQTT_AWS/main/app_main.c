/* MQTT Mutual Authentication Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.


    protocols/mqtt/tcp: MQTT over TCP, default port 1883

    protocols/mqtt/ssl: MQTT over TLS, default port 8883

    protocols/mqtt/ssl_ds: MQTT over TLS using digital signature peripheral for authentication, default port 8883

    protocols/mqtt/ssl_mutual_auth: MQTT over TLS using certificates for authentication, default port 8883

    protocols/mqtt/ssl_psk: MQTT over TLS using pre-shared keys for authentication, default port 8883

    protocols/mqtt/ws: MQTT over WebSocket, default port 80

    protocols/mqtt/wss: MQTT over WebSocket Secure, default port 443

    1883 : MQTT, unencrypted, unauthenticated
    1884 : MQTT, unencrypted, authenticated
    8883 : MQTT, encrypted, unauthenticated
    8884 : MQTT, encrypted, client certificate required
    8885 : MQTT, encrypted, authenticated
    8886 : MQTT, encrypted, unauthenticated
    8887 : MQTT, encrypted, server certificate deliberately expired
    8080 : MQTT over WebSockets, unencrypted, unauthenticated
    8081 : MQTT over WebSockets, encrypted, unauthenticated
    8090 : MQTT over WebSockets, unencrypted, authenticated
    8091 : MQTT over WebSockets, encrypted, authenticated

    code, nhận data từ /mqttmanh/led1
    nhận 1 thì on led
    nhận 0 thì off led

    xác thực từ client
    client .crt and .key file

    cách lấy key các thứ
    https://www.youtube.com/watch?v=0Lt-bMbJyKc&list=PLp2zie0oiqdc7PBOGzxPjtYmX6fd0M-Gc&index=10
   

*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include  "driver/gpio.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#define SSID "Ppp"
#define PASS "12341234"
static const char *TAG = "MQTTS_EXAMPLE";

extern const uint8_t certificate_crt_start[] asm("_binary_cer_crt_start");
extern const uint8_t certificate_crt_end[] asm("_binary_cer_crt_end");
extern const uint8_t private_key_start[] asm("_binary_private_key_start");
extern const uint8_t private_key_end[] asm("_binary_private_key_end");
extern const uint8_t root_ca_pem_start[] asm("_binary_rootca_pem_start");
extern const uint8_t root_ca_pem_end[] asm("_binary_rootca_pem_end");

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_subscribe(client, "/manhht9x/dht11", 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            msg_id = esp_mqtt_client_publish(client, "/manhht9x/dht11", "data", 0, 0, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            if(strncmp(event->topic, "/manhht9x/dht11",15) == 0)
            { 
                printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
                printf("DATA=%.*s\r\n", event->data_len, event->data);
                if(strncmp( event->data, "1",1) == 0)
                {
                    gpio_set_level(2,1);
                    printf("ledon ne\n");
                }
                if(strncmp( event->data, "0",1) == 0)
                {
                    gpio_set_level(2,0);
                    printf("ledoff ne\n");
                }
            }
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_app_start(void)
{
    const esp_mqtt_client_config_t mqtt_cfg = {
        .uri = "mqtts://a3b98u33kv5yzl-ats.iot.us-east-1.amazonaws.com:8883",
        .event_handle = mqtt_event_handler,
        .client_cert_pem = (const char *)certificate_crt_start,
        .client_key_pem = (const char *)private_key_start,
        .cert_pem = (const char *)root_ca_pem_start,
    };


    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(client);
}


static void wifi_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    switch (event_id)
    {
        case WIFI_EVENT_STA_START:
            printf("WiFi connecting ... \n");
            break;
        case WIFI_EVENT_STA_CONNECTED:
            printf("WiFi connected ... \n");
            break;
        case WIFI_EVENT_STA_DISCONNECTED:
            printf("WiFi lost connection ... \n");
            esp_wifi_connect();
            break;
        case IP_EVENT_STA_GOT_IP:
            printf("WiFi got IP ... \n\n");
            break;
        default:
            break;
    }
}

void wifi_connection()
{
    // 1 - Wi-Fi/LwIP Init Phase
    esp_netif_init();                    // TCP/IP initiation 					s1.1
    esp_event_loop_create_default();     // event loop 			                s1.2
    esp_netif_create_default_wifi_sta(); // WiFi station 	                    s1.3
    wifi_init_config_t wifi_initiation = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wifi_initiation); // 					                    s1.4
    // 2 - Wi-Fi Configuration Phase
    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_event_handler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, wifi_event_handler, NULL);
    wifi_config_t wifi_configuration = {
        .sta = {
            .ssid = SSID,
            .password = PASS}};
    esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_configuration);
    // 3 - Wi-Fi Start Phase
    esp_wifi_start();
    // 4- Wi-Fi Connect Phase
    esp_wifi_connect();
}


void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    gpio_pad_select_gpio(2);
    gpio_set_direction(2, GPIO_MODE_OUTPUT);

    wifi_connection();
    mqtt_app_start();
}
