/*
Tham khao:
 https://github.com/SIMS-IOT-Devices/FreeRTOS-ESP-IDF-WebSockets-Client/blob/main/proj2.c
 https://docs.espressif.com/projects/esp-idf/en/v4.2.5/esp32/api-reference/protocols/esp_websocket_client.html
    Kết nối đến ws://echo.websocket.events server, gửi data 10 lần và nhận lại data echo từ server in ra monitor
*/
#include <stdio.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_websocket_client.h"
#include "esp_event.h"
#include "wifi_connect.h"

static const char *TAG = "WEBSOCKET";

extern const uint8_t certificate_pem_start[] asm("_binary_cert_pem_start");
extern const uint8_t certificate_pem_end[] asm("_binary_cert_pem_end");

void websocket_event_handler (void* event_handler_arg,esp_event_base_t event_base,int32_t event_id,void* event_data)
{
    esp_websocket_event_data_t *data = (esp_websocket_event_data_t *)event_data;
    if(event_id == WEBSOCKET_EVENT_CONNECTED)
    {
        ESP_LOGI(TAG,"Websocket_connect");
    }
    else if(event_id == WEBSOCKET_EVENT_DATA)
    {   
        ESP_LOGI(TAG,"Recieve data = %.*s\n",data->data_len, data->data_ptr );
    }
}





static void websocket_app(void)
{
    const esp_websocket_client_config_t ws_cfg = {
        .uri = "ws://echo.websocket.events",
        .port = 80,
    };
    //  const esp_websocket_client_config_t ws_cfg_auth = {
    //     .uri = "wss://echo.websocket.events",
    //     .port = 443,
    //     .cert_pem = (const char *)certificate_pem_start
    // };
    esp_websocket_client_handle_t client = esp_websocket_client_init(&ws_cfg);
    esp_websocket_register_events(client, WEBSOCKET_EVENT_ANY, websocket_event_handler, (void *)client);
    esp_websocket_client_start(client);
    // hàm test gửi data echo back
    
    int i = 0;
    char* buff = "hello from esp32";
    while (i<10)
    {  
        if(esp_websocket_client_is_connected(client))
        {
            esp_websocket_client_send(client,buff, strlen(buff),100);
            ESP_LOGI(TAG,"send buff %s", buff);
            i++;
        }
        vTaskDelay(1000/portTICK_PERIOD_MS);  
    }  
    // stop websocket
    esp_websocket_client_stop(client);
    esp_websocket_client_destroy(client);
    ESP_LOGE(TAG,"stop websocket");
}

void test_websocket(esp_websocket_client_handle_t client)
{ 
    int i = 0;
    char* buff = "hello from esp32";
    while (i<10)
    {  
        esp_websocket_client_send(client,buff, strlen(buff),100);
        i++;
        vTaskDelay(1000/portTICK_PERIOD_MS);  
    }

    
}
void app_main(void)
{
    wifi_connection("Tplink 2.4 ghz","hoianhson");
    vTaskDelay(1000 / portTICK_RATE_MS);
    websocket_app();
}


