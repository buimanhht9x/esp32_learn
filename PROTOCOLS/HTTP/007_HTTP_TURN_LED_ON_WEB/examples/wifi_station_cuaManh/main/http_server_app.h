#ifndef HTTP_SERVER_APP_H
#define HTTP_SERVER_APP_H

typedef void (*http_post_callback_t) (uint8_t *data,uitn8_t *len);
typedef void (*http_get_callback_t) (void);
void start_webserver(void);
void stop_webserver(void);


#endif
