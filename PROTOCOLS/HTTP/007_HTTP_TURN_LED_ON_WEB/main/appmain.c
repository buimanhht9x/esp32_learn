// Input to HTML Server for ESP32

#include <stdio.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_http_server.h"
#include "esp_http_client.h"
#include "driver/gpio.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#define LED_GPIO 2


// Khai báo file html
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");

//---------------------------------cấu hình wifi chạy station------------------------------------------------//
#define EXAMPLE_ESP_WIFI_SSID      "Tplink 2.4 ghz"
#define EXAMPLE_ESP_WIFI_PASS      "hoianhson"
#define EXAMPLE_ESP_MAXIMUM_RETRY  5000

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

static const char *TAG = "wifi station";

static int s_retry_num = 0;

static void wifi_station_event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } 
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) 
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } 
        else 
        {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } 
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) 
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_connection_station(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_station_event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &wifi_station_event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
}


//----------------------------------Handler uri-----------------------------------------------------//

esp_err_t html_get_handler(httpd_req_t *r)
{
    httpd_resp_set_type(r,HTTPD_TYPE_TEXT);
    httpd_resp_send(r,(char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
}

int randomm(int minN, int maxN){
	return minN + rand() % (maxN + 1 - minN);
}

esp_err_t dht11_get_handler(httpd_req_t *r)
{    
    char resp_str[100];
    // gán chuỗi
    sprintf(resp_str,"{\"temp\":\"%d\",\"humi\":\"%d\"}",randomm(20,35),randomm(30,95));
    // kiểu data gửi đi
    httpd_resp_set_type(r,HTTPD_TYPE_JSON);
    // gửi data
    httpd_resp_send(r,resp_str, strlen(resp_str));
    return ESP_OK;
}

esp_err_t led1_post_handler(httpd_req_t* req)
{
    char buf[100];
    httpd_req_recv(req, buf,req->content_len);
    printf("led : %s\n",buf);
    if(strstr((char*)buf,"1"))
        gpio_set_level(LED_GPIO, 1); 
    if(strstr((char*)buf,"0"))
        gpio_set_level(LED_GPIO, 0);

    httpd_resp_send(req,"ESP response post", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t sw1_post_handler(httpd_req_t* req)
{
    char buf[100];
    httpd_req_recv(req, buf,req->content_len);
    printf("sw1 : %s\n",buf);
    if(strstr((char*)buf,"1"))
        gpio_set_level(LED_GPIO, 1); 
    if(strstr((char*)buf,"0"))
        gpio_set_level(LED_GPIO, 0);

    httpd_resp_send(req,"ESP response post", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t slider1_post_handler(httpd_req_t* req)
{
    char buf[100];
    httpd_req_recv(req, buf,req->content_len);
    printf("slider : %s\n",buf);

    httpd_resp_send(req,"ESP response post", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t color_post_handler(httpd_req_t* req)
{
    char buf[100];
    char color[10];
    char R[10];
    char G[10];
    char B[10];
    httpd_req_recv(req, buf,req->content_len);
    strncpy( color, buf, 7 );
    strncpy( R, buf+1, 2 );
    strncpy( G, buf+3, 2 );
    strncpy( B, buf+5, 2 );
    printf("color %s : R %s  G %s  B %s\n",color,R,G,B);

    httpd_resp_send(req,"ESP response post", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

//-------------------------------------đăng kí uri------------------------------------------------------//

httpd_uri_t html_uri_get = {
    .handler = html_get_handler,
    .method = HTTP_GET,
    .uri =  "/",
    .user_ctx = NULL
};
httpd_uri_t led1_uri_post = {
    .handler = led1_post_handler,
    .method = HTTP_POST,
    .uri =  "/led1",
    .user_ctx = NULL
};

httpd_uri_t sw1_uri_post = {
    .handler = sw1_post_handler,
    .method = HTTP_POST,
    .uri =  "/sw1",
    .user_ctx = NULL
};

httpd_uri_t slider1_uri_post = {
    .handler = slider1_post_handler,
    .method = HTTP_POST,
    .uri =  "/slider1",
    .user_ctx = NULL
};

httpd_uri_t color_uri_post = {
    .handler = color_post_handler,
    .method = HTTP_POST,
    .uri =  "/color",
    .user_ctx = NULL
};

httpd_uri_t dht11_uri_get = {
    .handler = dht11_get_handler,
    .method = HTTP_GET,
    .uri =  "/dht11",
    .user_ctx = NULL
};

//-------------------------------------------đăng kí start webserver-------------------------------------------------//

void start_webserver(void)
{
    httpd_handle_t  server = NULL;
    httpd_config_t  config = HTTPD_DEFAULT_CONFIG(); 
     if (httpd_start(&server, &config) == ESP_OK) 
     {
        httpd_register_uri_handler(server, &html_uri_get);
        httpd_register_uri_handler(server, &led1_uri_post);
        httpd_register_uri_handler(server, &sw1_uri_post);
        httpd_register_uri_handler(server, &slider1_uri_post);
        httpd_register_uri_handler(server, &color_uri_post);
        httpd_register_uri_handler(server, &dht11_uri_get);
     }
     
}

//------------------------------------cấu hình wifi softAP 192.168.4.1----------------------------------------------------//
/*
#define EXAMPLE_ESP_WIFI_SSID      "esp"
#define EXAMPLE_ESP_WIFI_PASS      "12345678"
#define EXAMPLE_ESP_WIFI_CHANNEL   5
#define EXAMPLE_MAX_STA_CONN       5

static const char *TAG = "wifi softAP";

static void wifi_softap_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
}

void wifi_connection_softap(void)
{
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_ap();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,
                                                ESP_EVENT_ANY_ID,
                                                &wifi_softap_event_handler,
                                                NULL));
    ESP_ERROR_CHECK(esp_event_handler_register( IP_EVENT, 
                                                IP_EVENT_AP_STAIPASSIGNED, 
                                                &wifi_softap_event_handler,
                                                 NULL));

    wifi_config_t wifi_config = {
        .ap = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .ssid_len = strlen(EXAMPLE_ESP_WIFI_SSID),
            .channel = EXAMPLE_ESP_WIFI_CHANNEL,
            .password = EXAMPLE_ESP_WIFI_PASS,
            .max_connection = EXAMPLE_MAX_STA_CONN,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK
        },
    };
    if (strlen(EXAMPLE_ESP_WIFI_PASS) == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS, EXAMPLE_ESP_WIFI_CHANNEL);
}
*/

//......................................Ham gui data len thingspeak.................................................//
char api_key[] = "7BA0VBIINAGGTTEB";
#define WEB_SERVER "api.thingspeak.com"
#define WEB_PORT "80"
char REQUEST[512];
char SUBREQUEST[100];
char recv_buf[512];
void send_data_to_thingspeak(void *pvParameters)
{
    const struct addrinfo hints = {
        .ai_family = AF_INET,
        .ai_socktype = SOCK_STREAM,
    };
    struct addrinfo *res;
    struct in_addr *addr;
    int s, r;

    while(1) {
        int err = getaddrinfo(WEB_SERVER, WEB_PORT, &hints, &res);

        if(err != 0 || res == NULL) {
            ESP_LOGE(TAG, "DNS lookup failed err=%d res=%p", err, res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }

        /* Code to print the resolved IP.

           Note: inet_ntoa is non-reentrant, look at ipaddr_ntoa_r for "real" code */
        addr = &((struct sockaddr_in *)res->ai_addr)->sin_addr;
        ESP_LOGI(TAG, "DNS lookup succeeded. IP=%s", inet_ntoa(*addr));

        s = socket(res->ai_family, res->ai_socktype, 0);
        if(s < 0) {
            ESP_LOGE(TAG, "... Failed to allocate socket.");
            freeaddrinfo(res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "... allocated socket");

        if(connect(s, res->ai_addr, res->ai_addrlen) != 0) {
            ESP_LOGE(TAG, "... socket connect failed errno=%d", errno);
            close(s);
            freeaddrinfo(res);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            continue;
        }

        ESP_LOGI(TAG, "... connected");
        freeaddrinfo(res);

        //sprintf(SUBREQUEST, "api_key=38V514DI2S48GSUW&field1=%d&field2=%d",50,50);
        //sprintf(REQUEST,"POST /update HTTP/1.1\nHost: api.thingspeak.com\nConnection: close\nContent-Type: application/x-www-form-urlencoded\nContent-Length:%d\n\n%s\n",strlen(SUBREQUEST),SUBREQUEST);
        sprintf(REQUEST, "GET http://api.thingspeak.com/update.json?api_key=7BA0VBIINAGGTTEB&field1=%d&field2=%df\n\n",randomm(25,35),randomm(25,85));
        if (write(s, REQUEST, strlen(REQUEST)) < 0) {
            ESP_LOGE(TAG, "... socket send failed");
            close(s);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "... socket send success");

        struct timeval receiving_timeout;
        receiving_timeout.tv_sec = 5;
        receiving_timeout.tv_usec = 0;
        if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &receiving_timeout,
                sizeof(receiving_timeout)) < 0) {
            ESP_LOGE(TAG, "... failed to set socket receiving timeout");
            close(s);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            continue;
        }
        ESP_LOGI(TAG, "... set socket receiving timeout success");

        /* Read HTTP response */
        do {
            bzero(recv_buf, sizeof(recv_buf));
            r = read(s, recv_buf, sizeof(recv_buf)-1);
            for(int i = 0; i < r; i++) {
                putchar(recv_buf[i]);
            }
        } while(r > 0);

        ESP_LOGI(TAG, "... done reading from socket. Last read return=%d errno=%d.", r, errno);
        close(s);
        for(int countdown = 10; countdown >= 0; countdown--) {
            ESP_LOGI(TAG, "%d... ", countdown);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
        ESP_LOGI(TAG, "Starting again!");
    }
}




void app_main(void)
{

    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    gpio_pad_select_gpio(LED_GPIO);
    gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);

   // wifi_connection_softap();

    wifi_connection_station();

    start_webserver();
    xTaskCreate(&send_data_to_thingspeak, "send_data_to_thingspeak", 8192, NULL, 6, NULL);
}