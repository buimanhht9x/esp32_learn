// https://docs.espressif.com/projects/esp-idf/en/v4.2.5/esp32/api-reference/protocols/esp_http_server.html
// https://github.com/SIMS-IOT-Devices/FreeRTOS-ESP-IDF-HTTP-Server/blob/main/Server_POST.c
// https://www.youtube.com/watch?v=_-tSO6tiodQ&list=PLgrKXQgo8LPt86ZDjnhrkrVnziC9kkIDm&index=2

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_http_server.h"
#include "lwip/err.h"
#include "lwip/sys.h"

#define SSID "Tplink 2.4 ghz"
#define PASS "hoianhson"

#define MIN(a, b) ((a) < (b) ? (a) : (b))

static void wifi_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    switch (event_id)
    {
    case WIFI_EVENT_STA_START:
        printf("WiFi connecting ... \n");
        break;
    case WIFI_EVENT_STA_CONNECTED:
        printf("WiFi connected ... \n");
        break;
    case WIFI_EVENT_STA_DISCONNECTED:
        printf("WiFi lost connection ... \n");
        ESP_ERROR_CHECK(esp_wifi_connect());
        break;
    case IP_EVENT_STA_GOT_IP:;
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data ;
        ESP_LOGI("TAG", "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        break;
    default:
        break;
    }
}

void wifi_connection()
{
    // 1 - Wi-Fi/LwIP Init Phase
    ESP_ERROR_CHECK(esp_netif_init());                    // TCP/IP initiation 					s1.1
    ESP_ERROR_CHECK(esp_event_loop_create_default());     // event loop 			s1.2
    esp_netif_create_default_wifi_sta(); // WiFi station 	s1.3
    wifi_init_config_t wifi_initiation = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_initiation)); // 					s1.4
    // 2 - Wi-Fi Configuration Phase
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, wifi_event_handler, NULL));
    wifi_config_t wifi_configuration = {
        .sta = {
            .ssid = SSID,
            .password = PASS}};
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_configuration));
    // 3 - Wi-Fi Start Phase
    ESP_ERROR_CHECK(esp_wifi_start());
    // 4- Wi-Fi Connect Phase
    ESP_ERROR_CHECK(esp_wifi_connect());
}

esp_err_t post_handler(httpd_req_t* req)
{
    char buff[100];
    int ret = httpd_req_recv(req,buff, MIN(sizeof(buff), req->content_len));
    printf("%s\n",buff);
    if(ret <= 0)
    {
        if(ret == HTTPD_SOCK_ERR_TIMEOUT)
        {
             httpd_resp_send_408(req);
        }
        return ESP_FAIL;
    }

    httpd_resp_send(req,"ESP response post", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t get_handler(httpd_req_t* req)
{
    httpd_resp_send(req, "ESP response ok nha",HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

httpd_uri_t uri_post = {
        .handler = post_handler,
        .method = HTTP_POST,
        .uri = "/",
        .user_ctx = NULL
};

httpd_uri_t uri_get = {
        .handler = get_handler,
        .method = HTTP_GET,
        .uri = "/",
        .user_ctx = NULL
};

void server_initiation()
{
    httpd_handle_t server =NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    if(httpd_start(&server, &config) == ESP_OK)
    {
        httpd_register_uri_handler(server, &uri_post);
        httpd_register_uri_handler(server, &uri_get);
        ESP_LOGI("HTTP", "HTTP_start");
    }
    
    
    

}

void app_main(void)
{
    nvs_flash_init();
    wifi_connection();
    server_initiation();
}

