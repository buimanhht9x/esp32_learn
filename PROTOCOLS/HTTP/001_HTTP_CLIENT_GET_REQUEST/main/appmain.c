// HTTP Client - FreeRTOS ESP IDF - GET
// https://www.youtube.com/watch?v=2NZgq_pRdN0&list=PLgrKXQgo8LPuZUNiYZjNZRmyrNeENrzVH&index=1
// https://github.com/SIMS-IOT-Devices/FreeRTOS-ESP-IDF-HTTP-Client/blob/main/proj4.c

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_http_client.h"

#define SSID "Tplink 2.4 ghz"
#define PASS "hoianhson"


static void event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if(event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ESP_LOGI("WIFI connect", "WIFI connected");
    }
}

void wifi_connection()
{
    // https://docs.espressif.com/projects/esp-idf/en/v4.2.5/esp32/api-guides/wifi.html#esp32-wi-fi-station-general-scenario
    esp_netif_init();
    esp_event_loop_create_default();
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t  wifi_init_config = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wifi_init_config);

    wifi_config_t wifi_config = {
        .sta = {
                .ssid = SSID,
                .password = PASS
               }
    };
    esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config );
    esp_wifi_start();
    esp_wifi_connect();

    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, event_handler, NULL);
}

void client_event_get_handler(esp_http_client_event_handle_t evt)
{
    if(evt->event_id == HTTP_EVENT_ON_DATA) // esp_http_client.h
    {
        printf("data %.*s", evt->data_len, (char*) evt->data);
    }
}

void rest_get()
{
    //https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/protocols/esp_http_client.html
    esp_http_client_config_t esp_http_client_config = {
        .cert_pem =  0,
        .url = "http://worldtimeapi.org/api/timezone/Asia/Bangkok",
        .method = HTTP_METHOD_GET,
        .event_handler = client_event_get_handler
    };
    esp_http_client_handle_t client = esp_http_client_init(&esp_http_client_config);
    esp_http_client_perform(client);
    esp_http_client_cleanup(client);

}
void app_main(void)
{
    nvs_flash_init();
    wifi_connection();
    vTaskDelay(2000/ portTICK_PERIOD_MS);
    rest_get();

}