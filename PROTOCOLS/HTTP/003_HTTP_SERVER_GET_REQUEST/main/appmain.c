//https://docs.espressif.com/projects/esp-idf/en/v4.2.5/esp32/api-reference/protocols/esp_http_server.html

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_http_server.h"

static void wifi_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    switch (event_id)
    {
    case WIFI_EVENT_STA_START:
        printf("WiFi connecting ... \n");
        break;

    case WIFI_EVENT_STA_CONNECTED:
        printf("WiFi connected ... \n");
        break;

    case WIFI_EVENT_STA_DISCONNECTED:
        printf("WiFi lost connection ... \n");
        esp_wifi_connect();
        break;

	case IP_EVENT_STA_GOT_IP:
		printf("WiFi got IP ... \n\n");
		break;

    default:
        break;
    }
}

void wifi_connection()
{
    // 1 - Wi-Fi/LwIP Init Phase
    esp_netif_init(); // TCP/IP initiation 					s1.1
	esp_event_loop_create_default(); // event loop 			s1.2
	esp_netif_create_default_wifi_sta(); // WiFi station 	s1.3
    wifi_init_config_t wifi_initiation = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wifi_initiation); // 					s1.4
	
	// 2 - Wi-Fi Configuration Phase
	esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_event_handler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, wifi_event_handler, NULL);
    wifi_config_t wifi_configuration = {
        .sta = {
            .ssid = "MINH NHAT",
            .password = "minhnhat0608"}};
    esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_configuration);

	// 3 - Wi-Fi Start Phase
    esp_wifi_start();

	// 4- Wi-Fi Connect Phase
	esp_wifi_connect();
}

static esp_err_t get_handler(httpd_req_t *req)
{
    const char resp[] = "URI GET Response";
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}
httpd_uri_t  uri_get = {
        .uri = "/",
        .method = HTTP_GET,
        .handler = get_handler, // tạo handler cho uri get
        .user_ctx = NULL
};
void server_initiation()
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    httpd_start(&server,&config);
    httpd_register_uri_handler(server, &uri_get); // dăng kí sự kiện cho uri get
}

void app_main(void)
{
    nvs_flash_init();   
	wifi_connection();  
	server_initiation();
}