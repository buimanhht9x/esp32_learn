//
//#include<stdio.h>
//#include<string.h>
//
//int main()
//{
//     char string1[] = "Bro";
//     char string2[] = "Code";
//     strlwr(string1);             // Convert string to lowercase    => bro
//     strupr(string1);             // Convert string to uppercase  => BRO
//     strcat(string1,string2);     // Appends string2 to end of string1  => BroCode
//     strncat(string1,string2,2);  // Appends n characters from string2 to end of string1  => BroCo
//     strcpy(string1, string2);    // Copy string2 to string1  => Code
//     strncpy(string1, string2,2); // Copy n characters from string2 to string1  => Coo
//     strset(string1,'$');         // Set all characters of a tring to a given character  => $$$
//     strnset(string1, '$', 2);    // Set n characters of a tring to a given character  => $$o
//     strrev(string1);             // Reverses a string  => orB
//     printf(string1);
//
//     int len = strlen(string1);   // returns string length as int
//     printf("%d",len);
//     int result = strcmp(string1,string2);   // string compare all characters < 0 str1 shorter than str2, =0 str1 = str2
//     int result = strncmp(string1,string2,1);   // string compare n characters
//     int result = strcmpi(string1,string2);   // string compare all (ignore case) mean Bro = BRO
//     int result = strncmpi(string1,string2);   // string compare n characters (ignore case) mean Bro = BRO
//     printf("%d",result);
//
//     return 0;
//}
//
//
