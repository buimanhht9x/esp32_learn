/*
#include<stdio.h>
#include<stdlib.h>
int main ()
{
  // array
  int arr[10];
  arr[0] = 42;
  arr[1] = 43;
  arr[2] = 48;
  printf("The address of the arr is: %p\n", arr);
  printf("size of the arr is: %d\n", sizeof(arr));

  printf("%p\n", &arr);
  printf("%p\n", arr);

  printf("%p\n", &arr + 1);
  printf("%p\n", arr + 1);

  puts("\n\n");

  //fake array
  int * const pt = alloca(sizeof(int)*10);  // hàm cấp phát bộ nhớ trong stack
  *(pt + 0 ) = 42;
  *(pt + 1 ) = 43;
  *(pt + 2 ) = 48;

  printf("The address of the pt is: %p\n", pt);
  printf("size of the pt is: %d\n", sizeof(pt));

  printf("%p\n", &pt);
  printf("%p\n", pt);

  printf("%p\n", &pt + 1);
  printf("%p\n", pt + 1);

  return 0;
}
*/
/*
#include<stdio.h>
#include<stdlib.h>

#define SIZE 100
void print_elements(int *v, int size)
{
    for(int i=0; i< size; ++i)
    {
      printf("%d\n", v[i]);
    }
}

int main ()
{
    int v[SIZE];
    for(int i =0; i< SIZE; ++i)
    {
        v[i] = rand()%100;
    }
    print_elements(v,SIZE);
}*/

/*
#include<stdio.h>
#include<stdlib.h>


int main ()
{
    int v[10];
    int *pt = v;
    v[0] = 42;
    v[1] = 43;
    printf("%d\n",*pt);
    printf("%d\n",*(pt+1));
    printf("%d\n",*v);
    printf("%d\n",*(v+1));
}



#include<stdio.h>
#include<stdlib.h>


int main (int argc, char **argv)
{
    int j, k;
    j = 0;
    while(argv[j])
    {
        k = 0;
        while(argv[j][k])
        {
            printf("%c", argv[j][k]);
            ++k;
        }
        puts("");
        ++j;
    }
}

*/

//#include<stdio.h>
//#include<stdlib.h>
////void fakemain(char **av)
////{
////    while(*av)
////        printf("%s\n", *av++);
////}
//void fakemain(char *av[])
//{
//    while(*av)
//        printf("%s\n", *av++);
//}
//
//int main (int argc, char **argv)
//{
//    char *av[]  = {"./prog","1","2","3", NULL};   // 1 mảng pointer to chars
//   // char **av1  = {"./prog","1","2","3"};   // không hợp lệ
//    char **av1 = av;
//    fakemain(av1);
//}

//#include<stdio.h>
//#include<stdlib.h>
//
//int fool()
//{
//    return 5;
//}
//int main ()
//{
//   printf("%d\n", fool)  ;    // In ra địa chỉ của hàm
//   printf("%d\n", fool())  ;  // In ra giá trị của hàm
//}


//#include<stdio.h>
//#include<stdlib.h>

//typedef void (*func_pt)(int);
//func_pt pt = NULL;
//
//void set_callback(func_pt *cb)
//{
//    pt = cb;
//}
//
//void callback(int a)
//{
//    printf("%d\n",a);
//}
//int main ()
//{
//   printf("hello\n" )  ;
//   set_callback(callback);
//   printf("%p\n",pt )  ;
//   printf("%p\n",callback )  ;
//   pt(5);
//   callback(5);
//}

#include<stdio.h>
#include<stdlib.h>
typedef int (*pheptinh)(int, int);

int cong(int a,int b)
{
    return a+b;
}
int tru(int a,int b)
{
    return a-b;
}
void tinhtoan(int a, int b, pheptinh phep)
{
    printf("%d\n",phep(a,b)) ;
}
void main()
{
    tinhtoan(6,5,cong);
    tinhtoan(6,5,tru);
}

