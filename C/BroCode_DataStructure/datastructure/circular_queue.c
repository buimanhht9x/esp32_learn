//
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <stdbool.h>
//typedef struct {
//    int MAX_SIZE;
//    char *buff[20]; // Mảng con trỏ 20 phần tử, muốn gán được chuỗi vào thì phải cấp phát bộ nhớ
//                    // cho nó bằng hàm malloc hoặc calloc
//                    // đơn giản hơn thì khai báo con trỏ 2 chiều với số phần tử cấp phát tĩnh
//                    // biết trước, ví dụ như buff[20][20]( mảng 20 phàn tử, mỗi phần tử 20 char)
//    int head;
//    int rear;
//    int numberElements;
//}QUEUE;
//
//QUEUE queue = {
//    .MAX_SIZE = 5,
//    .head = 0,
//    .rear = 0,
//    .numberElements = 0
//};
//
//void Enqueue(QUEUE *q, char *data) {
//   if(q->numberElements == q->MAX_SIZE)
//     printf("Over Flow\n");
//   else
//   {
//       //malloc(strlen("hello") + 1) được sử dụng để cấp phát bộ nhớ cho chuỗi "hello"
//       // cùng với khoảng trống dành cho dấu kết thúc null '\0'.
//       q->buff[q->rear] = (char *)malloc(strlen(data)+1);
//       strcpy(q->buff[q->rear],data);
//       q->rear = (q->rear + 1) % (q->MAX_SIZE);
//       q->numberElements++;
//   }
//}
//
//void Dequeue(QUEUE *q) {
//   if(q->numberElements == 0 )
//   {
//      printf("Queue Empty\n");
//   }
//   else
//   {
//      // memset(q->buff[q->head], strlen(q->buff[q->head]), '\0');
//      // strnset(q->buff[q->head], '\0',  sizeof(q->buff[q->head]));
//      // printf("%d\n", strlen(q->buff[q->head]));
//       free(q->buff[q->head]);
//       q->head = (q->head + 1) % (q->MAX_SIZE);
//       q->numberElements--;
//   }
//}
//
//char* Peek(QUEUE *q) {
//    return q->buff[q->head];
//}
//
//void PrintQueue(QUEUE *q)
//{
//    if(q->head >= q->rear)
//    {
//        for(int i = q->head; i <= q->MAX_SIZE - 1; ++i)
//        {
//            printf("%s\n",q->buff[i]);
//          //   printf("head %d \n",q->head);
//        }
//        for(int i = 0; i < q->rear; ++i)
//        {
//            printf("%s\n",q->buff[i]);
//         //    printf("head %d  \n",q->head);
//        }
//    }
//    else if(q->head < q->rear)
//    {
//        for(int i = q->head; i < q->rear; ++i)
//        {
//            printf("%s\n",q->buff[i]);
//          //   printf("head %d \n",q->head);
//        }
//    }
//}
//
//int Size(QUEUE *q) {
//    return q->numberElements;
//}
//
//bool IsEmpty(QUEUE *q) {
//    return (q->rear == q->head);
//}
//
//int main() {
//    Enqueue(&queue,"hello1"); printf("head %d rare %d \n",queue.head,queue.rear);
//    Enqueue(&queue,"hello2"); printf("head %d rare %d \n",queue.head,queue.rear);
//    Enqueue(&queue,"hello3"); printf("head %d rare %d \n",queue.head,queue.rear);
//    Enqueue(&queue,"hello4"); printf("head %d rare %d \n",queue.head,queue.rear);
//    Enqueue(&queue,"hello4"); printf("head %d rare %d \n",queue.head,queue.rear);
//     Enqueue(&queue,"hello4"); printf("head %d rare %d \n",queue.head,queue.rear);
//    PrintQueue(&queue);
//    Dequeue(&queue); printf("head %d rare %d \n",queue.head,queue.rear);
//    Dequeue(&queue); printf("head %d rare %d \n",queue.head,queue.rear);
//    PrintQueue(&queue);
//    Enqueue(&queue,"hello5"); printf("head %d rare %d \n",queue.head,queue.rear);
//    Enqueue(&queue,"hello6"); printf("head %d rare %d \n",queue.head,queue.rear);
//    PrintQueue(&queue);
//    Dequeue(&queue); printf("head %d rare %d \n",queue.head,queue.rear);
//    PrintQueue(&queue);
//
//
//    return 0;
//}
